<?php
$servername = "localhost";
$database = "micamimi_micamimi18";
$username = "micamimi_mica18";
$password = "I&9%ZB}X^W~F";

// Create connection

$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection

if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
}


$subDistricts = array();

$subDistrictCodeQuery = mysqli_query($conn, "SELECT Sub_District_Code FROM `villages` GROUP BY Sub_District_Code");

while($subDistrictCodeData = mysqli_fetch_array($subDistrictCodeQuery))
{
    $subDistricts[] = $subDistrictCodeData['Sub_District_Code'];
}

$subDistricts = array_filter($subDistricts);

$variables = array(
	/*"Power_Supply_For_Agriculture_UseSummerAprSepperdayin_Hrs",
	"Power_Supply_For_Agriculture_UseWinterOctMarper_dayin_Hrs",
	"Power_Supply_For_All_Users_Summer_AprSep_per_day_in_Hrs",
	"Power_Supply_For_All_Users_Winter_OctMar_per_day_in_Hrs",*/
	"Power_Supply_For_Commercial_Use_SummerAprSepper_dayin_Hrs",
	"Power_Supply_For_Domestic_Use_SummerAprSepper_day_in_Hrs",
	"Power_Supply_For_Domestic_Use_WinterOctMarper_day_in_Hrs",
	"Power_Supply_For_Commercial_Use_WinterOctMarper_dayin_Hrs"
	);

foreach ($variables as $vkey => $variable) {
	foreach ($subDistricts as $key => $subDistrict)
 	{
 		// For Median Calculation
		$sql = 'SELECT AVG(dd.'.$variable.') as median_val
					FROM (
					SELECT d.Sub_District_Code, d.'.$variable.', @rownum:=@rownum+1 as `row_number`, @total_rows:=@rownum
					  FROM villages d, (SELECT @rownum:=0) r
					  WHERE  d.'.$variable.' is NOT NULL and
					  d.'.$variable.' != "" and
					  Sub_District_Code = ('.$subDistrict.')
					  ORDER BY length(d.'.$variable.'), '.$variable.'
					) as dd
					WHERE dd.row_number IN ( FLOOR((@total_rows+1)/2), FLOOR((@total_rows+2)/2) )';

		$result = mysqli_query($conn, $sql);

		if($result === false) {
		    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		} else {
		    // use mysqli_fetch_row() here.
		    $records = mysqli_fetch_assoc($result);
		}

		if ($records['median_val'] == "" || $records['median_val'] == NULL) {
			$records['median_val'] = 0;
		}

		// Update for total_village field
		$query2 = 'update villages_summary_sub_dist set '.$variable.'_0 = '.$records['median_val'].' where Sub_District_Code = '.$subDistrict;

		if (mysqli_query($conn, $query2)) {
      		echo "New record created successfully -> " .$variable. " -> " .$subDistrict;
      		echo "<br>";
		} else {
      		echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}
	}
}
echo "<pre/>";print_r("All Done");
mysqli_close($conn);

?>