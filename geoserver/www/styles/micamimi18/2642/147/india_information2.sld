<?xml version="1.0" encoding="ISO-8859-1"?>
			<StyledLayerDescriptor version="1.0.0"
 xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
 xmlns="http://www.opengis.net/sld"
 xmlns:ogc="http://www.opengis.net/ogc"
 xmlns:xlink="http://www.w3.org/1999/xlink"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><NamedLayer>
			<Name><![CDATA[india_information]]></Name>

	<UserStyle>
	<!-- Styles can have names, titles and abstracts -->
	  <Title>Default Polygon</Title>
	  <Abstract>A sample style that draws a polygon</Abstract>
	  <!-- FeatureTypeStyles describe how to render different features -->
	  <!-- A FeatureTypeStyle for rendering polygons -->
	  <FeatureTypeStyle>

		<Rule>
		  <Name>Mediaum</Name>

		 <PolygonSymbolizer>
			<Fill>
			  <CssParameter name="fill">#FFE084</CssParameter>
			</Fill>

		  </PolygonSymbolizer>
		  <LineSymbolizer>
		 <Stroke>
		   <CssParameter name="stroke">#E9AD84</CssParameter>
		   <CssParameter name="stroke-width">1</CssParameter>
		   <CssParameter name="stroke-dasharray">1 1</CssParameter>
		 </Stroke>
	   </LineSymbolizer>
		 <TextSymbolizer>
		 <Label>
		   <ogc:PropertyName>distshp</ogc:PropertyName>
		 </Label>
		 <Font>
		   <CssParameter name="font-family">Dialog.plain</CssParameter>
		   <CssParameter name="font-size">10</CssParameter>
		   

		 </Font>

		 <LabelPlacement>
		   <PointPlacement>
			 <AnchorPoint>
			   <AnchorPointX>0.5</AnchorPointX>
			   <AnchorPointY>0.5</AnchorPointY>
			 </AnchorPoint>
		   </PointPlacement>
	<VendorOption name="spaceAround">-1</VendorOption>
	<VendorOption name="autoWrap">10</VendorOption>
	<VendorOption name="goodnessOfFit">0.1</VendorOption>
<VendorOption name="spaceAround">10</VendorOption>
 <VendorOption name="conflictResolution">true</VendorOption>
		 </LabelPlacement>
		 <Halo>
		  <Fill>
			<CssParameter name="fill">#FDFDFD</CssParameter>
			<CssParameter name="fill-opacity">0.75</CssParameter>
		  </Fill>
	  </Halo>
	  <Fill>
		<CssParameter name="fill">#242424</CssParameter>
		</Fill>

<VendorOption name="spaceAround">-1</VendorOption>
<VendorOption name="autoWrap">12</VendorOption>
<VendorOption name="polygonAlign">mbr</VendorOption>
<VendorOption name="maxDisplacement">20</VendorOption>
<VendorOption name="goodnessOfFit">0.5</VendorOption>
	   </TextSymbolizer>



		</Rule>

	  </FeatureTypeStyle>


	</UserStyle>

			</NamedLayer></StyledLayerDescriptor>