<?php
/**
 * $Id: cities.php 10 2013-11-10 22:53:48Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');
?>
<table class="table table-striped" id="top10city">
			<thead>
				<tr>
					<th>
						<?php echo JText::_('COM_SAXUMIPLOGGER_CITY'); ?>
					</th> 
					<th width=100>
						<?php echo JText::_('COM_SAXUMIPLOGGER_NUM_OF_OCCURENCES'); ?>
					</th> 
				</tr>			
			</thead>
			<?php
			$k = 0;
			for ($i=0, $n=count( $cities ); $i < $n; $i++)
			{
				$row = &$cities[$i];
				?>
				<tr class="<?php echo "row$k"; ?>">
					<td>
						<?php 
						echo JHTML::_('image', 'administrator/components/com_saxumiplogger/assets/images/flags/'.$row->country_code.'.png', $row->country_code);
						echo ' '.$row->country_name.', '; 
						echo $row->city;
						?>
					</td>
					<td>
						<?php echo $row->num; ?>
					</td>
				</tr>
				<?php
				$k = 1 - $k;
			}
			?>
			</table>