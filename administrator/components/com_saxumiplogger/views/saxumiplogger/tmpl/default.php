<?php
/**
 * $Id: default.php 10 2013-11-10 22:53:48Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access'); 
$canDo = SaxumiploggerController::getActions();
?>
<div>
<?php if (!empty( $this->sidebar)) : ?>
<div id="j-sidebar-container" class="span2">
	<?php echo $this->sidebar; ?>
</div>
<div id="j-main-container" class="span10">
<?php else : ?>
<div id="j-main-container">
<?php endif;?>
	<div class="adminform">
		<div class="sax-cpanel-left">
			<form action="index.php" method="post" name="adminForm" id="adminForm">
				<div id="cpanel">
				<?php
				$link = 'index.php?option=com_saxumiplogger&view=report';
				echo SaxumiploggerController::quickIconButton( $link, 'icon-48-report.png', JText::_( 'COM_SAXUMIPLOGGER_REPORT' ) );
				?>
				<?php
				$link = 'index.php?option=com_saxumiplogger&view=statistics';
				echo SaxumiploggerController::quickIconButton( $link, 'icon-48-statistics.png', JText::_( 'COM_SAXUMIPLOGGER_STATISTICS' ) );
				?>
				<?php
				if ($canDo->get('core.block')) 
				{
					$link = 'index.php?option=com_saxumiplogger&view=blocks';
					echo SaxumiploggerController::quickIconButton( $link, 'icon-48-block.png', JText::_( 'COM_SAXUMIPLOGGER_BLOCKED' ) );
				}
				?>
				<?php
				if ($canDo->get('core.excp')) 
				{
					$link = 'index.php?option=com_saxumiplogger&view=excps';
					echo SaxumiploggerController::quickIconButton( $link, 'icon-48-excp.png', JText::_( 'COM_SAXUMIPLOGGER_EXCP' ) );
				}
				?>
				<?php
				if ($canDo->get('core.email')) 
				{
					$link = 'index.php?option=com_saxumiplogger&view=emails';
					echo SaxumiploggerController::quickIconButton( $link, 'icon-48-email.png', JText::_( 'COM_SAXUMIPLOGGER_EMAIL' ) );
				}
				?>
				<?php
				if ($canDo->get('core.update')) 
				{
					$link = 'index.php?option=com_saxumiplogger&view=update';
					echo SaxumiploggerController::quickIconButton( $link, 'icon-48-update.png', JText::_( 'COM_SAXUMIPLOGGER_UPDATE' ) );
				}
				?>
				<?php
				$link = 'index.php?option=com_saxumiplogger&view=info';
				echo SaxumiploggerController::quickIconButton( $link, 'icon-48-info.png', JText::_( 'COM_SAXUMIPLOGGER_INFO' ) );
				?>
				</div>
				<input type="hidden" name="option" value="com_saxumiplogger" />
				<input type="hidden" name="view" value="saxumiplogger" />
				<?php echo JHtml::_('form.token'); ?>
			</form>
		</div>
		
		<div class="sax-cpanel-right" style="text-align: center;">
		<table>
			<tr>
				<td width="100%" valign="middle">
				<form target="_blank" action="https://www.paypal.com/cgi-bin/webscr" method="post">
					<br>If you find this extension useful, please support the development and make a donation. Thank you.<br><br>
					<input type="hidden" name="cmd" value="_s-xclick">
					<input type="hidden" name="hosted_button_id" value="88ELEX3NM2MG2">
					<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
					<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
				</form>
				</td>
			</tr>
		</table>
		</div>
		
		<div class="sax-cpanel-left clrlft">
		<fieldset class="adminform">
		<legend><?php echo JText::_( 'COM_SAXUMIPLOGGER_PLUGINS' ); ?></legend>
		<table class="table table-striped" id="plugins">
			<?php 
			foreach ($this->plugins as $plugins) {
				echo '<tr>';
				echo '<td><span>'.JText::_($plugins->name).'</span></td>';
				echo '<td>';
				if ($plugins->enabled) echo JHTML::_('image', 'administrator/components/com_saxumiplogger/assets/images/icon-16-yes.png', JText::_( 'JENABLED' ));				   
				if (!$plugins->enabled) echo JHTML::_('image', 'administrator/components/com_saxumiplogger/assets/images/icon-16-no.png', JText::_( 'JDISABLED' ));
				echo '</td>';
				echo '</tr>';
			}
			?>
		</table>
		<div class="icon">
		<form action="<?php echo JRoute::_("index.php?option=com_plugins")?>" method="post" id="pluginform" >
			<input type="hidden" name="filter_search" id="filter_search" value="Saxum IPLogger" title="Search in plug-in title." />
			<input type="submit" class="btn btn-primary" value="<?php echo JText::_('COM_SAXUMIPLOGGER_GOTOPLUGINS')?>">
		</form>
		</div>
		</fieldset>
		</div>

	</div>
</div>

</div>
<?php 
include_once(JPATH_COMPONENT.DIRECTORY_SEPARATOR.'footer.php'); 
?>
