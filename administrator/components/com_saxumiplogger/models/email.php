<?php
/**
 * $Id: email.php 140 2015-11-28 11:25:23Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

jimport('joomla.application.component.modeladmin');

class SaxumiploggerModelEmail extends JModelAdmin
{
	public function getTable($type = 'Email', $prefix = 'SaxumiploggerTable', $config = array()) 
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	public function getForm($data = array(), $loadData = true) 
	{
		// Get the form.
		$form = $this->loadForm('com_saxumiplogger.email', 'email',
		                        array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) 
		{
			return false;
		}
		return $form;
	}

	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_saxumiplogger.edit.email.data', array());
		if (empty($data)) 
		{
			$data = $this->getItem();
		}
		return $data;
	}
	
	function getFormat($type)
	{
		$query = $this->_db->getQuery(true);

		$query->select($this->_db->quoteName(array('id','type','subject','body')));
		$query->from($this->_db->quoteName('#__saxum_iplogger_emails'));
		$query->where($this->_db->quoteName('type') . ' LIKE '. $this->_db->quote($type));

        $this->_db->setQuery($query);
        return $this->_db->loadObjectList();
	}
	
	function getAdmins()
	{
		jimport( 'joomla.access.access' );
		$params	= JComponentHelper::getParams('com_saxumiplogger');
		$users = JAccess::getUsersByGroup($params->get('admin_usergroup',8));
		
		if (empty($users))
		{
    		JFactory::getApplication()->enqueueMessage(JText::_( 'COM_SAXUMIPLOGGER_NO_USER_IN_ADMIN_GROUP'),'warning' );
			return array();
		} else {
			
			$query = $this->_db->getQuery(true);

			$query->select($this->_db->quoteName(array('id','email')));
			$query->from($this->_db->quoteName('#__users'));
			$query->where($this->_db->quoteName('id') . ' IN ('. implode(',',$users).')');

	        $this->_db->setQuery($query);
	        return $this->_db->loadObjectList();
			
		}
	}
}
?>
