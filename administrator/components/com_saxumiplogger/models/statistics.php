<?php
/**
 * $Id: statistics.php 23 2014-01-04 22:34:23Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.model' );

class SaxumiploggerModelStatistics extends JModelList
{
	public function __construct($config = array())
	{
		$tpl = JRequest::getCmd('tpl');
		$task = JRequest::getCmd('task');
		if ($tpl=='user' || $tpl=='ip' && $task=='change_group' ) {
			$config['filter_fields'] = array(
				'name', 'username',
				'total','ip_count','country_count','city_count'
			);
		}
		if ($tpl=='ip' || $tpl=='user' && $task=='change_group') {
			$config['filter_fields'] = array(
				'total','user_count',
				'ip','country_code','country_name','city'
			);
			
		}

		parent::__construct($config);
	}

	protected function getListQuery()
	{
		// Create a new query object.		
		$db = $this->getDBO();
		$query = $db->getQuery(true);
		// Select some fields
		$query->select(
			$this->getState(
				'list.select',
				'a.*'
				)
			);
		$query->select('null as params');
		$query->from('#__saxum_iplogger as a');
		
		$query->join('LEFT','#__users as u ON a.user_id=u.id');
		$query->select('ifnull(u.username,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\') as username');
		$query->select('ifnull(u.name,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\') as name');
		$query->select('count(a.id) as total');
		$query->select('count(distinct a.ip) as ip_count');
		$query->select('count(distinct a.country_code) as country_count');
		$query->select('count(distinct a.city) as city_count');
		$query->select('u.id as userid');
		$query->group('ifnull(u.username,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\')');
		$query->group('ifnull(u.name,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\')');
	
		// Filter by search in title.
		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (stripos($search, 'user:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 5), true) . '%');
				$query->where('(ifnull(u.username,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\') LIKE ' . $search . ')');
			}
			elseif (stripos($search, 'name:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 5), true) . '%');
				$query->where('(ifnull(u.name,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\') LIKE ' . $search . ')');
			}
			else
			{
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(ifnull(u.username,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\') LIKE ' . $search . ')');
			}
		}
		
		$orderCol	= $this->state->get('list.ordering');
		$orderDirn	= $this->state->get('list.direction');
		$query->order($db->escape($orderCol.' '.$orderDirn));
		
		return $query;
	}

    function getIpData()
    {	
    	$db = $this->getDBO();
		$query = $db->getQuery(true);
		// Select some fields
		$query->select(
			$this->getState(
				'list.select',
				'a.*'
				)
			);
		$query->select('count(a.id) as total');
		$query->select('count(distinct a.user_id) as user_count');
		$query->from('#__saxum_iplogger as a');
		
		$query->group('a.ip');
		$query->group('a.country_code');
		$query->group('a.country_name');
		$query->group('a.city');
	
		// Filter by search in title.
		$search = $this->getState('filter.search');
		if (!empty($search))
		{
		if (stripos($search, 'ip:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 3), true) . '%');
				$query->where('(ip LIKE ' . $search . ')');
			}
			elseif (stripos($search, 'ip-address:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 11), true) . '%');
				$query->where('(ip LIKE ' . $search . ')');
			}
			elseif (stripos($search, 'country:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 8), true) . '%');
				$query->where('(country_name LIKE ' . $search . ')');
			}
			elseif (stripos($search, 'city:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 5), true) . '%');
				$query->where('(city LIKE ' . $search . ')');
			}
			else
			{
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(ip LIKE ' . $search . ')');
			}
		}
		
		$orderCol	= $this->state->get('list.ordering');
		$orderDirn	= $this->state->get('list.direction');
		$query->order($db->escape($orderCol.' '.$orderDirn));
		
		$db->setQuery($query);
		$results = $db->loadObjectList();

		// Check for database errors
		if ($error = $db->getErrorMsg()) {
			JError::raiseError(500, $error);
			return false;
		};

		return $results;
    }
	
	protected function populateState($ordering = null, $direction = null) 
	{
		// Initialise variables.
		$app = JFactory::getApplication('administrator');

		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);		
		
        $value = $this->getUserStateFromRequest($this->context.'.ordercol', 'filter_order', $ordering);
        if (!in_array($value, $this->filter_fields)) {
            $value = $ordering;
            $app->setUserState($this->context.'.ordercol', $value);
        }
        $this->setState('list.ordering', $value);

        // Check if the ordering direction is valid, otherwise use the incoming value.
        $value = $app->getUserStateFromRequest($this->context.'.orderdirn', 'filter_order_Dir', $direction);
        if (!in_array(strtoupper($value), array('ASC', 'DESC', ''))) {
            $value = $direction;
            $app->setUserState($this->context.'.orderdirn', $value);
        }
        $this->setState('list.direction', $value);
				
        parent::populateState('total', 'desc');
	}

	public static function getCountries($limit=10)
    {	
    	$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('a.country_code,a.country_name, COUNT(1) as num');
		$query->from('#__saxum_iplogger AS a');
		$query->where('country_code is not null');
		$query->where('country_code<>\'\'');
		$query->group('a.country_code,a.country_name');
		$query->order('3 desc');
		$db->setQuery($query, 0, $limit);
		$results = $db->loadObjectList();

		// Check for database errors
		if ($error = $db->getErrorMsg()) {
			JError::raiseError(500, $error);
			return false;
		};

		return $results;
    }
	
	public static function getCities($limit=10)
    {
    	$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('a.city,a.country_code,a.country_name, COUNT(1) as num');
		$query->from('#__saxum_iplogger AS a');
		$query->where('city is not null');
		$query->where('city<>\'\'');
		$query->group('a.city,a.country_code,a.country_name');
		$query->order('4 desc');
		$db->setQuery($query, 0, $limit);
		$results = $db->loadObjectList();

		// Check for database errors
		if ($error = $db->getErrorMsg()) {
			JError::raiseError(500, $error);
			return false;
		};

		return $results;
    }
	
	function getIps($limit=10)
    {
    	$db = JFactory::getDbo();
		$query = $db->getQuery(true);

		$query->select('a.ip, COUNT(1) as num');
		$query->from('#__saxum_iplogger AS a');
		$query->group('a.ip');
		$query->order('2 desc');
		$db->setQuery($query, 0, $limit);
		$results = $db->loadObjectList();

		// Check for database errors
		if ($error = $db->getErrorMsg()) {
			JError::raiseError(500, $error);
			return false;
		};

		return $results;
    }
	
	function getDetails()
    {
		$mode = JRequest::getCmd('mode');
		$user_id = JRequest::getCmd('user_id');
		$ip = JRequest::getCmd('ip');
		
		// Create a new query object.		
		$db = $this->getDBO();
		$query = $db->getQuery(true);

		switch ($mode){
			case 'ip':
			{
				$query->group('a.ip');	
				$query->select('a.ip as mode');	
				$query->where('a.ip!=\'\'');
				$query->order('num desc');
        		//parent::populateState('num', 'desc');
			} break;
			case 'country':
			{
				$query->group('concat("TOCHANGE",a.country_code," ",a.country_name)');	
				$query->select('concat("TOCHANGE",a.country_code," ",a.country_name) as mode');	
				$query->where('a.country_code is not null');	
				$query->order('num desc');
			} break;
			case 'city':
			{
				$query->group('concat("TOCHANGE",a.country_code," ",a.country_name,", ",a.city)');	
				$query->select('concat("TOCHANGE",a.country_code," ",a.country_name,", ",a.city) as mode');	
				$query->where('a.city is not null AND a.city !=\'\'');	
				$query->order('num desc');
			} break;
			case 'user':
			{
				$query->group('ifnull(u.username,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\'),ifnull(u.name,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\')');	
			} break;
		}
    
		if ($mode=='user'){
			$query->select(
				$this->getState(
					'list.select',
					'a.*'
					)
				);
			$query->from('#__saxum_iplogger as a');
			
			$query->join('LEFT','#__users as u ON a.user_id=u.id');
			$query->select('ifnull(u.username,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\') as username');
			$query->select('ifnull(u.name,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\') as name');
			$query->select('count(1) as num');
			$query->select('u.id as userid');
			$query->group('ifnull(u.username,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\')');
			$query->group('ifnull(u.name,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\')');
			$query->where('a.ip='.$db->quote($ip));
		} else {
			// Select some fields
			$query->select(
				$this->getState(
					'list.select',
					'a.*'
					)
				);
			$query->from('#__saxum_iplogger as a');
			
			$query->join('LEFT','#__users as u ON a.user_id=u.id');
			$query->select('ifnull(u.username,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\') as username');
			$query->select('ifnull(u.name,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\') as name');
			$query->select('count(1) as num');
			$query->select('u.id as userid');
			$query->group('ifnull(u.username,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\')');
			$query->group('ifnull(u.name,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\')');
			$query->where('a.user_id='.$user_id);
		}
			
		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			if (stripos($search, 'ip:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 3), true) . '%');
				$query->where('(ip LIKE ' . $search . ')');
			}
			elseif (stripos($search, 'user:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 5), true) . '%');
				$query->where('(ifnull(u.username,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\') LIKE ' . $search . ')');
			}
			elseif (stripos($search, 'name:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 5), true) . '%');
				$query->where('(ifnull(u.name,\''.JText::_('COM_SAXUMIPLOGGER_GUEST').'\') LIKE ' . $search . ')');
			}
			elseif (stripos($search, 'country:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 8), true) . '%');
				$query->where('(country_name LIKE ' . $search . ')');
			}
			elseif (stripos($search, 'city:') === 0)
			{
				$search = $db->quote('%' . $db->escape(substr($search, 5), true) . '%');
				$query->where('(city LIKE ' . $search . ')');
			}
			else
			{
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(ip LIKE ' . $search . ')');
			}
		}
	/*
		$orderCol	= $this->state->get('list.ordering');
		$orderDirn	= $this->state->get('list.direction');
		$query->order($db->escape($orderCol.' '.$orderDirn));
		*/
		return $this->_getList($query);
    }
}
