<?php
/**
 * $Id: install.php 186 2016-04-26 08:09:52Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class com_SaxumiploggerInstallerScript
{
	/**
	 * method to install the component
	 *
	 * @return void
	 */
	function install($parent) 
	{
		// installing plugin
		$manifest = $parent->get("manifest");
		$parent = $parent->getParent();
		$source = $parent->getPath("source");
		$installer = new JInstaller();
		
		// Install plugins
		foreach($manifest->plugins->plugin as $plugin) {
			$attributes = $plugin->attributes();
			$plg = $source . DIRECTORY_SEPARATOR . $attributes['folder'].DIRECTORY_SEPARATOR.$attributes['plugin'];
			$installer->install($plg);
		}
		
        // enabling userplugin
        $db = JFactory::getDBO();
        $db->setQuery('update #__extensions set enabled = 1 where element = "saxumiplogger" and folder = "user"');
        $db->query();
          
	}
 
	/**
	 * method to uninstall the component
	 *
	 * @return void
	 */
	function uninstall($parent) 
	{
		// $parent is the class calling this method
		//echo '<p>' . JText::_('COM_SAXUMIPLOGGER_UNINSTALL_TEXT') . '</p>';
			 
		// uninstalling plugin
		$manifest = $parent->get("manifest");
		$parent = $parent->getParent();
		$source = $parent->getPath("source");
		
		$installer = new JInstaller();
		$db	= JFactory::getDBO();
		
		// Uninstall plugins
		foreach($manifest->plugins->plugin as $plugin) {
			$attributes = $plugin->attributes();
			$db->setQuery("select extension_id from #__extensions where name = '".$attributes['name']."' and type = 'plugin' and element = '".$attributes['plugin']."'");
	        $plugin = $db->loadObject();
			$installer->uninstall('plugin',$plugin->extension_id);
		}
	}
 
	/**
	 * method to update the component
	 *
	 * @return void
	 */
	function update($parent) 
	{
		// $parent is the class calling this method
		//echo '<p>' . JText::_('COM_SAXUMIPLOGGER_UPDATE_TEXT') . '</p>';
			
		// installing plugin
		$manifest = $parent->get("manifest");
		$parent = $parent->getParent();
		$source = $parent->getPath("source");
		$installer = new JInstaller();
		
		// Install plugins
		foreach($manifest->plugins->plugin as $plugin) {
			$attributes = $plugin->attributes();
			$plg = $source . DIRECTORY_SEPARATOR . $attributes['folder'].DIRECTORY_SEPARATOR.$attributes['plugin'];
			$installer->install($plg);
		}
	}
 
	/**
	 * method to run before an install/update/uninstall method
	 *
	 * @return void
	 */
	function preflight($type, $parent) 
	{
		// $parent is the class calling this method
		// $type is the type of change (install, update or discover_install)
		//echo '<p>' . JText::_('COM_SAXUMIPLOGGER_PREFLIGHT_' . $type . '_TEXT') . '</p>';
	}
 
	/**
	 * method to run after an install/update/uninstall method
	 *
	 * @return void
	 */
	function postflight($type, $parent) 
	{
		// $parent is the class calling this method
		// $type is the type of change (install, update or discover_install)
		//echo '<p>' . JText::_('COM_SAXUMIPLOGGER_POSTFLIGHT_' . $type . '_TEXT') . '</p>';
	}
}