CREATE TABLE IF NOT EXISTS `#__saxum_iplogger_emails` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(32) NOT NULL COMMENT 'Short name of the email message type.',
  `description` mediumtext NOT NULL COMMENT 'Description of the email message type',
  `subject` varchar(128) NOT NULL COMMENT 'Email subject title for email message type',
  `body` longtext NOT NULL COMMENT 'Template for the email message itself.',
  PRIMARY KEY  (`id`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8
;

INSERT IGNORE INTO `#__saxum_iplogger_emails` (`id`, `type`, `description`, `subject`, `body`) VALUES
(1, 'user_login', 'Email sent in case of too many logins', 'Warning - [sitename]', '<p>Dear [username]!</p>\r\n<p>You (or someone else) have already logged in into [sitename] website [num_of_logins] times today. There is a risk of unauthorized use.</p>\r\n<p>If more logins are registered, your username will be disabled automatically.</p>\r\n<p>Regards, the team of [sitename]</p>'),
(2, 'admin_login', 'Email sent to site administrator in case of too many logins', '[sitename] - Suspected abuse (too many logins): [username]', '<p>Suspected abuse: [username] user has logged in [num_of_logins] times into the [sitename] website today.</p>'),
(3, 'user_ip', 'Email sent in case of logins from too many IP addresses', 'Warning - [sitename]', '<p>Dear [username]!</p>\r\n<p>You (or someone else) have already logged in into [sitename] website from [num_of_logins] different IP addresses. There is a risk of unauthorized use.</p>\r\n<p>If more IP addresses are registered, your username will be disabled automatically.</p>\r\n<p>Regards, the team of [sitename]</p>'),
(4, 'admin_ip', 'Email sent to site administrator in case of logins from too many IP addresses', '[sitename] - Suspected abuse: [username] - too many IP', '<p>Suspected abuse: [username] user has logged in from [num_of_logins] different IP addresses into [sitename] website today.</p>'),
(5, 'user_location', 'Email sent in case of logins from too many locations', 'Warning - [sitename]', '<p>Dear [username]!</p>\r\n<p>You (or someone else) have already logged in into [sitename] website from [num_of_logins] different location. There is a risk of unauthorized use.</p>\r\n<p>If more locations are registered, your username will be disabled automatically.</p>\r\n<p>Regards, the team of [sitename]</p>'),
(6, 'admin_location', 'Email sent in case of logins from too many locations', '[sitename] - Suspected abuse: [username] - too many location', '<p>Suspected abuse: [username] user has logged in from [num_of_logins] different locations into [sitename] website today.</p>');

