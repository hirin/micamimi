CREATE TABLE IF NOT EXISTS `#__saxum_iplogger_excp` (
	`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
	`ip` VARCHAR(255),
	`description` VARCHAR(255),
	PRIMARY KEY (`id`)
) ENGINE = MYISAM DEFAULT CHARSET=utf8;