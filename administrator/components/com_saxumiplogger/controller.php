<?php
/**
 * $Id: controller.php 138 2015-11-25 21:22:15Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class SaxumiploggerController extends JControllerLegacy
{
	function display($cachable = false, $urlparams = false)
	{
		// set default view if not set
		JRequest::setVar('view', JRequest::getCmd('view', 'Saxumiplogger'));
		// call parent behavior
		parent::display($cachable);
	}

	static function quickIconButton( $link, $image, $text ) {
		
		$lang	= JFactory::getLanguage();
		$button = '';
		/*if ($lang->isRTL()) {
			$button .= '<div style="float:right;">';
		} else {
			$button .= '<div style="float:left;">';
		}*/
		$button .=	'<div class="thumbnails sax-icon">'
				   .'<a class="thumbnail sax-icon-inside" href="'.$link.'">'
				   .JHtml::_('image',  'administrator/components/com_saxumiplogger/assets/images/'.$image, $text )
				   .'<br /><span>'.$text.'</span></a>'
				   .'</div>';
		//$button .= '</div>';

		return $button;
		
	}
	
	public static function addSubmenu($submenu) 
	{
		$canDo = SaxumiploggerController::getActions();
		JHtmlSidebar::addEntry(JText::_('COM_SAXUMIPLOGGER_CONTROLPANEL'), 'index.php?option=com_saxumiplogger', $submenu == 'controlpanel');
		JHtmlSidebar::addEntry(JText::_('COM_SAXUMIPLOGGER_REPORT'), 'index.php?option=com_saxumiplogger&view=report', $submenu == 'report');
		JHtmlSidebar::addEntry(JText::_('COM_SAXUMIPLOGGER_STATISTICS'), 'index.php?option=com_saxumiplogger&view=statistics', $submenu == 'statistics');
			if ($canDo->get('core.block')) 
		{
			JHtmlSidebar::addEntry(JText::_('COM_SAXUMIPLOGGER_BLOCKED'), 'index.php?option=com_saxumiplogger&view=blocks', $submenu == 'blocks');
		}
		if ($canDo->get('core.excp')) 
		{
			JHtmlSidebar::addEntry(JText::_('COM_SAXUMIPLOGGER_EXCP'), 'index.php?option=com_saxumiplogger&view=excps', $submenu == 'excps');
		}
		if ($canDo->get('core.email')) 
		{
			JHtmlSidebar::addEntry(JText::_('COM_SAXUMIPLOGGER_EMAIL'), 'index.php?option=com_saxumiplogger&view=emails', $submenu == 'emails');
		}
		if ($canDo->get('core.update')) 
		{
			JHtmlSidebar::addEntry(JText::_('COM_SAXUMIPLOGGER_UPDATE'), 'index.php?option=com_saxumiplogger&view=update', $submenu == 'update');
		}
		JHtmlSidebar::addEntry(JText::_('COM_SAXUMIPLOGGER_INFO'), 'index.php?option=com_saxumiplogger&view=info', $submenu == 'info');
	}
	
	function refresh(){
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		SaxumiploggerController::location_update($cids);
		$msg = JText::_( 'COM_SAXUMIPLOGGER_REFRESHED' );
		$this->setRedirect( 'index.php?option=com_saxumiplogger&view=report', $msg );
	}
      
	static function location_update($ids=array()) {
		
		if (file_exists(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_saxumiplogger'.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'inc_VisitorData'.DIRECTORY_SEPARATOR.'GeoLiteCity.dat')){
			if (filesize(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_saxumiplogger'.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'inc_VisitorData'.DIRECTORY_SEPARATOR.'GeoLiteCity.dat')!=0){
				if (!function_exists('GeoIP_record_by_addr')) {
					include(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_saxumiplogger'.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'inc_VisitorData'.DIRECTORY_SEPARATOR.'geoipcity.inc');
				}
				$geoip = geoip_open(JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_saxumiplogger'.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'inc_VisitorData'.DIRECTORY_SEPARATOR.'GeoLiteCity.dat', GEOIP_STANDARD);
				
				$db	= JFactory::getDBO();
				$query = $db->getQuery(true);

				$query->select('id,ip');
				$query->from('#__saxum_iplogger');
				if (!empty($ids)){
					$query->where('id in ('.implode(',',$ids).')');
				} else {
					$query->where('country_code = \'\' or country_code is null');
				}
				$db->setQuery( $query );

				foreach( $db->loadObjectList() as $row )
				{
					$country_code=strtolower(geoip_country_code_by_addr($geoip, $row->ip ));
					$country_name=geoip_country_name_by_addr($geoip, $row->ip); 
					$addr=GeoIP_record_by_addr($geoip, $row->ip);
					if (!empty($addr)) {
						$city=$addr->city;
					}
					if($country_code!='' && $country_name!='' /*&& $city!=''*/){
						$query = $db->getQuery(true);
 
						$fields = array(
						    'country_code='.$db->quote($country_code),
						    'country_name='.$db->quote($country_name),
						    'city='.$db->quote($city));
						 
						$conditions = array(
						    'id='.$row->id);
						 
						$query->update('#__saxum_iplogger')->set($fields)->where($conditions);
 
						$db->setQuery($query);
						//$db->setQuery("UPDATE #__saxum_iplogger SET country_code='".$country_code."', country_name='".$country_name."', city='".$city."' WHERE id=".$row->id);
						$db->query();
					}
				}

				geoip_close($geoip);
				
				$query = $db->getQuery(true);
 
				$fields = array('city=NULL');
				$conditions = array('city=\'\'');
				$query->update('#__saxum_iplogger')->set($fields)->where($conditions);
 
				$db->setQuery($query);
				//$db->setQuery("UPDATE #__saxum_iplogger SET city=NULL WHERE city=''");
				$db->query();
			}
		}
		return;
	}
	
	function block()
	{
		$ip=JRequest::getVar('ip');
		$model = $this->getModel('report');
		 if(!$model->block($ip)) {
  			JError::raiseWarning( 500, JText::_( 'COM_SAXUMIPLOGGER_BLOCK_ERROR') );
  			$msg='';
		 } else {
		    $msg = JText::_( 'COM_SAXUMIPLOGGER_BLOCK_OK' );
		 }
		$this->setRedirect( 'index.php?option=com_saxumiplogger&view=report', $msg );
	}
	
	function purge()
	{
		$model = $this->getModel('report');
		 if(!$model->delete()) {
		    $msg = JText::_( 'COM_SAXUMIPLOGGER_ERROR_DELETE');
		 } else {
		    $msg = JText::_( 'COM_SAXUMIPLOGGER_DELETE_OK' );
		 }
		$this->setRedirect( 'index.php?option=com_saxumiplogger&view=report', $msg );
	}

	function export(){
		JRequest::setVar( 'layout', 'export'  );
		parent::display();
	}

	function cancel()
	{
		$this->setRedirect( 'index.php?option=com_saxumiplogger' );
	}
	
	function details()
	{
		$document	= JFactory::getDocument();
		$vType		= $document->getType();
	
		$model		= $this->getModel( 'statistics' );
		$view		= $this->getView( 'statistics', $vType );
		$view->setModel( $model, true );		
		$view->setLayout( 'details');

		$view->details('user');
	}	
	
	function details_ip()
	{
		$document	= JFactory::getDocument();
		$vType		= $document->getType();
	
		$model		= $this->getModel( 'statistics' );
		$view		= $this->getView( 'statistics', $vType );
		$view->setModel( $model, true );	
		$view->setLayout( 'details');

		$view->details('ip');
	}	
	
	function change_group()
	{
		$document	= JFactory::getDocument();
		$vType		= $document->getType();
	
		$model		= $this->getModel( 'statistics' );
		$view		= $this->getView( 'statistics', $vType );
		$view->setModel( $model, true );		
		//$view->setLayout( 'details');
		$tpl=JRequest::getCmd('tpl');
		
		if ($tpl=='ip') $view->display('user');
		if ($tpl=='user') $view->display('ip');
	}

	function copy_the_file($source, $destination)
	{
	  $resource = curl_init();
	 
	  curl_setopt($resource, CURLOPT_URL, $source);
	  curl_setopt($resource, CURLOPT_HEADER, false);
	  curl_setopt($resource, CURLOPT_RETURNTRANSFER, true);
	  curl_setopt($resource, CURLOPT_CONNECTTIMEOUT, 600);
	 
	  $content = curl_exec($resource);
	 
	  curl_close($resource);
	 
	  if($content != '')
	  {
	    $fp = fopen($destination, 'w');
	    $fw = fwrite($fp, $content);
	    fclose($fp);
	 
	    if($fw != false)
	    {
	      return true;
	    }
	  }
	 
	  return false;
	}
	
	function file_upload()
	{		
		// Get the uploaded file information
		$userfile = JRequest::getVar('upload_file', null, 'files', 'array');
		
		$file_to= JPATH_COMPONENT.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'inc_VisitorData'.DIRECTORY_SEPARATOR.$userfile['name'];
		
		// Move uploaded file
		jimport('joomla.filesystem.file');
		$uploaded = JFile::upload($userfile['tmp_name'], $file_to);
		
    	$link= JRoute::_('index.php?option=com_saxumiplogger&view=update', false);
    	$this->setRedirect( $link, $message);
	}
	
	function file_update()
	{		
		$file_to= JPATH_COMPONENT.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR.'inc_VisitorData'.DIRECTORY_SEPARATOR.'GeoLiteCity.dat';
		$file	='http://saxum2003.hu/downloads/category/2-iplogger.html?download=10%3Adatafile';
	
		if (in_array('curl', get_loaded_extensions())) {
	        if( $this->copy_the_file($file,$file_to) ) {
	        	$message = JText::_('COM_SAXUMIPLOGGER_FILE_UPDATED_SUCCESSFULLY');
	        } else {
				$err =error_get_last();
	        	$message = $err[message];
	        }
		} elseif (ini_get('allow_url_fopen')==1) {
			set_time_limit ( 900 ) ;
			if( copy($file,$file_to) ) {
	        	$message = JText::_('COM_SAXUMIPLOGGER_FILE_UPDATED_SUCCESSFULLY');
	        } else {
				$err =error_get_last();
	        	$message = $err[message];
        	}
		} else {
	        $message = JText::_('COM_SAXUMIPLOGGER_FILE_NOT_UPDATED');			
		}
    	$link= JRoute::_('index.php?option=com_saxumiplogger&view=update', false);
    	$this->setRedirect( $link, $message);
	}
	/**
	 * Get the actions
	 */
	public static function getActions($messageId = 0)
	{	
		jimport('joomla.access.access');
		$user	= JFactory::getUser();
		$result	= new JObject;
 
		if (empty($messageId)) {
			$assetName = 'com_saxumiplogger';
		}
		else {
			$assetName = 'com_saxumiplogger.message.'.(int) $messageId;
		}
 
		$actions = JAccess::getActions('com_saxumiplogger', 'component');
 
		foreach ($actions as $action) {
			$result->set($action->name, $user->authorise($action->name, $assetName));
		}
 
		return $result;
	}
	
	static function getip()
	{
		if (isset($_SERVER))
		{
			if (isset($_SERVER["HTTP_X_FORWARDED_FOR"]))
			{
				$realip = $_SERVER["HTTP_X_FORWARDED_FOR"];
		        $ip_array = explode(",", $realip);
		        $realip = trim($ip_array[0]); 
			}
			elseif (isset($_SERVER["HTTP_CLIENT_IP"]))
			{
				$realip = $_SERVER["HTTP_CLIENT_IP"];
			}
			else
			{
				$realip = $_SERVER["REMOTE_ADDR"];
			}
		}
		else
		{
			if ( getenv( 'HTTP_X_FORWARDED_FOR' ) )
			{
				$realip = getenv( 'HTTP_X_FORWARDED_FOR' );
		        $ip_array = explode(",", $realip);
		        $realip = trim($ip_array[0]); 
			}
			elseif ( getenv( 'HTTP_CLIENT_IP' ) )
			{
				$realip = getenv( 'HTTP_CLIENT_IP' );
			}
			else
			{
				$realip = getenv( 'REMOTE_ADDR' );
			}
		}
		return $realip;
	}
	
	static function ipinlist($ipTable,$ip) {
		if(empty($ipTable)) return;
		if(!function_exists('ip2long')) return;

		$block=0;
		$block2=1;

		if(strstr($ip, '-'))
		{	// Inclusive IP range, i.e. 123.123.123.123-124.125.126.127
			list($ipfrom,$ipto) = explode('-', $ip, 2);
			$ipfrom = sprintf("%u\n", ip2long(trim($ipfrom)));
			$ipto = sprintf("%u\n", ip2long(trim($ipto)));
			// Swap from/to if they're in the wrong order
			if($ipfrom > $ipto) list($ipfrom, $ipto) = array($ipto, $ipfrom);
  			for ($i=$ipfrom-$ipfrom; $i <= $ipto-$ipfrom; $i++) {		
				$block2=($block2 && self::ipinList($ipTable,long2ip($i+$ipfrom)));
			}
			if ($block2==1) return $block2;
		}
		else
		{
			// Standard IP address, i.e. 123.123.123.123 or partial IP address, i.e. 123.[123.][123.][123]
			$ip = trim($ip);
			$dots = 0;
			if(substr($ip, -1) == '.') {
				// Partial IP address.
				foreach(count_chars($ip,1) as $i => $val) {
					if($i == 46) $dots = $val;
				}
  				//	JError::raiseWarning( 500, $dots );
				switch($dots) {
					case 1:	
			  			for ($i=0; $i < 255; $i++) {		
							$block2=($block2 && self::ipinList($ipTable,$ip.$i.'.'));
							if ($block2==0) break;
						}	
						break;
						
					case 2:
			  			for ($i=0; $i < 255; $i++) {		
							$block2=($block2 && self::ipinList($ipTable,$ip.$i.'.'));
							if ($block2==0) break;
						}	
						break;
						
					case 3:
			  			for ($i=0; $i < 255; $i++) {		
							$block2=($block2 && self::ipinList($ipTable,$ip.$i));
							if ($block2==0) break;
						}
						break;
						
					default:
						$dots = 0;
				}
				if ($block2==1) return $block2;
			}
		}

		$myIP = sprintf("%u\n",ip2long($ip));
	
		foreach($ipTable as $ipExpression)
		{
			if(strstr($ipExpression, '-'))
			{
				// Inclusive IP range, i.e. 123.123.123.123-124.125.126.127
				list($from,$to) = explode('-', $ipExpression, 2);
				$from = sprintf("%u\n", ip2long(trim($from)));
				$to = sprintf("%u\n", ip2long(trim($to)));
				// Swap from/to if they're in the wrong order
				if($from > $to) list($from, $to) = array($to, $from);
				if( ($myIP >= $from) && ($myIP <= $to) ) $block=1;
			}
			else
			{
				// Standard IP address, i.e. 123.123.123.123 or partial IP address, i.e. 123.[123.][123.][123]
				$ipExpression = trim($ipExpression);
				$dots = 0;
				if(substr($ipExpression, -1) == '.') {
					// Partial IP address.
					foreach(count_chars($ipExpression,1) as $i => $val) {
						if($i == 46) $dots = $val;
					}
					switch($dots) {
						case 1:
							$netmask = '255.0.0.0';
							$ipExpression .= '0.0.0';
							break;
							
						case 2:
							$netmask = '255.255.0.0';
							$ipExpression .= '0.0';
							break;
							
						case 3:
							$netmask = '255.255.255.0';
							$ipExpression .= '0';
							break;
							
						default:
							$dots = 0;
					}
					
					if($dots) {
						// Convert netmask to CIDR
						$long = ip2long($netmask);
						$base = ip2long('255.255.255.255');
						$netmask = 32 - log(($long ^ $base)+1,2);
		
						// Compare the IP to the masked IP
						$ip_binary_string = sprintf("%032b",ip2long($ip));
						$net_binary_string = sprintf("%032b",ip2long(trim($ipExpression)));
						if( substr_compare($ip_binary_string,$net_binary_string,0,$netmask) === 0 )
						{
							$block=1;
						}
					}
				}
				if(!$dots) {
					$ip2 = ip2long(trim($ipExpression));
					if($ip2 == ip2long($ip)) $block=1;
				}
			}
		}
		return $block;
	}
		
	static function sendmail($options)
	{
		$app	= JFactory::getApplication();	
		$user	= JFactory::getUser($options['user']);
		$mailer = JFactory::getMailer();
		$config = JFactory::getConfig();
		$sender_name = $config->get( 'fromname' );
		
		JLoader::import('joomla.application.component.model');  
		JLoader::import( 'email', JPATH_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_saxumiplogger' . DIRECTORY_SEPARATOR . 'models' );
		$model = JModelLegacy::getInstance( 'email', 'SaxumiploggerModel' );
		$format=$model->getFormat($options['type']);
		
		if (substr($options['type'],0,5)=='admin'){
			$recipient=$model->getAdmins();
		} else {
	    	$recipient[0] = $user;
		}
				
	    $subject = $format[0]->subject;
	    $body = '';
	    $body.= $format[0]->body;

	    $body = str_replace('[userloginname]', $user->username, $body);
	    $body = str_replace('[username]', $user->name, $body);
	    $body = str_replace('[sitename]', $sender_name, $body);
	    $body = str_replace('[num_of_logins]', $options['num_of_logins'], $body);

	    $subject = str_replace('[userloginname]', $user->username, $subject);
	    $subject = str_replace('[username]', $user->name, $subject);
	    $subject = str_replace('[sitename]', $sender_name, $subject);
	    $subject = str_replace('[num_of_logins]', $options['num_of_logins'], $subject);
	    
	 	jimport( 'joomla.mail.helper' );
	    $sender_name = JMailHelper::cleanAddress($sender_name);
	    $subject = JMailHelper::cleanSubject($subject);
	    $body = JMailHelper::cleanBody($body);
	    
		$sender = array( 
		    $config->get( 'mailfrom' ),
		    $sender_name );
		 
		$mailer->setSender($sender);
	    $mailer->setSubject($subject);
		$mailer->isHTML(true);
		$mailer->Encoding = 'base64';
		$mailer->setBody($body);
	    
	    foreach ( $recipient as $r)
	    {
	    	$mailer->addRecipient($r->email);
		    $mailer->Send();
	    }
	    		    
	    return;		
	}
	
}

