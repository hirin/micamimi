<?php
/**
 * $Id: email.php 140 2015-11-28 11:25:23Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die();

class SaxumiploggerTableEmail extends JTable
{
	/** @var int Primary key */
	var $id = 0;
	 
	function __construct(&$db)
 	{
    	parent::__construct( '#__saxum_iplogger_emails', 'id', $db );
    }

    public function check()
    {
    	$model = JModelLegacy::getInstance( 'email', 'SaxumiploggerModel' );
    	$recipient=$model->getAdmins();
		if (empty($recipient)) {	
    		JFactory::getApplication()->enqueueMessage(JText::_( 'COM_SAXUMIPLOGGER_NO_USER_IN_ADMIN_GROUP'),'warning' );
		}
    	return true;
    }
}
