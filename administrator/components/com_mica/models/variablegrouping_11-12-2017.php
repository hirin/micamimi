<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\Utilities\ArrayHelper;

/**
 * Methods supporting a list of Variable Grouping records.
 *
 * @since  1.6
 */
class MicaModelVariablegrouping extends JModelList
{

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	/**
	 * Gets the list of users and adds expensive joins to the result set.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6
	 */
	public function getItems()
	{
		$items = parent::getItems();
		if (empty($items)){
			return array();
		}
		return $items;
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 *
	 * @since   1.6
	 */
	protected function getListQuery(){
		// Create a new query object.
		$db    = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select('grp.*');
		$query->from($db->quoteName('#__mica_group') . ' AS grp ');
		$query->join(" INNER ", $db->quoteName('#__mica_group_field')." AS allfields ON ".$db->quoteName('allfields.groupid')." = ".$db->quoteName('grp.id'));
		$query->order($db->quoteName('grp.ordering').' ASC ');
		$query->group($db->quoteName('grp.id'));

		return $query;
	}

	/**
	 * Gets list of all fields.
	 */
	function getAllFields(){
		$db_table = array(
			"State"    => "rail_state",
			"District" => "india_information",
			"Urban"    => "jos_mica_urban_agglomeration",
			"Town"     => "my_table"
		);

		// Create a new query object.
		$db        = $this->getDbo();
		$allFields = array("State" => array(), "District" => array(), "Urban" => array(), "Town" => array() );
		foreach ($db_table as $db_table_key => $each_db_table) {
			$SQL_DB_TABLE = "DESCRIBE ".$db->quoteName($each_db_table);
			$db->setQuery($SQL_DB_TABLE);
			$db_table_fields = $db->loadObjectList();

			$mylist = array();
			foreach ($db_table_fields as $key => &$eachfield) {
				$eachfield->Field = str_replace("Urban_","",$eachfield->Field);
				$eachfield->Field = str_replace("Rural_","",$eachfield->Field);
				$eachfield->Field = str_replace("Total_","",$eachfield->Field);
				if (!in_array($eachfield->Field, $mylist)) {
					$mylist[] = $eachfield->Field;
				}
			}
			$allFields[$db_table_key] = $mylist;
		}
		return $allFields;
	}

	/**
	 * Returns Group Information.
	 */
	public function getGroupDetail($id = 0){
		$db = JFactory::getDBO();

		$groupdetail = array(
			'id'   => $id,
			'name' => '',
			'icon' => '',
			'fields' => array()
		);
		if ($id > 0 ) {
			$SQL = " SELECT g.*  FROM ".$db->quoteName('#__mica_group')." AS g
				WHERE ".$db->quoteName('g.id')." = ".$db->quote($id);
			$db->setQuery($SQL);
			$group = $db->loadAssoc();

			if (count($group) > 0) {
				$groupdetail['name'] = $group['group'];
				$groupdetail['icon'] = $group['icon'];

				$GROUP_FIELDS_SQL = " SELECT ".$db->quoteName('field').", ".$db->quoteName('table')."
					FROM ".$db->quoteName('#__mica_group_field')."
					WHERE ".$db->quoteName('groupid')." = ".$db->quote($id);
				$db->setQuery($GROUP_FIELDS_SQL);
				$group_fields = $db->loadObjectList();

				if (count($group_fields) > 0) {
					foreach ($group_fields as $key => $each_field) {
						if (!array_key_exists($each_field->table, $groupdetail['fields'])) {
							$groupdetail['fields'][$each_field->table] = array();
						}
						$groupdetail['fields'][$each_field->table][] = $each_field->field;
					}
				}
			}
		}
		return $groupdetail;
	}

}
