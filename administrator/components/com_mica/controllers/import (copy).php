<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

jimport('joomla.filesystem.file');
jimport('joomla.filesystem.folder');

/**
 * Import controller class.
 *
 * @since  1.6
 */
class MicaControllerImport extends JControllerLegacy
{

	/**
	 * Upload File from which data needs to be imported.
	 *
	 * @return  boolean
	 *
	 * @since   1.5
	 */
	public function upload(){
		// Check for request forgeries
		JSession::checkToken('request') or jexit(JText::_('JINVALID_TOKEN'));

		$com_media_params = JComponentHelper::getParams('com_media');
		define('COM_MEDIA_BASE', JPATH_ROOT . '/' . $com_media_params->get('file_path', 'images'));

		// Get some data from the request
		$file   = $this->input->files->get('toupload', array(), 'array');
		$folder = 'imported_xls';

		// If there are no files to upload - then bail
		if (empty($file)){
			return false;
		}

		// Instantiate the media helper
		$mediaHelper = new JHelperMedia;
		$upload_path = COM_MEDIA_BASE.DIRECTORY_SEPARATOR.$folder;
		if (!is_dir($upload_path)){
			if (JFolder::create($upload_path)){
				$data = "<html>\n<body bgcolor=\"#FFFFFF\">\n</body>\n</html>";
				JFile::write($upload_path.'/index.html', $data);
			}
		}
		if (!is_dir($upload_path)){
			JError::raiseWarning(100, JText::_('Upload Destination Folder is not exist: '.$upload_path));
			return false;
		}

		// Perform basic checks on file info before attempting anything
		$file['name']     = JFile::makeSafe($file['name']);
		$file['name']     = str_replace(' ', '-', $file['name']);
		$file['filepath'] = JPath::clean(implode(DIRECTORY_SEPARATOR, array($upload_path, $file['name'])));

		if ($file['error'] == 1){
			// File size exceed either 'upload_max_filesize' or 'upload_maxsize'.
			$this->setRedirect('index.php?option=com_mica&view=import', JText::_('Error, Uploaded file contains error!'), 'error');
			return false;
		}

		if (JFile::exists($file['filepath'])){
			unlink($file['filepath']);
		}

		if (!isset($file['name'])){
			// No filename (after the name was cleaned by JFile::makeSafe)
			$this->setRedirect('index.php?option=com_mica&view=import', JText::_('Invalid Request'), 'error');
			return false;
		}

		$options['format']    = '{DATE}\t{TIME}\t{LEVEL}\t{CODE}\t{MESSAGE}';
		$options['text_file'] = 'upload.error.php';
		JLog::addLogger($options, JLog::INFO, array('error'));

		// Load the helper class
		$format = $file['type'];
		$err    = null;// The request is valid
		if (!$mediaHelper->canUpload($file, 'com_media')){
			if ($format == 'application/vnd.ms-excel' || $format == 'application/wps-office.xls') {
				JLog::add('Invalid: '.$file['filepath'].': Unsupported Media Type', JLog::INFO, 'error');
				header('HTTP/1.0 415 Unsupported Media Type');
				$this->setRedirect('index.php?option=com_mica&view=import', JText::_('Error. Unsupported Media Type!'), 'error');
				return false;
			}else{
				// Error in upload
				$this->setRedirect('index.php?option=com_mica&view=import', JText::_($err), 'error');
				return false;
			}
		}

		if (JFile::exists($file['filepath'])) {
			if ($format == 'application/vnd.ms-excel' || $format == 'application/wps-office.xls') {
				JLog::add('File already exists: '.$file['filepath'], JLog::INFO, 'error');
				header('HTTP/1.0 409 Conflict');
				$this->setRedirect('index.php?option=com_mica&view=import', JText::_('Error. File already exists'), 'error');
				return false;
			} else {
				$this->setRedirect('index.php?option=com_mica&view=import', JText::_($err), 'error');
				return false;
			}
		}

		// Trigger the onContentBeforeSave event.
		$object_file = new JObject($file);
		if (!JFile::upload($object_file->tmp_name, $object_file->filepath) ){
			if ($format == 'application/vnd.ms-excel' || $format == 'application/wps-office.xls') {
				JLog::add('Cannot upload: '.$filepath, JLog::INFO, 'error');
				header('HTTP/1.0 400 Bad Request');
				$this->setRedirect('index.php?option=com_mica&view=import', JText::_('Error. Unable to upload file'), 'error');
				return false;
			} else {
				JError::raiseWarning(100, JText::_('Error. Unable to upload file'));
				return false;
			}
		}else{
			if ($format == 'application/vnd.ms-excel' || $format == 'application/wps-office.xls') {
				JLog::add($object_file->name, JLog::INFO, 'success');
			}
			$this->setRedirect('index.php?option=com_mica&view=import&ajaxprocess=1&filename='.$object_file->name, JText::_('Upload complete.')."<br />".JText::_('Your file in process...'), 'success');
			return false;
		}
	}

	/**
	 *
	 */
	public function insertToDatabasePreview(){
		$com_media_params = JComponentHelper::getParams('com_media');
		define('COM_MEDIA_BASE', JPATH_ROOT.'/'.$com_media_params->get('file_path', 'images'));
		$folder      = 'imported_xls';
		$upload_path = COM_MEDIA_BASE.DIRECTORY_SEPARATOR.$folder;

		$filename  = $this->input->get('filename', '', 'string');
		$operation = $this->input->get('operation', '', 'string');
		$sheet     = $this->input->get('sheet', '', 'string');

		require_once JPATH_ADMINISTRATOR.'/components/com_mica/controllers/Excel/reader.php';

		$data = new Spreadsheet_Excel_Reader();
		$data->setOutputEncoding('CP1251');
		$data->read($upload_path.DIRECTORY_SEPARATOR.$filename);

		error_reporting(E_ALL ^ E_NOTICE);
		$toprint = array();

		echo "</br></br><b>Select Operation</b></br>";
		echo '<input type="radio" name="operation" value="overwrite" /> overwrite<br /><input type="radio" name="operation" value="new" /> new</br>';
		echo "</br></br><b>Where to Insert?</b>";
		echo '</br><input type="radio" name="table" value="state" /> State Data<br />
			<input type="radio" name="table" value="district" /> District Data</br>
			<input type="radio" name="table" value="Urban" /> Urban  Data</br>
			<input type="radio" name="table" value="Town" /> Town  Data</br></br>
			<input type ="button" name="Step3" id="step3" value="Insert to Database" class="btn btn-small btn-success" /></br></br></br>';
		echo "<div id='preview'><table class='table table-striped' >";

		for ($i = 1; $i <= $data->sheets[$sheet]['numRows']; $i++) {
			if($i == 1){
				echo "<thead><tr bgcolor='gray' >";
			}else{
				echo "<tr>";
			}

			for ($j = 1; $j <= $data->sheets[$sheet]['numCols']; $j++) {
				$toprint[$i][$data->sheets[$sheet]['cells'][1][$j]] = $data->sheets[$sheet]['cells'][$i][$j];
				echo "<td>".$data->sheets[$sheet]['cells'][$i][$j]."</td>";
			}

			if($i == 1){
				echo "</tr></thead>";
			}else{
				echo "</tr>";
			}

			if($i == 3){
				echo "</table>";break;
			}
		}
		echo "</table></div>";exit;
	}

	/**
	 *
	 */
	public function insertToDatabase(){

		$com_media_params = JComponentHelper::getParams('com_media');
		define('COM_MEDIA_BASE', JPATH_ROOT.'/'.$com_media_params->get('file_path', 'images'));
		$folder      = 'imported_xls';
		$upload_path = COM_MEDIA_BASE.DIRECTORY_SEPARATOR.$folder;

		$filename  = $this->input->get('filename', '', 'string');
		$operation = $this->input->get('operation', '', 'string');
		$sheet     = $this->input->get('sheet', '', 'string');
		$table     = $this->input->get('table', '', 'string');

		require_once JPATH_ADMINISTRATOR.'/components/com_mica/controllers/Excel/reader.php';
		$data = new Spreadsheet_Excel_Reader();
		$data->setOutputEncoding('CP1251');
		$data->read($upload_path.DIRECTORY_SEPARATOR.$filename);

		error_reporting(E_ALL ^ E_NOTICE);
		$toprint = array();

		for ($i = 1; $i <= $data->sheets[$sheet]['numRows']; $i++) {
			for ($j = 1; $j <= $data->sheets[$sheet]['numCols']; $j++) {
				$toprint[$i][$data->sheets[$sheet]['cells'][1][$j]] = $data->sheets[$sheet]['cells'][$i][$j];
			}
		}

		if($operation==""){
			return "Please Select Operation";
		}

		if($operation == "new"){

			$resu = $this->operationAdd($table, $toprint[1], $toprint);
		}

		if($operation == "overwrite"){

			$resu = $this->operationUpdate($table, $toprint[1], $toprint);
			//$resu .= $this->addLanguageVariables($table, $toprint[1], $toprint);
		}

		echo "<pre>";print_r($resu);exit;
	}

	/**
	 *
	 */
	public function selectSheet(){
		$com_media_params = JComponentHelper::getParams('com_media');
		define('COM_MEDIA_BASE', JPATH_ROOT.'/'.$com_media_params->get('file_path', 'images'));
		$folder      = 'imported_xls';
		$upload_path = COM_MEDIA_BASE.DIRECTORY_SEPARATOR.$folder;

		$filename = $this->input->get('filename', '', 'string');

		require_once JPATH_ADMINISTRATOR.'/components/com_mica/controllers/Excel/reader.php';

		$data = new Spreadsheet_Excel_Reader();
		$data->setOutputEncoding('CP1251');
		$data->read($upload_path.DIRECTORY_SEPARATOR.$filename);

		error_reporting(E_ALL ^ E_NOTICE);
		echo 'Select Sheet :<select  id="sheet" >';
		for ($i = 0; $i < count($data->boundsheets); $i++) {
			echo "<option value='".$i."'>".$data->boundsheets[$i]['name']."</option>";
		}
		echo '</select>';
		echo '</br><input type="button" name="Step2" id="step2" value="Preview" class="btn btn-small btn-success" />';
		exit;
	}

	/**
	 *
	 */
	private function operationAdd($table,$columnstoinsert,$toprint){
		if($table == "state"){
			$db_table = "rail_state";
			$dndArray = array('OGR_FID','india_state','name','id','StateName','LocationID' );
		}else if($table == "district"){
			$db_table = "india_information";
			$dndArray = array('OGR_FID','india_information','distshp','state');
		}else if($table == "Urban"){
			$db_table = "jos_mica_urban_agglomeration";
			$dndArray = array('OGR_FID');
		}else if($table == "Town"){
			
			$db_table = "villages";
			$dndArray = array('OGR_FID');
		}else if($table == "language"){
			return $this->addLanguageVariables($db_table, $columnstoinsert, $toprint);
		}

		$db    = JFactory::getDBO();
		$query = 'DESCRIBE '.$db_table;
		$db->setQuery($query);
		$data  = $db->loadObjectList();

		
		
		foreach($data as $eachdata){
			if(!in_array($eachdata->Field, $dndArray)){
				$field_array[] = $eachdata->Field;
				$query = "ALTER TABLE ".$db_table." DROP ".$eachdata->Field;


				$db->setQuery($query);
				$db->execute();
			}
		}
		
		return $this->operationUpdate($table,$columnstoinsert,$toprint);
	}

	/**
	 *
	 */
	private function addLanguageVariables($table,$columnstoinsert,$toprint){

		if($table == "state"){
			$db_table = "rail_state";
		}else if($table == "district"){
			$db_table = "india_information";
		}else if($table == "Urban"){
			$db_table = "jos_mica_urban_agglomeration";
		}else if($table == "Town"){
			$db_table = "villages";
		}
		$db    = JFactory::getDBO();
		$query = 'DESCRIBE '.$db_table;
		$db->setQuery($query);
		$data  = $db->loadObjectList();

		$str   = "";
		

		foreach($toprint as $field){
			foreach($field as $key => $eachfield){
				//if(trim($key) == "Variable"){
					$eachfield = trim($eachfield);
					$eachfield = str_replace(" ","_",$eachfield);
					$eachfield = str_replace(")","",$eachfield);
					$eachfield = str_replace("(","",$eachfield);
					$eachfield = str_replace("-","_",$eachfield);
					$eachfield = str_replace("/","_",$eachfield);
					$eachfield = str_replace("'","_",$eachfield);
					$eachfield = str_replace("`","_",$eachfield);
					$eachfield = str_replace(",","_",$eachfield);
					$eachfield = str_replace("|","_",$eachfield);
					$eachfield = str_replace(".","",$eachfield);


					$eachfieldkey = str_replace("\\","_",$eachfield);
          if(strlen($eachfieldkey) > 63)
          {
            $k="";
            $eachfieldwords = preg_split("/[\s,_-]+/", $eachfieldkey);
            foreach ($eachfieldwords as $w) {
              $k .= $w[0].$w[1];
            }
            $eachfieldkey=$k;
          }
					if($eachfieldkey!=""){
						$str .= strtoupper($eachfieldkey)."='".$key."' \n";
					}
				// }else{
				// 	if($eachfieldkey!=""){
				// 		$str .= $eachfield."' \n";
				// 	}
				// }
			}
      break;
		}

		$str1 .= "Please Copy and Pest Below code in '/language/en-GB/en-GB.com_mica.ini' \n <pre>".$str."</pre>";
		return ($str1);
	}

	/**
	 *
	 */

	private function operationUpdate($table,$columnstoinsert,$toprint){


		if($table == "state"){
			$db_table = "rail_state";
		}else if($table == "district"){
			$db_table = "india_information";
		}else if($table == "Urban"){
			$db_table = "jos_mica_urban_agglomeration";
		}else if($table == "Town"){
			$db_table = "villages";
		}

		$db    = JFactory::getDBO();
		$query = 'DESCRIBE '.$db_table;
		$db->setQuery($query);
		$data  = $db->loadObjectList();

		foreach($data as $eachdata) {
			$field_array[] = $eachdata->Field;
		}
			/*echo "<pre>";print_r($field_array); echo "</pre>";exit;
			die("werwre" .$db_table);*/
			


		foreach($columnstoinsert as $eachfield){

			if (!in_array($eachfield, $field_array)){


				if(stristr($eachfield,"name")){
					$datatype = " varchar(100)";
				}else{
					$datatype = " TEXT";
				}

				if($eachfield == "Rating"){
					$datatype = " INT(100)";
				}

				$eachfield = trim($eachfield);
				$eachfield = str_replace(" ","_",$eachfield);
				$eachfield = str_replace(")","",$eachfield);
				$eachfield = str_replace("(","",$eachfield);
				$eachfield = str_replace("-","",$eachfield);
				$eachfield = str_replace("/","",$eachfield);
				$eachfield = str_replace("'","",$eachfield);
				$eachfield = str_replace("`","",$eachfield);
				$eachfield = str_replace(",","",$eachfield);
				$eachfield = str_replace("|","",$eachfield);
				$eachfield = str_replace(".","",$eachfield);
				$eachfield = str_replace(".","",$eachfield);
				$eachfield = str_replace("-","",$eachfield);
				$eachfield = str_replace("&","",$eachfield);

				$eachfield = str_replace("\\","",$eachfield);
				$eachfield = str_replace("__","_",$eachfield);

				if(strlen($eachfield) > 63)
		        {
		          $k="";
		          $eachfieldwords = preg_split("/[\s,_-]+/", $eachfield);
		          foreach ($eachfieldwords as $w) {
		            $k .= $w[0].$w[1];
		          }
		          $eachfield=$k;
		        }

		        
				$query = 'ALTER TABLE '.$db_table.' ADD '. $eachfield.$datatype;
				
				$db->setQuery($query);
				try {
					$db->execute();
				} catch (Exception $e) {

				}
				//$result[] = $query;
				//$result[] = 'ALTER TABLE '.$db_table.' ADD '. $eachfield.' INT(100)';
			}
		}

		$options['format']    = '{DATE}\t{TIME}\t{LEVEL}\t{CODE}\t{MESSAGE}';
		$options['text_file'] = 'error.php';
		JLog::addLogger($options, JLog::INFO, array('error'));

		$toprint    = array_reverse($toprint);
		$queryarray = array();
		foreach($toprint as $key => $val){
			$queryarray[$db_table][] = $val;
		}
		$cnt = 0;
		$cntd = 0;
		
		foreach($queryarray as $table=>$values){


			foreach($values as $val){
				$qvalues = array();
				
				foreach($val as $k => $v){
					if($v!=null){
						
						$eachfield = trim($k);
						$eachfield = str_replace("-","",$eachfield);
						$eachfield = str_replace(" ","_",$eachfield);
						$eachfield = str_replace(")","",$eachfield);
						$eachfield = str_replace("(","",$eachfield);
						$eachfield = str_replace("/","",$eachfield);
						$eachfield = str_replace("'","",$eachfield);
						$eachfield = str_replace("`","",$eachfield);
						$eachfield = str_replace(",","",$eachfield);
						$eachfield = str_replace("|","",$eachfield);
						
	

						$k = str_replace("\\","",$eachfield);
						$k = str_replace("__","_",$eachfield);
						if($k == "Rating"){
							$v = intval($v);
						}

						if(strlen($k) > 63)
			            {
			              $ks="";
			              $eachfieldwords = preg_split("/[\s,_-]+/", $k);
			              foreach ($eachfieldwords as $w) {
			                $ks .= $w[0].$w[1];
			              }
			              $k=$ks;
			            }

						$qvalues[] = "`".$k."` = \"".ucwords(strtolower($v))."\" ";


					
						
					}
				}



				
				$qvaluess = implode(",",$qvalues);


               
				if($val['State'] != ""){

					if($table == "india_information"){

						/*$db->setQuery('UPDATE  `'.$table.'` SET  '.$qvaluess.' WHERE  `OGR_FID`  = '.$val['OGR_FID']);
						echo "select OGR_FID from india_information where state like '".$val['State']."' and distshp like '".$val['District']."'";exit;
						*/$db->setQuery("select OGR_FID from india_information where state like '".$val['State']."' and distshp like '".$val['District']."'");
						//echo "select OGR_FID from india_information where state like '".$val['State']."' and distshp like '".$val['District']."'";
						$ogr_fid = $val['OGR_FID']=$db->loadResult();
						if($ogr_fid!=""){
							$db->setQuery('UPDATE  `'.$table.'` SET  '.$qvaluess.' WHERE `state` = "'.ucwords(strtolower($val['State'])).'" and `distshp` = "'.ucwords(strtolower($val['District'])).'"');
							$q = $ogr_fid.'->'.'UPDATE  `'.$table.'` SET  '.$qvaluess.' WHERE `state` = "'.ucwords(strtolower($val['State'])).'" and `distshp` = "'.ucwords(strtolower($val['District'])).'"';
						}
						/*}else{
							$q=$ogr_fid.'->'.ucwords(trim($val['District'])).'"=>"'.ucwords(trim($val['State']));
						}*/

						print_r('UPDATE  `'.$table.'` SET  '.$qvaluess.' WHERE `state` = "'.ucwords(strtolower($val['State'])).'" and `distshp` = "'.ucwords(strtolower($val['District'])).'"');
						echo "<BR><BR>";
						
					}
				}

				if($table == "villages"){
						if($val['OGR_FID']!='')
						{
							$db->setQuery("select OGR_FID from villages where OGR_FID = ".$val['OGR_FID']);
							$ogr_fid=$db->loadResult();
						}

						if($ogr_fid!=""){
							$db->setQuery('UPDATE  `'.$table.'` SET  '.$qvaluess.' WHERE `OGR_FID` = '.$val['OGR_FID']);
						}
				else {
                         
                      echo "<pre/>";print_r('INSERT into  `'.$table.'` set  '.$qvaluess);exit;
						$db->setQuery('INSERT into  `'.$table.'` set  '.$qvaluess);
					}
				}
				

				if ($val['OGR_FID'] != 0) {

					$res          = $db->execute();
					$affectedRows = (int) $db->getAffectedRows($res);

					if(!$res || $affectedRows == 0){
						echo "<BR>Error <BR>";
						JLog::add($q, JLog::INFO, 'error');
					}
					else 
						echo "<BR>Success<BR>";
				}

				/*if($table == "villages")
				{


				

               
						if($val['OGR_FID']!='')
						{

						$db->setQuery("select OGR_FID from villages where OGR_FID = ".$val['OGR_FID']);
						$ogr_fid=$db->loadResult();
						}



						if($ogr_fid!="")
						{

						$db->setQuery('UPDATE  `'.$table.'` SET  '.$qvaluess.' WHERE `OGR_FID` = '.$val['OGR_FID']);
						}
						else
						{


							if($val['District Code']!="" && $val['State Code']!="" && $val['Sub District Code']!="")
							{
								
								$db->setQuery('UPDATE  `'.$table.'` SET  '.$qvaluess.' WHERE `District_Code` = '.$val['District Code'] .' and `State_Code` = '.$val['State Code'].' and `Sub_District_Code` = '.$val['Sub District Code']);

							}
							//else


		                        echo "<pre/>";print_r('INSERT into  `'.$table.'` set  '.$qvaluess);exit;

								$db->setQuery('INSERT into  `'.$table.'` set  '.$qvaluess);
								$db->execute();


							}
					
				}*/
				//die();
				

				/*if ($val['OGR_FID'] != 0) {

					$res          = $db->execute();
					$affectedRows = (int) $db->getAffectedRows($res);

					if(!$res || $affectedRows == 0){
						echo "<BR>Error <BR>";
						JLog::add($q, JLog::INFO, 'error');
					}
					else 
						echo "<BR>Success<BR>";
				}*/
				
			}
		}
		echo implode("", $ar);
		return "<b>Done!!</b>";
	}
}
