<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * MICA Variable Grouping controller class.
 *
 * @since  1.6
 */
class MicaControllerUserInquiry extends JControllerLegacy
{
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'UserInquiry', $prefix = 'MicaModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	/**
	 * A task to be called to delete groups.
	 *
	 * @return  void
	 */
	public function delete()
	{
		$cid = $this->input->post->get('cid', array(), 'array');

		if (count($cid) > 0)
		{
			$cid = implode(",", $cid);
			$db = JFactory::getDBO();
			$DELETE_SQL = "DELETE FROM " . $db->quoteName('#__user_question_inquiry') . " WHERE " . $db->quoteName('id') . " IN ('" . $cid . "')";
			$db->setQuery($DELETE_SQL);
			$db->execute();

			$cid = $this->input->post->get('cid', array(), 'array');

			$msg = (count($cid) > 1) ? JText::_('User Inquiries Deleted Successfully.') : JText::_('User Inquiry Deleted Successfully.');
			$this->setRedirect("index.php?option=com_mica&view=userinquiry", $msg, 'success');
		}
		else
		{
			$this->setRedirect("index.php?option=com_mica&view=userinquiry", JText::_('Kindly select group first.'), 'error');
		}
	}
}
