<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
use Joomla\Utilities\ArrayHelper;

/**
 * MICA Variable Grouping controller class.
 *
 * @since  1.6
 */
class MicaControllerTableaucodes extends JControllerLegacy
{


    public function publish(){

        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        $ids    = $this->input->get('cid', array(), 'array');

        $values = array('state_publish' => 1, 'state_unpublish' => 0);
        $task   = $this->getTask();
        $value  = ArrayHelper::getValue($values, $task, 0, 'int');
        if (empty($ids))
        {
            JError::raiseWarning(500, JText::_('No Codes Selected'));
        }
        else
        {


            // Change the state of the records.

                if ($value == 1)
                {

                    $db = JFactory::getDBO();
                    $SQL = "update " . $db->quoteName('#__mica_tableau_pages') . " 
                     set state = 0, modified = CURRENT_TIMESTAMP WHERE " . $db->quoteName('id') . " IN ('" . implode(",",$ids) . "')";
                    $db->setQuery($SQL);
                    try{
                        $db->execute();
                    }catch (Exception $e){
                        print_r($e);
                        die();
                    }
                    $ntext = 'Code Unpublished Successfully';
                }
                else
                {

                    $db = JFactory::getDBO();
                    $SQL = "update " . $db->quoteName('#__mica_tableau_pages') . " 
                     set state = 1, modified = CURRENT_TIMESTAMP WHERE " . $db->quoteName('id') . " IN ('" . implode(",",$ids) . "')";
                    $db->setQuery($SQL);
                    try{
                        $db->execute();
                    }catch (Exception $e){
                        print_r($e);
                        die();
                    }


                    $ntext = 'Code Published Successfully';
                }

                $this->setMessage(JText::plural($ntext, count($ids)));

        }

        $this->setRedirect('index.php?option=com_mica&view=tableaucodes');


    }
	/**
	 * Proxy for getModel.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.6
	 */
	public function getModel($name = 'Tableaucode', $prefix = 'MicaModel', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	/**
	 * A task to be called to delete groups.
	 *
	 * @return  void
	 */
	public function delete()
	{
		$cid = $this->input->post->get('cid', array(), 'array');

		if (count($cid) > 0)
		{
			$cid = implode(",", $cid);
			$db = JFactory::getDBO();
			$DELETE_SQL = "DELETE FROM " . $db->quoteName('#__mica_tableau_pages') . " WHERE " . $db->quoteName('id') . " IN ('" . $cid . "')";

			$db->setQuery($DELETE_SQL);
			$db->execute();
			$msg = (count($cid) > 1) ? JText::_('User Inquiries Deleted Successfully.') : JText::_('Tableau Codes Deleted Successfully.');
			$this->setRedirect("index.php?option=com_mica&view=tableaucodes", $msg, 'success');
		}
		else
		{
			$this->setRedirect("index.php?option=com_mica&view=tableaucodes", JText::_('Kindly select codes first.'), 'error');
		}
	}
}
