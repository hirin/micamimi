<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for Variable Grouping.
 *
 * @since  1.6
 */
class MicaViewVariableGrouping extends JViewLegacy
{

	/**
	 * The item data.
	 *
	 * @var   object
	 * @since 1.6
	 */
	protected $items;

	/**
	 * The pagination object.
	 *
	 * @var   JPagination
	 * @since 1.6
	 */
	protected $pagination;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{


		$app   = JFactory::getApplication();
		$model = $this->getModel();

		$this->items      = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->addoperator = $this->get('addoperator');

		MicaHelper::addSubmenu('variablegrouping');
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
	}

	public function editsummayoperators($tpl=null)
	{

		$app   = JFactory::getApplication();

		$this->model     = $this->getModel();
		$mydata = $this->model->getAllFields_v2();
		$this->allfields = $mydata['allfields'];
		$this->groupname =$mydata['groupname'];

		$id = $app->input->get->get('id', 0, 'int');
		$this->groupdetails = $this->model->getGroupDetail($id);
		$this->operatorslist = $this->model->getOperators($this->allfields);

			MicaHelper::addSubmenu('variablegrouping');
			$bar = JToolbar::getInstance('toolbar');
			if ($id > 0) {
				JToolbarHelper::title(JText::_('Operators for - '.$this->groupdetails['name']), 'users user');
			}else{
				JToolbarHelper::title(JText::_('Variable Grouping : New'), 'users user');
			}
			JToolbarHelper::cancel('variablegrouping.cancel_saveoperators', 'JTOOLBAR_CLOSE');
			JToolbarHelper::save('variablegrouping.saveoperators');
			$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
	}

	/**
	 * A view method to add group.
	 */
	public function add($tpl = null){

		$app   = JFactory::getApplication();

		$this->model     = $this->getModel();
		$this->allfields = $this->model->getAllFields();


		$id = $app->input->get->get('id', 0, 'int');
		$this->groupdetails = $this->model->getGroupDetail($id);

			MicaHelper::addSubmenu('variablegrouping');
			$bar = JToolbar::getInstance('toolbar');
			if ($id > 0) {
				JToolbarHelper::title(JText::_('Variable Grouping : Update '), 'users user');
			}else{
				JToolbarHelper::title(JText::_('Variable Grouping : New'), 'users user');
			}
			JToolbarHelper::cancel('variablegrouping.cancel_add', 'JTOOLBAR_CLOSE');
			JToolbarHelper::save('variablegrouping.save');
			$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolbar(){
		// Get the toolbar object instance
		$bar = JToolbar::getInstance('toolbar');
		JToolbarHelper::title(JText::_('Variable Grouping'), 'users user');

		JToolbarHelper::addNew('variablegrouping.add');
		JToolbarHelper::divider();

		JToolbarHelper::deleteList('JGLOBAL_CONFIRM_DELETE', 'variablegrouping.delete', 'JTOOLBAR_DELETE');
		JToolbarHelper::divider();

		JToolbarHelper::cancel('variablegrouping.cancel');
	}


}
