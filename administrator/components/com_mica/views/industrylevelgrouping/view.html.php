<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for Industry Level Grouping.
 *
 * @since  1.6
 */
class MicaViewIndustryLevelGrouping extends JViewLegacy
{
	/**
	 * The item data.
	 *
	 * @var   object
	 * @since 1.6
	 */
	protected $items;

	/**
	 * The pagination object.
	 *
	 * @var   JPagination
	 * @since 1.6
	 */
	protected $pagination;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		$app              = JFactory::getApplication();
		$model            = $this->getModel();
		$this->items      = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		MicaHelper::addSubmenu('industrylevelgrouping');
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
	}

	/**
	 * A view method to add group.
	 *
	 * @param   [type]  $tpl  [description]
	 *
	 * @return  void
	 */
	public function add($tpl = null)
	{
		$app                = JFactory::getApplication();
		$this->model        = $this->getModel();
		$this->allfields    = $this->model->getAllFields();
		$id                 = $app->input->get->get('id', 0, 'int');
		$this->groupdetails = $this->model->getGroupDetail($id);
		MicaHelper::addSubmenu('industrylevelgrouping');
		$bar = JToolbar::getInstance('toolbar');

		if ($id > 0)
		{
			JToolbarHelper::title(JText::_('Industry Level Grouping : Update'), 'users user');
		}
		else
		{
			JToolbarHelper::title(JText::_('Industry Level Grouping : New'), 'users user');
		}

		JToolbarHelper::cancel('industrylevelgrouping.cancel_add', 'JTOOLBAR_CLOSE');
		JToolbarHelper::save('industrylevelgrouping.save');
		$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		// Get the toolbar object instance
		$bar = JToolbar::getInstance('toolbar');
		JToolbarHelper::title(JText::_('Industry Level Grouping'), 'users user');
		JToolbarHelper::addNew('industrylevelgrouping.add');
		JToolbarHelper::divider();
		JToolbarHelper::deleteList('JGLOBAL_CONFIRM_DELETE', 'industrylevelgrouping.delete', 'JTOOLBAR_DELETE');
		JToolbarHelper::divider();
		JToolbarHelper::cancel('industrylevelgrouping.cancel');
	}
}
