<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app    = JFactory::getApplication();
$plan   = $app->input->get->get('plan', 0, 'string');
$planid = $app->input->get->get('planid', 0, 'string');

$selectedtable = "";
foreach($this->selecteddata as $eachplan){
	$selectedtable[$eachplan->dbtable] = $eachplan->attribute;
} ?>

<script type="text/javascript">
	/*function submitbutton(event){
		if(event == "save"){
			jQuery("#myform").submit();//ajaxsave();
		}else if(event == "cancel"){
			window.location = "index.php?option=com_mica&view=users";
		}
	}

	function ajaxsave(){
		var tablearray = ["rail_state","india_information"];
		var counton    = [];

		for(var i=0;i<tablearray.length;i++){
			jQuery("input."+tablearray[i]+"_attributes:checkbox").each(function () {
				if(this.checked){
					counton.push(jQuery(this).val());
				}
			});

			if(counton.length > 0){
				save(counton,tablearray[i]);
				counton=[];
			}
		}
		alert("Saved");
	}

	function save(counton,myclass){
		var myplan = "<?php echo $planid ?>";
		if(myplan == ""){
			alert("Select Plan First");
		}

		jQuery.ajax({
			url  : "index.php?option=com_mica&view=users&task=users.saveAttr&planid=<?php echo $planid ?>&table="+myclass,
			type : 'POST',
			data : {
				attr : counton
			}
		}).done(function(data) {
			console.log("success");
		});
	}*/
</script>
<form  name="adminForm" id="adminForm" enctype="multipart/form-data" method="post" action ="<?php echo JRoute::_('index.php?option=com_mica&view=users'); ?>">
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif; ?>
			<?php $j = $k = 0;
			$checked    = false;
			$javascript = "";
			$myarray    = array();

			foreach($this->db_table as $eachtable){
				$eachtable_attributes = $this->allattributes[$eachtable];

				// india_information
				$values = array();
				if($eachtable == "State"){
					$label = "State Field";
					$j     = 1;
				}else if($eachtable == "District"){
					$label = "District Field";
					$j     = 2;
				}else if($eachtable == "Town"){
					$label = "Town Field";
					$j     = 3;
				}else if($eachtable == "Urban"){
					$label = "Urban Field";
					$j     = 4;
				} ?>
				<script type="text/javascript">
					function toggleChecked<?php echo $j;?>(status){
						jQuery(".checkbox<?php echo $j;?>").each( function() {
							jQuery(this).attr("checked",status);
						});
					}
				</script>
				<table style='float:left;width:24%;' class='table table-striped adminlist' >
					<thead>
						<tr class="nowrap left">
							<th colspan="2"><?php echo $label; ?><th>
						<tr>
						<tr class="nowrap left">
							<th>
								<a href='#' onClick='toggleChecked<?php echo $j;?>(true)'>Check All</a> / <a href='#'  onClick='toggleChecked<?php echo $j;?>(false)'>Uncheck All</a>
							</th>
							<th>Select Attribute</th>
						</tr>
					</thead>
					<?php
					for ($i = 0; $i < count($eachtable_attributes); $i++){
						$myarray[$k][$eachtable_attributes[$i]->table][$eachtable_attributes[$i]->group][] = $eachtable_attributes[$i]->field;
					}

					if (count($myarray) > 0 && array_key_exists($k, $myarray) && array_key_exists($eachtable, $myarray[$k])) {
						foreach($myarray[$k][$eachtable] as $key => $val){ ?>
							<tr>
								<td colspan="2"><strong><?php echo $key; ?></strong></td>
							</tr>
							<?php for($i = 0; $i < count($val); $i++){
								$checked = (@strstr($selectedtable[$eachtable], $val[$i])) ? " checked " : ""; ?>
								<tr>
									<td align="left" valign="top">
										<input type="checkbox" name="<?php echo $eachtable; ?>_attributes[]" <?php echo $checked; ?>
											class="<?php echo $eachtable; ?>_attributes checkbox<?php echo $j;?>" value="<?php echo $val[$i]; ?>" />
									</td>
									<td align="left" valign="top"><?php echo $val[$i]; ?></td>
								</tr>
							<?php }
						}
						$k++;
					} ?>
				</table>
			<?php } ?>
			<input type='hidden' name='task' value='' />
			<input type='hidden' name='tables' value='<?php echo implode(',',$this->db_table); ?>' />
			<input type='hidden' name='planid' value='<?php echo $planid ?>' />
			<?php echo JHtml::_('form.token'); ?>
		</div>
</form>
