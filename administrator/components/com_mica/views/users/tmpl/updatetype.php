<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app    = JFactory::getApplication();
$userid = $app->input->get->get("userid", 0, 'string');
$planid = $app->input->get->get('planid', 0, 'string');

$this->typesData = explode(',', $this->typesData);
$chk1 = (in_array('Rural',$this->typesData)) ? " checked " : "";
$chk2 = (in_array('Urban',$this->typesData)) ? " checked " : "";

?>
<script type="text/javascript">
	/*function submitbutton(event){
		if(event == "save"){
			jQuery("#adminForm").submit();//ajaxsave();
		}else if(event == "cancel"){
			window.location = "index.php?option=com_mica&view=users";
		}
	}*/
</script>
<form  name="adminForm" id="adminForm" enctype="multipart/form-data" method="post" action ="<?php echo JRoute::_('index.php?option=com_mica&view=users'); ?>">
	<?php if (!empty( $this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
	<?php else : ?>
		<div id="j-main-container">
	<?php endif; ?>
		<table style='float:left;width:24%;' class='table table-striped adminlist' >
			<thead>
				<tr class="nowrap left">
					<th colspan="2"><?php echo JText::_('Select Type'); ?><th>
				<tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<input type='checkbox' name='types[]' id='types[]' value='Rural' <?php echo $chk1; ?> />
						<span style='line-height: 20px;vertical-align: top;'>Rural</span>
					</td>
				</tr>
				<tr>
					<td>
						<input type='checkbox' name='types[]' id='types[]' value='Urban' <?php echo $chk2; ?> />
						<span style='line-height: 20px;vertical-align: top;'>Urban</span>
					</td>
				</tr>
			</tbody>
		</table>
		<input type='hidden' name='task' value='' />
		<input type='hidden' name='planid' value='<?php echo $planid ?>' />
		<?php echo JHtml::_('form.token'); ?>
		</div>
</form>