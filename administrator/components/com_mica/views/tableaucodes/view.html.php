<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for Industry Level Grouping.
 *
 * @since  1.6
 */
class MicaViewTableaucodes extends JViewLegacy
{
	/**
	 * The item data.
	 *
	 * @var   object
	 * @since 1.6
	 */
	protected $items;

	/**
	 * The pagination object.
	 *
	 * @var   JPagination
	 * @since 1.6
	 */
	protected $pagination;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	function display($tpl = null)
	{
		$app              = JFactory::getApplication();
		$model            = $this->getModel();
		$this->items      = $this->get('Items');

		$this->pagination = $this->get('Pagination');

		MicaHelper::addSubmenu('tableaucodes');
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();

		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		// Get the toolbar object instance
		$bar = JToolbar::getInstance('toolbar');
        JToolbarHelper::addNew('tableaucode.add');
        JToolbarHelper::editList('tableaucode.edit');
        JToolbarHelper::publish('articles.publish', 'JTOOLBAR_PUBLISH', true);
        JToolbarHelper::unpublish('articles.unpublish', 'JTOOLBAR_UNPUBLISH', true);
		JToolbarHelper::title(JText::_('Tableau codes'), 'tableaucodes codes');



		JToolbarHelper::divider();
		JToolbarHelper::deleteList('JGLOBAL_CONFIRM_DELETE', 'tableaucodes.delete', 'JTOOLBAR_DELETE');
		JToolbarHelper::divider();
		
	}
}
