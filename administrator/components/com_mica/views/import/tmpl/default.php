<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die; ?>

<style type="text/css">
	#facebook{
		margin-top:30px;
		float:left;
	}
	.facebook_block{
		background-color:#9FC0FF;
		border:2px solid #3B5998;
		float:left;
		height:30px;
		margin-left:5px;
		width:8px;
	    opacity:0.1;
		-webkit-transform:scale(0.7);
		-webkit-animation-name: facebook;
	 	-webkit-animation-duration: 1s;
	 	-webkit-animation-iteration-count: infinite;
	 	-webkit-animation-direction: linear;
	}
	#block_1{
	 	-webkit-animation-delay: .1s;
	}
	#block_2{
	 	-webkit-animation-delay: .2s;
	}
	#block_3{
	 	-webkit-animation-delay: .3s;
	}
	#block_4{
	 	-webkit-animation-delay: .4s;
	}
	#block_5{
	 	-webkit-animation-delay: .5s;
	}
	#block_6{
	 	-webkit-animation-delay: .6s;
	}
	#loadingDiv{
	  position:fixed;
	  top:0px;
	  right:0px;
	  width:100%;
	  height:100%;
	  background-color:#666;
	  background-image:url('components/com_mica/images/2.gif');
	  background-repeat:no-repeat;
	  background-position:center;
	  z-index:10000000;
	  opacity: 0.4;
	  filter: alpha(opacity=40); /* For IE8 and earlier */
	  display: none;
	}

	.float-container {
    
	}

	.float-child {
	    width: 45%;
	    float: left;
	    padding: 20px;
	    border: 2px solid red;
	}  


	@keyframes spin {
	  0% { transform: rotate(0deg); }
	  100% { transform: rotate(360deg); }
	}
	@-webkit-keyframes facebook{
		0%{-webkit-transform: scale(1.2);opacity:1;}
		100%{-webkit-transform: scale(0.7);opacity:0.1;}
	}
</style>

<?php $app = JFactory::getApplication();

if($app->input->get('ajaxprocess', 0, 'int') == 0 ) { ?>

	<form  name="adminForm" id="adminForm" enctype="multipart/form-data" method="post" action ="<?php echo JRoute::_('index.php?option=com_mica&view=import'); ?>">
		<?php if (!empty( $this->sidebar)) : ?>
			<div id="j-sidebar-container" class="span2">
				<?php echo $this->sidebar; ?>
			</div>
			<div id="j-main-container" class="span10">
		<?php else : ?>
			<div id="j-main-container">
		<?php endif; ?>
				<table class="table table-striped" id="userList">
					<tr class="row0">
						<td>
							<input type="file" 	 name="toupload" 	value="" />
							<input type="hidden" name="option" 		value="com_mica" />
							<input type="hidden" name="task" 		value="import.upload" />
							<input type="submit" name="Submit" id="submit" />
							<?php echo JHtml::_('form.token'); ?>
						</td>
					</tr>
				</table>
			</div>
	</form>

<?php } else {?>

	<!-- <fieldset></fieldset>orm name="adminForm" id="adminForm" method="post"> -->
	<fieldset></fieldset>
		<?php if (!empty( $this->sidebar)) : ?>
			<div id="j-sidebar-container" class="span2">
				<?php echo $this->sidebar; ?>
			</div>
			<div id="j-main-container" class="span10">
		<?php else : ?>
			<div id="j-main-container">
		<?php endif; ?>
				<table class="table table-striped" id="userList">
					<tr class="row0">
						<td>
							<div id="preloader" style="display:none;">
								<div id='facebook' >
									<div id='block_1' class='facebook_block'></div>
									<div id='block_2' class='facebook_block'></div>
									<div id='block_3' class='facebook_block'></div>
									<div id='block_4' class='facebook_block'></div>
									<div id='block_5' class='facebook_block'></div>
									<div id='block_6' class='facebook_block'></div>
								</div>
							</div>
							<div id="loadingDiv">
							    <div class="loader"></div>
							</div>
							<div id="op"></div>
							<div id="SheetData"></div>
							<div id="RowLevelHtml"></div>
							<div id="RowLevelAfterSelectHtml"></div>
						</td>
					</tr>
				</table>
			</div>
	</form>

	<script type="text/javascript">
		var formate_array = new Array();
		var formate_array1 = new Array();
		var tree = new Array();
		
		jQuery(document).ready(function(){
			
			jQuery("#op").html(jQuery("#preloader").html());
			jQuery.ajax({
				url  : 'index.php?option=com_mica&ajaxprocess=1&filename=<?php echo $app->input->get("filename");?>&task=import.selectSheet&view=import',
				type : 'GET'
			}).done(function(dataResp) {
				
				jQuery("#op").html();
				jQuery("#op").html(dataResp);
			});
		});
function rec(ele2,treearray){
	console.log(treearray,"-----1");
	children2 = jQuery('.social_economic_select[data-parent="'+ele2+'"]');

	//debugger;
	if(typeof children2 !=="undefined" && children2.length>0){
	//console.log(ele2,children2,"---2 val");	
		jQuery(children2.val()).each(function(index3, ele3) {		
				
				//var x =  [];
				console.log(ele3,treearray,"---3");			
				 rec(ele3,treearray[ele2]);
				//return x;					
		});
	} else 
	{

		var x =[];
		x[ele2]='1';
		return x;
	}

}

		jQuery(document).on('click', '#start', function(event) {
			event.preventDefault();
			if(jQuery("#rows_level option:selected").val() && jQuery("#coloumn_level option:selected").val()){
				jQuery.ajax({
					url  : "index.php?option=com_mica&ajaxprocess=1&filename=<?php echo $app->input->get('filename');?>&task=import.RowLevel&view=import&operation="+jQuery("#adminForm input[name='operation']:checked").val()+"&sheet="+jQuery("#adminForm #sheet").val()+"&import_type="+jQuery("input[name='import_type']:checked").val()+"&rows_level="+jQuery("#rows_level option:selected").val(),
					type : 'GET'
				}).done(function(dataResp) {
					jQuery("#RowLevelHtml").html();
					jQuery("#RowLevelHtml").html(dataResp);
				});
			} else {
				if(jQuery("#rows_level option:selected").val()){
					alert('Please Select Coloumn Level');
				}else if(jQuery("#coloumn_level option:selected").val()){
					alert('Please Select Row Level');
				} else {
					alert('Please Select Coloumn Level and Row Level');
				}
			}
			
		});
		jQuery(document).on('click', '#step2', function(event) {
			event.preventDefault();
			jQuery.ajax({
				url  : "index.php?option=com_mica&ajaxprocess=1&filename=<?php echo $app->input->get('filename');?>&task=import.insertToDatabasePreview&view=import&operation="+jQuery("#adminForm input[name='operation']:checked").val()+"&sheet="+jQuery("#adminForm #sheet").val()+"&import_type="+jQuery("input[name='import_type']:checked").val(),
				type : 'GET'
			}).done(function(dataResp) {
				jQuery("#SheetData").html();
				jQuery("#SheetData").html(dataResp);
			});
		});

		jQuery(document).on('change','.social_economic_select',function () {
			var formate_array = new Array();
			var formate_array1 = new Array();
			jQuery('.social_economic_select').each(function(index, el) {
				parent = jQuery(this).data('parent');
				jQuery(this).find("option:selected").each(function(index, el) {
				var value = jQuery(this).val();
				var level = jQuery(this).data('level');
					if(jQuery.inArray(value, formate_array1) == -1){
						if(parent){
							formate_array.push({'depth':level,'id':value,'parent_id' : parent});
							formate_array1.push(value);	
						} else {
							formate_array.push({'depth':level,'id':value});
							formate_array1.push(value);
						}
					}

				});				
			});
			console.log(formate_array,'ujash123');
			data = formate_array;
			tree = function (data, root) {
		        var t = {};
		        data.forEach(o => {
		            Object.assign(t[o.id] = t[o.id] || {}, o);
		            t[o.parent_id] = t[o.parent_id] || {};
		            t[o.parent_id].children = t[o.parent_id].children || [];
		            t[o.parent_id].children.push(t[o.id]);
		        });
		        return t[root].children;
		    }(data, undefined);

		console.log(tree,'treee');
		});

		jQuery(document).on('change', '.social_economic_select', function(e) {
			var treearray = new Array();
			var root_level_selected = jQuery('#root_level').val();
			jQuery(root_level_selected).each(function(index, ele) {
			//var selected = el.split(',');
				
					if(!treearray.hasOwnProperty(ele)){
						
						treearray[ele] = [];//new Object();
						children = jQuery('.social_economic_select[data-parent="'+ele+'"]');
						if(typeof children !=="undefined"){
								jQuery(children.val()).each(function(index2, ele2) {		
									//treearray[ele].push(rec(ele2,treearray[ele]));
									//{x:{},y:{},z:{}}
									//console.log(ele2,treearray[ele],"---2");
								});
							
						}
					}					
								
			});

			/*if(jQuery(this).data('parent')) {
				jQuery('.social_economic_select').each(function(index, el) {
						parent = jQuery(this).data('parent');
						if(parent){
							var selected_val = jQuery(this).val();
							jQuery(selected_val).each(function(index, el) {
								var selected = el.split(',');
								jQuery(selected).each(function(index, ele) {
									treearray[parent][ele] = new Object();
										
										console.log(treearray,'ujash');
										

							
								});
							});
						}
						
					
				});
			}*/
			/*jQuery('.social_economic_select').each(function(index, el) {
				if(jQuery(el).data('parent') && jQuery(el).data('parent') == parent){
					console.log(jQuery(el).data('parent'),parent);
				
				} else {				
					jQuery(selected_val).each(function(index, el) {
						var selected = el.split(',');
						jQuery(selected).each(function(index, ele) {
							if(!treearray.hasOwnProperty(ele)){
								treearray[ele] = new Object();
							}						
						});
						
					});
				}
			});*/
			//take root element
			//take blank array
			//for loop of root elemnet (r)
			
				//check children of root is found "new table"
				//if yes then for loop of children
					//check children of above
					//if yes then for loop of above
					//else
					//	return {}	
			//endfor loop
			//push in blank array
			//
			
			console.log(treearray);
			/*jQuery.ajax({
				url  : "index.php?option=com_mica&ajaxprocess=1&filename=<?php echo $app->input->get('filename');?>&task=import.treestructure&view=import&parent="+parent+"&selected_value="+jQuery(this).val()+"&oldtree="+JSON.stringify(treearray),
				type : 'GET'
			}).success(function(dataResp) {
				treearray = dataResp;
				console.log(dataResp);
			});*/
			/*if(jQuery(this).data('parent')){
				var parent = jQuery(this).data('parent');
				formate_array.filter(function(index) {
							console.log(index,'ujash');
						});
				if(parent in formate_array){
					jQuery(jQuery(e.target).val()).each(function(index, el) {				
						
						
						
						if(jQuery.inArray(el, formate_array[parent]) == -1){
							formate_array[parent][el]={};
						}
					
					});
				}
				
				console.log(formate_array);
				
			} else {
				jQuery(jQuery(e.target).val()).each(function(index, el) {					
					if(jQuery.inArray(el, formate_array) == -1){
						formate_array[el] = {};
					}
					
				});
				
			}*/
		});
		
		jQuery(document).on('change', '#rows_level', function(event) {
			
		});
		jQuery(document).on('click', '#finish', function(event) {
			jQuery('#loadingDiv').show();
			var identifiers_variable = []
			jQuery('.identifiers_variable:checked').each(function(index, el) {
				
				var name = jQuery(this).attr('name').replace('__','');
				var value = jQuery(this).val();
					
				identifiers_variable.push({'name':name,'value':value}); 
				
			});
			console.log(identifiers_variable)
			event.preventDefault();
			jQuery.ajax({
				url  : "index.php?option=com_mica&ajaxprocess=1",
				type : 'POST',
				data: { 
			        'filename':'<?php echo $app->input->get('filename');?>',
			        'task':'import.finalTreeInsert',
			        'view':'import',
			        'sheet':jQuery("#adminForm #sheet").val(),
			        'rows_level':jQuery("#rows_level option:selected").val(),
			        'column_level':jQuery("#coloumn_level option:selected").val(),
			        'tree':JSON.stringify(tree),
			        'identifiers_variable':JSON.stringify(identifiers_variable)
			    },
			}).done(function(dataResp) {
				jQuery('#loadingDiv').hide();
				//jQuery("#RowLevelAfterSelectHtml").html();
				//jQuery("#RowLevelAfterSelectHtml").append(dataResp);
			});
			
		});
		jQuery(document).on('click', '#next', function(event) {
			event.preventDefault();
			if(!jQuery("#root_level").val()){
				alert('Please Select Root Level Coloumn');
				return ;
			} 
			var level = jQuery(this).data('level');
			selText = [];
			rows_level_selected = [];
			jQuery(".child_level_select option:selected").each(function () {
			   var $this = jQuery(this);
			   if ($this.length) {
			    selText.push($this.val());
			   }
			});
			jQuery("#root_level option:selected").each(function(index, el) {
				rows_level_selected.push(jQuery(this).data('int')+'_'+jQuery(this).val());
			});
			console.log(rows_level_selected);
			jQuery.ajax({
				url  : "index.php?option=com_mica&ajaxprocess=1&filename=<?php echo $app->input->get('filename');?>&task=import.RowLevel&view=import&operation="+jQuery("#adminForm input[name='operation']:checked").val()+"&sheet="+jQuery("#adminForm #sheet").val()+"&import_type="+jQuery("input[name='import_type']:checked").val()+"&rows_level="+jQuery("#rows_level option:selected").val()+"&rows_level_selected="+jQuery("#root_level").val()+"&child_level_selected="+selText.toString()+"&level="+level+"&name=root",
				type : 'GET'
			}).done(function(dataResp) {
				//jQuery("#RowLevelAfterSelectHtml").html();
				jQuery("#RowLevelAfterSelectHtml").append(dataResp);
			});
			
		});jQuery(document).on('click', '#nextchild', function(event) {
			event.preventDefault();
			if(!jQuery("#root_level").val()){
				alert('Please Select Root Level Coloumn');
				return ;
			} 
			var level = jQuery(this).data('level');
			selText = [];
			jQuery(".child_level_select option:selected").each(function () {
			    if(jQuery(this).data('level') == level){
			    	var $this = jQuery(this);
			   		if ($this.length) {
			    		selText.push($this.val());	
			    }
			   
			   }
			});
			    
			jQuery.ajax({
				url  : "index.php?option=com_mica&ajaxprocess=1&filename=<?php echo $app->input->get('filename');?>&task=import.RowLevel&view=import&operation="+jQuery("#adminForm input[name='operation']:checked").val()+"&sheet="+jQuery("#adminForm #sheet").val()+"&import_type="+jQuery("input[name='import_type']:checked").val()+"&rows_level="+jQuery("#rows_level option:selected").val()+"&child_level_selected="+selText.toString()+"&level="+jQuery(this).data('level')+"&name="+jQuery(this).data('level'),
				type : 'GET'
			}).done(function(dataResp) {
				//jQuery("#RowLevelAfterSelectHtml").html();
				jQuery("#RowLevelAfterSelectHtml").append(dataResp);
			});
			
		});
		jQuery(document).on('change', '.child', function(event) {
			event.preventDefault();
			if(jQuery(this).val() == 'yes'){
				jQuery(this).closest('tr').next('tr').show();
			} else {
				jQuery(this).closest('tr').next('tr').hide();
			}
			
		});
		jQuery(document).on('change', '.child_select', function(event) {
			event.preventDefault();
			var finalarray = Array();
			var selected_child = jQuery(this).val(); 
			var selected_parent = jQuery(this).data('parent'); 
			jQuery.each(selected_child,function(index, el) {
				jQuery(".child_select").each(function() {
				    if(jQuery(this).data('parent') == selected_parent){
				    	finalarray[jQuery(this).data('parent')] = el;

				    }
				    if(jQuery(this).data('parent') != selected_parent ){
				    	jQuery(this).find('option[value="'+el+'"]').remove();
				    	/*console.log(jQuery(this).find('option[value="'+el+'"]').remove());*/
				    	
				    }
				    
				})
			});
			console.log(finalarray);
			
		});

		jQuery(document).on('click', '#step3', function(event) {
			event.preventDefault();

			jQuery("#preview").html(jQuery("#preloader").html());
			jQuery.ajax({
				url  : "index.php?option=com_mica&ajaxprocess=1&filename=<?php echo $app->input->get('filename');?>&task=import.insertToDatabase&view=import&operation="+jQuery("#adminForm input[name='operation']:checked").val()+"&sheet="+jQuery("#adminForm #sheet").val()+"&table="+jQuery("#adminForm input[name='table']:checked").val()+"",
				type : 'GET'
			}).done(function(dataResp) {
				jQuery("#SheetData").html();
				jQuery("#SheetData").html(dataResp);
			});
		});
		
		function findkey(parent,value,obj) {
			console.log(parent,value);

			if(!jQuery.isEmptyObject(obj[parent]))
			{
				var allkeys = Object.keys(obj[parent]);
				jQuery(allkey).each(function(i,e){
					var x = findkey(e,obj[parent][e],obj[parent]);
					console.log(x);
					return x;

				});

			} else {
				
				var x = jQuery.extend(true, {}, obj);
				xt[value] = {};
			  	return x;
			}clo
			
			
			/*for (const [key, val] of Object.entries(obj)) {
			  	if(key == parent ){
			  		obj[parent][value] = {};
			  		return true;
			  	} else {
			  		if(!jQuery.isEmptyObject(val)){
			  			console.log(parent,val,'ujash')
						findkey(parent,value,val)
			  		}
			  	}
			  	console.log(`${key}: ${value}`);
			}
			/*console.log(value,obj);
			if(obj.hasOwnProperty(parent)){
				if (typeof obj[parent] === "object"){
					obj[parent][value] = {};
				}
			} else {
				for (const [key, value] of Object.entries(obj)) {
				  
				  console.log(`${key}: ${value}`);
				}
				//findkey(parent,value,obj[parent])
			}*/			
			//return obj;
		}
	</script>

<?php
}
