<?php
defined('_JEXEC') or die('Restricted access');
JToolBarHelper::title(JText::_('Add user Frontend'), 'generic.png');
JToolBarHelper::preferences('com_adduserfrontend');
?>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&appId=129359547156559&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<!-- Default administrator message -->
<div style="float:left; width:100%; padding-bottom:25px;">
<div style="background-color:#FC0; float:right; padding:2px; clear:both;"><strong>Click on the &quot;Options&quot; button to edit your settings!</strong></div></div>
<div style="float:right; width:510px; padding-left:10px;"><strong>Like our facebookpage:</strong> We will keep you informed regarding updates and issues!<br />
<div class="fb-like-box" data-href="https://www.facebook.com/joomlacy" data-width="500" data-height="800" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="true" data-show-border="true"></div></div>
<a href="http://www.joomlacy.com/" target="_blank"><img src="http://www.joomlacy.com/templates/ghanja/images/logos/joomlacy-logo.png" style="padding-top:1px;"border="0" /></a>
<h1>Add User Frontend - Free version</h1>
<p>Thank you for using the <strong>Add User Frontend</strong> component from <a href="http://www.joomlacy.com/" target="_blank">Joomlacy.com</a>. On this page you will find instructions on how to use it. First of all read the instructions on this page. You can  find more documentation on <a href="http://www.joomlacy.com/documentation/add-user-frontend" target="_blank">http://www.joomlacy.com/documentation/add-user-frontend</a>.</p>
<p><strong>I don't offer free support.</strong> If you need support you will have to purchase <a href="http://www.joomlacy.com/gopro" target="_blank">Add User Frontend Pro</a>! If you need customization, please mail me at <a href="mailto:info@joomlacy.com">info@joomlacy.com</a>, and tell me what you need. I will make you an offer you can't refuse ;)</p>
<p><strong><span style="color:#C00;">Dont forget to like our facebookpage:</span></strong>&nbsp;We will keep you informed regarding updates and issues! <strong>&#8599;</strong></p>
<p>&nbsp;</p>
<h2>About Add User Frontend</h2>
<p>Add User Frontend has been around since 2009. It has become very popular and thats the reason I keep updating it even if I have almost no time to  do so. I spend most of my time working on webdesign and SEO projects. I have a webdesign and SEO company called <a href="http://www.nethit.nl/en/" target="_blank">Nethit.nl</a> located in Haarlem (not far from Amsterdam, The Netherlands). But I am also very active in Belgium because I was born there. I am Belgian. But I married a beautiful Dutch girl  and moved to the Netherlands ;)</p>
<p>I specialize in SEO, Usability and Web Psychology. And Unlike many SEO experts I also have webdevelopment and webdesign skills. If you ever need a reliable SEO expert just contact me and I will be glad to help you. </p>
<p>You can contact me at <a href="mailto:info@joomlacy.com">info@joomlacy.com</a>. 
  
  I only work on projects in English, French, or Dutch. You can also mail me in those languages.</p>
<p>&nbsp;</p>
<h2>Configuring Add User Frontend</h2>
<p>If you just installed this component you need to set it up properly. </p>
<p><strong>1. </strong>Make a link to the component in one of your menu's</p>
<p><strong>2.</strong> Now you need to configure the component. You can set the global configuration here by clicking on the &quot;Options Button&quot;.</p>
<p><em>You can also configure the component at the menu-item in the menumanager. This will give you the possibility to run multiple instances of the component with different settings. <br />
  This can come in handy if you want to add users to multiple usergroups. (Create a menu called 'Add user' and add multiple links to Add User Frontend such as 'Add registered', 'Add author' or 'Add editor'. Off course you should uniquely configure each menu item to Add User Frontend differently for this to work.)</em></p>
<p><em><strong>How can I define these settings?<br />
  </strong>When making the link in the menu manager you will see a tab called &quot;Add user frontend&quot;.</em><br />
  <em>In this tab you will see the component settings for that menu-item only. These settings override the global component settings. You can choose a different usergroup for each menu link to Add User Frontend. </em></p>
<h2>How to configure the component?</h2>
<p>Lets have a look at the options.</p>
<p><strong><s>1. Mode<br />
</s></strong><s>Add user to Joomla or to Joomla and Community Builder. (You should off course only use that last option when you have that component installed.)</s></p>
<p><strong>2. Namemode<br />
</strong>Do you want to have  one inputfield for the name on your form or 2? If you want to use 2 fields then the component will  ask for &nbsp;first name and last name.</p>
<p><strong><s>3. Username<br />
</s></strong><s>How should usernames be created? Should they be autogenerated, selfchosen or would you like to use the emailaddress as username.</s></p>
<p><strong>4. Check Username<br />
</strong>When trying to add a user the system will check for double usernames. If the chosen username already  exists the username can be automatically renamed by adding a number to the username or the user can be prompted to choose  another username via a system message.</p>
<p><strong>5. Check email<br />
</strong>Do you want to check for existing email addresses? You can also choose to ignore  double emailaddresses.</p>
<p><strong><s>6. Choose password<br />
</s></strong><s>Do you want the password to be autogenerated or chosen manually?</s></p>
<p><s><strong>7. Choose email address<br />
</strong>This is rarely needed. But You can also choose to NOT display the email input field and autogenerate (fake) email adress when adding a user. </s></p>
<p><s><strong>8. Email notification<br />
</strong>Do you want to send a notification email to the user You just added that contains his login data?</s></p>
<p><s><strong>9. Email notification subject</strong><br />
  Customize the subject of the user notification email. Use [NAME] or [URL] or [USERNAME] or [PASSWORD]. These will be replaced by the userdata. <strong>(Leave this field empty to use a standard subject)</strong></s></p>
<p><s><strong>10. Email notification content</strong><br />
  Customize the content of the user notification email. Use [NAME] or [URL] or [USERNAME] or [PASSWORD]. These will be replaced by the userdata. <strong>(Leave this field empty to use standard content)</strong></s></p>
<p><s><strong>11. Admin email notification<br />
</strong>Do you want to sent a notification email to the Administrator of the website when a user is added?<strong> (Will be send to the 'sent from' email in the global configuration of Joomla)</strong></s></p>
<p><s><strong>12. Send admin notification to</strong><br />
  If you have the admin email notification turned on. You should provide a mail address here. The notification will be sent there.</s></p>
<p><s><strong>13. Usertype mode<br />
</strong>Where do you want to choose the usertype for the user you are adding? In the frontend on the form? Or in the backend?<strong> (If you choose FRONTEND you can NOT use fields 15 and 16)</strong></s></p>
<p><s><strong>14. Hide standard usergroups<br />
</strong> This option hides all default usergroups (registered, author, ...) from  the user type selectbox on the add user frontend form.<strong> (This should only be used when 'Usertype mode'(13) is set to 'Frontend'.)</strong></s></p>
<p><s><strong>15. Usertype<br />
</strong>Which type of user should be added trough the add user frontend form? <strong>(Should only be used when 'Usertype mode'(13) is set to 'Backend'.)</strong></s></p>
<p><s><strong>16. Custom Groups<br />
  </strong> Enter the ID of the usergroup to which you want to add the new user.<strong> (Should only be used when 'Usertype mode'(13) is set to 'Backend' and  'Usertype'(15) is set to 'Custom Group'.)</strong><br />
  <strong>BE CAREFULL WHEN YOU CHOOSE USERTYPES WITH A LOT OF PERMISSIONS! </strong> </s></p>
<h2><s>If you choose the usergroup from the frontend:</s></h2>
<p><s>If you set up Add User Frontend to be able choose a usergroup from the frontend you cannot control  exactly which usergroups will be displayed in the selectbox of usergroups on the  form. The selectbox will simply display all usergroups available that have less rights then the users who is about to add a new user. (Authors can add registered members, editors can add authors and so on...) </s></p>
<p><s>If you dont want this you can make  a menu called 'Add user' and add multiple links to Add User Frontend like 'Add registered', 'Add author' or 'Add editor'. Off course You should uniquely configure each menulink to Add User Frontend differently for this to work as described above. </s></p>
<p><span style="color:#C00"><strong>Striked through = Not available in free version</strong></span> - Get the PRO version at <a href="http://www.joomlacy.com/gopro/" target="_blank">http://www.joomlacy.com/gopro/</a>
<hr />
<strong>Thank you for using Add User Frontend.</strong> You can find us at <a href="http://www.joomlacy.com/" target="_blank">Joomlacy.com</a>. 
<p></p>