<?php
$servername = "localhost";
$database = "micamimi_micamimi18";
$username = "micamimi_mica18";
$password = "I&9%ZB}X^W~F";

// Create connection

$conn = mysqli_connect($servername, $username, $password, $database);

// Check connection

if (!$conn) {
      die("Connection failed: " . mysqli_connect_error());
}

$subDistrict = array("03011","03017","03010","03021","02995","03008","03014","03007","03015","03013","03012","03003","03001","03018","03016","03004","03009","03000","03020","03002","03005","03006","03019","03142","03150","03147","03149","03145","03141","03144","03148","03151","03152","03146","03143","02900","02897","02893","02901","02895","02904","02891","02899","02890","02892","02906","02903","02905","02889","02902","02896","02898","02908","02894","02907","02768","02769","02763","02765","02766","02767","02764","02762","02758","02759","02756","02760","02757","02755","02761","03130","03129","03132","03127","03128","03131","02910","02920","02911","02915","02914","02912","02919","02913","02921","02918","02916","02917","02950","02946","02947","02948","02951","02964","02954","02962","02965","02952","02961","02963","02945","02953","02960","02956","02944","02959","02967","02958","02966","02957","02955","02949","02801","02800","02802","02803","02996","02990","02995","02999","02991","02997","02988","02989","02994","02992","02987","02998","02993","03100","03108","03106","03104","03101","03105","03102","03109","03107","03103","03078","03077","03074","03093","03072","03087","03085","03079","03096","03075","03086","03095","03094","03090","03073","03098","03080","03083","03082","03097","03091","03081","03089","03109","03084","03092","03076","03071","02937","02943","02941","02938","02940","02936","02942","02935","02934","02939","02982","02983","02985","02984","02979","02977","02974","02978","02973","02980","02971","02972","02975","02981","02986","02976","02969","02970","02777","02773","02772","02775","02776","02779","02778","02771","02774","02770","03163","03168","03167","03170","03166","03165","03159","03169","03162","03160","03161","03164","03171","03119","03122","03124","03116","03125","03114","03110","03113","03123","03120","03118","03111","03112","03126","03117","03115","03121","02925","02930","02924","02928","02933","02932","02929","02931","02926","02922","02923","02854","02838","02834","02841","02835","02839","02852","02853","02850","02851","02840","02837","02847","02844","02845","02856","02846","02849","02843","02836","02857","02855","02848","02842","03045","03042","03043","03053","03054","03035","03034","03038","03050","03049","03047","03036","03037","03044","03041","03039","03051","03048","03052","03046","03205","03201","03211","03200","03217","03202","03209","03206","03207","03208","03219","03199","03210","03203","03212","03214","03204","03213","03218","03215","03216","03223","03225","03227","03229","03220","03221","03230","03222","03226","03224","03228","02866","02859","02880","02875","02872","02873","02882","02881","02864","02876","02869","02862","02860","02867","02863","02885","02870","02883","02874","02887","02878","02865","02861","02868","02879","02886","02877","02871","02858","02884","03190","03192","03191","03198","03194","03195","03188","03196","03193","03187","03197","03189","03024","03023","03027","03022","03029","03026","03028","03030","03025","03031","03033","03032","03157","03153","03156","03155","03154","03158","03060","03067","03065","03056","03068","03062","03059","03061","03069","03058","03057","03063","03066","03064","03172","03178","03175","03174","03180","03184","03183","03176","03177","03173","03185","03186","03181","03182","03179","02790","02788","02796","02795","02789","02780","02787","02792","02794","02783","02793","02782","02781","02798","02797","02785","02791","02786","02784","03135","03137","03133","03134","03139","03138","03140","03136","02830","02812","02806","02816","02818","02819","02820","02824","02807","02828","02817","02804","02825","02810","02826","02813","02827","02823","02805","02831","02821","02815","02814","02809","02808","02811","02822","02829");

$subDistrict = array_filter($subDistrict);
$subDistrict = implode("','",$subDistrict);

$attributes = Array
(
'Area_Irrigated_by_Source_in_Hectares',
'Area_under_NonAgricultural_Uses_in_Hectares',
'Barren_Uncultivable_Land_Area_in_Hectares',
'Culturable_Waste_Land_Area_in_Hectares',
'Current_Fallows_Area_in_Hectares',
'Fallows_Land_other_than_Current_Fallows_Area_in_Hectares',
'Forest_Area_in_Hectares',
'Land_Under_Miscellaneous_Tree_Crops_etc_Area_in_Hectares',
'Net_Area_Sown_in_Hectares',
'Permanent_Pastures_and_Other_Grazing_Land_Area_in_Hectares',
'Total_Unirrigated_Land_Area_in_Hectares',
'ATM',
'Commercial_Bank',
'Cooperative_Bank',
'Distance_to_Nearest_Commercial_Bank_Available',
'Distance_to_Nearest_Cooperative_Bank_Available',
'Nearest_ATM_Facility_Distance_in_Km',
'TapWaterTreated_Functioning_in_Summer_months_AprSep',
'TapWater_Untreated_Functioning_in_Summer_monthsAprSep',
'Tap_WaterTreated',
'Tap_WaterTreated_Functioning_All_round_the_year',
'Tap_Water_Untreated',
'Tap_Water_Untreated_Functioning_All_round_the_year',
'Rural_Score_Basic_Amenities',
'Rural_Score_Commercial_Usage_of_Premises',
'Rural_Score_Financial_Services',
'Rural_Score_for_Agriculture',
'Rural_Score_Media_Ownership',
'Total_Female_Population_of_Village',
'Total_Male_Population_of_Village',
'Total_Population_of_Village',
'Total_Scheduled_Castes_Female_Population_of_Village',
'Total_Scheduled_Castes_Male_Population_of_Village',
'Total_Scheduled_Castes_Population_of_Village',
'Total_Scheduled_Tribes_Female_Population_of_Village',
'Total_Scheduled_Tribes_Male_Population_of_Village',
'Total_Scheduled_Tribes_Population_of_Village',
'Total__Households',
'Distance_toNearestOther_types_of_EducationInstitute_Available',
'Distance_to_NearestPrimary_SchoolAvailable',
'Distance_to_Nearest_Arts_and_Science_DegreeCollegeAvailable',
'Distance_to_Nearest_Engineering_College_Available',
'Distance_to_Nearest_Management_Institute_Available',
'Distance_to_Nearest_Medicine_College_Available',
'Distance_to_Nearest_Middle_School_Available',
'Distance_to_Nearest_Non_Formal_Training_Centre_Available',
'Distance_to_Nearest_Polytechnic_Available',
'Distance_to_Nearest_Pre_Primary_School',
'Distance_to_Nearest_School_For_Disabled_Available',
'Distance_to_Nearest_Secondary_School_Available',
'Distance_to_Nearest_Senor_Secondary_School_Available',
'Distance_to_Nearest_Vocational_Training_SchoolITI_Available',
'Government_Non_Formal_Training_Centre',
'Government_Other_types_of_EducationInstitute',
'Government_Other_types_of_EducationInstituteNO',
'Government_School_For_Disabled',
'Government_School_For_DisabledNO',
'Govt_Arts_and_Science_Degree_College',
'Govt_Arts_and_Science_Degree_CollegeNO',
'Govt_Engineering_College',
'Govt_Engineering_CollegeNO',
'Govt_Management_Institute',
'Govt_Management_InstituteNO',
'Govt_Medicine_College',
'Govt_Medicine_CollegeNO',
'Govt_Middle_School',
'Govt_Middle_SchoolNO',
'Govt_Polytechnic',
'Govt_PolytechnicNO',
'Govt_PrePrimary_School_NurseryLKGUKG',
'Govt_Pre_Primary_School_NurseryLKGUKGNO',
'Govt_Primary_School',
'Govt_Primary_SchoolNO',
'Govt_Secondary_School',
'Govt_Senior_Secondary_School',
'Govt_Senior_Secondary_SchoolNO',
'Govt_Vocational_Training_SchoolITI',
'Govt_Vocational_Training_SchoolITINO',
'Nearest_Arts_and_Science_Degree_College',
'Nearest_Engineering_College',
'Nearest_Engineering_College_Available',
'Nearest_Management_Institute',
'Nearest_Medicine_College',
'Nearest_Middle_School',
'Nearest_Middle_School_Available',
'Nearest_Non_Formal_Training_Centre',
'Nearest_Other_types_of_EduInstitute',
'Nearest_Polytechnic',
'Nearest_Primary_School',
'Nearest_Primary_SchoolAvailable',
'Nearest_School_For_Disabled',
'Nearest_Secondary_School',
'Nearest_Senior_Secondary_School',
'Nearest_Vocational_Training_SchoolITI',
'PrePrimary_School_NurseryLKGUKG_Nearest_Facility',
'Private_Arts_and_Science_Degree_College',
'Private_Arts_and_Science_Degree_CollegeNO',
'Private_Engineering_College',
'Private_Engineering_CollegeNO',
'Private_Management_Institute',
'Private_Medicine_College',
'Private_Medicine_CollegeNO',
'Private_Middle_SchoolNO',
'Private_Non_Formal_Training_Centre',
'Private_Non_Formal_Training_CentreNO',
'Private_Other_types_of_EduInstitute',
'Private_Other_types_of_EduInstituteNO',
'Private_Polytechnic',
'Private_PolytechnicNO',
'Private_Pre_Primary_School_NurseryLKGUKG',
'Private_Pre_Primary_School_NurseryLKGUKGNO',
'Private_Primary_School',
'Private_Primary_SchoolNO',
'Private_School_For_DisabledNO',
'Private_School_For_Disabled__A1NA2',
'Private_Secondary_School',
'Private_Secondary_SchoolNO',
'Private_Senior_Secondary_School',
'Private_Senior_Secondary_SchoolNO',
'Private_Vocational_Training_SchoolITI',
'Private_Vocational_Training_SchoolITINO',
'ASHA',
'Community_Health_CentreNO',
'Community_Health_Centre_Doctors_In_PositionNO',
'Community_Health_Centre_Doctors_Total_StrengthNO',
'Community_Health_Centre_ParaMedical_Staff_Total_StrengthNO',
'Community_Health_Centre_Para_Medical_Staff_In_PositionNO',
'DispensaryNO',
'Dispensary_Doctors_In_PositionNO',
'Dispensary_Doctors_Total_StrengthNO',
'Dispensary_Para_Medical_Staff_In_PositionNO',
'Dispensary_Para_Medical_Staff_Total_StrengthNO',
'DistancetoNearestNutritional_CentresAnganwadiCentre_Available',
'Distance_to_Maternity_And_Child_Welfare_Centre',
'Distance_to_NearestPrimary_Health_CentreAvailable',
'Distance_to_Nearest_ASHA_Available',
'Distance_to_Nearest_Community_Health_Centre_Available',
'Distance_to_Nearest_Dispensary_Available',
'Distance_to_Nearest_Nutritional_CentresOthers_Available',
'Distance_to_Nearest_Place_Family_Welfare_Centre_Available',
'Distance_to_Nearest_Place_Hospital_Allopathic_Available',
'Distance_to_Nearest_Primary_Health_Sub_Centre_Available',
'Distance_to_Nearest_TB_Clinic_Available',
'Distance_to_Nearest_whereNutritional_CentresICDS_Available',
'DitoNePlHoAlMeAv',
'Family_Welfare_CentreNO',
'Family_Welfare_Centre_Doctors_In_PositionNO',
'Family_Welfare_Centre_Doctors_Total_StrengthNO',
'Family_Welfare_Centre_Para_Medical_Staff_In_PositionNO',
'Family_Welfare_Centre_Para_Medical_Staff_Total_StrengthNO',
'Hospiltal_Alternative_MedicineNO',
'Hospiltal_Alternative_Medicine_Doctors_In_PositionNO',
'Hospiltal_Alternative_Medicine_Doctors_Total_StrengthNO',
'Hospital_AllopathicNO',
'Hospital_Allopathic_Doctors_In_PositionNO',
'Hospital_Allopathic_Doctors_Total_StrengthNO',
'Hospital_Allopathic_Para_Medical_Staff_In_PositionNO',
'Hospital_Allopathic_Para_Medical_Staff_TotalStrengthNO',
'Hospital_Alternative_Medicine_ParaMedical_Staff_TNO',
'HSP_Alternative_Medicine_Para_Medical_Staff_In_PositionNO',
'MaternityAndChild_WelfareCentreParaMedicalStaffInPositionNO',
'Maternity_And_Child_Welfare_CentreNO',
'Maternity_And_Child_Welfare_Centre_Doctors_In_PositionNO',
'Maternity_And_Child_Welfare_Centre_Doctors_NO',
'Maternity_And_Child_Welfare_Centre_Para_Medical_StaffNO',
'NonGovtMedical_facilities_Medicine_ShopNO',
'NonGovtMedical_facilities_OthersNO',
'NonGovt_Medical_facilities_MF_CharitableNO',
'NonGovt_Medical_facilities_MF_In_And_Out_PatientNO',
'NonGovt_Medical_facilities_MF_Out_PatientNO',
'NonGovt_MF_Medical_Practitioner_with_no_DegreeNO',
'NonGovt_MF_Medical_Prctitioner_with_MBBS_DegreeNO',
'NonGovt_MF_Medical_Prctitioner_with_other_DegreeNO',
'NonGovt_MF_Traditional_Practitioner_and_Faith_HealerNO',
'Nutritional_CentresAnganwadi_Centre',
'Nutritional_CentresICDS',
'Nutritional_CentresOthers',
'PrimaryHeallth_SubCentre_ParaMedical_Staff_StrengthNO',
'Primary_Heallth_Sub_CentreNO',
'Primary_Heallth_Sub_Centre_Doctors_In_PositionNO',
'Primary_Heallth_Sub_Centre_Doctors_Total_StrengthNO',
'Primary_Heallth_Sub_Centre_Para_Medical_Staff_In_PositionNO',
'Primary_Health_CentreNO',
'Primary_Health_Centre_Doctors_In_PositionNO',
'Primary_Health_Centre_Doctors_Total_StrengthNO',
'Primary_Health_Centre_Para_Medical_Staff_In_PositionNO',
'Primary_Health_Centre_Para_Medical_Staff_Total_StrengthNO',
'TB_ClinicNO',
'TB_Clinic_Doctors_In_PositionNO',
'TB_Clinic_Doctors_Total_StrengthNO',
'TB_Clinic_Para_Medical_Para_Medical_Staff_In_PositionNO',
'TB_Clinic_Para_Medical_Para_Medical_Staff_Total_StrengthNO',
'Agricultural_Credit_Societies',
'Agricultural_Marketing_Society',
'Distance_to_Nearest_Agricultural_Credit_Societies_Available',
'Distance_to_Nearest_Agricultural_Marketing_Society_Available',
'Distance_to_Nearest_Public_Distribution_System_Shop_Available',
'Distance_to_Nearest__MandisRegular_Market_Available',
'Distance_to_Nearest__Weekly_Haat_Available',
'MandisRegular_Market',
'Public_Distribution_System_PDS_Shop',
'Weekly_Haat',
'Daily_Newspaper_Supply',
'DistancetoNearest_InternetCafesCommonServiceCentre_available',
'Distance_to_Nearest_Daily_Newspaper_Supply_Available',
'Distance_to_Nearest__Mobile_Phone_Coverage_Available',
'Distance_to_Nearest__Public_Call_Office_Mobile_PCO_Available',
'Distance_to_Nearest__Telephone_landlines_Available',
'Internet_Cafes_Common_Service_Centre_CSC',
'Mobile_Phone_Coverage',
'Public_Call_Office_Mobile_PCO',
'Telephone_landlines',
'Assembly_Polling_Station',
'Birth_and_Death_Registration_Office',
'Distance_to_Nearest_Assembly_Polling_Station_Available',
'Distance_to_Nearest__Foothpath_Available',
'Distance_to_Nearest__Private_Courier_Facility_Available',
'Distance_to_Nearest__Self_Help_Group_SHG__Available',
'DitoNeBianDeReOfAv',
'Foothpath',
'Private_Courier_Facility',
'Self_Help_Group_SHG',
'Distance_to_Nearest_Mobile_Health_Clinic_Available',
'Mobile_Health_ClinicNO',
'Mobile_Health_Clinic_Doctors_In_PositionNO',
'Mobile_Health_Clinic_Doctors_Total_StrengthNO',
'Mobile_Health_Clinic_Para_Medical_Staff_In_PositionNumbers',
'Mobile_Health_Clinic_Para_Medical_Staff_Total_StrengthNO',
'Rural_MPI',
'Power_Supply_For_Agriculture_Use',
'Power_Supply_For_All_Users',
'Power_Supply_For_Commercial_Use',
'Power_Supply_For_Commercial_Use_WinterOctMarper_dayin_Hrs',
'Power_Supply_For_Domestic_Use',
'Closed_Drainage',
'Community_Toilet_Complexexcluding_Bathfor_Gen_Public',
'Community_Toilet_Complexincluding_Bathfor_General_Public',
'Drain_water_discharged_directly_intoSewarPlantsWaterBodies',
'Is_Area_Covered_under_Total_Sanitation_CampaignTSC',
'Nearest_Rural_Production_centresSanitary_hardware_outlets',
'No_Drainage',
'Open_Drainage',
'Open_Kuccha_Drainage',
'Open_Pucca_Drainage_Covered_with_Tiles_Slabs',
'Open_Pucca_Drainage_Uncovered',
'Rural_Prod_martSanitary_hardware_outlet_Available_nr_village',
'All_Weather_Road',
'AutoModified_Autos',
'Black_Topped_pucca_Road',
'Carts_Drivens_by_Animals',
'Cyclepulled_Rickshaws_machine_driven',
'Cyclepulled_Rickshaws_manual_driven',
'DistanceToNearestCyclepulledRickshawsMachineDrivenAvailable',
'Distance_toNearestCyclepulledRickshawsManualDrivenAvailable',
'Distance_to_Nearest_All_Weather_Road_Available',
'Distance_to_Nearest_AutoModified_Auto_Available',
'Distance_to_Nearest_Black_Topped_pucca_Road_Available',
'Distance_to_Nearest_Carts_Drivens_by_Animals_Available',
'Distance_to_Nearest_Gravel_kuchha_Roads_Available',
'Distance_to_Nearest_Major_District_Road_Available',
'Distance_to_Nearest_Navigable_WaterwaysRiverCanal_Available',
'Distance_to_Nearest_Other_District_Road_Available',
'Distance_to_Nearest_Public_Bus_Service_Available',
'Distance_to_Nearest_SeaRiverFerry_Service_Available',
'Distance_to_Nearest_State_Highway_Available',
'Distance_to_Nearest_Taxi_Available',
'Distance_to_Nearest_Vans_Available',
'Distance_to_Nearest_Water_Bounded_Macadam_Available',
'Distance_to_Nearest__National_Highway_Available',
'Distance_to_Nearest__Private_Bus_Service_Available',
'Distance_to_Nearest__Railway_Station_Available',
'Distance_to_Nearest__Tractors_Available',
'Gravel_kuchha_Roads',
'Major_District_Road',
'National_Highway',
'Navigable_Waterways_RiverCanal',
'Other_District_Road',
'Private_Bus_Service',
'Public_Bus_Service',
'Railway_Station',
'SeaRiverFerry_Service',
'State_Highway',
'Taxi',
'Tractors',
'Vans',
'Water_Bounded_Macadam',
'Distance_to_Nearest_Veterinary_Hospital_Available',
'Veterinary_HospitalNO',
'Veterinary_Hospital_Doctors_In_PositionNO',
'Veterinary_Hospital_Doctors_Total_StrengthNO',
'Veterinary_Hospital_Para_Medical_Staff_In_PositionNO',
'Veterinary_Hospital_Para_Medical_Staff_Total_StrengthNO',
'Community_Biogasrecycle_of_waste_for_production_use',
'Community_waste_disposal_System_house_to_house_collection',
'No_System_Garbage_on_roadstreet',
'Canals_Area_in_Hectares',
'Covered_Well',
'Covered_Well_Functioning_All_round_the_year',
'Covered_Well_Functioning_in_Summer_months_AprSep',
'Hand_Pump',
'Hand_Pump_Functioning_All_round_the_year',
'Hand_Pump_Functioning_in_Summer_months_AprSep',
'Others',
'Others_Functioning_All_round_the_year',
'Others_Functioning_in_Summer_months_AprSep',
'Other_Source_specify_Area_in_Hectares',
'RiverCanal',
'RiverCanal_Functioning_All_round_the_year',
'RiverCanal_Functioning_in_Summer_months_AprSep',
'Spring',
'Spring_Functioning_All_round_the_year',
'Spring_Functioning_in_Summer_months_AprSep',
'TankPondLake',
'TankPondLake_Functioning_All_round_the_year',
'TankPondLake_Functioning_in_Summer_months_AprSep',
'TanksLakes_Area_in_Hectares',
'Tube_WellsBorehole',
'Tube_WellsBorehole_Functioning_All_round_the_year',
'Tube_WellsBorehole_Functioning_in_SummerMonthsAprSep',
'Uncovered_Well',
'Uncovered_Well_Functioning_All_round_the_year',
'Uncovered_Well_Functioning_in_Summer_months_AprSep',
'Waterfall_Area_in_Hectares',
'WellsTube_Wells_Area_in_Hectares'
);

	$query = "SELECT `field`,`operator`, `table`, `field_label`, `field_sublabel` FROM pqwoe_mica_group_field_summary_map WHERE  `field` in('".implode("','",$attributes). "')";

	$row_data = mysqli_query($conn, $query);

	$states = "ORISSA";

	$villages_fields = array();
	$villages_column_fields = array();
	$villages_where  = array();

	while($value = mysqli_fetch_object($row_data))
	{
		$village_column_names[$value->field] = $value;
		$field_label     = $value->field_label;
		$field_labels    = str_replace(" ","_",$field_label);
		$field_sublabel  = $value->field_sublabel;
		$field_sublabels = str_replace(" ","_",$field_sublabel);

		switch ($value->table) {
			case 'Villages':
			switch ($value->operator) {
				case 'Total':
					array_push($villages_fields,"ROUND( sum(".$value->field."),2) as ".$value->field."____0" );
					array_push($villages_column_fields, $value->field."____0" );
				break;
				case 'Countsofab':
				array_push($villages_fields," count(case ".$value->field." when '1' then 1 else null end) ".$value->field."____0,count(case ".$value->field." when '2' then 1 else null end) ".$value->field."____1");
				array_push($villages_column_fields, $value->field."____0" );
				array_push($villages_column_fields, $value->field."____1" );
				break;
				case 'FixedvalueDistrictlevel':
				array_push($villages_fields,"ROUND(sum(".$value->field.")/count(".$value->field."),2) as ".$value->field."____0");
				array_push($villages_column_fields, $value->field."____0" );
				break;
				case 'Countsofabc':
				array_push($villages_fields,"count(case ".$value->field." when 'a' then 1 else null end) ".$value->field."____0,count(case ".$value->field." when 'b' then 1 else null end) ".$value->field."____1,count(case ".$value->field." when 'c' then 1 else null end) ".$value->field."____2");
				array_push($villages_column_fields, $value->field."____0" );
				array_push($villages_column_fields, $value->field."____1" );
				array_push($villages_column_fields, $value->field."____2" );
				break;
				/*case 'RowElements':
					if ($value->field == "village_count") {
						array_push($selectqueryfields,$value->field);
					}
					//array_push($selectqueryfields,$value->field.'____0');
					//array_push($villages_fields,"(".$value->field.") as ".$value->field."____0");
				break;*/
			}
			array_push($villages_where," ".$value->field." is not null ");

			if (!empty($subDistrict)) {
			 	$villages_appendquery = "WHERE Sub_District_Code IN ('".$subDistrict."') AND State_name IN ('".$states."')  GROUP BY Sub_District_Code";
			}else{
				$villages_appendquery = "WHERE india_information_OGR_FID IN ('".$districts."') AND State_name IN ('".$states."')  GROUP BY  district";
			}
		}
	}

	if(!empty($villages_fields))
	{
		if (!empty($subDistrict)) {
			/* 	Added by hirin for faster query results -

				created new tables to dump all formula results in another table and fetching records from new table

				Sub_District_Name 	District_name 	Distance_to_NearestPrimary_SchoolAvailable_2 	State_name 	district 	Sub_District_Code 	india_information_OGR_FID
			*/
			$finalQuery = "INSERT INTO villages_summary_sub_dist ( ".str_replace("____","_",implode(",",$villages_column_fields)). ", Sub_District_Name ,District_name,State_name,district, Sub_District_Code, india_information_OGR_FID) Select ".str_replace("____","_",implode(",",$villages_fields)). ", Sub_District_Name ,District_name,State_name,district, Sub_District_Code, india_information_OGR_FID from villages ".$villages_appendquery;
		}else{
			$finalQuery = "INSERT INTO villages_summary_dist ( ".str_replace("____","_",implode(",",$villages_column_fields)). ",District_name,State_name ,district, india_information_OGR_FID) Select ".str_replace("____","_",implode(",",$villages_fields)). ",District_name,State_name ,district, india_information_OGR_FID from villages ".$villages_appendquery;
		}

		if (mysqli_query($conn, $finalQuery)) {
      		echo "New record created successfully -> ";
      		echo "<br>";
		} else {
      		echo "Error: " . $sql . "<br>" . mysqli_error($conn);
		}

		echo "<pre/>";print_r($finalQuery);exit;



	}

echo "<pre/>";print_r("All Done");
mysqli_close($conn);

?>