<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$app = JFactory::getApplication('site');

require_once( JPATH_SITE.'/components/com_osmembership/helper/subscription.php');
$subs = OSMembershipHelperSubscription::getSubscriptions($app->input->get('mica_user'));
$user = JFactory::getUser($subs[0]->subscriptions[0]->user_id);

?>
<div class="registration-complete<?php echo $this->pageclass_sfx; ?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
	<h1>
		<?php echo $this->escape($this->params->get('page_heading')); ?>
	</h1>
	<?php endif; ?>

	<div>
		<div><?php echo JText::_('Subscription Expired!') ?></div>
		<div>
			<?php echo JText::_('Dear ').$user->get('name'); ?>
		</div>
		<div>
			<?php echo JText::_('You Subscription has expired on: ').JHtml::_('date', $subs[0]->subscription_to_date, "d-M-y"); ?>
		</div>
	</div>
</div>
