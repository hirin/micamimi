<?php
/**
 * Joomla! 3.0 component Add user Frontend
 *
 * @version $Id: adduserfrontend.php 2014-08-24 23:00:13 svn $
 * @author Kim Pittoors
 * @package Joomla
 * @subpackage Add user Frontend
 * @license GNU/GPL
 *
 * Add users to Community builder on the frontend
 *
* @Copyright Copyright (C) 2009 - 2014 - Kim Pittoors - www.joomlacy.com
* @license GNU/GPL http://www.gnu.org/copyleft/gpl.html
 *
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');
/**
 * Add user Frontend Component Add user Frontend Model
 *
 * @author      notwebdesign
 * @package		Joomla
 * @subpackage	Add user Frontend
 * @since 3.0
 */
class AdduserfrontendModelChildusers extends JModelLegacy {
    /**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
    }

    function getCreatedAccount() {
		$db   = JFactory::getDBO ();
		/*$sql  = " SELECT a.*, g.name AS groupname FROM ".$db->quoteName('#__users')." AS a
			INNER JOIN ".$db->quoteName('#__core_acl_aro')." 			AS aro 	ON ".$db->quoteName('aro.value')." = ".$db->quoteName('a.id')."
			INNER JOIN ".$db->quoteName('#__core_acl_groups_aro_map')." AS gm 	ON ".$db->quoteName('gm.aro_id')." = ".$db->quoteName('aro.id')."
			INNER JOIN ".$db->quoteName('#__core_acl_aro_groups')." 	AS g 	ON ".$db->quoteName('g.id')." = ".$db->quoteName('gm.group_id')."
			LEFT JOIN ".$db->quoteName('#__mica_adduser')." 			AS adu 	ON ".$db->quoteName('adu.created_userid')."= ".$db->quoteName('a.id')."
			WHERE ".$db->quoteName('adu.login_userid')." = ".$db->quote(JFactory::getUser()->id)."
				GROUP BY ".$db->quoteName('a.id')."
				ORDER BY ".$db->quoteName('a.name')." DESC ";*/
		$sql  = " SELECT DISTINCT a.id FROM ".$db->quoteName('#__users')." AS a
			INNER JOIN ".$db->quoteName('#__mica_adduser')." AS adu ON ".$db->quoteName('adu.created_userid')." = ".$db->quoteName('a.id')."
			WHERE ".$db->quoteName('adu.login_userid')." = ".$db->quote(JFactory::getUser()->id)."
			ORDER BY ".$db->quoteName('a.name')." DESC ";
		$db->setQuery($sql);
		$usersData = $db->loadColumn();

		$users = array();
		foreach ($usersData as $key => $each_usersdata) {
			$each_user = JFactory::getUser($each_usersdata);
			$users[]   = array(
				'id'    => $each_user->get('id'),
				'name'  => $each_user->get('name'),
				'email' => $each_user->get('email')
			);
		}
		return $users;
	}

	function getSaveChanges(){
		$post           = JFactory::getApplication()->input->post;
		$passwords      = $post->get('password', array(), 'array');
		$verifypassword = $post->get('verifypassword', array(), 'array');

		$not_matched = array();
		if (count($passwords) > 0 ) {
			$db = JFactory::getDBO ();
			foreach ($passwords as $each_userid => $each_passwords) {
				$each_passwords = trim($each_passwords);
				if (	(!empty($each_passwords) || $each_passwords != '') && ($each_userid > 0) && ($each_passwords == $verifypassword[$each_userid]) ){
					$each_user = JUser::getInstance($each_userid);

					$each_user->password       = JUserHelper::hashPassword($each_passwords);
					$each_user->activation     = '';
					$each_user->password_clear = $each_passwords;
					if (!$each_user->save(true)){// Save the user to the database.
						$not_matched[] = $each_userid;
					}
				}else{
					$not_matched[] = $each_userid;
				}
			}
		}
		return $not_matched;
	}

}
?>
