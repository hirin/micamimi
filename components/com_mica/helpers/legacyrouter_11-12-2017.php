<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Legacy routing rules class from com_mica
 *
 * @since       3.6
 * @deprecated  4.0
 */
class MicaRouterRulesLegacy implements JComponentRouterRulesInterface
{
	/**
	 * Constructor for this legacy router
	 *
	 * @param   JComponentRouterAdvanced  $router  The router this rule belongs to
	 *
	 * @since       3.6
	 * @deprecated  4.0
	 */
	public function __construct($router)
	{
		$this->router = $router;
	}

	/**
	 * Preprocess the route for the com_mica component
	 *
	 * @param   array  &$query  An array of URL arguments
	 *
	 * @return  void
	 *
	 * @since       3.6
	 * @deprecated  4.0
	 */
	public function preprocess(&$query)
	{
	}

	/**
	 * Build the route for the com_mica component
	 *
	 * @param   array  &$query     An array of URL arguments
	 * @param   array  &$segments  The URL arguments to use to assemble the subsequent URL.
	 *
	 * @return  void
	 *
	 * @since       3.6
	 * @deprecated  4.0
	 */
	public function build(&$query, &$segments)
	{
		// Declare static variables.
		static $items;
		static $default;
		static $dashboard;
		static $fullmap;
		static $micafront;
		static $showresults;

		// Get the relevant menu items if not loaded.
		if (empty($items))
		{
			// Get all relevant menu items.
			$items = $this->router->menu->getItems('component', 'com_mica');

			// Build an array of serialized query strings to menu item id mappings.
			for ($i = 0, $n = count($items); $i < $n; $i++)
			{
				// Check to see if we have found the dashboard menu item.
				if (empty($dashboard) && !empty($items[$i]->query['view']) && ($items[$i]->query['view'] == 'dashboard'))
				{
					$dashboard = $items[$i]->id;
				}

				// Check to see if we have found the fullmap menu item.
				if (empty($fullmap) && !empty($items[$i]->query['view']) && ($items[$i]->query['view'] == 'fullmap'))
				{
					$fullmap = $items[$i]->id;
				}

				// Check to see if we have found the micafront menu item.
				if (empty($micafront) && !empty($items[$i]->query['view']) && ($items[$i]->query['view'] == 'micafront'))
				{
					$micafront = $items[$i]->id;
				}

				// Check to see if we have found the showresults menu item.
				if (empty($showresults) && !empty($items[$i]->query['view']) && ($items[$i]->query['view'] == 'showresults'))
				{
					$showresults = $items[$i]->id;
				}

			}
		}

		if (!empty($query['view']))
		{
			switch ($query['view'])
			{
				default:
				case 'dashboard':
					if ($query['Itemid'] = $dashboard)
					{
						unset ($query['view']);
					}
					else
					{
						$query['Itemid'] = $default;
					}
					break;

				case 'fullmap':
					if ($query['Itemid'] = $fullmap)
					{
						unset ($query['view']);
					}
					else
					{
						$query['Itemid'] = $default;
					}
					break;

				case 'micafront':
					if ($query['Itemid'] = $micafront)
					{
						unset ($query['view']);
					}
					else
					{
						$query['Itemid'] = $default;
					}
					break;

				case 'showresults':
					if ($query['Itemid'] = $showresults)
					{
						unset ($query['view']);
					}
					else
					{
						$query['Itemid'] = $default;
					}
					break;
			}
		}

		$total = count($segments);

		for ($i = 0; $i < $total; $i++)
		{
			$segments[$i] = str_replace(':', '-', $segments[$i]);
		}
	}

	/**
	 * Parse the segments of a URL.
	 *
	 * @param   array  &$segments  The segments of the URL to parse.
	 * @param   array  &$vars      The URL attributes to be used by the application.
	 *
	 * @return  void
	 *
	 * @since       3.6
	 * @deprecated  4.0
	 */
	public function parse(&$segments, &$vars)
	{
		$total = count($segments);

		for ($i = 0; $i < $total; $i++)
		{
			$segments[$i] = preg_replace('/-/', ':', $segments[$i], 1);
		}

		// Only run routine if there are segments to parse.
		if (count($segments) < 1)
		{
			return;
		}

		JError::raiseError(404, JText::_('JGLOBAL_RESOURCE_NOT_FOUND'));
	}
}
