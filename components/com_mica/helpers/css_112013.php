<?php
jimport('joomla.application.component.helper');

class cssHelper
{
	var $db="";
	var $vars="";
	var $filter="";
	var $PolygonSymbolizer="";
	var $TextSymbolizer="";
	var $rule="";
	var $dominator="";
	var $_result="";
	var $pointSymbolizer;
	function getLayerProperty($activeworkspace,$toserver=null,$activetable)
	{	
		$db= & JFactory::getDBO();
			$query="SELECT * from #__mica_map_sld where profile_id = ".$activeworkspace." and layer like '".$activetable."' order by level DESC";

	  		$db->setQuery($query);
			$db->query();
			$result=$db->loadAssocList();
			$result=array_reverse($result);
			//echo "<pre>";print_r($result);exit;
//			if(empty($result))
//			{
//				return false;
//			}
		$this->_result=$result;
		$defaultvalue="";
		$bytype=array();
		$rules=array();
		
		foreach($result as $row)
		{	//$seg=explode(":",$row['layer']);
			$bytype[$row['layer'].".".$row['propertytype']][]=$row;
			
    		}
		
		$lastkey="";
		{$i=1;
		foreach($bytype as $key=>$val)
		
			foreach($val as $innserkey=>$innerval)
			{
				$rules[$key][] =cssHelper::$this->rule. cssHelper::makeRule($innerval);
				
			}
		$i++;
		//$rules[$key][] = array();//cssHelper::addRestRegionRule($key);//.$this->rule;
		}
		
		if($toserver==1)
		{   
			return cssHelper::toServer($rules,$row,$activetable);
		}
		else
		{	
			return cssHelper::makeXml($rules,$row,$activetable);
		}
		
	}
	function addRestRegionRule($key)
	{	$outterseg=explode(".",$key);
		//$rs=mysql_query("SELECT * from css  ORDER BY propertytype");
		$condition="";
		$condition .="
		    
		       <ogc:PropertyName>".$outterseg[1]."</ogc:PropertyName>";
		foreach($this->_result as $row)
		{	
			$seg=explode(":",$row['layer']);
			
			if($outterseg[1]==$row['propertytype'])
			{
			$condition .="
		    
		       <ogc:Literal><![CDATA[".$row['text']."]]></ogc:Literal>
		     
		   ";
			}
    		}
		return '<Rule>
			<ogc:Filter>
			<ogc:PropertyIsNotEqualTo>
			'.$condition.'
			</ogc:PropertyIsNotEqualTo>
			</ogc:Filter>
		
		 <LineSymbolizer>
         <Stroke>
           <CssParameter name="stroke">#E9AD84</CssParameter>
           <CssParameter name="stroke-width">1</CssParameter>
           <CssParameter name="stroke-dasharray">1 1</CssParameter>
         </Stroke>
       </LineSymbolizer>
		
			</Rule>';
	}
	function toServer($rules,$row)
	{ 
		$str="";
		foreach($rules as $key=>$val)
		{
			$seg=explode(".",$key);
			foreach($val as $key1=>$val1)
			{
			$str .=$val1;
			}
		}
		if(count($rules)==0)
		{
			$str ='<PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#FCE086</CssParameter>
            </Fill>
        
          </PolygonSymbolizer>';
		}
	
		return '<StyledLayerDescriptor>
			<NamedLayer>
			<Name><![CDATA['.$seg[0].']]></Name>
			<UserStyle>
			<Title><![CDATA['.$row['layer'].']]></Title>
			<FeatureTypeStyle>
			'.$str.'
			</FeatureTypeStyle>
			</UserStyle>
			</NamedLayer>
			</StyledLayerDescriptor>';
	}
	function makeXml($rules,$row,$activetable=null)
	{	
		if($activetable=="my_table" || $activetable=="jos_mica_urban_agglomeration")
		{
			$activefield="place_name";
			$pointsymb="<PointSymbolizer>
         <Graphic>
           <Mark>
             <WellKnownName>circle</WellKnownName>
             <Fill>
               <CssParameter name='fill'>#A74944</CssParameter>
             </Fill>
           </Mark>
           <Size>12</Size>
           <Rotation>45</Rotation>
         </Graphic>
       </PointSymbolizer>";
		}
		else if($activetable=="rail_state" )
		{
			$activefield="name";
		}
		else if($activetable=="jos_mica_urban_agglomeration")
		{
			$activefield="place_name";
	
		}
		else
		{
			$activefield="distshp";
			$pointsymb="";
		}

		$finalstr="";
		$i=0;
		if(count($rules)==0)
		{
			return $a='<?xml version="1.0" encoding="ISO-8859-1"?>
			<StyledLayerDescriptor version="1.0.0" 
 xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
 xmlns="http://www.opengis.net/sld" 
 xmlns:ogc="http://www.opengis.net/ogc" 
 xmlns:xlink="http://www.w3.org/1999/xlink" 
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><NamedLayer>
			<Name><![CDATA['.$activetable.']]></Name>
			
    <UserStyle>
    <!-- Styles can have names, titles and abstracts -->
      <Title>Default Polygon</Title>
      <Abstract>A sample style that draws a polygon</Abstract>
      <!-- FeatureTypeStyles describe how to render different features -->
      <!-- A FeatureTypeStyle for rendering polygons -->
      <FeatureTypeStyle>
        
        <Rule>
          <Name>Mediaum</Name>
      
         <PolygonSymbolizer>
            <Fill>
              <CssParameter name="fill">#FFE084</CssParameter>
            </Fill>
        
          </PolygonSymbolizer>
          <LineSymbolizer>
         <Stroke>
           <CssParameter name="stroke">#E9AD84</CssParameter>
           <CssParameter name="stroke-width">1</CssParameter>
           <CssParameter name="stroke-dasharray">1 1</CssParameter>
         </Stroke>
       </LineSymbolizer>
         <TextSymbolizer>
         <Label>
           <ogc:PropertyName>'.$activefield.'</ogc:PropertyName>
         </Label>
         <Font>
           <CssParameter name="font-family">Dialog.plain</CssParameter>
           <CssParameter name="font-size">12</CssParameter>
           <CssParameter name="font-style">bold</CssParameter>
           
         </Font>
 
         <LabelPlacement>
           <PointPlacement>
             <AnchorPoint>
               <AnchorPointX>0.5</AnchorPointX>
               <AnchorPointY>0.5</AnchorPointY>
             </AnchorPoint>
           </PointPlacement>
	<VendorOption name="spaceAround">-1</VendorOption>
	<VendorOption name="autoWrap">10</VendorOption>
	<VendorOption name="goodnessOfFit">0.1</VendorOption>
<VendorOption name="spaceAround">10</VendorOption>
 <VendorOption name="conflictResolution">true</VendorOption>
         </LabelPlacement>
         <Halo>
          <Fill>
            <CssParameter name="fill">#FDFDFD</CssParameter>
            <CssParameter name="fill-opacity">0.85</CssParameter>
          </Fill>
      </Halo>
      <Fill>
        <CssParameter name="fill">#242424</CssParameter>
        </Fill>

<VendorOption name="spaceAround">-1</VendorOption>
	<VendorOption name="autoWrap">12</VendorOption>
<VendorOption name="polygonAlign">mbr</VendorOption>
<VendorOption name="maxDisplacement">20</VendorOption>
<VendorOption name="goodnessOfFit">0.5</VendorOption>
       </TextSymbolizer>
'.$pointsymb.'


        </Rule>
        
      </FeatureTypeStyle>
      
       
    </UserStyle>
  
			</NamedLayer></StyledLayerDescriptor>';
		}
		foreach($rules as $key=>$val)
		{	$seg=explode(".",$key);
			$finalstr[$seg[0]] .='<?xml version="1.0" encoding="ISO-8859-1"?>
			<StyledLayerDescriptor version="1.0.0" 
 xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd" 
 xmlns="http://www.opengis.net/sld" 
 xmlns:ogc="http://www.opengis.net/ogc" 
 xmlns:xlink="http://www.w3.org/1999/xlink" 
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"><NamedLayer>
			<Name><![CDATA['.$seg[0].']]></Name>
			<UserStyle>
			<Title>s'.$i.'</Title>
			<FeatureTypeStyle>
			'.implode("",$val).'
			</FeatureTypeStyle>
			</UserStyle>
			</NamedLayer></StyledLayerDescriptor>';
			
			$i++;
		}
		return $finalstr;
	}
	function makeRule($row)
	{
		cssHelper::getFilter($row);
	if($row['level']==1)
		{
			 cssHelper::pointSymbolizer($row);
			 		cssHelper::getTextSymbolizer($row);
		return "
			<Rule>
			".$this->filter.$this->pointSymbolizer.$this->TextSymbolizer."
			</Rule>";
		}
		else if($row['level']==2)
		{
			 cssHelper::pointSymbolizer($row);
			 		cssHelper::getTextSymbolizer($row);
		return "
			<Rule>
			".$this->filter.$this->pointSymbolizer.$this->TextSymbolizer."
			</Rule>";
		}
		else
		{
	        cssHelper::getPolygonSymbolizer($row);
	        		cssHelper::getTextSymbolizer($row);
		return "
			<Rule>
			".$this->filter.$this->PolygonSymbolizer.$this->TextSymbolizer."
			</Rule>";
		}

	}
	
	function getFilter($row)
	{

		$this->filter='<ogc:Filter>
			<ogc:PropertyIsEqualTo>
			<ogc:PropertyName><![CDATA['.$row['propertytype'].']]></ogc:PropertyName>
			<ogc:Literal><![CDATA['.$row['text'].']]></ogc:Literal>
			</ogc:PropertyIsEqualTo>
			</ogc:Filter>';
		
	}
	function getgeofield($layer)
	{
		if($layer=="rail_state")
		{
			return "india_state";
		}
		else if($layer=="india_information")
		{
			return "india_information";
		}
		else if($layer=="my_table")
		{
			return "india_city";
		}
		else if($layer=="jos_mica_urban_agglomeration")
		{
			return "india_city";
		}
	}
	function pointSymbolizer($row)
	{
		$getpin=$row['layerfill'];
		$getpin=str_replace("#","",$getpin);
		if($row['level']==1)
		{
			$img="layer1/pin".$getpin.".png";
		}
		else if($row['level']==2)
		{
			$img="layer2/map/star".$getpin.".png";
		}
		$this->pointSymbolizer='<PointSymbolizer>
		
     <Graphic>
        <ExternalGraphic>
           <OnlineResource xlink:type="simple" xlink:href="'.JUri::base().'components/com_mica/maps/img/'.$img.'" />
           <Format>image/png</Format>
        </ExternalGraphic>
     </Graphic>
    <Geometry>
       <ogc:Function name="centroid">
         <ogc:PropertyName>'.cssHelper::getgeofield($row['layer']).'</ogc:PropertyName>
       </ogc:Function>
     </Geometry>
 </PointSymbolizer>';
		
	}
	  function getPolygonSymbolizer($row)
	{

//		if($row['zoom']>=0 && $row['zoom'] < 3)
//		{
//			$opacity='<CssParameter name="fill-opacity">.8</CssParameter>';
//		}
//		else if($row['zoom']>=3)
//		{
//			$opacity='<CssParameter name="fill-opacity">.8</CssParameter>';
//		}
	//	$opacity='<CssParameter name="fill-opacity">.8</CssParameter>';
		$this->PolygonSymbolizer='<PolygonSymbolizer><Fill><CssParameter name="fill">'.$row['layerfill'].'</CssParameter></Fill>
		</PolygonSymbolizer> <LineSymbolizer>
         <Stroke>
           <CssParameter name="stroke">#E9AD84</CssParameter>
           <CssParameter name="stroke-width">1</CssParameter>
           <CssParameter name="stroke-dasharray">1 1</CssParameter>
         </Stroke>
       </LineSymbolizer>';
           

		
	}
	  function getTextSymbolizer($row)
	{

//		if($row['zoom']>=0 && $row['zoom'] < 3)
//		{
//			$name="NAME";
//		}
//		else if($row['zoom']>=3 && $row['zoom'] < 6)
//		{
//			$name="DISTSHP";
//		}
//		else
//		{
//			$name="NAME_2";
//		}
		$this->TextSymbolizer="<TextSymbolizer>
         <Label>
           <ogc:PropertyName>".$row['propertytype']."</ogc:PropertyName>
         </Label>
         
         <Font>
           <CssParameter name='font-family'>Dialog.plain</CssParameter>
           <CssParameter name='font-size'>10</CssParameter>
           <CssParameter name='font-style'>normal</CssParameter>
           
         </Font>
         <LabelDISTSHPment>
           <PointDISTSHPment>
             <AnchorPoint>
               <AnchorPointX>0.5</AnchorPointX>
               <AnchorPointY>0.5</AnchorPointY>
             </AnchorPoint>
           </PointDISTSHPment>
         </LabelDISTSHPment>
         <Halo>
          <Fill>
            <CssParameter name='fill'>#FDFDFD</CssParameter>
            <CssParameter name='fill-opacity'>0.20</CssParameter>
          </Fill>
      </Halo>
	<VendorOption name='spaceAround'>-1</VendorOption>
	<VendorOption name='autoWrap'>12</VendorOption>
<VendorOption name='polygonAlign'>mbr</VendorOption>
<VendorOption name='maxDisplacement'>20</VendorOption>
<VendorOption name='goodnessOfFit'>0.5</VendorOption>
         </TextSymbolizer>";
		
      /*
       * 
       * <Geometry>
       <ogc:Function name='centroid'>
         <ogc:PropertyName>".cssHelper::getgeofield($row['layer'])."</ogc:PropertyName>
       </ogc:Function>
     </Geometry>
       * */   
                
	
	}
 function getSldToDataBase()
  {
  		$session=JFactory::getSession();
  		$activetable=$session->get('activetable');
  		$activeworkspace=$session->get('activeworkspace');
  		if($activetable=="")
  		{
  		$activetable=JRequest::getVar('layer');
  		}
  		$server=0;
  		if($server==0)
  		{
  		$sld=cssHelper::getLayerProperty($activeworkspace,$server,$activetable);
  		
		cssHelper::createSld($sld,$activetable);
  		}
  		else
  		{
  		$sld=cssHelper::getLayerProperty($activeworkspace,1,$activetable);
  		//$sld=array_values($sld);
		cssHelper::createSld($sld,$activetable);
  		}
		return true;
  }
  function createSld($sld,$activetable)
  { 
  
  	
  	$user = &JFactory::getUser();
  	$session=JFactory::getSession();
  	$activeworkspace=$session->get('activeworkspace');
  	$filepath=TOMCAT_STYLE_ABS_PATH.$user->id."/".$activeworkspace."/".$activetable.".sld";
  	
  	$dirpath=TOMCAT_STYLE_ABS_PATH.$user->id."/".$activeworkspace;
  	//$iscreate=mkdir($dirpath,0755,true); 
  	$iscreate=cssHelper::createPath($dirpath);
  	
  	if($iscreate)
  	{
	 $handle=fopen($filepath,"w+");
//	 echo "<pre>";
//	 print_r($sld[$activetable]);exit;
  	if(is_array($sld))
  	{	
   		fwrite($handle, trim($sld[$activetable]));
  	}
  	else
  	{
  		fwrite($handle, trim($sld));
  	}
	
	 fclose($handle);
  	}
  	else
  	{
  		echo "unable to create Dir.";
  		exit;
  		
  	}
  }
  
function createPath($path) {
    if (is_dir($path)) return true;
    $prev_path = substr($path, 0, strrpos($path, '/', -2) + 1 );
    
    $return = cssHelper::createPath($prev_path);
    return ($return && is_writable($prev_path)) ? mkdir($path,0755,true) : false;
}
function saveSldToDatabase($innercall,$isUpdate)
{		
		$user = &JFactory::getUser();
		$id=JRequest::getVar("id");
		if($user->id==0)
		{
			return -1;
		}
		
  		$session=JFactory::getSession();
  		 $activeworkspace=$session->get('activeworkspace');
		$activetable=$session->get('activetable');
		 $activedata=$session->get('activedata');
		$activenamevariable=$session->get('activenamevariable');
		$activesearchvariable=$session->get('activesearchvariable');
		$customformula=$session->get('customformula');
		
		$customformula=explode("|",$customformula);
		$customformula=array_filter($customformula);
  		$activesearchvariable=$activesearchvariable;
		foreach($activedata as $eachdata)
		{	if(isset($eachdata->$activesearchvariable))
			{
			$id[]=$eachdata->$activesearchvariable;
			}
		}
		$id=implode(",",$id);
		$froms=array();
		$tos=array();
		$colors=array();
		$formulas=array();
		$customformula=array_unique($customformula);
		$alllevels=array();
		
		foreach($customformula as $eachformula)
		{
			$getcustom=explode(":",$eachformula);
			$getattr=explode("->",$getcustom[1]);
			$froms[]=$getattr[0];
			$tos[]=$getattr[1];
			$colors[]=$getattr[2];
			$formulas[]=$getcustom[0];
			$alllevels[]=$getattr[3];
		}
		
		$formulas=array_unique($formulas);
		$k=0;
		foreach ($formulas as $formula)
		{
		$from=array();
		$to=array();
		$color=array();
		//$formula=$formulas[$k];
		$from=$froms[$k];
		$from=explode(",",$from);
		//$from=array_filter($from);
		
		$to=$tos[$k];
		$to=explode(",",$to);
		$to=array_filter($to);
		
		$color=$colors[$k];
		$color=explode(",",$color);
		$color=array_filter($color);

		
		$db= & JFactory::getDBO();
		if($isUpdate==1)
		{
			
		 $query="DELETE FROM #__mica_map_sld  where profile_id = ".$activeworkspace." and level = ".$alllevels[$k];
		
  		$db->setQuery($query);
		$db->query();
		 $query="DELETE FROM #__mica_sld_legend  where workspace_id = ".$activeworkspace."  and level=".$alllevels[$k];
	  		$db->setQuery($query);
			$db->query();
		}
	
		$result=array();
		
		//	return true;
//			
		
		for($i=0;$i<=(count($from));$i++)
		{	
			
				
			if((count($to))==($i))
			{
				 $to[$i]=$to[$i]+1;
			}
			
	 $query="SELECT  ".$activenamevariable." , ".$activesearchvariable." FROM ".$activetable." where ".$activesearchvariable ." in (".$id.") AND ((".$formula.") >= ".($from[$i])." AND (".$formula.") <=".($to[$i]) ." )";
	
	  		$db->setQuery($query);
			$db->query();
			$result[]=$db->loadObjectList();
			 
			
			if($isUpdate!=1)
			{
			$query="INSERT INTO #__mica_sld_legend (workspace_id , range_from ,range_to , color ,custom_formula,level) VALUES ( ".$activeworkspace." , ".$from[$i]." , ".$to[$i]." , '#".$color[$i]."' , '".$formula."' ,'".$alllevels[$k]."')";
	  		$db->setQuery($query);
			$db->query();
			}
			else
			{
			$query="INSERT INTO #__mica_sld_legend (workspace_id , range_from ,range_to , color ,custom_formula,level) VALUES ( ".$activeworkspace." , ".$from[$i]." , ".$to[$i]." , '#".$color[$i]."' , '".$formula."' ,'".$alllevels[$k]."')";
	  		$db->setQuery($query);
			$db->query();
			}
		}
		
		
		
		
			$result=array_filter($result);
			
			foreach($result as $key=>$val)
			{
				foreach($val as $inkey=>$inval)
				{
					
					 $query="INSERT INTO #__mica_map_sld (profile_id , layer ,layerfill , text ,propertytype,level) VALUES ( ".$activeworkspace." , '".$activetable."' , '#".$color[$key]."' , '".$inval->$activenamevariable."' , '".$activenamevariable."' ,".$alllevels[$k]." )";
			  		$db->setQuery($query);
					$db->query();
					
				}
				
			}
			$k++;
		}
		
		if($innercall!="")
		{ 
			cssHelper::getSldToDataBase();
			
			return true;			
		}
		else
		{
			cssHelper::getSldToDataBase();
			exit;
		}
  		
  }
  function flushOrphandSld($activeworkspace)
  {
  		$db= & JFactory::getDBO();
		$result=array();
		$query="DELETE from #__mica_sld_legend where workspace_id = ".$activeworkspace;
	  	$db->setQuery($query);
		$db->query();
	
		$query="DELETE from #__mica_map_sld where profile_id = ".$activeworkspace;
	  	$db->setQuery($query);
		$db->query();
		$query="DELETE from #__mica_user_custom_attribute where profile_id = ".$activeworkspace;
	  	$db->setQuery($query);
		$db->query();
		cssHelper::getSldToDataBase();
		//$legendlevel=$db->loadRow();
			
				
  }

}

?>
