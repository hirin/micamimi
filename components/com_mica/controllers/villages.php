<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('MicaController', JPATH_COMPONENT . '/controller.php');

// DEVNOTE: import CONTROLLER object class
require_once JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'helpers/Workspace.php';
require_once JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'helpers/css.php';
require_once JPATH_COMPONENT . DIRECTORY_SEPARATOR . 'helpers/pdf.php';

/**
 * VillageShowresults controller class for MICA.
 *
 * @since  1.6
 */
class MicaControllerVillages extends MicaController
{

	public function getColorGradiant()
	{

		$value         = $this->input->get('value', '', 'raw');
		$steps         = $this->input->get('steps', '', 'raw');

		$theColorBegin = (isset($value)) ? hexdec("#" . $value) : 0x000000;
		$theColorEnd   = 0xffffff;
		$theNumSteps   = (isset($steps)) ? intval($steps) : 16;

		$theR0         = ($theColorBegin & 0xff0000) >> 16;
		$theG0         = ($theColorBegin & 0x00ff00) >> 8;
		$theB0         = ($theColorBegin & 0x0000ff) >> 0;

		$theR1         = ($theColorEnd & 0xff0000) >> 16;
		$theG1         = ($theColorEnd & 0x00ff00) >> 8;
		$theB1         = ($theColorEnd & 0x0000ff) >> 0;
		$str           = array();

		for ($i = 0; $i <= $theNumSteps; $i++)
		{
			$theR    = $this->interpolate($theR0, $theR1, $i, $theNumSteps);
			$theG    = $this->interpolate($theG0, $theG1, $i, $theNumSteps);
			$theB    = $this->interpolate($theB0, $theB1, $i, $theNumSteps);
			$theVal  = ((($theR << 8) | $theG) << 8) | $theB;
			$str[$i] = dechex($theVal);
		}

		echo implode(",", $str);
		exit;
	}

	/**
	 * [interpolate description]
	 *
	 * @param   [type]  $pBegin  [description]
	 * @param   [type]  $pEnd    [description]
	 * @param   [type]  $pStep   [description]
	 * @param   [type]  $pMax    [description]
	 *
	 * @return  [type]           [description]
	 */
	function interpolate($pBegin, $pEnd, $pStep, $pMax)
	{
		if ($pBegin < $pEnd)
		{
			return (($pEnd - $pBegin) * ($pStep / $pMax)) + $pBegin;
		}
		else
		{
			return (($pBegin - $pEnd) * (1 - ($pStep / $pMax))) + $pEnd;
		}
	}

	/**
	 * [getSldLevel description]
	 *
	 * @return  [type]  [description]
	 */
	function getSldLevel()
	{
		$db              = JFactory::getDBO();
		$session         = JFactory::getSession();
		$activeworkspace = $session->get('activeworkspace');
		$query           = "SELECT level FROM #__mica_sld_legend where workspace_id = " . $activeworkspace . " group by level";
		$db->setQuery($query);
		$result          = $db->loadObjectList();

		$str   = "";
		$level = "0,1,2";

		foreach ($result as $eachlevel)
		{
			$level = str_replace($eachlevel->level . ", ", "", $level);
		}

		return $level;
	}
	/**
	 * An ajax task.
	 *
	 * @return  void
	 */
	public function addthematicQueryToSession()
	{
		$session = JFactory::getSession();
		
		$formula = $this->input->get('formula', '', 'raw');
		$level   = $this->input->get('level', '', 'raw');

		if ($level == "")
		{
			$level = $this->getSldLevel();
			$level = explode(",", $level);
			$level = $level[0];
		}

		$from  = $this->input->get('from', '', 'raw');
		$to    = $this->input->get('to', '', 'raw');
		$color = $this->input->get('color', '', 'raw');

		$customformulafromsession = '';//$session->get('customformula');
		$geteachformula = explode("|", $customformulafromsession);

		foreach ($geteachformula as $key => $eachtocheck)
		{
			$singleformula = explode(":", $eachtocheck);

			if ($singleformula[0] == $formula || $singleformula[0] == "")
			{
				unset($geteachformula[$key]);
			}
		}

		$customformulafromsession = implode("|", $geteachformula);
		$customformula            = "|" . $formula . ":" . $from . "->" . $to . "->" . $color . "->" . $level;
		$customformulafromsession = $customformulafromsession . $customformula;
		$customformulafromsession = explode(":", $customformulafromsession);
		$customformulafromsession = array_filter($customformulafromsession);
		$customformulafromsession = array_unique($customformulafromsession);
		$customformulafromsession = implode(":", $customformulafromsession);
		$session->set('customformula', $customformulafromsession);
		// Edited
		$session->set('fromthematic', 1);
		// Edited ends
		echo $customformulafromsession;
		exit;
	}
	/**
	 * An ajax task.
	 *
	 * @return  void
	 */
	public function getMinMax()
	{
		$session     = JFactory::getSession();		
		$db          = JFactory::getDBO();
		$value       = $this->input->get('value', '', 'raw');
		
		
		$activetable = $session->get('activetable');
		$district  = $session->get('villagedistrict');
		$villageSubDistrict  = $session->get('villageSubDistrict');
		$ogrfid  = $session->get('villages_villages');
		$activedata  = explode(",", $activedata);	
		$where = [];
		if($district!='')
		{
			$where[]= $db->quoteName('district')." IN (".$district.")";
		}
		if($villageSubDistrict!='')
		{
			$where[]= $db->quoteName('Sub_District_Code')." IN (".$villageSubDistrict.")";
		}
		if($ogrfid!='')
		{
			$where[]= $db->quoteName('OGR_FID')." IN (".$ogrfid.")";
		}
		$result = array();
		if($value!=''){
			$where[] = $db->quoteName($value)." != ".$db->quote("N/A");
			$where[] = $db->quoteName($value)." != ".$db->quote("NA");
			$where[] = $db->quoteName($value)." != ".$db->quote("");
			$where[] = $db->quoteName($value)." != ".$db->quote("N/a");
			$where[] = $db->quoteName($value)." != ".$db->quote("na");
			$query = "SELECT (".$value.") as mymin, (".$value.") as mymax
					FROM villages
					WHERE ". implode(" AND ",$where);
			$db->setQuery($query);
			$result = $db->loadObjectList();
		
		}
		$min = min($result);
		$max = max($result);
		$results[] = (int)($min->mymin);
		$results[] = (int)($max->mymax)+1;
		echo implode(",",$results);exit;
		
		
	}
	public function viewRecordPlanLimit()
	{
		echo 10 ;
		die();
		$userid = JFactory::getUser()->id;

		// Get a db connection.
		$db = $this->getDBO();

		// Create a new query object.
		$view_record_plan_limit_query = $db->getQuery(true);

		$view_record_plan_limit_query = "SELECT user_id,mvmi_view_records FROM " . $db->quoteName('user_plan_setting') . "WHERE " . $db->quoteName('user_id') . " LIKE " . $db->quote($userid);

		$db->setQuery($view_record_plan_limit_query);

		$view_record_plan_limit = $db->loadAssoc();

		$view_record_plan_limit_value = $view_record_plan_limit['mvmi_view_records'];

		if ($view_record_plan_limit['user_id']) {
			$view_record_plan_limit_update_query = $db->getQuery(true);

			$view_record_plan_limit = $view_record_plan_limit_value+1;

			$userViewPlanLimit = $this->userViewPlanLimit();

			//$userViewPlanLimit = $userViewPlanLimit == "" ? 0 : $userViewPlanLimit;

			if($userViewPlanLimit >= $view_record_plan_limit)
			{
				// Fields to update.
				$fields = array(
				    $db->quoteName('mvmi_view_records') . ' = ' . $view_record_plan_limit
				);

				// Conditions for which records should be updated.
				$conditions = array(
				    $db->quoteName('user_id') . ' = ' . $userid
				);

				$view_record_plan_limit_update_query->update($db->quoteName('user_plan_setting'))->set($fields)->where($conditions);

				$db->setQuery($view_record_plan_limit_update_query);

				$db->execute();

				$allow_view = 1;
			}
			else
			{
				$allow_view = 0;
			}

		}else{
			// Create a new query object.
			$query = $db->getQuery(true);

			// Insert columns.
			$columns = array('user_id', 'mvmi_view_records');

			// Insert values.
			$values = array($userid, 1);

			// Prepare the insert query.
			$query
			    ->insert($db->quoteName('user_plan_setting'))
			    ->columns($db->quoteName($columns))
			    ->values(implode(',', $values));

			// Set the query using our newly populated query object and execute it.
			$db->setQuery($query);
			$db->execute();
		}

		return $allow_view;
	}
	/**
	 * A task to fetch attributes from AJAX call.
	 *
	 * @return  void
	 */
	public function getAttribute()
	{
		$zoom = $this->input->get('zoom', '', 'raw');
		$type = $this->input->get('type', '', 'raw');
		$user = JFactory::getUser();

		if ($user->id <= 0)
		{
			echo - 1;
			exit;
		}

		echo $values = $this->getPlanAttr($zoom, $type);
		exit;
	}
	/**
	 * [getPlanAttr description]
	 *
	 * @param   [type]  $zoom  [description]
	 * @param   [type]  $type  [description]
	 *
	 * @return  [type]         [description]
	 */
	function getPlanAttr($zoom, $type = null)
	{

		$prefix = 0;

		if ($type == null)
		{
			if ($zoom == 5)
			{
				$db_table = "State";
				$prefix   = 1;
			}
			elseif ($zoom > 5 && $zoom <= 7)
			{
				$db_table = "District";
				$prefix   = 1;
			}
			else
			{
				$db_table = "villages";
			}
		}
		else
		{
			$db_table = "district";
		}

		$userAttr = $this->userSelectedAttr($db_table);

		if ($db_table == "state")
		{
			$prefix = 1;
		}
		elseif ($db_table == "district")
		{
			$prefix = 1;
		}
		else
		{
			$prefix = 0;
		}

		if ($userAttr == -2)
		{
			return -2;
		}

		$str       = "";
		$todisplay = array();
		$db        = JFactory::getDBO();

		// Edited by date on 21/01/2013
		// $query="select * from #__mica_group_field allfields JOIN #__mica_group grp ON grp.id=allfields.groupid where allfields.table like '".$db_table."' and grp.publish =1 order by grp.group,allfields.field ASC";
		$query = "SELECT * FROM " . $db->quoteName('#__mica_group_field') . " AS allfields
			INNER JOIN " . $db->quoteName('#__mica_group') . " AS grp ON " . $db->quoteName('grp.id') . " = " . $db->quoteName('allfields.groupid') . "
			INNER JOIN " . $db->quoteName('#__mica_group_field_summary_map') . " AS summary_map ON " . $db->quoteName('summary_map.groupid') . " = " . $db->quoteName('allfields.groupid') . "
			AND ".$db->quoteName('summary_map.field')." = ". $db->quoteName('allfields.field') . "
			WHERE " . $db->quoteName('allfields.table') . " LIKE " . $db->quote($db_table) . "
				AND " . $db->quoteName('grp.publish') . " = " . $db->quote(1) . "
			ORDER BY " . $db->quoteName('grp.group') . ", " . $db->quoteName('allfields.field') . " ASC";

		$db->setQuery($query);
		$list = $db->loadAssocList();

		foreach ($list as $each)
		{
			if ($each['group'] != "Mkt Potential Index" && $each['group'] != "Score")
			{
				if ($prefix == 1)
				{
					// $each['field']               = str_replace($this->ruralprefix, "", $each['field']);
					// $each['field']               = str_replace($this->urnbanprefix, "", $each['field']);
					// $each['field']               = str_replace($this->totalprefix, "", $each['field']);
					$todisplay[$each['group']][] = $each['field'];
				}
				else
				{
					$todisplay[$each['group']]['field'][] = $each['field'];
					$todisplay[$each['group']]['field_label'][] = $each['field_label'];
				}

				$icon[$each['group']] = $each['icon'];
			}
			else
			{
				if ($prefix == 1)
				{
					// $each['field'] = str_replace($this->ruralprefix, "", $each['field']);
					// $each['field'] = str_replace($this->urnbanprefix, "", $each['field']);
					// $each['field'] = str_replace($this->totalprefix, "", $each['field']);
					$todisplay1[$each['group']][] = $each['field'];
				}
				else
				{
					//$todisplay1[$each['group']][] = $each['field'];
					$todisplay[$each['group']]['field'][] = $each['field'];
					$todisplay[$each['group']]['field_label'][] = $each['field_label'];
				}

				$icon[$each['group']] = $each['icon'];
			}
		}

		$todisplay = $todisplay;

		foreach ($todisplay as $eachgrp => $vals)
		{
			//$todisplay[$eachgrp] = array_unique($todisplay[$eachgrp]);
			$todisplay[$eachgrp]['field']       = array_unique($todisplay[$eachgrp]['field']);
			$todisplay[$eachgrp]['field_label'] = array_unique($todisplay[$eachgrp]['field_label']);
		}

		foreach ($userAttr as $each)
		{
			if ($prefix == 1)
			{
				// $each = str_replace($this->ruralprefix, "", $each);
				// $each = str_replace($this->urnbanprefix, "", $each);
				// $each = str_replace($this->totalprefix, "", $each);
				$userAttr1[] = $each;
			}
			else
			{
				$userAttr1[] = $each;
			}
		}

		$userAttr      = array_unique($userAttr1);
		$session       = JFactory::getSession();
		$oldattr       = $session->get('villagesattributes');
		$oldattr_array = explode(",", strtolower($oldattr));
		$urban         = "";
		$villages      = array();
		$other         = "";
		$str           = "";
		$i             = 0;
		$x             = 0;
		$str1          = "";
		$other         = array();
		$urban         = array();
		$total         = array();
		$grpname       = array();
		$header        = "";
		$div           = "";
		$returndata    = "";
		$shortcode = '';
		if ($this->input->get('grplist', '', 'raw') == "")
		{
			$returndata.='<ul class="list1 variablelist">';
			$shortcode.='<ul class="var_ul"><div class="variableslider">';
			foreach ($todisplay as $keys => $district_data)
			{
				 $lowerdata=strtolower($keys);
				 $string = preg_replace('/\s+/', '_', $lowerdata);
				 $keys = $keys == '-' ? "" : $keys;
				 $final=str_replace(" ","",$keys);

				//$returndata .= '<optgroup label="' . $keys . '">';
				$returndata .='<li class="variable_group" id="variable_group_'.$string.'"><label class="variable_label">'.$keys.'</label><div class="fright"><input type="checkbox" class="variableNameGrp allcheck variablelabel_checkbox-'.$final.'" id="variablelabel_allcheck-'.$final.'"><label>&nbsp;</label></div></li>';
				$shortcode.='<div><li><span class="tooltip-txt-box"><a class="'.$string.'" href="#variable_group_'.$string.'">'.$keys.'</a><span class="tooltiptext">'.$keys.'</span></span></li></div>';

				

				foreach ($district_data['field'] as $key => $eachattr)
				{
					if ($eachattr == "Rating" || $eachattr == "Rural_MPI" || $eachattr == "Total_Geographical_Area_in_Hectares" || $eachattr == 'Nearest_Town_Name' || $eachattr=="Rural_Score_Basic_Amenities" || $eachattr=="Rural_Score_Commercial_Usage_of_Premises" || $eachattr=="Rural_Score_Financial_Services" || $eachattr=="Rural_Score_for_Agriculture" || $eachattr=="Rural_Score_Media_Ownership")
					{

						$checked = "checked" . " disabled ";
					}
					elseif (in_array(strtolower($eachattr), $oldattr_array))
					{
						$checked = "checked";
					}
					else
					{
						$checked = "";
					}

					$class=strtolower(str_replace(" ", "_", $keys));
					$returndata .= '<li class="'.$class.'"><input type="checkbox" class="variable_checkbox variablelabel_checkbox-'.$final.'" name="villagesattributes[]" value="'.$eachattr.'" '.$checked.' id="variable_check_'.$x.'">
						<label for="variable_check_'.$x.'">'.JTEXT::_($district_data['field_label'][$key]).'</label></li>';
						$x++;
				}
			}
			$shortcode.='</div></ul>';
			$returndata .= '</ul>';

			return $returndata.'split'.$shortcode;
		}
		else
		{
			$grplist    = $this->input->get('grplist', '', 'raw');
			$grplist    = explode(",", $grplist);
			$returndata .= '<select name="villagesattributes[]" id="villagesattributes" class="inputbox" multiple="multiple" >';

			foreach ($todisplay as $key => $district_data)
			{
				if (in_array($key, $grplist))
				{
					$returndata .= '<optgroup label="' . $key . '">';

					foreach ($district_data as $key => $eachattr)
					{
						if ($eachattr == "Rating" || $eachattr == "Rural_MPI")
						{
							$checked = "selected";
						}
						elseif (in_array(strtolower($eachattr), $oldattr_array))
						{
							$checked = "selected";
						}
						else
						{
							$checked = "";
						}

						// Edited by dave 21/01/2013
						// $returndata .= '<option value="'.$eachattr.'" '.$checked.'>'.JTEXT::_($eachattr).'</option>';
						$returndata .= '<option value="' . $eachattr . '" ' . $checked . '>' . JTEXT::_($eachattr) . '</option>';
					}

					$returndata .= '</optgroup >';
				}
			}

			$returndata .= '</select>';

			return $returndata;
		}

		foreach ($todisplay as $key => $val)
		{
			$grpname[] = $key;
			$str[$i]   = "";

			if ($icon[$grpname[$i]] != "")
			{
				$img = $icon[$grpname[$i]];
			}
			else
			{
				$img = "default.png";
			}

			$header .= "<li ><a href='#" . str_replace(" ", "_", trim($grpname[$i])) . "grp'><div class='grpimg'><img src='" . JUri::base() . "components/com_mica/images/" . $img . "'/></div><div class='grptitle'>" . $grpname[$i] . "</div></a></li>";

			// $oldattr=explode(",",$oldattr);
			foreach ($userAttr as $eachattr)
			{
				if (in_array($eachattr, $val))
				{
					if ($eachattr == "Rating" || $eachattr == "Rural_MPI"  || $eachattr == "Total_Geographical_Area_in_Hectares" || $eachattr == 'Nearest_Town_Name' || $eachattr=="Rural_Score_Basic_Amenities" || $eachattr=="Rural_Score_Commercial_Usage_of_Premises" || $eachattr=="Rural_Score_Financial_Services" || $eachattr=="Rural_Score_for_Agriculture" || $eachattr=="Rural_Score_Media_Ownership")
					{
						$checked = "checked";
					}
					elseif (in_array(strtolower($eachattr), $oldattr_array))
					{
						$checked = "checked";
					}
					else
					{
						$checked = "";
					}

					$other[$i][] = '
					<div class="variablechkbox ">
					<input type="checkbox" name="villagesattributes[]" class="statetotal_attributes hideurban ' . str_replace(" ", "_", $key) . '" value="' . $eachattr . '" ' . $checked . '>
					</div>
					<div class="variabletext hideurban"><span class="hovertext ' . JTEXT::_(trim($eachattr)) . '">' . ucfirst(strtolower($eachattr)) . '</span></div>';
				}
			}

			$i++;
		}

		$str1 .= '<div class="maintableattr">
			<div class="left">
			<a class="statetotalselectall" href="javascript:void(0);" onclick="checkAll(statetotal_attributes);">' . JText::_('SELECT_ALL_LABEL') . '</a> / <a class="statetotalunselectall" onclick="uncheckall(statetotal_attributes);" href="javascript:void(0);">' . JText::_("UNSELECT_ALL_LABEL") . '</a>
			</div></div>';

		$finalstr   = "";
		$town1      = "";
		$urban1     = "";
		$others1    = "";
		$mystrarray = array();
		$total1     = array();

		for ($i = 0; $i < count($str); $i++)
		{
			$finalstr = "";
			$town1    = "";
			$urban1   = "";
			$others1  = "";
			$total1   = "";

			for ($j = 0; $j < count($other[$i]); $j++)
			{
				if (($j % 3) == 0)
				{
					$others1 .= "</div><div class='maintableattr'>";
				}

				$others1 .= $other[$i][$j];
			}

			$othersss = "<div id='" . str_replace(" ", "_", trim($grpname[$i])) . "grp' class='singlegrp'>";
			$others1  = $othersss . "<div>" . $others1;

			/*$finalstr .=$str[$i];
			$finalstr .="</div><div class='maintableattr ' >".$urban1."";
			$finalstr .="</div><div class='maintableattr '>".$total1."</div>";
			$others1  .="</div>";*/

			$finalstr .= $others1 . "";
			$finalstr .= '<div class="maintableattr">
				<div class="selectallfront">
				<a class="statetotalselectall" href="javascript:void(0);" onclick="checkAll(\'' . str_replace(" ", "_", trim($grpname[$i])) . '\');">' . JText::_('SELECT_ALL_LABEL') . '</a> / <a class="statetotalunselectall" onclick="uncheckall(\'' . str_replace(" ", "_", trim($grpname[$i])) . '\');" href="javascript:void(0);">' . JText::_("UNSELECT_ALL_LABEL") . '</a>
				</div></div></div></div>';
			$mystrarray[$i] = $finalstr;
		}

		return "<div id='tabs'><ul>" . $header . "</ul>" . implode("", $mystrarray) . "</div></div></div>";
	}
	/**
	 * [userSelectedAttr description]
	 *
	 * @param   [type]  $db_table  [description]
	 *
	 * @return  [type]             [description]
	 */
	function userSelectedAttr($db_table)
	{
		$db    = JFactory::getDBO();
		 $query = " SELECT plan_id
			FROM " . $db->quoteName('#__osmembership_subscribers') . " AS a
			WHERE " . $db->quoteName('plan_subscription_status') . " =  " . $db->quote(1) . "
				AND " . $db->quoteName('user_id') . " = " . $db->quote(JFactory::getUser()->id);
		$db->setQuery($query);
		$plan_id = (int) $db->loadResult();

		 $query = "SELECT attribute FROM " . $db->quoteName("#__mica_user_attribute") . "
			WHERE " . $db->quoteName('aec_plan_id') . " = " . $db->quote($plan_id) . "
				AND " . $db->quoteName('dbtable') . " LIKE " . $db->quote($db_table);
		$db->setQuery($query);
		$row = $db->loadResult();
		$attr = explode(",", $row);

		return $attr;
	}
	function sldPopup ($searchvarable, $searchvalue, $basetable, $searchtextvariable)
	{
		$session         = JFactory::getSession();
		$db              = JFactory::getDBO();
		$activeworkspace = $session->get('activeworkspace');
		$finalarray      = array();
		
		/*SELECT sld.*, sld.layerfill, t1.distshp , t2.* FROM `#__mica_map_sld` AS sld , `india_information` AS t1 , `#__mica_sld_legend` AS t2 WHERE `sld`.`text` = `t1`.`distshp` AND `t1`.`OGR_FID` = '347' AND `profile_id` = '401' AND `t2`.`workspace_id` = `profile_id` AND `sld`.`layerfill` = `t2`.`color` GROUP BY `t2`.`level` DESC Array ( )*/



		$query = "SELECT sld.*, sld.layerfill, t1." . $searchtextvariable . " , t2.*
			FROM " . $db->quoteName('#__mica_map_sld') . " AS sld ,
				" . $db->quoteName("villages_shapes") . " AS t1 ,
				" . $db->quoteName('#__mica_sld_legend') . " AS t2
			WHERE " . $db->quoteName('sld.text') . " = " . $db->quoteName("t1." . $searchtextvariable) . "
				AND " . $db->quoteName("t1.OGR_FID") . " = " . $db->quote($searchvalue) . "
				AND " . $db->quoteName('profile_id') . " = " . $db->quote($activeworkspace) . "
				AND " . $db->quoteName('t2.workspace_id') . " = " . $db->quoteName('profile_id') . "
				AND " . $db->quoteName('sld.layerfill') . " = " . $db->quoteName('t2.color') . "
			GROUP BY " . $db->quoteName('t2.level') . " DESC ";
		
		$db->setQuery($query);
		$list = $db->loadAssocList();
		/*print_r($list);
		die();*/
		foreach ($list as $eacharray)
		{
			 $query = "SELECT name
				FROM " . $db->quoteName('#__mica_user_custom_attribute') . "
				WHERE " . $db->quoteName('attribute') . " = " . $db->quote($eacharray['custom_formula']) . "
					AND " . $db->quoteName('profile_id') . " = " . $db->quote($eacharray['profile_id']);
			
			$db->setQuery($query);
			$customformulaname = $db->loadResult();

			if ($customformulaname == '')
			{
				$customname = array("custom_name" => "");
			}
			else
			{
				$customname = array("custom_name" => $customformulaname);
			}

			$eacharray = array_merge($customname, $eacharray);

			$query = "SELECT (" . $eacharray['custom_formula'] . ") as customval FROM villages WHERE OGR_FID =" . $searchvalue;
			
			
			$db->setQuery($query);

			$customformulavalue = $db->loadResult();
			
			$customvalue  = array("custom_value" => $customformulavalue);
			$eacharray    = array_merge($customvalue, $eacharray);
			$finalarray[] = $eacharray;
		}

		return $finalarray;
	}

	public function popupAttributes()
	{
		$session    = JFactory::getSession();
		$attributes = $session->get('villagesattributes');
		$villages   = $session->get('villages_villages');
		$urban      = $session->get('urban');
		$id         = $this->input->get("id", '', 'raw');
		$id         = trim($id);
		$zoom       = $this->input->get("zoom", '', 'raw');

		$searchvariable  = $this->districtsearchvariable;
		$district_orgfid = $session->get('district_orgfid');
		$session->set('district_orgfid', $district_orgfid . "," . $id);
		$sld = $this->sldPopup($searchvariable, $id, "villlages_shapes", "place_name");
		$str    = "<div style='min-height:150px' ><table class='popupmaintable ' cellspacing='0' cellpadding='0' border='1'>";
		$header = "";
		$sld    = array_reverse($sld);

		if (count($sld) > 0)
		{
			$header .= "<table class='popupheader' style='width:400px'><tr>";
			$header .= "<td class='popupleft'>Name</td>";
			$header .= "<td class='popupright'>" . $sld[0]["text"] . "</td>";
			$header .= "</tr></table>";

			foreach ($sld as $eachsld)
			{
				if ($eachsld['custom_name'] == "")
				{
					$name = $eachsld['custom_formula'];
				}
				else
				{
					$name = $eachsld['custom_name'];
				}

				$str .= "<tr >";
				$str .= "<td >" . JTEXT::_($name) . "</td>";
				$str .= "<td >" . $eachsld['custom_value'] . "</td>";

				if ($eachsld['level'] != 0)
				{
					$color = str_replace("#", "", $eachsld['color']);
					$str .= "<td ><img src='" . JURI::base() . "components/com_mica/maps/img/layer" . $eachsld['level'] . "/pin" . $color . ".png' /></td>";
				}
				else
				{
					$str .= "<td style='background:" . $eachsld['color'] . ";'></td>";
				}

				$str .= "</tr>";
			}
		}
		else
		{
			
			
			$db      = JFactory::getDBO();

			$query = "select ".$attributes.",concat(State_name,'-',district_name,'-',Sub_District_Name,'-',place_name) as name from villages where OGR_FID='".$_GET['id']."'";
			$db->setQuery($query);
			$data = $db->loadObject();
// State ---- district name ----sub-district name ----village name
			//$data   = $model->getCustomData(1, 1);

			$header = "";
			$cnt = count((array)$data);
			$cnt = (int)($cnt/8);
			$i=1;
			$str.="<tr>";
			foreach ($data as $key => $val)
			{
						if (strstr($key, "name"))
						{
							$header .= "<table class='popupheader' ><tr>";
							$header .= "<td class='popupleft'>" . JTEXT::_($key) . "</td>";
							$header .= "<td class='popupright' style='width:500px;'>" . $val . "</td>";
							$header .= "</tr></table>";
						}
						else
						{
							$key=ucfirst(strtolower(str_replace("_", " ", $key)));
							
							
							$str .= "<td class='popupleft'>" . JTEXT::_($key) . "</td>";
							$str .= "<td class='popupright'>" . $val . "</td>";
							if($i==$cnt)
							{
								$str .= "</tr><tr>";
								$i=0;
							}

							$i++;
							
						}
					
				
			}
		}

		$str .= "</table></div>";
		echo $header . $str;
		exit;
	}


	 public function getCustomAttrNameThematic($name,$activeworkspace) {

		$db    = JFactory::getDBO();
		// $activeworkspace = $session->get('activeworkspace');
		$query = "SELECT name, attribute FROM ".$db->quoteName('#__mica_user_custom_attribute')."
		WHERE ".$db->quoteName('profile_id')." = ".$db->quote($activeworkspace)."
			AND ".$db->quoteName('attribute')." LIKE ".$db->quote($name);
		$db->setQuery($query);
		$result = $db->loadAssoc();
		print_r($result);
		
		if(count($result) == 0){
			return array($name,$name);
		}else{
			return array($result[0],$result[1]);
		}
	}
	public function legendtable() {
		$db              = JFactory::getDBO();
		$session    = JFactory::getSession();
		$activeworkspace = $session->get('activeworkspace');
		$query = "SELECT * FROM ".$db->quoteName('#__mica_sld_legend')."
			WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace)."
			ORDER BY ".$db->quoteName('level').", ".$db->quoteName('range_from')." ASC";
		$db->setQuery($query);
		$result = $db->loadObjectList();				
		$id    = array();
		$level = array();
		$i     = 1;
		$j     = 1;
		$grouping = array();
		$groupingtable = array();

		foreach($result as $range){
			if($range->level == "0"){
				$str = " <div class='col_".$i."' style='height:10px;width:10px;background-color:".$range->color.";float:right;'></div></li>";
				$strtable = "<div class='col_".$i."' style='height:10px;width:10px;background-color:".$range->color.";float:right;'></div>";
				
			}else{
				$pin = str_replace("#","",$range->color);
				$str = " <div class='col_".$i."' style='float:right;'><img src='".JUri::base()."components/com_mica/maps/img/layer".$range->level."/pin".$pin.".png' /></div></li>";
				$strtable ="<img src='".JUri::base()."components/com_mica/maps/img/layer".$range->level."/pin".$pin.".png'  style='max-width:30px' />";
			}

			$grouping[$range->custom_formula][] = "<li class='range_".$range->level."' id='".$range->range_from." - ".$range->range_to."'><div style='float:left;width:80%;'>".$range->range_from." - ".$range->range_to."</div>".$str;
			$groupingtable[$range->custom_formula][] = "<tr><td class='range_".$range->level."' id='".$range->range_from." - ".$range->range_to."'>
			".$range->range_from." - ".$range->range_to."</td><td>".$strtable."</td></tr>";
			//$id[] = $range->custom_formula;
			//$grouping[$range->custom_formula] = $range->level;
			$level[$range->custom_formula][] = $range->level;
			$i++;
			$j++;
		}

		$i            = 0;
		$range        = array();
		$str          = array();
		$strtable          = array();
		$totalcount   = 0;
		$l            = 0;
		$tojavascript = array();
		$grpname = array();

		foreach($grouping as $key => $val){
			$grname         = $this->getCustomAttrNameThematic($key, $activeworkspace);
			$grpname[]      = $grname;
			$str[]          = implode(" ",$val);
			$strtable[]     = implode(" ",$groupingtable[$key]);
			$ranges[]       = count($val);
			$levelunique[]  = $level[$key][0];
			$tojavascript[] = $key;
			$l++;
		}

		$tojavascript = implode(",",$tojavascript);
		$html = "";
		$legendsDiv = '';
		$legendsShort='';
		//echo "<pre />";print_r($str);exit;
		for($i = 0; $i < count($grpname); $i++){
			$legendsDiv .= "<div class='contentblock1 '>
				<div class='blockcontent'>
					<ul class='maingrp bullet-add clear' >
						<li>
							<div class='themehead'>".$grpname[$i][0]."</div>
							<div class='themeedit'>
								<a class='edittheme ".$ranges[$i]."' id='".$grpname[$i][1]."__".$levelunique[$i]."' >
									<img src='".JUri::base()."components/com_mica/images/edit.png' />
								</a>
								<a class='deletegrp ".$levelunique[$i]."' id='del_".$grpname[$i][1]."' >
									&nbsp;&nbsp;&nbsp;<img src='".JUri::base()."components/com_mica/images/delete.png' />
								</a>
							</div>
							<ul class='range' >".$str[$i]."</ul>
						</li>
					</ul>
				</div>
			</div>";

			$legendsShort.="<tr><td colspan='2' class='themehead'>".$grpname[$i][0]."</td></tr>".$strtable[$i];
		}
		 $legendsShort = '<table width="90%">'.$legendsShort."</table>";
		echo $legendsShort;
		exit;
}


}
