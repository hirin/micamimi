<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

require_once(JPATH_ADMINISTRATOR.DS.'components/com_inscripcion/library/tcpdf/tcpdf.php');

/**
 * SEIShowresults view class for Mica.
 *
 * @since  1.6
 */
class MicaViewSEIshowresults extends JViewLegacy
{

	function display($tpl=null)
	{
		$document = JFactory::getDocument();
		$session  = JFactory::getSession();

		$document->setMimeEncoding('application/pdf');

		$pdflegend = $session->get('pdflegend');
		$oupputhtml="<table>
			<tr width='300px;'>
				".$pdflegend."
			</tr>
			<tr width='500px;'>
				<img src='".JUri::base()."userchart/final.png' />
			</tr>
		</table>";
		echo $oupputhtml;exit;
	}
}
