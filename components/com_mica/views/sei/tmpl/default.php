
<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$app = JFactory::getApplication('site');

$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastylefront.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui/css/smoothness/jquery-ui-1.8.18.custom.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/jquery.multiselect.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/jquery-ui.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.css', 'text/css');

$itemid        = 188;
$type1         = $app->input->get('m_type', '', 'raw');
$m_type_rating = $app->input->get("m_type_rating", '', 'raw');
$composite     = $app->input->get("composite", '', 'raw');
/*echo "<pre />";print_r($composite);
echo "<pre />";print_r($this->composite_attr);exit;*/
?>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-1.7.1.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui/js/jquery-ui-1.8.18.custom.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/jquery.multiselect.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.js"></script>
<script type="text/javascript">
	var choosenjq = JQuery.noConflict();
	choosenjq(document).ready(function(){
		choosenjq("select").multiselect({selectedList: 1});
	});
</script>
<script src="<?php echo JURI::base()?>components/com_mica/js/field.js"></script>
<script type="text/javascript">
	var statealert        = '<?php echo JText::_("SELECT_STATE_ALERT")?>';
	var preselected       = '<?php echo $app->input->get("district", "", "raw"); ?>';
	var preselecteddata   = '<?php echo $app->input->get("selected", "", "raw"); ?>';
	var preselectedm_type = '<?php echo $app->input->get("m_type", "", "raw"); ?>';

	x = 0;  //horizontal coord
	y = document.height; //vertical coord
	window.scroll(x,y);
	JQuery(function() {
		JQuery("#tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
		JQuery("#tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
	});
	JQuery(document).ready(function(){ });

	function checkAll(classname){
		JQuery("."+classname).each(function (){
			JQuery(this).attr("checked",true);
		});
	}

	function uncheckall(classname){
		JQuery("."+classname).each(function (){
			JQuery(this).attr("checked",false);
		});
	}
</script>
<div style="height:400px;">
	<form name="adminForm" id="micaform" action="index.php?option=com_mica&view=seishowresults&Itemid=<?php echo $itemid; ?>" method="POST">
		<div class="maincontainer">
			<div class="frontitle">
				<span><h1><?php echo JText::_('SELECT_CR_MSG');?></h1></span>
			</div>
			<div class="halfleft" >
				<div class="leftbottop">
					<div class="maintable">
						<div class="left">
							<h3><?php echo JText::_('STATE_LABEL'); ?></h3>
						</div>
						<div class="right">
							<h3>
								<select name="state[]" id="state" class="inputbox-mainscr" onchange="getfrontdistrict(this)" multiple="multiple">
									<?php /*<option value="">< ?php echo JText::_('PLEASE_SELECT');?></option> */
									$states = explode(",", $app->input->get("state", '', 'raw'));
									for($i = 0; $i < count($this->state_items); $i++){
										$selected = (in_array($this->state_items[$i]->name, $states)) ? " selected " :  ""; ?>
										<option value="<?php echo base64_encode($this->state_items[$i]->id); ?>" <?php echo $selected;?>>
											<?php echo $this->state_items[$i]->name; ?>
										</option>
									<?php  } ?>
								</select>
							</h3>
						</div>
					</div>
					<div class="maintable" >
						<div class="left">
							<h3><?php echo JText::_('DISCTRICT_LABEL'); ?></h3>
						</div>
						<div class="right">
							<h3>
								<span id="districtspan">
									<select name="district[]" id="district" class="inputbox-mainscr" multiple="multiple">
										<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
									</select>
								</span>
							</h3>
						</div>
					</div>
					<div class="maintable" >
						<div class="left">
							<h3>Type</h3>
						</div>
						<div class="right">
							<?php $type = explode(',', $this->typesArray); ?>
							<h3>
								<select	name="m_type[]" id="m_type" class="inputbox" multiple="multiple" >
									<?php if(in_array("Rural", $type)) { ?>
										<option value="Rural" <?php echo (in_array("Rural", $type1)) ? "selected" : "";?> >	<?php echo JText::_('RURAL'); ?>	</option>
									<?php }
									if(in_array("Urban", $type)) { ?>
										<option value="Urban" <?php echo (in_array("Urban", $type1)) ? "selected" : "";?> >	<?php echo JText::_('URBAN'); ?>	</option>
									<?php }
									if(count($type) == 2) { ?>
										<option value="Total" <?php echo (in_array("Total", $type1)) ? "selected" : "";?> >	<?php echo JText::_('ALL_VARIABLE'); ?>	</option>
									<?php } ?>
								</select>
							</h3>
						</div>
					</div>
				</div>
				<div class="leftbottom">
					<div class="maintable" >
						<div class="left">
							<h3>Variable</h3>
						</div>
						<div class="right">
							<h3><div id="statetotal"></div></h3>
						</div>
					</div>
					<div class="maintable">
						<div class="maintable togglingattribute" ></div>
					</div>
					<div class="maintable" >
						<div class="left">
							<h3>Mkt Potential Index</h3>
						</div>
						<div class="right">
							<h3>
								<select	name="m_type_rating[]"  class="inputbox" multiple="multiple" >
									<?php // edited heena 24/06/13 put a condition
									if(in_array("Rural", $type)) { ?>
										<option value="Rural" <?php echo (in_array("Rural",$m_type_rating)) ? "selected" : "";?> >	<?php echo JText::_('RURAL'); ?>	</option>
									<?php }
									if(in_array("Urban", $type)) { ?>
										<option value="Urban" <?php echo (in_array("Urban",$m_type_rating)) ? "selected" : "";?> >	<?php echo JText::_('URBAN'); ?>	</option>
									<?php }
									if(count($type) == 2){?>
										<option value="Total" <?php echo (in_array("Total",$m_type_rating)) ? "selected": (empty($m_type_rating)) ? "selected" : "";?> >	<?php echo JText::_('ALL_VARIABLE'); ?>	</option>
									<?php } // edited end?>
								</select>
							</h3>
						</div>
					</div>
					<div class="maintable" >
						<div class="left">
							<h3>Composite Score</h3>
						</div>
						<div class="right">
							<h3>
								<select name="composite[]" id="composite" class="inputbox-mainscr" multiple="multiple">
									<?php // edited heena 24/06/13 put a condition
									if(in_array("Rural", $type)) {
										$prefix = array("Rural");
									}
									if(in_array("Urban", $type)) {
										$prefix = array("Urban");
									}
									if(count($type) == 2){
										$prefix = array("Rural","Urban","Total");
									} // edited end

									foreach($this->composite_attr as $eachattr){
										echo '<optgroup  label="'.ucwords(strtolower(JTEXT::_($eachattr->field))).'">';
										foreach($prefix as $eachprefix){
											$checked = (in_array($eachprefix."_".$eachattr->field, $composite)) ? " selected " : "";
											echo '<option value="'.$eachprefix."_".$eachattr->field.'" '.$checked.'>'.ucwords(strtolower(JTEXT::_($eachprefix))).'</option>';
										}
										echo '</optgroup>';
									} ?>
								</select>
							</h3>
						</div>
					</div>
				</div>
			</div>
			<div class="halfright" >
				<div class="righttop">
					<div class="maintable" style="line-height: 36px;">
						<div class="right" >
							<h3>Step 1 :  Select State</h3>
						</div>
					</div>
					<div class="maintable" style="line-height: 36px;">
						<div class="right">
							<h3>Step 2 :  Select District of selected State</h3>
						</div>
					</div>
					<div class="maintable" style="line-height: 36px;">
						<div class="right">
							<h3>Step 3 : Select Urban or Rural or Total (Multiple selection allowed)</h3>
						</div>
					</div>
					<div class="maintable" style="line-height: 36px;">
						<div class="right">
							<h3>Step 4 : Select the Variables you want to view. (Multiple selection allowed) </h3>
						</div>
					</div>
					<div class="maintable" style="line-height: 36px;">
						<div class="right">
							<h3>Step 5 : Select MPI (Multiple selection allowed)</h3>
						</div>
					</div>
					<div class="maintable" style="line-height: 36px;">
						<div class="right">
							<h3>Step 6 : Select sector wise composite score. (Multiple selection allowed)</h3>
						</div>
					</div>
					<div class="maintable" style="line-height: 36px;">
						<div class="right">
							<h3><span style="color:black;">Note :</span> Mark  <span style="color:black;"> ‘All’ </span> in Step 3 while you are selecting variables Related to Registered Active company and Rainfall.</h3>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="maintable" style="float:right;width:50%; margin:10px 0px 0px 0px;">
			<div class="readon frontbutton">
				<input type="button" name="reset" onClick="window.location='index.php?option=com_mica&view=sei&reset=1';"
					class="readon button" value="<?php echo JText::_('Reset'); ?>" />
			</div>
			<div class="readon frontbutton">
				<input type="submit" name="submit" id="submit" class="readon button" value="<?php echo JText::_('SHOW_DATA'); ?>" />
			</div>
			<input type="hidden" name="refeterview" value="sei" />
			<input type="hidden" name="option" 		value="com_mica" />
			<input type="hidden" name="zoom" id="zoom" value="6" />
			<input type="hidden" name="view" 		value="seishowresults" />
			<input type="hidden" name="Itemid" 		value="<?php echo $itemid;?>" />
			<input type="hidden" id="comparedata" 	value="0" />
		</div>

	</form>
</div>
