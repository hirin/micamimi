<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * MICA Dashboard model.
 *
 * @since  1.6
 */
class MicaModelMicafront extends JModelLegacy
{
	var $_data       = null;
	var $_total      = null;
	var $_pagination = null;
	var $districtsearchvariable = "OGR_FID";

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	/**
	 *
	 */
	public function getStateItems(){
		$db    = $this->getDBO();
		$query = "SELECT state as id,state as name
			FROM ".$db->quoteName('india_information')."
			WHERE ".$db->quoteName('state')." IS NOT NULL
				AND ".$db->quoteName('state')." != ".$db->quote("Null")."
			GROUP BY ".$db->quoteName('state');
		$db->setQuery($query);
		return $db->loadObjectList();
	}

	/**
	 *
	 */
	public function getcompositeAttr(){
		$db    = $this->getDBO();
		$query = "SELECT mgf.field, mgf.id FROM ".$db->quoteName('#__mica_group_field')." 	AS mgf
				INNER JOIN ".$db->quoteName('#__mica_group')." 	AS gp
					ON 		".$db->quoteName('mgf.groupid')." = ".$db->quoteName('gp.id')."
						AND ".$db->quoteName('gp.group')." LIKE ".$db->quote('Score')."
			ORDER BY ".$db->quoteName('field')." ASC ";
		//select * from #__mica_group gp,#__mica_group_field mgf  where gp.`group` like 'Composite Score' and gp.id=mgf.groupid
		$db->setQuery($query);
		return $db->loadObjectList();
	}

	/**
	 *
	 */
	public function getAttributeType() {
		$db     = $this->getDBO();
		$userid = JFactory::getUser()->id;

		//edited heena 27/06/13 if child user then rural-urban comes from parent plan
		$sel = "SELECT login_userid FROM ".$db->quoteName('#__mica_adduser')." WHERE ".$db->quoteName('created_userid')." = ".$db->quote($userid);
		$db->setQuery($sel);
		$login_userid = (int) $db->loadResult();

		$subscriber_user_id = ($login_userid > 0) ? $login_userid : $userid;

		$query = "SELECT plan_id FROM ".$db->quoteName('#__osmembership_subscribers')." WHERE ".$db->quoteName('user_id')." = ".$db->quote($subscriber_user_id);
		$db->setQuery($query);
		$planid = $db->loadResult();

		// edited end
		$typeQry = "SELECT ".$db->quoteName('type')."
			FROM ".$db->quoteName('#__mica_user_attribute_type')."
			WHERE ".$db->quoteName('aec_plan_id')." = ".$db->quote($planid);
		$db->setQuery($typeQry);
		return $db->loadResult();
	}
	/**
	 * A function called from view.
	 */
	public function getCustomData($infotype = null, $pagefromcontroller = null){

		$session = JFactory::getSession();
		$app     = JFactory::getApplication();
		$db      = $this->getDBO();

		$states       = $session->get('state');
		$mystates     = explode(",",$states);
		$mystates     = array_filter($mystates);
		$statesinsert = implode("','",$mystates);

		$district     = $session->get('district');
		$town         = $session->get('town');
		$urban        = $session->get('urban');
		$m_types      = $session->get('m_type');

		$attributes   = $session->get('attributes');
		$attributes   = explode(",",$attributes);

		//if(substr_count($district,"362")) { $district=$district.",363"; }
		$mytmp = array_filter($attributes);
		if($pagefromcontroller != ""){
			$page        = 13;
			$statingpage = 0;
		}else{
			$page        = ($app->input->get("limitstart") != "" ) ? $app->input->get("limitstart") : 0;
			$statingpage = null;
			//$this->setState('limitstart',$page); $this->setState('limit',13);
		}

		$db_table   = "";
		$attributes = array();
		$attribute  = "";
		if($town == "" && $urban == ""){

			//if($district!="")
			$db_table = "india_information";
		 	// else
		 	// 	$db_table="rail_state";

			foreach($m_types as $m_type){
				foreach($mytmp as $eachvar){
					if($m_type == "Total" ){
						if(strpos($eachvar, $this->urnbanprefix)=== false && strpos($eachvar, $this->ruralprefix)=== false &&  strpos($eachvar, $this->totalprefix)=== false){
							$attributes[] = $this->totalprefix.$eachvar;
						}else{
							$attributes[] = $eachvar;
						}
					}else if($m_type == "Rural"){
				 		//  $db_table="india_information";
						if(strpos($eachvar, $this->urnbanprefix)=== false && strpos($eachvar, $this->ruralprefix)=== false && strpos($eachvar, $this->totalprefix)=== false){
				 	  		$attributes[] = $this->ruralprefix.$eachvar;
						}else{
							$attributes[] = $eachvar;
						}
				 	}else if($m_type == "Urban"){
						//$db_table     = "india_information";
						//$attributes[] = $this->urbanprefix.$eachvar;
						if(strpos($eachvar, $this->urnbanprefix)=== false && strpos($eachvar, $this->ruralprefix)=== false &&  strpos($eachvar, $this->totalprefix)=== false){
				 	  		$attributes[] = $this->urnbanprefix.$eachvar;
						}else{
						 	$attributes[] = $eachvar;
						}
				 	}else{
						//$attributes[] = implode(",",$mytmp);
				 	}
				}
			}
		}

		$attributes = implode(",",$attributes);
		$session->set('themeticattribute',$attributes);

		//echo $attributes=$session->get('themeticattribute');exit;
		if(trim($district) == null || trim($district) == ","){
			if($states != "all," && $states != ""  && $states != "all"){
				$mystates = explode(",",$states);
				$mystates = array_filter($mystates);
				$mystates = implode("','",$mystates);
				$where    = "WHERE ".$db->quoteName('name')." IN ('".$mystates."') ";
				if($session->get('state_orgfid') != ""){
					$state_orgfid = $session->get('state_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1    = explode(",", $state_orgfid);
					$mystates1    = array_filter($mystates1);
					$mystates1    = implode("','", $mystates1);
					$where       .= " AND ".$this->statesearchvariable." IN ('".$mystates1."')";
				}
			}else{
				$mystates = "1=1";
				$where    = "";
				if($session->get('state_orgfid') != ""){
					$state_orgfid = $session->get('state_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1    = explode(",", $state_orgfid);
					$mystates1    = array_filter($mystates1);
					$mystates1    = implode("','", $mystates1);
					$where       .= " WHERE ".$this->statesearchvariable." IN ('".$mystates1."') ";
				}
			}

			$query = "SELECT ".$attributes." , ".$this->statesearchvariable." , name
				FROM ".$db->quoteName('rail_state')."
				".$where."
				GROUP BY ".$db->quoteName('name')."
				ORDER BY ".$db->quoteName('name');
			$db->setQuery($query);

			$session->set('activetable',"rail_state");
			$session->set('activenamevariable',"name");
			$session->set('activesearchvariable',$this->statesearchvariable);
			if($infotype != ""){
				//$session->set('popupactivedata',$db->loadObjectList());
			}else{
				//$session->set('activedata',$db->loadObjectList());
			}

			try {
				$this->_data = $db->loadObjectList();
			} catch (Exception $e) {
				$this->_data = NULL;
			}
			return $this->_data;

		}else{

			//echo $attributes=implode(",",$attributes);exit;
			if($district != "all," && $district != "all"){
				$states   = explode(",",$states);
				$states   = array_filter($states);
				$states   = implode("','",$states);

				$district = explode(",",$district);
				$district = array_filter($district);
				$district = implode("','",$district);

				$where    = "WHERE ".$this->districtsearchvariable." IN ('".$district."') AND ".$db->quoteName('state')." IN ('".$states."') ";
				if($session->get('district_orgfid') != ""){
					$state_orgfid = $session->get('district_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1    = explode(",", $state_orgfid);
					$mystates1    = array_filter($mystates1);
					$mystates1    = implode("','", $mystates1);
					$where       .= " AND ".$this->districtsearchvariable." IN ('".$mystates1."') ";
				}
			}else{

				$states = explode(",",$states);
				$states = array_filter($states);
				$states = implode("','",$states);
				$where  = "WHERE 1 = 1 AND ".$db->quoteName('state')." IN ( '".$states."')";

				if($session->get('district_orgfid') != ""){
					$state_orgfid = $session->get('district_orgfid');
					//condition will be true when geoserver served ORG_FID, because of many shapes in db requested thes query
					$mystates1    = explode(",",$state_orgfid);
					$mystates1    = array_filter($mystates1);
					$mystates1    = implode("','",$mystates1);
					$where       .= " AND ".$this->districtsearchvariable." IN ('".$mystates1."')";
				}
			}

			$query = "SELECT ".$attributes." ,".$this->districtsearchvariable.", concat(distshp,'-',state) as name, state, distshp
				FROM ".$db->quoteName('india_information')."
				".$where."
					AND ".$db->quoteName('distshp')." <> ''
				ORDER BY ".$db->quoteName('state')." ASC
				LIMIT ".(($statingpage === null) ? (($page)*50) : $statingpage).",".(($statingpage === null) ? 50 : 650);
			$db->setQuery($query);


			$session->set('activetable', "india_information");
			$session->set('activenamevariable', "distshp");
			$session->set('activesearchvariable',$this->districtsearchvariable);
			if($infotype != ""){
				//$session->set('popupactivedata',$db->loadObjectList());
			}else{
				//$session->set('activedata',$db->loadObjectList());
			}

			try {
				$this->_data = $db->loadObjectList();
			} catch (Exception $e) {
				$this->_data = NULL;
			}
			return $this->_data;
		}

	}

	/**
	 * A function called from view.
	 */
	public function getcustomattrlib(){
		$session = JFactory::getSession();
		$db      = $this->getDBO();

		$activetable = $session->get("activetable");

		$query = "SELECT name, attribute
			FROM ".$db->quoteName('#__mica_user_custom_attribute_lib')."
			WHERE ".$db->quoteName('profile_id')." = ".$db->quote($session->get("activeworkspace"))."
				AND ".$db->quoteName('tables')." LIKE ".$db->quote($activetable);

		$db->setQuery($query);
		$allcostomattr = $db->loadAssocList();

		$str = "";
		foreach($allcostomattr as $eachcustomattr){
			$eachcustomattr['attribute'] = str_replace("'"," ",$eachcustomattr['attribute']);
			$str .= $eachcustomattr['name'].":".$eachcustomattr['attribute'].",";
		}
		$session->set('customattributelib',$str);
	}

	/**
	 * GetIndustryLevelGroups Method to get Industry Level Group list
	 *
	 * @return  array of database object/false
	 */
	/*public function getIndustryLevelGroups()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);

		// Create the base select statement.
		$query = "SELECT " . $db->quoteName('id') . ',' . $db->quoteName('group') . "
			FROM ".$db->quoteName('#__mica_industry_level_group')."
			WHERE ".$db->quoteName('publish')." = ".$db->quote(1);

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$groups = $db->loadObjectList();

			if (!empty($groups))
			{
				foreach ($groups as $key => $group)
				{
					$options[] = JHTML::_('select.option', $group->id, $group->group);
				}
			}

			$indLvlGrpList = JHTML::_('select.genericlist', $options, 'indlvlgrps[]', 'id="indlvlgrps" class="inputbox" multiple="multiple" onchange="getindlvlgrpvar()"', 'value', 'text',
				explode(',', JFactory::getSession()->get('indlvlgrps')));

			return $indLvlGrpList;
		}
		catch (RuntimeException $e)
		{
			JError::raiseWarning(500, $e->getMessage());
		}

		return false;
	}*/

	/**
	 * GetIndustryLevelGroups Method to get Industry Level Group list
	 *
	 * @return  array of database object/false
	 */
	public function getIndustryLevelGroups()
	{
		// Initialiase variables.
		$db    = JFactory::getDbo();
		$query = $db->getQuery(true);
		$preselected = explode(',', JFactory::getSession()->get('indlvlgrps'));

		// Create the base select statement.
		$query = "SELECT " . $db->quoteName('id') . ',' . $db->quoteName('group') . "
			FROM ".$db->quoteName('#__mica_industry_level_group')."
			WHERE ".$db->quoteName('publish')." = ".$db->quote(1);

		// Set the query and load the result.
		$db->setQuery($query);

		try
		{
			$groups = $db->loadObjectList();
			$returndata = '';
			if (!empty($groups))
			{
				//$returndata.='<ul class="list1 industryLevelList" onchange="getindlvlgrpvar(this.value)">';
				foreach ($groups as $key => $group)
				{
					for ($i = 0; $i < count($group); $i++)
					{
						if (in_array($group->id, $preselected))
						{
							$selected = "checked";
						}
						else
						{
							$selected = "";
						}

						$returndata.='<li><input type="radio" class="industryLevel_checkbox" name="indlvlgrps" value="'.$group->id.'" '.$selected.' id="indlvlgrps_check_'.$group->id.'" ><label for="indlvlgrps_check_'.$group->id.'">'.$group->group.'</label></li>';
					}
				}
			}
			else
			{
				$returndata.='<ul class="list1 districtlist"></ul>';
			}
			//$returndata .='</ul>';

				return $returndata;
			}
		catch (RuntimeException $e)
		{
			JError::raiseWarning(500, $e->getMessage());
		}

		return false;
	}

	public function planAuthentication()
	{
		$userid = JFactory::getUser()->id;
		$get_detail =JFactory::getUser();
		$parent_user= $get_detail->parent_user;



		$db    = $this->getDBO();

 $query = "SELECT os.allow_districtdata FROM " . $db->quoteName('#__osmembership_plans') . " AS op
				INNER JOIN " . $db->quoteName('#__osmembership_subscribers') . " AS os
					ON " . $db->quoteName('op.id') . " = " . $db->quoteName('os.plan_id') . "
						AND (" . $db->quoteName('os.user_id') . " LIKE " . $db->quote($userid) ;
						if($parent_user!='' && $parent_user!=0)
						{	
							$query.= " 
						or ". $db->quoteName('os.user_id') . " LIKE " . $parent_user . ")";
						}
						else {
							$query.=")";
						}
						$query.=" AND os.published = 1 ORDER BY " . $db->quoteName('os.id') . " DESC ";
/*echo str_replace("#__", "pqwoe_", $query);
die();*/
		$db->setQuery($query);

		$allow_districtdata = $db->loadResult();
		//print_r($allow_districtdata);

		return $allow_districtdata;
	}

}
