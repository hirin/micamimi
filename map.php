<?php

 error_reporting( E_ALL );
    /**
     * @package    Joomla.Site
     *
     * @copyright  Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
     * @license    GNU General Public License version 2 or later; see LICENSE.txt
     */

    /**
     * Define the application's minimum supported PHP version as a constant so it can be referenced within the application.
     */
    define('JOOMLA_MINIMUM_PHP', '5.3.10');

    if (version_compare(PHP_VERSION, JOOMLA_MINIMUM_PHP, '<'))
    {
        die('Your host needs to use PHP ' . JOOMLA_MINIMUM_PHP . ' or higher to run this version of Joomla!');
    }

    /**
     * Constant that is checked in included files to prevent direct access.
     * define() is used in the installation folder rather than "const" to not error for PHP 5.2 and lower
     */
    define('_JEXEC', 1);

    if (file_exists(__DIR__ . '/defines.php'))
    {
        include_once __DIR__ . '/defines.php';
    }

    if (!defined('_JDEFINES'))
    {
        define('JPATH_BASE', __DIR__);
        require_once JPATH_BASE . '/includes/defines.php';
    }

    require_once JPATH_BASE . '/includes/framework.php';
    $db  = JFactory::getDBO();
    // Create a new query object.
    $query = $db->getQuery(true);

    
    $_GET['sid']='24';
    $villageOGR = "";
    $districtsOGR ="OGR_FID in ('000')";


    if($_GET['sid']!='')
    {

      $state_name = "SELECT distinct state FROM `india_information` where StateCode = '".$_GET['sid']."'";
      $db->setQuery($state_name);
      $statename = $db->loadRow();


      $query = "SELECT AsText((india_state)) as poly
        FROM ".$db->quoteName('rail_state')."
        WHERE ".$db->quoteName('name')." IN ('".$statename[0]."') ";
        $db->setQuery($query);
        $initialGeometry = $db->loadRow();

      


        $states = "SELECT GROUP_CONCAT(CONCAT('''', OGR_FID, '''' )) as a FROM `india_information` where StateCode = '".$_GET['sid']."'";
        $db->setQuery($states);
        $statesData = $db->loadRow();
        
        if($statesData[0]!='')
        {
          
        $districtsOGR = "OGR_FID in (".$statesData[0].")";
        }
        
          
          $dist = "SELECT DISTINCT district, District_name FROM `villages_shapes` WHERE OGR_FID!='' AND state='".$_GET['sid']."' order by District_name";

           $db->setQuery($dist);
          $distData = $db->loadObjectList();


          if($_GET['did']!='' && $_GET['sid']=='24'){
            //$qVillage = "SELECT  OGR_FID as a FROM `villages_shapes` where OGR_FID!='' and state='".$_GET['sid']."' AND district = '".$_GET['did']."'";
            
            /*$db->setQuery($qVillage);
            $villageData = $db->loadObjectList();
            $string ='';
            foreach ($villageData as $key => $value) {
              $string.="'".$value->a."',";
              
            }*/
            
         
            $villageOGR = "district in ('".$_GET['did']."')";
            $villageLayer = 'villages_shapes_24';
         
          }
          
          

         
     
    }
    

    $qDist = "SELECT DISTINCT StateCode,State FROM `india_information` group by StateCode";
       
    $db->setQuery($qDist);
    $u = $db->loadObjectList(); 



?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Mimi Map</title>

    <!-- The line below is only needed for old environments like Internet Explorer and Android 4.x -->
      
    <link rel="stylesheet" href="./newmap/ol2.css" type="text/css">
    <script src="<?php echo JURI::base()?>components/com_mica/js/jquery-3.3.1.js"></script>
    <script src="https://www.mica-mimi.in//components/com_mica/maps/OpenLayers.js"></script>
    <!-- <script src="https://www.mica-mimi.in/newmap/ol2.js"></script> -->

    <!-- <script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=fetch,requestAnimationFrame,Element.prototype.classList,URL"></script> -->
    <style>
      .map {
        width: 100%;
        height:650px;
        border:1px solid red;
      }
    </style>
  </head>
  <body>
    <form type="get" id="frm" name="frm" style="padding: 10px;">
    <label for="sid">Select State </label>
    <select name="sid" id="sid"  disabled="disabled">
      <option>Select State....</option>
      <?php foreach($u as $v){ 
          if($v->StateCode=='')
            continue;
        ?>
        <option value="<?php echo $v->StateCode;?>" 
          <?php echo ('24'==$v->StateCode) ? 'selected="selected"' :  ''; ?>  
          ><?php echo $v->State;?></option>
      <?php } ?>
    </select>


    <label for="sid">Select District </label>
    <select name="did" id="did">
      <option>Select District....</option>
      
       <?php foreach($distData as $v){ 
          if($v->district=='')
            continue;
        ?>
        <option value="<?php echo $v->district;?>" 
          <?php echo ($_GET['did']==$v->district) ? 'selected="selected"' :  ''; ?>  
          ><?php echo $v->District_name;?></option>
      <?php } ?>
    </select>

    </form>
    <div id="map" class="map"></div>    
  </body>
  <script type="text/javascript">
var map, layer;

     var bounds = new OpenLayers.Bounds(-180.0,-85.0511,180.0,85.0511);
     var mainzoom =5;
     var sldlinkDistricts = 'http://www.mica-mimi.in/geoserver/www/styles/micamimi18/2642/147/india_information2.sld';
     var sldlinkVillage = 'http://www.mica-mimi.in/geoserver/www/styles/micamimi18/2642/147/village.sld';
     var mainGeometry = '<? echo $initialGeometry;?>';
    
    jQuery(document).ready(function($) {

      
      //jQuery('#map').height(height)

      jQuery('#sid').change(function($) {
        document.frm.submit();
      });

      jQuery('#did').change(function($) {
        document.frm.submit();
      });

     

     mapinit();
     var tomcaturl = 'http://www.mica-mimi.in:8080/geoserver/mimi/wms';
      var options = {
              controls      : [],
              maxExtent     : bounds,
              //projection    : "EPSG:32644",
              projection    : "EPSG:32644",
              maxResolution : 'auto',
              zoom          : mainzoom,
              units         : 'degrees'
          };
        
          map = new OpenLayers.Map('map', options);
          var opverviewland = new OpenLayers.Layer.WMS("State Boundaries overview",
              tomcaturl,
              {
                  Layers           : 'rail_state',
                  transparent      : false,
                  //projection    : "EPSG:4326",
                  format           : 'image/png',
                  styles           : "dummy_state",
                  transitionEffect : 'resize'
              },
              {
                  isBaseLayer: true
              }
          );
           

          var districts = new OpenLayers.Layer.WMS("districts",
              tomcaturl,
              {
                  Layer       : 'india_information',
                  transparent : true,
                  //projection    : "EPSG:4326",
                  format      : 'image/png',
                  CQL_FILTER  : "<?php echo $districtsOGR;?>",
                  SLD         : sldlinkDistricts
              },
              {
                  isBaseLayer: false
              },
              {
                  transitionEffect: 'resize'
              }
          );

          map.addLayers([opverviewland]);
          map.addLayers([districts]);


          <?php if ($villageOGR!=''){ ?>
          var villages = new OpenLayers.Layer.WMS("villages",
              tomcaturl,
              {
                  Layer       : '<?php echo $villageLayer;?>',
                  transparent : true,
                  projection    : "EPSG:32644",
                  format      : 'image/png',
                  CQL_FILTER  : "<?php echo $villageOGR;?>",
                  SLD         : sldlinkVillage
              },
              {
                  isBaseLayer: false
              },
              {
                  transitionEffect: 'resize'
              }
          );
         

          map.addLayers([villages]);
        <?php } ?>
          
          map.zoomToMaxExtent();
          map.addControl(new OpenLayers.Control.PanZoomBar({
              position : new OpenLayers.Pixel(5, 15)
          }));

          map.addControl(new OpenLayers.Control.Navigation({
              dragPanOptions: {enableKinetic: true}
          }));


          /*if(mainzoom==5 || mainzoom==6 ){
                
            }else if(mainzoom==7){
                map.addLayers([districts]);
                districts.redraw(true);
            }else if(mainzoom==8){
                map.addLayers([urban]);
            }else{
                map.addLayers([cities]);
            }*/

            map.div.oncontextmenu = function noContextMenu(e) {
              if (OpenLayers.Event.isRightClick(e)){
                  displaymenu(e);
              }
              // return false; //cancel the right click of brower
            };

  //          var click = new OpenLayers.Control.Click();
/*          map.addControl(click);
          click.activate();
          map.zoomTo(mainzoom);
*/
          /*if(mainGeometry != 0 && mainGeometry != "")
          {
              var format    = new OpenLayers.Format.WKT();
              var feature   = format.read(mainGeometry);
              var homepoint = feature.geometry.getCentroid();
          }*/

          map.addLayers([opverviewland]);
          map.zoomToExtent(bounds);
          map.setCenter(new OpenLayers.LonLat(71.706289,22.866185), mainzoom);



         
    });


    
    function mapinit(){
      OpenLayers.Control.Click = OpenLayers.Class(OpenLayers.Control, {
              defaultHandlerOptions: {
                  'single'         : true,
                  'double'         : false,
                  'pixelTolerance' : 0,
                  'stopSingle'     : true,
                  'stopDouble'     : false
              },
              initialize: function(options) {
                  this.handlerOptions = OpenLayers.Util.extend(
                      {}, this.defaultHandlerOptions
                  );

                  OpenLayers.Control.prototype.initialize.apply(
                      this, arguments
                  );

                  this.handler = new OpenLayers.Handler.Click(
                      this, {
                          'click':this.getfeaturenfo,'dblclick': this.onDblclick
                      }, this.handlerOptions
                  );
              },
              onDblclick: function(e) {
                  JQuery(".olPopup").css({"display":"none"});
                  JQuery("#featurePopup_close").css({"display":"none"});
                  //changeAttrOnZoom(map.zoom);
              },
              getfeaturenfo :function(e) {
              coordinates = e;
              var params  = {
                      REQUEST       : "GetFeatureInfo",
                      projection    : "EPSG:32644",
                      EXCEPTIONS    : "application/vnd.ogc.se_xml",
                      BBOX          : map.getExtent().toBBOX(10),
                      SERVICE       : "WMS",
                      INFO_FORMAT   : 'text/html',
                      QUERY_LAYERS  : selectlayer(map.zoom),
                      FEATURE_COUNT : 6,
                      Layers        : selectlayer(map.zoom),
                      WIDTH         : map.size.w,
                      HEIGHT        : map.size.h,
                      X             : parseInt(e.xy.x),
                      Y             : parseInt(e.xy.y),
                      CQL_FILTER    : selectfilter(),
                      srs           : map.layers[0].params.SRS
                  };

                  // handle the wms 1.3 vs wms 1.1 madness
                  if(map.layers[0].params.VERSION == "1.3.0") {
                      params.version = "1.3.0";
                      params.i       = e.xy.x;
                      params.j       = e.xy.y;
                  } else {
                      params.version = "1.1.1";
                      params.y       = parseInt(e.xy.y);
                      params.x       = parseInt(e.xy.x);
                  }
                  OpenLayers.loadURL(tomcaturl, params, this, onFeatureSelect, onFeatureSelect);
                  var lonLat = new OpenLayers.LonLat(e.xy.x, e.xy.y) ;
                  
                  //lonLat.transform(map.displayProjection,map.getProjectionObject());
                  //map.setCenter(lonLat, map.zoom);
                  map.panTo(lonLat);
                  //map.addControl(ovControl);
                  //OpenLayers.Event.stop(e);
              }
          });
    } 



function onFeatureSelect(response)
    {
      response = response.trim("");

      var getsegment=response.split("OGR_FID:");
      var id;

      if(typeof(getsegment[1])=="undefined")
      {
        return false;
      }
      else
      {
        id = getsegment[1].trim();
        alert(getsegment[3]);
        if(typeof(getsegment[2])!="undefined" ){
          if(getsegment[2] == 362 || getsegment[2] == 344 || getsegment[2] == 205 || getsegment[2] == 520 || getsegment[2] == 210 || getsegment[2] == 211 || getsegment[2] == 206){
            id = getsegment[2].trim();
          }

            if(getsegment[3] == 209){
              id = getsegment[3].trim();
            }
          if(getsegment[4] == 209){
            id = getsegment[4].trim();
          }

          if(getsegment[3] == 207){
            id = getsegment[3].trim();
          }

          if(getsegment[1] == 205){
            id = getsegment[1].trim();
          }

          if(getsegment[1] == 206 && getsegment[2] == 207 && getsegment[3] == 212){
            id = getsegment[2].trim();
          }
        }
      }
    }

    function selectfilter(){
  if(javazoom==5 || javazoom==6 ){
    return stateCqlFilter();
  }else if(javazoom==7){
    return districtCqlFilter();
  }else  if(javazoom==8){
    return urbanCqlFilter();
  }else{
    return cityCqlFilter();
  }
}
function selectlayer(cuurzoom){
  if(javazoom==5 || javazoom==6){
    return "rail_state";
  }else if(javazoom==7){
    return "india_information";
  }else  if(javazoom==8){
    return "jos_mica_urban_agglomeration";
  }else{
    return "my_table";
  }
}


  </script>
    
</html>