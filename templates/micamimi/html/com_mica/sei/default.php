<?php
/**
 * edited  by salim
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
$doc     = JFactory::getDocument();
$app     = JFactory::getApplication('site');
$db      = JFactory::getDbo();
$session = JFactory::getSession();

$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastylefront.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/css/micastyle.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui/jquery-ui.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui-multiselect-checkbox/prettify.css', 'text/css');

$doc->addStyleSheet(JURI::base().'components/com_mica/js/jquery-ui/css/smoothness/jquery-ui-1.8.18.custom.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/css/jquery.jscrollpane.css', 'text/css');

// For Vertical Slider
$doc->addStyleSheet(JURI::base().'components/com_mica/js/slick/slick.css', 'text/css');
$doc->addStyleSheet(JURI::base().'components/com_mica/css/ol.css', 'text/css');
$itemid          = 188;
$type1           = $app->input->get('m_type', '', 'raw');
$m_type_rating   = $app->input->get("m_type_rating", '', 'raw');
$composite       = $app->input->get("composite", '', 'raw');
$custolibmarray  = explode(",",$session->get('customattributelib'));
$customattribute = $session->get('customattribute');
$customarray     = explode(",",$customattribute);
$finalarray      = array(array_filter($customarray), array_filter($custolibmarray));
?>

<script src="<?php echo JURI::base()?>components/com_mica/js/Chart.bundle.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/utils.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-3.3.1.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.jscrollpane.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.mousewheel.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-ui/js/jquery-ui-1.8.18.custom.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/newjs/jquery-ui.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/newjs/ol.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery.cookie.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/colorpicker.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/raphael.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.speedometer.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/jquery.jqcanvas-modified.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/speed/excanvas-modified.js"></script>
<script src="<?php //echo JURI::base()?>components/com_mica/maps/OpenLayers.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/jquery-migrate-1.2.1.js"></script>
<!-- dowlode pdf -->
<script src="<?php echo JURI::base()?>components/com_mica/js/jspdf.min.js"></script>
<!-- For Vertical Slider -->
<script src="<?php echo JURI::base()?>components/com_mica/js/slick/slick.min.js"></script>
<script type="text/javascript">
	var JQuery = jQuery.noConflict();
	var $      = jQuery.noConflict();
</script>
<script src="<?php echo JURI::base()?>components/com_mica/js/custom_sei.js"></script>
<script src="<?php echo JURI::base()?>templates/micamimi/js/jquery.scrollbar.js"></script>
<!-- start bubble chart js-->
<script src="<?php echo JURI::base()?>components/com_mica/js/d3.min.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/bubble_chart.js"></script>
<script src="<?php echo JURI::base()?>components/com_mica/js/canvg.min.js"></script>
<!-- end bubble chart js-->

<style type="text/css">
	is-multiple {display:block}
	canvas {
		-moz-user-select: none;
		-webkit-user-select: none;
		-ms-user-select: none;
	}
	.btn-disabled,
	.btn-disabled[disabled] {
	cursor: default !important;
	pointer-events: none;
	}
	/* Tooltip container */

</style>

<script type="text/javascript">
	var filealert          = '<?php echo JText::_("SELECT_FILE_ALERT")?>';
	var statealert         = '<?php echo JText::_("SELECT_STATE_ALERT")?>';
	var districtalert      = '<?php echo JText::_("SELECT_DISTRICT_ALERT")?>';
	var industryLevelalert = '<?php echo JText::_("Please select IndustryLevel")?>';
	var typealert          = '<?php echo JText::_("SELECT_TYPE_ALERT")?>';
	var variablealert      = '<?php echo JText::_("SELECT_VARIABLE_ALERT")?>';
	var mpiealert          = '<?php echo JText::_("SELECT_MPI_ALERT")?>';
	var swcsalert          = '<?php echo JText::_("SELECT_SWCS_ALERT")?>';
	var metervariabel      = '<?php echo JText::_("Please Select variabel first!!!")?>';
	var meterdistrict      = '<?php echo JText::_("Please Select district first!!!")?>';
	var preselected        = '<?php echo $app->input->get("district", "", "raw"); ?>';
	var preselecteddata    = '<?php echo $app->input->get("selected", "", "raw"); ?>';
	var preselectedm_type  = '<?php echo $app->input->get("m_type", "", "raw"); ?>';
	var chart1;
	var keys               = [];
	var trashedLabels      = [];
	var trashedData        = [];
	var datasetValues      = [];
	var flag               = false;
	var is_updatecustom    = 0;
	var itemid             = <?php echo $itemid; ?>

	x = 0;  //horizontal coord
	y = document.height; //vertical coord
	window.scroll(x,y);
	jQuery(function() {
		jQuery("#tabs").tabs().addClass('ui-tabs-vertical ui-helper-clearfix');
		jQuery("#tabs li").removeClass('ui-corner-top').addClass('ui-corner-left');
	});

	function checkAll(classname){
		jQuery("."+classname).each(function (){
			jQuery(this).attr("checked",true);
		});
	}

	function uncheckall(classname){
		jQuery("."+classname).each(function (){
			jQuery(this).attr("checked",false);
		});
	}

	var user_id = <?php echo $session->get('user_id'); ?>

    jQuery(document).ready(function(){
        jQuery('.scrollbar-inner').scrollbar();

        if (user_id == 0) {
        	window.location.replace(window.location.href+'?reload');
        	//location.reload();
        	//window.location = 'index.php?option=com_users&view=login&Itemid=108';
        }
    });
    console.firebug=true;

</script>

<div class="row-fluid top-row">
	<div class="col-md-4">
		<div class="sidebar-toggle-box" title="Fullscreen">
          	<i class="fa fa-bars" aria-hidden="true"></i>
      	</div>
		<h1 class="title-explore"><?php echo JText::_('SEI');?></h1>
	</div>
	<div class="col-md-8 text-right">
		<div class="sorter-tab">
			<ul>
				<li><a href="javascript:void(0);" id="tabledata" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-table"></i><?php echo JText::_('TEXT_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="loadchart" class="actionbtn btn-disabled" disabled="disabled"><i class="far fa-chart-bar"></i><?php echo JText::_('GRAPHS_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="gisdata" class="actionbtn btn-disabled" disabled="disabled"><i class="fas fa-map-marker-alt"></i><?php echo JText::_('GIS_TAB_LABEL'); ?></a></li>
				<li><a href="javascript:void(0);" id="matrix" class="actionbtn btn-disabled" disabled="disabled" ><i class="fas fa-list-ul"></i><?php echo JText::_('Matrix'); ?></a></li>

				<!--
					<li><a href="javascript:void(0);" id="tabledata" class="actionbtn btn-disabled" disabled="disabled" onclick="getDataNew();"><i class="fas fa-table"></i><?php echo JText::_('TEXT_TAB_LABEL'); ?></a></li>
					<li><a href="javascript:void(0);" id="loadchart" class="actionbtn btn-disabled" disabled="disabled" onclick="loadCharts(null, null, null, 1);"><i class="far fa-chart-bar"></i><?php echo JText::_('GRAPHS_TAB_LABEL'); ?></a></li>
					<li><a href="javascript:void(0);" id="gisdata" class="actionbtn btn-disabled" disabled="disabled" onclick="loadGis('gis');"><i class="fas fa-map-marker-alt"></i><?php echo JText::_('GIS_TAB_LABEL'); ?></a></li>
					<li><a href="javascript:void(0);" id="matrix" class="actionbtn btn-disabled" disabled="disabled" ><i class="fas fa-list-ul"></i><?php echo JText::_('Matrix'); ?></a></li>
				-->

				<li><a href="javascript:void(0);" id="pmdata" class="actionbtn btn-disabled" disabled="disabled" ><i class="fas fa-tachometer-alt"></i><?php echo JText::_('Speedometer'); ?></a></li>
			</ul>
		</div>
		<div class="worspacce-edit">
			My workspace <a class="workspacce-edit" href="javascript:void(0)" onclick="document.getElementById('workspacceedit').style.display='block';document.getElementById('fade').style.display='block'">
				<i class="fas fa-pencil-alt"></i>
			</a>
			<h2>
				<div id="activeworkspacename">
			</div>
			</h2>
		</div>
	</div>
</div>

<div id="workspacceedit" style="display:none;" class="white_content2 large-popup">
	<div class="divclose">
		<a href="javascript:void(0);" onclick="document.getElementById('workspacceedit').style.display='none';document.getElementById('fade').style.display='none';">
			<img src="media/system/images/closebox.jpeg" alt="X">
		</a>
	</div>
	<div class="poptitle">
		<?php echo JTEXT::_('MY_WORKSPACE');?>
	</div>

	<div class="scroll_div">
		<div class="col-md-8">
			<div class="blockcontent">
				<ul class="allworkspace">
					<?php
						$this->user = JFactory::getUser();
						$id = $this->user->id;

						if($id == 0){
							return -1;
						}

						$db   = JFactory::getDBO();
						$query = "SELECT name, id, is_default
							FROM ".$db->quoteName('#__mica_user_workspace')."
							WHERE ".$db->quoteName('userid')." = ".$db->quote($id)."
								AND ".$db->quoteName('is_default')." <> ".$db->quote(1) . "AND" . $db->quoteName('data') . " REGEXP '.*\"dataof\";s:[0-9]+:\"District\".*'";

						$db->setQuery($query);
						$result = $db->loadAssocList();

						$app               = JFactory::getApplication('site');
						$session = JFactory::getSession();
						$str               = "";

						$activeworkspace   = $session->get('activeworkspace');
						$selected          = "";
						$activeprofilename = $eachresult['name'];
						$i                 = 1;
						foreach($result as $eachresult){
							if(($i%2)==0){
								$clear="clear";
							}else{
								$clear="";
							}

							if($activeworkspace == $eachresult['id']){

								$selected          = "selected";
								$edit              = "Edit/Delete";
								$activeprofilename = $eachresult['name'];
								echo "<script type='text/javascript'>JQuery('#activeworkspacename').html('<font style=\'font-weight:normal;\'> ".$activeprofilename."</font>  ');</script>";
								$onclick = "document.getElementById('lightn').style.display='block';document.getElementById('fade').style.display='block'";
								echo "<input type='hidden' value='".$eachresult['id']."' id='profile' />";
								$active_text = "<span style='font-weight:normal;'>(Active)<span>";
								$active_text = "";

							}else{

								$selected    = "";
								$edit        = "Select";
								$onclick     = "changeWorkspace(".$eachresult['id'].")";
								$active_text = "";
							}

							$str .= '
								<li class="'.$clear." ".$selected.'">
									<div style="width:70%;float:left;">
										<label class="workspacelabel">'.$eachresult['name'].$active_text.'</label>
									</div>
									<div style="width:30%;float:right;">
										<a href="#" onclick="'.$onclick.'">'.$edit.'</a>
									</div>
								</li>';// '.$selected.'
						}
						echo $str;
						echo '<div id="lightn" class="white_contentn" style="display: none;">
						<div class="divclose">
							<a href="javascript:void(0);" onclick="document.getElementById(\'lightn\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';">
								<img src="media/system/images/closebox.jpeg" alt="X" />
							</a>
						</div>
						<div align="left">
							<input type="textbox" id="updateworkspacetext" value="'.$activeprofilename.'" class="inputbox"/>
							<div class="frontbutton readon">
								<input type="button" id="updateworkspace" onClick="JQuery(\'#new_w_txt\').val(JQuery(\'#updateworkspacetext\').val())" Value="Update" class="frontbutton readon"/>
							</div>
							<div class="frontbutton">
								<input type="button" name="new"  id="deleteworkspace"  value="Delete" class=""/>
							</div>
						</div>';

					?>
				</ul>
			</div>
		</div>

		<div class="col-md-4">
			<div class="contentblock endcontent">
				<div class="contenthead">New Workspace</div>
				<div class="blockcontent newworkspace">
					<input type="text"  id="new_w_txt" class="inputbox" value="<?php echo $activeprofilename;?>"  class="newtextbox"/>
					<input type="button" name="Create" class="newbutton frontbutton" value="Create" id="createworkspace" />
				</div>
			</div>
		</div>
	</div>
</div>

<div class="explore-data">
	<form name="adminForm" id="micaform" action="" method="POST">
		<div class="row">
			<div class="col-md-4 left-section">
	            <ul class="nav nav-tabs" role="tablist">
	                <li role="presentation" class="active" id="selectfile_tab" title="Select File">
	                	<a href="#tabselectfile" aria-controls="selectfile" role="tab" data-toggle="tab">
	                		<em class="icon-swcs"></em>Select File
	                	</a>
	                </li>
	                <li role="presentation" data-id="state" id="state_tab" title="State" style="">
	                	<a href="#tabstate" aria-controls="state" role="tab" data-toggle="tab">
	                		<em class="icon-state"></em>State
	                	</a>
	                </li>
	                <li role="presentation" data-id="district" title="District" style="">
	                	<a href="#tabdistrict" aria-controls="district" role="tab" data-toggle="tab" id="district_tab">
	                		<em class="icon-district"></em>District
	                	</a>
	                </li>
	                <li role="presentation" data-id="sub district" title="Sub District" style="display: none;">
	                	<a href="#tabsubdistrict" aria-controls="sub_district" role="tab" data-toggle="tab" id="sub_district_tab">
	                		<em class="icon-district"></em>Sub District
	                	</a>
	                </li>
	                <li role="presentation" data-id="village" title="Village" style="display: none;">
	                	<a href="#tabvillage" aria-controls="village" role="tab" data-toggle="tab" id="village_tab">
	                		<em class="icon-district"></em>Village
	                	</a>
	                </li>
	                <li role="presentation" data-id="variables" title="Variables" style="">
	                	<a href="#tabvariables" aria-controls="variables" role="tab" data-toggle="tab" id="variable_tab">
	                		<em class="icon-variables"></em>Variables
	                	</a>
	                </li>
	                <!-- <li role="presentation" title="Industry Level Group">
	                	<a href="#tabIndustryLevel" aria-controls="IndustryLevel" role="tab" data-toggle="tab" id="industry_level_tab">
	                		<em class="icon-district"></em>Industry Level Group
	                	</a>
	                </li>
	                <li role="presentation" title="Type">
	                	<a href="#tabtype" aria-controls="type" role="tab" data-toggle="tab" id="type_tab">
	                		<em class="icon-type"></em>Type
	                	</a>
	                </li>
	                <li role="presentation" title="Variables">
	                	<a href="#tabvariables" aria-controls="variables" role="tab" data-toggle="tab" id="variable_tab">
	                		<em class="icon-variables"></em>Variables
	                	</a>
	                </li>
	                <li role="presentation" title="Market Potential Index">
	                	<a href="#tabmpi" aria-controls="mpi" role="tab" data-toggle="tab" id="mpi_tab">
	                		<em class="icon-mpi"></em>MPI
	                	</a>
	                </li>
	                <li role="presentation" title="Composite Score">
	                	<a href="#tabswcs" aria-controls="swcs" role="tab" data-toggle="tab" id="swcs_tab">
	                		<em class="icon-swcs"></em>SWCS
	                	</a>
	                </li> -->
	            </ul>

	            <!-- START Tab panes -->
	            <div class="tab-content leftcontainer" id="leftcontainer">
	            	<!-- tabselectfile start -->
	                <div role="tabpanel" class="tab-pane active" id="tabselectfile">
		                <div class="loader-div" id="loader" style="display: none">
					    	<img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
						</div>
	                	<div id="tabselectfilespan">
							<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('SELECTFILE_LABEL'); ?></h3>
		                			</div>
		                		</div>
	                		</div>
							<div class="row">
								<div class="col-md-12 col-xs-12">
									<select name="exportFile" id="exportFile" class="rsform-select-box">
										<option value="">Select File</option>
										<?php
										foreach ($this->export_files as $key => $value) {
											echo '<option value="'.$value->id.'">'.$value->name.'</option>';
										}
										?>
									</select>
								</div>
							</div>
	                    </div>
	                </div>
					<!-- tabselectfile End -->
	            	<!-- State start -->
	                <div role="tabpanel" class="tab-pane" id="tabstate">
		                <div class="loader-div" id="loader" style="display: none">
					    	<img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
						</div>
	                	<div id="statespan">
							<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('STATE_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="state_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="state_checkall" class="allcheck" id="state_allcheck">
		                				<label for="state_allcheck"></label>
		                				|
		                				<a href="javascript:void(0);" onclick="sortListDir('id01')">
			                					<i class="fa fa-sort"></i>
			                				</a>
		                			</div>
		                		</div>
	                		</div>
							<div class="scrollbar-inner">
	                            <ul class="list1 statelist" id="id01">
	                            	<?php
									$states = explode(",", $app->input->get("state", '', 'raw'));

										for($i = 0; $i < count($this->state_items); $i++){
											$checked = (in_array($this->state_items[$i]->name, $states)) ? " checked " :  ""; ?>
											<li>
												<input type="checkbox" class="state_checkbox" name="state[]" value="<?php echo base64_encode($this->state_items[$i]->id); ?>" <?php echo $checked;?> id="check<?php echo $i;?>">
												<label for="check<?php echo $i;?>"><?php echo $this->state_items[$i]->name; ?></label>
											</li>
									<?php  } ?>
								</ul>
	                    	</div>
	                    </div>
	                </div>
					<!-- State End -->

					<!-- district start -->
	                <div role="tabpanel" class="tab-pane" id="tabdistrict">
	                	<div class="loader-div" id="loaderdistrict" style="display: none">
				     		<img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
						</div>

	                	<div id="districtspan">
							<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('DISTRICT_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="district_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="district_checkall" class="allcheck" id="district_allcheck">
		                				<label for="district_allcheck"></label>
		                				|
		                				<a href="javascript:void(0);" onclick="district_sortListDir()">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-10">
	                				<div class="scrollbar-inner">
			                            <ul class="list1 districtlist" id="district_list">
										</ul>
									</div>
	                			</div>
	                			<div class="col-md-2 padding-none">
	                				<div class="dist-icon-list">
	                					<div id= "statecode" >
	                					</div>
	                				</div>
	                			</div>
	                		</div>
						</div>
	                </div>

	                <!-- Industry Level Group Start-->
	                <div role="tabpanel" class="tab-pane" id="tabIndustryLevel">
		                <div class="loader-div" id="loader" style="display: none">
					    	<img src="images/loader.gif" style="position: relative;top: 50%;margin-top: -24px;">
						</div>
	                	<div id="IndustryLevelSpan">
							<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('INDUSTRY_LEVEL_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="industryLevel_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<a href="javascript:void(0);" onclick="sortListDir('industryLevelList')">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
	                		</div>
							<div class="scrollbar-inner">
								<ul class="list1 industryLevellist" id="industryLevelList" onchange="getindlvlgrpvar(this.value)">
	                            	<?php echo $this->industrylevelgroups;?>
	                            </ul>
	                    	</div>
	                    </div>
	                </div>

	               	<!-- type start -->
	            	<div role="tabpanel" class="tab-pane" id="tabtype">
	               		<div id="typespan">
	                    	<div class="top-sorter-div">
	                			<div class="row">
	                				<div class="col-md-5 col-xs-5">
	                					<h3><?php echo JText::_('TYPE_LABEL'); ?></h3>
	                				</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="type_searchtop" placeholder="Search">
		                				</div>

		                				<input type="checkbox" name="type_checkall" class="allcheck" id="type_allcheck">
		                				<label for="type_allcheck"></label>

		                				<a href="javascript:void(0);" onclick="sortListDir('typelist')">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
	                			</div>
	            			</div>
							<?php $type = explode(',', $this->typesArray); ?>
	                    	<ul class="list1 typelist" id="typelist">
	                    		<li>
		                           <?php if(in_array("Rural", $type)) { ?>
									<input type="checkbox"	 id="m_type_rural" class="type_checkbox" name="m_type[]" value="Rural" <?php echo (in_array("Rural", $type1)) ? "checked" : "";?> id="m_type_rural">

				                      <label for="m_type_rural"><span><?php echo JText::_('RURAL'); ?></span></label>
				                      <?php } ?>
								</li>
								<li>
									<?php
				               		if(in_array("Urban", $type)) { ?>
										<input type="checkbox" class="type_checkbox" name="m_type[]" value="Urban" <?php echo (in_array("Urban", $type1)) ? "checked" : "";?> id="m_type_urban">
										<label for="m_type_urban"><span><?php echo JText::_('URBAN'); ?></span></label>
									<?php } ?>
								</li>
								<li>
									<?php if(count($type) == 2) { ?>
										<input type="checkbox"	 id="m_type_all" class="type_checkbox" name="m_type[]" value="Total" <?php echo (in_array("Total", $type1)) ? "checked" : "";?> id="m_type_variabel">
										<label for="m_type_all"><span><?php echo JText::_('ALL_VARIABLE'); ?></span></label>
			                     	<?php } ?>
								</li>
							</ul>
	              		</div>
					</div>
					<!-- type END-->

	                <!-- variables start-->
	          		<div role="tabpanel" class="tab-pane" id="tabvariables">
	                	<div id="variablespan">
		                	<div class="top-sorter-div">
		                		<div class="row">
		                			<div class="col-md-5 col-xs-5">
		                				<h3><?php echo JText::_('VARIABLE_LABEL'); ?></h3>
		                			</div>
		                			<div class="col-md-7 col-xs-7 text-right">
		                				<div class="searchbox">
		                					<input type="search" class="searchtop" id="variable_searchtop" placeholder="Search">
		                				</div>
		                				|
		                				<input type="checkbox" name="villages_checkall" class="allcheck" id="variable_allcheck">
		                				<label for="viarable_allcheck"></label>
		                				|
		                				<a href="javascript:void(0);" onclick="variable_sortListDir()">
		                					<i class="fa fa-sort"></i>
		                				</a>
		                			</div>
		                		</div>
	                		</div>
	                		<div class="row">
	                			<div class="col-md-10">
	                				<div class="scrollbar-inner">
				                		<div id="statetotal"></div>
				                		<div id="variables"></div>
				                	</div>
	                			</div>
	                			<div class="col-md-2" style="display: none;">
	                				<div id="variableshortcode"></div>
	                			</div>
	                		</div>
		                </div>
		            </div>
	              	<!-- variables END-->

	                <!-- mpi start -->
	                <div role="tabpanel" class="tab-pane" id="tabmpi">
						<div class="top-sorter-div">
	                		<div class="row">
	                			<div class="col-md-5 col-xs-5">
	                				<h3><?php echo JText::_('MPI LABEL'); ?></h3>
	                			</div>
	                			<div class="col-md-7 col-xs-7 text-right">
	                				<div class="searchbox">
	                					<input type="search" class="searchtop" id="mpi_searchtop" placeholder="Search">
	                				</div>
	                				|
	                				<input type="checkbox" name="mpi_checkall" class="allcheck" id="mpi_allcheck">
	                				<label for="mpi_allcheck"></label>
	                				|
	                				<a href="javascript:void(0);" onclick="sortListDir('mpilist')">
	                					<i class="fa fa-sort"></i>
	                				</a>
	                			</div>
	                		</div>
	            		</div>

	                   	<ul class="list1 mpilist" id="mpilist">
	                    	<li>
	                    		<?php if(in_array("Rural", $type)) { ?>
								<input type="checkbox" id="m_type_rural1" class="mpi_checkbox" name="m_type_rating[]" value="Rural" <?php echo (in_array("Rural",$m_type_rating)) ? "checked" : "";?> >

			                    <label for="m_type_rural1"><span><?php echo JText::_('RURAL'); ?></span></label>
			                    <?php } ?>
	                    	</li>
	                    	<li>
	                    		<?php
				               if(in_array("Urban", $type)) { ?>
									<input type="checkbox" id="m_type_urban1" class="mpi_checkbox" name="m_type_rating[]" value="Urban" <?php echo (in_array("Urban",$m_type_rating)) ? "checked" : "";?> >
									<label for="m_type_urban1"><span><?php echo JText::_('URBAN'); ?></span></label>
									<?php } ?>
	                    	</li>
	                    	<li>
	                    		<?php if(count($type) == 2) { ?>
									<input type="checkbox"	 id="m_type_all1" class="mpi_checkbox" name="m_type_rating[]" value="Total" <?php echo (in_array("Total",$m_type_rating)) ? "checked": (empty($m_type_rating)) ? "checked" : "";?> >

			                     <label for="m_type_all1"><span><?php echo JText::_('ALL_VARIABLE'); ?></span></label>
			                     <?php } ?>
	                    	</li>
	                    </ul>
	                </div>
	               	<!-- mpi END -->

	                <!-- swcs start -->
	                <div role="tabpanel" class="tab-pane" id="tabswcs">
						<div class="top-sorter-div">
	                		<div class="row">
	                			<div class="col-md-5 col-xs-5">
	                				<h3><?php echo JText::_('SWCS LABEL'); ?></h3>
	                			</div>
	                			<div class="col-md-7 col-xs-7 text-right">
	                				<div class="searchbox">
	                					<input type="search" class="searchtop" id="swcs_searchtop" placeholder="Search">
	                				</div>
	                				|
	                				<input type="checkbox" name="swcs_checkall" class="allcheck" id="swcs_allcheck">
	                				<label for="state_allcheck"></label>
	                				|
	                				<a href="javascript:void(0);" onclick="swsc_sortListDir()">
	                					<i class="fa fa-sort"></i>
	                				</a>
	                			</div>
	                		</div>
	            		</div>
	                    <div class="scrollbar-inner">
							<?php // edited heena 24/06/13 put a condition
							if(in_array("Rural", $type)) {
								$prefix = array("Rural");
							}
							if(in_array("Urban", $type)) {
								$prefix = array("Urban");
							}
							if(count($type) == 2){
								$prefix = array("Rural","Urban","Total");
							} // edited end
							$x=0;
                             echo '<ul class="list1 swcslist">';
							foreach($this->composite_attr as $eachattr){
								echo '<li class="variable_group"><label class="swsc_label">'.ucwords(strtolower(JTEXT::_($eachattr->field))).'</label></li>';
									$keys= ucwords(strtolower(JTEXT::_($eachattr->field)));
									$class=strtolower(str_replace(" ", "__", $keys));
                                   foreach($prefix as $eachprefix){
									$checked = (in_array($eachprefix."_".$eachattr->field, $composite)) ? " checked " : "";
									echo '<li class="'.$class.'"><input type="checkbox" name="composite[]" value="'.$eachprefix."_".$eachattr->field.'" '.$checked.' id="variable_check_'.$x.'" class="swcs_checkbox">';

									echo '<label for="variable_check_'.$x.'">'.ucwords(strtolower(JTEXT::_($eachprefix))).'</label></li>';
									 $x++;
								}
							}
							 echo '</ul>';
							 ?>
						</div>
	                </div>
	                <!-- swcs END -->
	            </div>
	            <!-- END Tab panes -->
			</div>
			<div class="col-md-8 right-section">
				<div class="full-data-view">
					<div id="result_table" style="display:none">
						<div class="text contenttoggle1" id="textscroll">
							<div class="button-area text-right">
								<a href="#alldata" onclick="downloadAction();"><i class="fas fa-download"></i><?php echo JText::_('DOWNLOAD_XLS_LABEL'); ?></a>
							</div>
							<div class="collpsable1"></div>
							<div class="activecontent right-scroll-div tableview" >
								<div class="alldata contenttoggle2" id="jsscrollss alldata"></div>
								<?php echo "<br /><br />"; ?>
							</div>
						</div>
					</div>

					<!--Start Graph -->
					<div id="graph" style="display:none;">
						<div class="graphs contenttoggle1">
							<div class="row">
								<div class="col-md-4">
									<div class="chart_type">
										<label>Chart Type:</label>
										<select name="charttype" class="inputbox" id="chartype" onchange="loadCharts(null, null, null, 1);">
											<option value="bar" selected>Bar Chart</option>
											<option value="line">line Chart</option>
											<option value="radar">Radar Chart</option>
											<option value="bubble">Bubble Chart</option>
											<option value="bar">Secondary Axis</option>
										</select>
										<label>Show Legend</label>
										<input type="checkbox" id="enablelegend" name="enablelegend"  value="1" checked/>
									</div>
								</div>
								<div class="col-md-5"></div>
								<div class="col-md-3 graphpopup">
									<div id="graphfunction" class="white_content2 large-popup variable_add_popup" style="display: none;">
										<div class="frontbutton btn-create">
											<input type="button" name="Cancel" id="Clear" class="" value="Clear">
											<input type="button" name="apply_changes" id="graph_apply_changes" class="" value="Apply">
										</div>
										<div class="divclose">
											<a id="closeextra" href="javascript:void(0);" onclick="document.getElementById('graphfunction').style.display='none';document.getElementById('fade').style.display='none';">
												<img src="media/system/images/closebox.jpeg" alt="X" />
											</a>
										</div>
										<div class="graphFilter">
											<ul class="nav">
												<li id="graphDistrict" class="active"><a data-toggle="tab" href="#graphDistrictTab">Districts</a></li>
												<li id="graphVariables"><a data-toggle="tab" href="#graphVariablesTab" >Variables</a></li>
												<li id="secondLevelGraphvariable" style="display:none;"><a data-toggle="tab" href="#secndLvlGrphVarTab" >Secondary Axis Variables</a></li>
												<!-- <li id="secondLevelGraphDistrict"><a data-toggle="tab" href="#secndLvlGrphDistTab" >Second Level Graph Districts</a></li> -->
											</ul>
											<div class="tab-content">
											    <div id="graphDistrictTab" class="tab-pane fade in active">
											    	<div class="poptitle">
														Select District
													</div>
													<div id="cblist"></div>
											    </div>

											    <div id="graphVariablesTab" class="tab-pane fade">
											    	<div class="poptitle">
														Select Variable
													</div>
													<div id="variabelist"></div>
											    </div>

												<div id="secndLvlGrphVarTab" class="tab-pane fade">
											    	<div class="poptitle">
														Secondary Axis Variables
													</div>
											     	<div id="secondLevelVariabelist"></div>
											    </div>

											    <!-- <div id="secndLvlGrphDistTab" class="tab-pane fade">
											    	<div class="poptitle">
											    														Second Level Graph Districts
											    													</div>
											     	<div id="secondLevelDistrictlist"></div>
											    </div> -->

											</div>
										</div>
									</div>
									<div class="button-area-2">
										<div id="filterChartButton" class="exportbtn">
											<input type="button" id="graphfunction" onClick="document.getElementById('graphfunction').style.display='block';document.getElementById('fade').style.display='block'"  class="button btn-red" value="Filters">
										</div>
										<div id="exportchartbutton" class="exportbtn">
											<input type="button"  name="downloadchart" id="export" class="button" value="Export" />
										</div>
									</div>
									<!-- <div class="button-area-2">
										<div id="exportchartbutton" class="exportbtn">
											<input type="button"  name="downloadchart" id="export" class="button" value="Export" />
										</div>
									</div> -->
								</div>
							</div>
							 <canvas id="myChart"></canvas>
							 <div id="venn" style="width:100%; height:100%; background-color:#FFFFFF;display:none;" ><svg></svg></div>
							<div id="venn_label"></div>
							<!-- end of amcharts script -->
						</div>
					</div>
					<!--End Graph -->

					<!-- START GIS -->
					<div id="gis" style="display:none;">
						<div class="gis contenttoggle1">
							<div class="text contenttoggle1">
								<div class="button-area text-right">
									<a href="javascript:void(0);" onclick="downloadMapPdf_v2();" id="options" class="frontbutton" style="text-decoration: none;">
									<input type="button" class="frontbutton" name="Export" value="Export">
									</a>
								</div>
								<div class="button-area text-right">
									<?php  $fromthematic = $session->get('fromthematic'); ?>
									<a href="javascript:void(0);" id="fullscreen" class="frontbutton" <?php if($fromthematic==1){ ?> fromthematic="1" <?php }else{?> fromthematic="0"  <?php }?> >Full Screen
											<!-- <input style="margin-right: 3px;" type="button" class="frontbutton" name="fullscreen" value="Full Screen"> -->
									</a>
								</div>
								<div class="button-area text-right">
									<a href="javascript:void(0);" id="thematic_btn" class="frontbutton" onclick="openthematicQuery()">Thematic Query
										<!-- <input style="margin-right: 3px;" type="button" class="frontbutton" name="thematicButton" value="Thematic Query"> -->
									</a>
								</div>
							</div>
							<div class="map_controllers"></div>
							<div id="map" style=" height:400px;width: 100%;"></div>
						</div>
					</div>
					<!-- END GIS -->

					<!-- START Matrix -->
					<div id="quartiles" class="matrix contenttoggle1">
						<div id="fullscreentable"></div>
					</div>
					<!-- END MATRIX -->

					<!-- START SPEED -->
					<div id="potentiometer" style="display:none;">
						<div class="speedometer contenttoggle1" >
							<div class="filter-section">
								<div style="display:none;" id="speedfiltershow"></div>
								<div class="sfilter">
									<div class="top-filter-sec">
										<div class="fl_title">
											<span class="filterbylabel"></span>
										</div>
										<div class="fl_type radio-list fl_right">
											<!-- start popup for potentiometer Filters-->
											<div id="variablefiltetr" style="display:none;" class="white_content2 filterpopup">
												<div class="divclose">
													<a href="javascript:void(0);" data-me="variablefiltetr" class="closeme1">
													<img src="media/system/images/closebox.jpeg" alt="X">
													</a>
												</div>
												<div class="frontbutton btn-create">

													<input type="button" name="showspeed" id="showspeed" class="disrictchnage" value="Create">
												</div>
												<div class="filterbylabel"></div>
												<div class="sfilter">
													<div>
														<div class="lft speed" id="potentiolMeter" style="display:none;">
															<div class="frontbutton ">
																<input type="button" name="showspeed" id="showspeed" class="" value="Create">
															</div>
														</div>
													</div>
												</div>
											</div>
											<!-- end popup for potentiometer Filters-->
											<ul>
												<li>
													<a href="javascript:void(0);" id="filter1" data-val="0"  class="filterspeed">Filter By  Layer</a>
												</li>
												<li>
													<a href="javascript:void(0);" id="filter2"  data-val="1"  class="filterspeed">Filter By  Variable</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<div id="spedometer_region" class="right-scroll-div spedometer_sec"></div>
						</div>
					</div>
					<!-- END SPEED -->

                    <!-- START DEFAULT3 -->
					<div id="default3" class="default3 contenttoggle1">
						<p>Step 1 : Select State</p>
						<p>Step 2 : Select District of selected State</p>
						<p>Step 3 : Select Urban or Rural or Total (Multiple selection allowed)</p>
						<p>Step 4 : Select the Variables you want to view. (Multiple selection allowed)</p>
						<p>Step 5 : Select MPI (Multiple selection allowed),</p>
						<p>Step 6 : Select sector wise composite score. (Multiple selection allowed)</p>
						<p>Note : Mark ‘All’ in Step 3 while you are selecting variables Related to Registered Active company and Rainfall.</p>
					</div>
					<!-- END DEFAULT3 -->

					<!-- START DEFAULT -->
					<div id="default" class="white_content2 alert-variable">
						<div class="popup_alert">
							<div class="default contenttoggle1" >
								<div class="divclose">
									<a href="javascript:void(0);" onclick="document.getElementById('default').style.display='none';document.getElementById('fade').style.display='none';">
										<img src="media/system/images/closebox.jpeg" alt="X">
									</a>
								</div>
								<div class="box-white">
									<p>Please select variables from each of the following dimensions to view a report. You can select from left panel or by clicking the links above.</p>
								</div>
								<ul class="list1">
									<li id="statetext"><i class="far fa-check-square"></i>State</li>
									<li id="districttext"><i class="far fa-check-square"></i>District</li>
									<!-- <li id="typetext"><i class="far fa-check-square"></i>Type</li> -->
									<li id="variabletext"><i class="far fa-check-square"></i>Variable</li>
								</ul>

							    <div align="right">
							    	<a href="javascript:void(0);" id="apply_chnages" class="actionbtn btn-disabled btn-submit" disabled="disabled"><i class="fas fa-table"></i>Apply</a>
							    </div>
							</div>
						</div>
					</div>
					<!-- END DEFAULT -->

					<!-- START DEFAULT1 -->
					<div id="default1" class="white_content2 alert-variable" style="display:none;">
						<div class="popup_alert">
							<div class="divclose">
								<a href="javascript:void(0);" onclick="document.getElementById('default1').style.display='none';">
									<img src="media/system/images/closebox.jpeg" alt="X">
								</a>
							</div>
							<div class="box-white">
								<p>Selections have been modified. Click on "Apply" at any time to refresh the report with the changes made. Otherwise, click on "Cancel" to go back to previous selections.</p>
							</div>
							<div align="right">
							 	<a href="javascript:void(0);" id="applyChangesInitial" class="actionbtn btn-disabled btn-submit" disabled="disabled">
							 		<i class="fas fa-table"></i>Apply
							 	</a>
							</div>
						</div>
					</div>
					<!-- END DEFAULT1 -->
			   	</div>
			</div>

			<div class="maintable" style="float:right;width:50%; margin:10px 0px 0px 0px;">
				<input type="hidden" name="apply_chnages_val" id="apply_chnages_val" value="0" />
				<input type="hidden" name="refeterview" value="micafront" />
				<input type="hidden" name="option" 		value="com_mica" />
				<input type="hidden" name="zoom" id="zoom" value="6" />
				<input type="hidden" name="view" 		value="seishowresults" />
				<input type="hidden" name="Itemid" 		value="<?php echo $itemid;?>" />
				<input type="hidden" id="comparedata" 	value="0" />
				<input type="hidden" id="viewRecordPlanLimit" value="0" />
				<input type="hidden" id="downloadAllowedPlanLimit" value="0" />
				<input type="hidden" id="logo_pdf_chart" value="<?php echo JPATH_BASE."/templates/mica/images/logo_pdf_chart.png" ?>" />
				<input type="hidden" id="oldattrval" 		value="" / >
			</div>

			<!-- custom variable calcultor start-->
			<div id="light" class="white_content2 large-popup variable_add_popup" style="display: none;">
				<div class="divclose">
					<a id="closeextra" href="javascript:void(0);" onclick="document.getElementById('light').style.display='none'; undofield();">
						<img src="media/system/images/closebox.jpeg" alt="X" />
					</a>
				</div>

				<script type="text/javascript">
					$('#exportFile').on('change',function(e){
						var fileId = $(this).val();
						$.ajax({
							url: 'index.php?option=com_mica&task=sei.getFillterable',
							type: 'POST',
							data: {
								fileId: fileId,
							},
							success : function (res) {
								/*if (res.length) {
									var result = JSON.parse(res);
									for(var i = 0; i < result.length; i++) {
										$("[data-id="+result[i].name+"]").show();
									}
									$("[data-id=variables]").show();
								}*/
							}
						});
						
					});
					var lastchar = '';
					function moverightarea(){
						//var val      = document.getElementById('custom_attribute').innerHTML;
						var val = jQuery("#custom_attribute").val();
						var selected = 0;
						for(i = document.adminForm.avail_attribute.options.length-1; i >= 0; i--){
							var avail_attribute = document.adminForm.avail_attribute;
							if(document.adminForm.avail_attribute[i].selected){
								selected = 1;
								if(lastchar=='attr'){
									alert("<?php echo JText::_('CONSECUTIVE_ATTR_ALERT');?>");
								}else{
									//document.getElementById('custom_attribute').innerHTML = val+' '+document.adminForm.avail_attribute[i].value+' ';
									jQuery("#custom_attribute").val(val+' '+document.adminForm.avail_attribute[i].value+' ');
									lastchar = 'attr';
								}
							}
						}
						if(selected==0){
							alert("<?php echo JText::_('NOT_SELECTED_COMBO_ALERT');?>");
						}
					}

					function addplussign(){
						//var val = document.getElementById('custom_attribute').innerHTML;
						var val = jQuery("#custom_attribute").val();
						if(lastchar=='opr'){
							alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
						}else{
							//document.getElementById('custom_attribute').innerHTML = val+"+";
							jQuery("#custom_attribute").val(val+"+");
							lastchar = 'opr';
						}
					}

					function addminussign(){
						//var val = document.getElementById('custom_attribute').innerHTML;
						var val = jQuery("#custom_attribute").val();
						if(lastchar=='opr'){
							alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
						}else{
							//document.getElementById('custom_attribute').innerHTML = val+"-";
							jQuery("#custom_attribute").val(val+"-");
							lastchar = 'opr';
						}
					}

					function addmultiplysign(){
						//var val = document.getElementById('custom_attribute').innerHTML;
						var val = jQuery("#custom_attribute").val();
						if(lastchar=='opr'){
							alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
						}else{
							//document.getElementById('custom_attribute').innerHTML = val+"*";
							jQuery("#custom_attribute").val(val+"*");
							lastchar = 'opr';
						}
					}

					function adddivisionsign(){
						//var val = document.getElementById('custom_attribute').innerHTML;
						var val = jQuery("#custom_attribute").val();
						if(lastchar=='opr'){
							alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
						}else{
							//document.getElementById('custom_attribute').innerHTML = val+"/";
							jQuery("#custom_attribute").val(val+"/");
							lastchar = 'opr';
						}
					}

					function addLeftBraket(){
						//var val = document.getElementById('custom_attribute').innerHTML;
						var val = jQuery("#custom_attribute").val();
						//document.getElementById('custom_attribute').innerHTML = val+"(";
						jQuery("#custom_attribute").val(val+"(");
						checkIncompleteFormula();
					}

					function addRightBraket(){
						//var val = document.getElementById('custom_attribute').innerHTML;
						var val = jQuery("#custom_attribute").val();
						//document.getElementById('custom_attribute').innerHTML = val+")";
						jQuery("#custom_attribute").val(val+")");
						checkIncompleteFormula();
					}

					function addPersentage(){
						//var val = document.getElementById('custom_attribute').innerHTML;
						var val = jQuery("#custom_attribute").val();
						//if(lastchar=='opr'){
							//alert("<?php echo JText::_('CONSECUTIVE_OPR_ALERT');?>");
						//}else{
							//document.getElementById('custom_attribute').innerHTML = val+"%";
							jQuery("#custom_attribute").val(val+"%");
							lastchar = 'opr';
						//}
					}

					function addNumeric(){
						//var val       = document.getElementById('custom_attribute').value;
						var val       = jQuery("#custom_attribute").val();
						//var customval = document.getElementById('custom_numeric').value;
						var customval = jQuery("#custom_numeric").val();

						jQuery("#custom_attribute").val(val+customval);
						//document.getElementById('custom_attribute').innerHTML = val+customval;
						lastchar = '';
					}

					function checkIncompleteFormula(){
						//var str   = document.getElementById('custom_attribute').innerHTML;
						var str   = jQuery("#custom_attribute").val();

						var left  = str.split("(");
						var right = str.split(")");

						if(left.length==right.length){
							document.getElementById('custom_attribute').style.color="black";
						}else{
							document.getElementById('custom_attribute').style.color="red";
						}
					}

					function Click(val1){
						//var val = document.getElementById('custom_attribute').innerHTML;
						var val = jQuery("#custom_attribute").val();
						//document.getElementById('custom_attribute').innerHTML = val+val1;
						jQuery("#custom_attribute").val(val+val1);
						checkIncompleteFormula();
					}

					function checkfilledvalue(){
						//var tmp1 = document.getElementById('custom_attribute').innerHTML;
						var tmp1 = jQuery("#custom_attribute").val();
						//var tmp2 = document.getElementById('new_name').value;
						var tmp2 = jQuery("#new_name").val();
						var flg  = 0;
						var len1 = parseInt(tmp1.length) - 1;
						var len2 = parseInt(tmp2.length) - 1;
						if(tmp1.charAt(len1) == "%" || tmp2.indexOf(len2) == "%" ) {
							flg = 1;
						}

						if(tmp1 == '' || tmp2 == ''){
							alert("<?php echo JText::_('CUSTOM_ATTR_BLANK_ALERT');?>");
						}else if(lastchar=='opr' && flg == 0){
							alert("<?php echo JText::_('LAST_CHAR_OPR_ALERT');?>");
						}else{
							if(!validateFormula()){
								return false;
							}
							addCustomAttr(tmp2,encodeURIComponent(tmp1));
						}
					}

					function undofield(){
						//document.getElementById('custom_attribute').innerHTML = '';
						jQuery("#custom_attribute").val("");
						lastchar = '';
					}

					function addCustomAttr(attrname,attributevale)
					{
						JQuery.ajax({
							url     : "index.php?option=com_mica&task=seishowresults.AddCustomAttr&attrname="+attrname+"&attributevale="+attributevale,
							method  : 'GET',
							success : function(data)
							{
								// Clear Previouse Value
								//$("#new_name").val("");
								//$("#custom_numeric").val("");
								//$("#custom_attribute").val("");
								$('#customlist').addClass('active');
						        $('#custom_manage').removeClass('active');
						    	$('#custom_edit').removeClass('active');
						        $('#home').addClass('in active');
						        $('#menu1').removeClass('in active');
						        $('#menu2').removeClass('in active');

						        getDataNew();
								getAttribute_v2(5);
							}
						});
					}

					JQuery(document).on('click','#editvariable',function(){
						JQuery('#save').attr("id","updatecustom");
						JQuery('#updatecustom').attr("onclick","javascript:void(0)");
						is_updatecustom=1;
						JQuery('#updatecustom').attr("value","Update");
					});

					JQuery(document).on('click','customedit',function(){
						if(is_updatecustom == 1){
							JQuery('#new_name').val(JQuery(this).find(":selected").text());
							JQuery('#new_name').attr('disabled', true);
							oldattrval = JQuery(this).find(":selected").val();
							alert(oldattrval)
							JQuery('#oldattrval').val(oldattrval);
							JQuery('textarea#custom_attribute').text("");
							lastchar = '';
							moverightarea();
						}
					});
				</script>

				<div class="variable_custom_data">
					<ul class="nav">
						<li id="customlist" class="active"><a data-toggle="tab" href="#home" >Custom</a></li>
						<li id="custom_manage"><a data-toggle="tab" href="#menu1" class="getmanagedata">Manage</a></li>
						<li id="custom_edit"><a data-toggle="tab" href="#menu2" >Add New</a></li>
					</ul>
					<div class="tab-content">
					    <div id="home" class="tab-pane fade in active">
					    	<div class="poptitle">
								Custom Variable
							</div>
					    	<div id="CustomVariable">
					      	</div>
					      	<p>&nbsp;</p>
							<div align="right">
						    	<a href="javascript:void(0);" class="actionbtn btn-submit" onclick="getDataNew();"><i class="fas fa-table"></i>Apply</a>
						    </div>
					    </div>

					    <div id="menu1" class="tab-pane fade">
					     	<div id="customvariables">
								<div id="lightmanagevariable">
									<div class="poptitle">
										<?php echo JTEXT::_('MANAGE_CUSTOM');?>
									</div>
									<table width="100%" cellpadding="0" cellspacing="1" class="cacss">
										<thead>
											<tr>
												<th align="center" width="10%">Use</th>
												<th align="center" >Variable</th>
												<th align="center" width="15%">Action</th>
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
									<div class="readon frontbutton">
										<input type="button" name="Remove" value="Remove Selected" class="frontbutton deletecustomvariable" />
									</div>
								</div>
						  	</div>
					    </div>

						<div id="menu2" class="tab-pane fade">
					       	<div class="row">
								<div class="poptitle">
									<?php echo JText::_('ADD_ATTRIBUTE_LABEL');?>
								</div>

					       	 	<div class="col-md-12">
					       	 		<div class="row">
					       	 			<div class="col-md-3"><label><?php echo JText::_('NAME_LABEL'); ?></label></div>
					       	 			<div class="col-md-9"><input type="text" name="new_name" id="new_name" class="inputbox"></div>
					       	 		</div>
					       	 	</div>

					  			<div class="col-md-12">
					  				<div class="row">
					  					<div class="col-md-3"><label>Select Variable</label></div>
					  					<div class="col-md-7" id="avail_attribute_for_custom">
					  					</div>
					  					<div class="col-md-2">
					  						<input type="button" title="<?php echo JText::_('MOVE_RIGHT_TIP');?>" name="moveright" id="moveright" class="btn btn-success" value="Add Variable"  onclick="moverightarea();">
					  					</div>
					  				</div>
					  			</div>

								<div class="col-md-12">
			      					<table border="0" align="center" cellpadding="0" cellspacing="0" class="cal_t" style="background: #dbdbdd">
										<tr>
						 					<td width="60">
												<input type="button" title="<?php echo JText::_('ADD_PLUS_TIP');?>" name="addplus" id="addplus" class="btn btn-warning btn-sm btn-block"  value="+" onclick="addplussign();">
											</td>
										   	<td width="60">
										   		<input type="button" title="<?php echo JText::_('ADD_MINUS_TIP');?>" name="addminus" id="addminus" class="btn btn-warning btn-sm btn-block"  value="-" onclick="addminussign();">
										   	</td>
											<td width="60">
												<input type="button" title="<?php echo JText::_('ADD_MULTIPLY_TIP');?>" name="addmultiply" id="addmultiply" class="btn btn-warning btn-sm btn-block"  value="*" onclick="addmultiplysign();">
											</td>
										   	<td width="60">
										   		<input type="button" title="<?php echo JText::_('ADD_DIVISION_TIP');?>" name="adddivision" id="adddivision" class="btn btn-warning btn-sm btn-block"  value="/" onclick="adddivisionsign();">
										   	</td>
											<td width="60">
												<input type="button" title="<?php echo JText::_('ADD_PERSENT_TIP');?>" name="ADDPERSENT" id="addPersent" class="btn btn-warning btn-sm btn-block"  value="%" onclick="addPersentage();">
											</td>

											<td width="60">
												<input name="sin" type="button" id="sin" value="sin" onclick="Click('SIN(')" class="btn btn-warning btn-sm btn-block">
											</td>
										</tr>
										<tr>
											<td width="60">
												<input name="cos" type="button" id="cos" value="cos" onclick="Click('COS(')" class="btn btn-warning btn-sm btn-block">
											</td>
										   	<td width="60">
										   		<input name="tab" type="button" id="tab" value="tan" onclick="Click('TAN(')" class="btn btn-warning btn-sm btn-block">
										   	</td>
											<td width="60">
												<input name="log" type="button" id="log" value="log" onclick="Click('LOG(')" class="btn btn-warning btn-sm btn-block">
											</td>
										 	<td width="60">
										 		<input name="1/x" type="button" id="1/x2" value="log10" onclick="Click('LOG10(')" class="btn btn-warning btn-sm btn-block">
										 	</td>

											<td width="60">
												<input name="sqrt" type="button" id="sqrt" value="sqrt" onclick="Click('SQRT(')" class="btn btn-warning btn-sm btn-block">
											</td>
											<td width="60">
												<input name="exp" type="button" id="exp" value="exp" onclick="Click('EXP(')" class="btn btn-warning btn-sm btn-block">
											</td>
										</tr>
										<tr>
											<td width="60">
												<input name="^" type="button" id="^" value="^" onclick="Click('^')" class="btn btn-warning btn-sm btn-block">
											</td>
											<td width="60">
												<input name="ln" type="button" id="abs22" value="ln" onclick="Click('LN(')" class="btn btn-warning btn-sm btn-block">
											</td>
											<td width="60">
												<input name="pi" type="button" id="pi3" value="pi" onclick="Click('PI(')" class="btn btn-warning btn-sm btn-block">
											</td>

											<td width="60">
												<input type="button" title="<?php echo JText::_('ADD_LEFTBRAC_TIP');?>" name="ADDLEFTBRAC" id="addleftbrac" class="btn btn-warning btn-sm btn-block"  value="(" onclick="addLeftBraket();">
											</td>
											<td width="60">
												<input type="button" title="<?php echo JText::_('ADD_RIGHTBRAC_TIP');?>" name="ADDRIGHTBRAC" id="addleftbrac" class="btn btn-warning btn-sm btn-block"   value=")" onclick="addRightBraket();" >
											</td>
											<td></td>
										</tr>
									</table>
							   	</div>

				      			<div class="col-md-12">
				      				<div class="row">
				      					<div class="col-md-3"><label><?php echo JText::_('AVAILABLE_ATTRIBUTE_LABEL'); ?></label></div>
				      					<div class="col-md-7"><input type="text" name="custom_attribute" id="custom_numeric" class="inputbox"></div>
				      					<div class="col-md-2"><input type="button" title="<?php echo JText::_('NUMERIC_VALUE_LABEL'); ?>" name="numericval" id="numericval" class="btn btn-success" value="Add Value" onclick="addNumeric();"></div>
				      				</div>
								</div>

				      			<div class="col-md-12">
									<textarea class="form-control" name="custom_attribute" id="custom_attribute" rows="3"  readonly="readonly"></textarea>
				      			</div>

					      		<div class="col-md-12 text-right custom_cancel">
									<input type="button" name="clear" id="clear" class="btn btn-danger btn-bottom" value="<?php echo JText::_('CLEAR_LABEL');?>" onclick="undofield();">
									<input type="button" name="save" id="save" class="btn btn-primary btn-bottom" value="<?php echo JText::_('SAVE_LABEL');?>" onclick="checkfilledvalue();">
					      		</div>
					       	</div>
					    </div>
					</div>
				</div>
			</div>
			<!-- custom variable calcultor end-->
		</div>
	</form>
</div>

<div id="fade" class="black_overlay" style="display:none"></div>

<!--Start Thematic -->
<div id="light_thematic" style="display: none;" class="tq white_content2 large-popup">

	<div class="divclose">
		<a href="javascript:void(0);" onclick="document.getElementById('light_thematic').style.display='none'; document.getElementById('fade').style.display='none';">
			<img src="media/system/images/closebox.jpeg" alt="X">
		</a>
	</div>

	<div class="" style="width:100%;float:left;">
		<div class="thematicarea clearfix">
			<div class="blockcontent">
				<?php
				function getCustomAttrNameThematic($name,$activeworkspace) {
					$db    = JFactory::getDBO();
					$query = "SELECT name, attribute FROM ".$db->quoteName('#__mica_user_custom_attribute')."
					WHERE ".$db->quoteName('profile_id')." = ".$db->quote($activeworkspace)."
						AND ".$db->quoteName('attribute')." LIKE ".$db->quote($name);
					$db->setQuery($query);
					$result = $db->loadAssoc();

					if(count($result) == 0){
						return array($name,$name);
					}else{
						return array($result[0],$result[1]);
					}
				}

				$activeworkspace = $this->activeworkspace;
				$query = "SELECT * FROM ".$db->quoteName('#__mica_sld_legend')."
					WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace)."
					ORDER BY ".$db->quoteName('level').", ".$db->quoteName('range_from')." ASC";
				$db->setQuery($query);
				$result = $db->loadObjectList();

				$id    = array();
				$level = array();
				$i     = 1;
				$j     = 1;
				$grouping = array();

				foreach($result as $range){
					if($range->level == "0"){
						$str = " <div class='col_".$i."' style='height:10px;width:10px;background-color:".$range->color.";float:right;'></div></li>";
					}else{
						$pin = str_replace("#","",$range->color);
						$str = " <div class='col_".$i."' style='float:right;'><img src='".JUri::base()."components/com_mica/maps/img/layer".$range->level."/pin".$pin.".png' /></div></li>";
					}

					$grouping[$range->custom_formula][] = "<li class='range_".$range->level."' id='".$range->range_from." - ".$range->range_to."'><div style='float:left;width:80%;'>".$range->range_from." - ".$range->range_to."</div>".$str;
					//$id[] = $range->custom_formula;
					//$grouping[$range->custom_formula] = $range->level;
					$level[$range->custom_formula][] = $range->level;
					$i++;
					$j++;
				}

				$i            = 0;
				$range        = array();
				$str          = array();
				$totalcount   = 0;
				$l            = 0;
				$tojavascript = array();
				$grpname = array();

				foreach($grouping as $key => $val){
					$grname         = getCustomAttrNameThematic($key, $activeworkspace);
					$grpname[]      = $grname;
					$str[]          = implode(" ",$val);
					$ranges[]       = count($val);
					$levelunique[]  = $level[$key][0];
					$tojavascript[] = $key;
					$l++;
				}
				$tojavascript = implode(",",$tojavascript);
				$html = "";
				//echo "<pre />";print_r($str);exit;
				for($i = 0; $i < count($grpname); $i++){
					echo "<div class='contentblock1 '>
						<div class='blockcontent'>
							<ul class='maingrp bullet-add clear' >
								<li>
									<div class='themehead'>".$grpname[$i][0]."</div>
									<div class='themeedit'>
										<a class='edittheme ".$ranges[$i]."' id='".$grpname[$i][1]."__".$levelunique[$i]."' >
											<i class='fa fa-edit'></i>
											<!--- <img src='".JUri::base()."components/com_mica/images/edit.png' /> -->
										</a>
										<a class='deletegrp ".$levelunique[$i]."' id='del_".$grpname[$i][1]."' >
											<i class='fa fa-trash'></i>
											<!--- &nbsp;&nbsp;&nbsp;<img src='".JUri::base()."components/com_mica/images/delete.png' /> -->
										</a>
									</div>
									<ul class='range' >".$str[$i]."</ul>
								</li>
							</ul>
						</div>
					</div>";
				}
				?>

				<div class='contentblock3'>
					<script type='text/javascript'>
						var totalthemecount     = "<?php echo count($grpname); ?>";
						var usedlevel           = '<?php echo implode(",",$levelunique); ?>';
						var havingthematicquery = '<?php echo $tojavascript; ?>';
					</script>
					<div id="themeconent">
						<form name="micaform3" id="micaform3" action="index.php?option=com_mica&view=seishowresults&Itemid=<?php echo $this->Itemid; ?>" method="get" >
							<table cellspacing="4" cellpadding="0" border="0" width="100%">
								<tr>
									<td colspan="2" align="left"><h3><?php echo JText::_('SELECT_PARAMETER_LABEL');?></h3></td>
								</tr>
								<tr>
									<td width="35%">
										<b><?php echo JText::_('ATTRIBUTE_LABEL');?></b>
									</td>
									<td>
										<select name="thematic_attribute" id="thematic_attribute" class="inputbox" onchange="getMinmaxVariable_v2(this.value);">
											<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
											<optgroup label="Default Variable">
												<?php $attr = $this->themeticattribute;
												$eachattr = explode(",",$attr);
												foreach($eachattr as $eachattrs){
													echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
												} ?>
											</optgroup>
											<optgroup label="Custom Variable">
												<?php $attr = $this->customattribute;
												$eachattr = explode(",",$attr);
												$eachattr = array_filter($eachattr);
												foreach($eachattr as $eachattrs){
													$eachattrs=explode(":",$eachattrs);
													echo '<option value="'.$eachattrs[1].'">'.JTEXT::_($eachattrs[0]).'</option>';
												} ?>
											</optgroup>
										</select>
									</td>
								</tr>
								<tr id="minmaxdisplay">
									<td></td>
									<td class="minmaxdisplay"></td>
								</tr>
								<tr>
									<td>
										<b><?php echo JText::_('NO_OF_INTERVAL_LABEL');?></b>
									</td>
									<td>
										<input type="text" name="no_of_interval" id="no_of_interval" class="inputbox" value="" style="width: 194px;">
									</td>
								</tr>
								<tr class="colorhide">
									<td>
										<b><?php echo JText::_('SELECT_COLOR_LABEL');?></b>
									</td>
								<td>
									<input class="simple_color" value="" style="width: 194px;"/>
								</td>
								</tr>
								<tr>
									<td colspan="2">
										<table cellspacing="0" cellpadding="0" border="1" width="100%" class="popupfromto" id="displayinterval"></table>
									</td>
								</tr>
								<input type="hidden" name="maxvalh" id="maxvalh" value="">
								<input type="hidden" name="minvalh" id="minvalh" value="">
								<input type="hidden" name="level" id="level" value="">
								<tr>
									<td></td>
									<td align="left">
										<div class="readon ">
											<input type="button" name="save" id="savesld" class="frontbutton" style="width: 100px;" value="<?php echo JText::_('OK_LABEL');?>" onclick="saveSLD()"/>
										</div>
										<div class="readon ">
											<input type="button" name="cancel" id="cancel" class="frontbutton" style="width: 100px;" value="<?php echo JText::_('CANCEL_LABEL');?>" onclick="closePopup_v2();"/>
										<div>
									</td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--End Thematic -->

<div id="light2" class="white_content2" style="display: none;">
	<div class="divclose">
		<a href="javascript:void(0);" onclick="closePopup_v2();"><img src="media/system/images/closebox.jpeg" alt="X" /></a>
	</div>
	<div class="themeconent"></div>
</div>

<script type="text/javascript">
	var siteurl   = '<?php echo JURI::base();?>';
	var tomcaturl = "<?php echo $this->tomcaturl; ?>";


	/*-------------export -----------*/
	document.getElementById('export').addEventListener("click", downloadPDF);

	/*
		Download Chart
	*/
	function downloadPDF()
	{
		var img       = document.querySelector('h1.logo_img > a > img');
		var doc = new jsPDF('landscape');

		doc.setFontSize(20);
		doc.addImage(img, 'PNG', 5, 5);
		doc.text(130, 40, $('#chartype option:selected').text());

		if($('#chartype option:selected').text() == "Bubble Chart")
		{
			var svg = document.getElementsByTagName('svg')[0];

			svg.setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns', 'http://www.w3.org/2000/svg');
			svg.setAttributeNS('http://www.w3.org/2000/xmlns/', 'xmlns:xlink', 'http://www.w3.org/1999/xlink');

			var serializer = new XMLSerializer();
			var serialSVG  = serializer.serializeToString(svg);
			var svg        = serialSVG;

		  	if (svg){
		  		svg = svg.replace(/\r?\n|\r/g, '').trim();
		  	}

		  	var canvas = document.createElement('canvas');
		  	var context = canvas.getContext('2d');

			canvg(canvas, svg);

			var canvasImg = canvas.toDataURL("image/jpeg", 1.0);
			doc.addImage(canvasImg, 'JPEG', 50, 50);

			/*var imgData = canvas.toDataURL('image/png');
			doc.addImage(imgData, 'PNG', 0, 50, 280, 150 );*/
			//doc.addImage(imgData, 'png', 0, 50);
		}
		else
		{
			var canvas    = document.querySelector('#myChart');
			var canvasImg = canvas.toDataURL("image/jpeg", 1.0);
			doc.addImage(canvasImg, 'JPEG', 0, 50, 280, 150 );
		}

		doc.save($('#chartype option:selected').text()+'.pdf');
	}




	/*-------------export end-----------*/

	/*JQuery(document).on('click','#district_tab',function(){
  		JQuery('.slider').slick({
	  		dots: false,
	    	vertical: true,
	    	slidesToShow: 11,
	    	slidesToScroll: 1,
	    	verticalSwiping: true,
	    	arrrow:true,
	  	});
	});*/

	JQuery(document).on('click','a.getmanagedata',function(){
		JQuery.ajax({
			url     : "index.php?option=com_mica&task=seishowresults.getManageajax",
			method  : 'GET',
			success : function(data)
			{
				JQuery('.cacss tbody').html(data);
				JQuery('#cancel').remove();

				return;
			}
		});
	});

	$("#enablelegend").click(function() {
	    chart1.options.legend.display = true;
	    chart1.update();
	    if (document.getElementById('enablelegend').checked) {
	        chart1.options.legend.display = true;
	        chart1.update();
	    } else {
	        chart1.options.legend.display = false;
	        chart1.update();
	    }
	});

	var gis_state              = "<?php echo str_replace(",","','",$session->get('state'));?>";
	var gis_gis_district       = '';
	var districtsearchvariable = '';
	var gis_UnselectedDistrict = '';
	var javazoom               = 6;
	var gis_town               = '';
	var townsearchvariable     = '';
</script>

<div id="gis_script">
	<script type="text/javascript" id="j1">
		var globalcorrdinates      = "";
		// var map                 = new OpenLayers.Map("map");
		var oldzoom                = 0;
		var geo                    = [];
		var dystate                = [];
		var geo                    = [];
		//OpenLayers.ProxyHost     = "http://192.168.5.159/cgi-bin/proxy.cgi?url=";
		// use a CQL parser for easy filter creation
		var map, infocontrols,infocontrols1, water, highlightlayer,coordinates,exportMapControl;
		OpenLayers.DOTS_PER_INCH   = 25.4 / 0.28;
		var tomcaturl              = "<?php echo $this->tomcaturl; ?>";
		var mainzoom               = "<?php echo $this->zoom; ?>";
		var tomcatpathlink         = "<?php echo 'http://www.mica-mimi.in/'.TOMCAT_SLD_FOLDER.'/'; ?>";
		var userid                 = "<?php echo $this->userid; ?>";
		var activeworkspace        = "<?php echo $this->activeworkspace; ?>";
		var sldlinkStateBoundaries = tomcatpathlink+userid+"/"+activeworkspace+"/rail_state.sld";
		var sldlinkDistricts       = tomcatpathlink+userid+"/"+activeworkspace+"/india_information.sld";
		var sldlinkCities          = tomcatpathlink+userid+"/"+activeworkspace+"/my_table.sld";
		var sldlinkUrban           = tomcatpathlink+userid+"/"+activeworkspace+"/jos_mica_urban_agglomeration.sld";
		var mainGeometry           = "<?php echo $this->geometry; ?>";
		var mainlonglat            = "<?php echo $app->input->get('longlat', '', 'raw'); ?>";
		var mainState              = "<?php echo $this->state; ?>";
		var mainDistrict           = "<?php echo $this->district; ?>";
		var mainSubDistrict        = "<?php echo $this->subDistrict; ?>";
		var mainUnselectedDistrict = "<?php echo $this->UnselectedDistrict; ?>";
		var javazoom               = mainzoom;
		var mainTown               = "<?php echo $this->town; ?>";
		var mainUrban              = "<?php echo $this->urban; ?>";

		var districts = new OpenLayers.Layer.WMS("districts",
			"<?php echo $this->tomcaturl; ?>",
			{
				Layer       : 'india:india_information',
				transparent : true,
				format      : 'image/png',
				CQL_FILTER  : districtCqlFilter(),
				SLD         : "<?php echo JURI::base().TOMCAT_SLD_FOLDER.'/'.$this->userid.'/'.$this->activeworkspace.'/india_information.sld'; ?>"
			},
			{
				isBaseLayer: false
			},
			{
				transitionEffect: 'resize'
			}
		);

		function onPopupClose(evt) {
			// 'this' is the popup.
			JQuery("#featurePopup").remove();
		}

		function displaymenu(e){
			//$("#menupopup").remove();
			//var OuterDiv=$('<div  />');
			//OuterDiv.append($("#menu").html());
			//OuterDiv.append($("#menu").html());
			//console.log(e);
			//OuterDiv.css({"z-index":"100000","left":e.screenX,"top":e.screenY,"display":"block"});
			//console.log({e.layerX,e.layerY});

			// popup = new OpenLayers.Popup("menupopup",map.getLonLatFromPixel(new OpenLayers.Pixel({e.layerX,e.layerY})),new OpenLayers.Size(200,300),$("#menu").html(), false);
			//	map.popup = popup;
			//evt.feature = feature;
			//	map.addPopup(popup);
		}

		function setHTML(response){
			changeAttrOnZoom(map.zoom,response);
		};

		function selectfilter(){
			if(javazoom==5 || javazoom==6 ){
				return stateCqlFilter();
			}else if(javazoom==7){
				return districtCqlFilter();
			}else  if(javazoom==8){
				return urbanCqlFilter();
			}else{
				return cityCqlFilter();
			}
		}

		function selectlayer(cuurzoom){
			if(javazoom==5 || javazoom==6){
				return "rail_state";
			}else if(javazoom==7){
				return "india_information";
			}else  if(javazoom==8){
				return "jos_mica_urban_agglomeration";
			}else{
				return "my_table";
			}
		}

		function onFeatureSelect(response)
		{
			response = response.trim("");

			var getsegment=response.split("OGR_FID:");
			var id;

			if(typeof(getsegment[1])=="undefined")
			{
				return false;
			}
			else
			{
				id = getsegment[1].trim();
				//alert(getsegment[3]);
				if(typeof(getsegment[2])!="undefined" ){
					if(getsegment[2] == 362 || getsegment[2] == 344 || getsegment[2] == 205 || getsegment[2] == 520 || getsegment[2] == 210 || getsegment[2] == 211 || getsegment[2] == 206){
						id = getsegment[2].trim();
					}

				   	if(getsegment[3] == 209){
				   		id = getsegment[3].trim();
				   	}
					if(getsegment[4] == 209){
						id = getsegment[4].trim();
					}

					if(getsegment[3] == 207){
						id = getsegment[3].trim();
					}

					if(getsegment[1] == 205){
						id = getsegment[1].trim();
					}

					if(getsegment[1] == 206 && getsegment[2] == 207 && getsegment[3] == 212){
						id = getsegment[2].trim();
					}
				}
			}

			id = parseInt(id);
			JQuery(".olPopup").css({"display":"none"});

			JQuery.ajax({
				url     : "index.php?option=com_mica&task=seishowresults.popupAttributes&id="+id+"&zoom="+javazoom,
				method  : 'GET',
				success : function(data){
					popup = new OpenLayers.Popup.FramedCloud("featurePopup",
						map.getLonLatFromPixel(new OpenLayers.Pixel(coordinates.xy.x,coordinates.xy.y)),
						new OpenLayers.Size(300,150),
						data,null ,true, onPopupClose
					);
					map.popup = popup;
					//evt.feature = feature;
					map.addPopup(popup);
				}
			});
		}

		function onFeatureUnselect(evt) {
			feature = evt.feature;
			if (feature.popup) {
				popup.feature = null;
				map.removePopup(feature.popup);
				feature.popup.destroy();
				feature.popup = null;
			}
		}

		function stateCqlFilter()
		{
			if(mainState != "all")
			{
				statetojavascript = mainState.replace("," , "','");
				return "name in ('"+statetojavascript+"')";
			}
		}

		function districtCqlFilter()
		{
			if(mainDistrict != "all" && mainDistrict != "" ) {
				statetojavascript = mainDistrict;
				//statetojavascript =statetojavascript.replace(",", "','");
				return "OGR_FID in ("+statetojavascript+")";
				//return "OGR_FID in ('345','83','347','623')";

				//return "OGR_FID in ('"+statetojavascript+"')";

				/*if(mainUnselectedDistrict.length > mainDistrict)
				{

					statetojavascript = mainDistrict;
					statetojavascript ="'"+ statetojavascript.replace(",", "','")+"'";
					return "OGR_FID in ('"+statetojavascript+"')";
				}
				else
				{
					alert('123');
					statetojavascript = mainUnselectedDistrict;
					statetojavascript = statetojavascript.replace(",","','");
					return "OGR_FID not in ('"+statetojavascript+"')";
				}*/
			}
			else if(mainDistrict == "all") {
				return  "state in ('"+statetojavascript+"')";
			}
		}

		function cityCqlFilter(){
			if(mainTown != "all" &&  mainTown != "")
			{
				statetojavascript = mainTown;
				statetojavascript =statetojavascript.replace(",", "','");
				return "OGR_FID in ('"+statetojavascript+"')";
			}

			if(mainTown == "all")
			{
				return  "state in ('"+statetojavascript+"')";
			}
			<?php  /*if($this->town!="all" && $this->town!="" ){
				//$stateArray=explode(",",JRequest::GetVar('town'));
				$statetojavascript = $this->town;
				$statetojavascript = str_replace(",","','",$statetojavascript);
				echo " return \"".$this->townsearchvariable." in ('".$statetojavascript."')\";";
			}

			if($this->town == "all" ){
				//$stateArray=explode(",",JRequest::GetVar('town'));
				echo " return  \"state in ('".$statetojavascript."')\";";
			}*/ ?>
		}

		function urbanCqlFilter()
		{
			if(mainUrban != "all" && mainUrban != "")
			{
				statetojavascript = mainUrban;
				statetojavascript =statetojavascript.replace(",", "','");
				return "OGR_FID in ('"+statetojavascript+"')";
			}

			if(mainUrban == "all" ){
				//$stateArray=explode(",",JRequest::GetVar('town'));
				return  "state in ('"+statetojavascript+"')";
			}

			<?php  /*if($this->urban != "all" && $this->urban != ""){
				//$stateArray=explode(",",JRequest::GetVar('state'));
				$statetojavascript = $this->urban;
				$statetojavascript = str_replace(",","','",$statetojavascript);
				echo " return \"".$this->urbansearchvariable." in ('".$statetojavascript."')\";";
			}

			if($this->urban == "all" ){
				//$stateArray=explode(",",JRequest::GetVar('town'));
				echo " return  \"state in ('".$statetojavascript."') AND place_name <> ''\";";
			}*/ ?>
		}

		function changeAttrOnZoom(javazoom,response){
			onFeatureSelect(response.responseText);
		}

		function reloadCustomAttr(){
			JQuery.ajax({
				url     : "index.php?option=com_mica&task=seishowresults.getCustomAttr",
				method  : 'GET',
				success : function(data){
					JQuery("#addcustomattribute").html(data);
				}
			});
		}

		JQuery(".delcustom").live("click",function(){
			var myid    = JQuery(this).attr('id');
			var segment = myid.split("_");
			JQuery.ajax({
				url     : "index.php?option=com_mica&task=seishowresults.deleteCustomAttr&id="+segment[2],
				method  : 'GET',
				success : function(data){
					JQuery("#"+myid).parent("tr").remove();
				}
			});
		});

		$('#variablespan').on('change', '.variable_checkbox', function() {
			checkvalidation();
		});

		$('#typespan').on('change', '.type_checkbox', function() {
			checkvalidation();
		});


		function openthematicQuery(){
			$(document).ready(function(){
				$('#main_loader').show();
			   	setTimeout(function(){
			      document.getElementById('light_thematic').style.display='block';
			      $('#main_loader').hide();
			   },3000); // 3000 to load it after 3 seconds from page load
			});
		}
	</script>
</div>
