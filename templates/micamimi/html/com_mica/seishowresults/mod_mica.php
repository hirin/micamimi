<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/*$app     = JFactory::getApplication('site');
$session = JFactory::getSession();
$db      = JFactory::getDBO();
$user    = JFactory::getUser();
$itemid = $app->input->get('Itemid', 0, 'int');*/

//$state      = JRequest::getVar('state','');
//$district   = JRequest::getVar('district','');
//$town       = JRequest::getVar('town','');
//$m_type     = JRequest::getVar('m_type','');
//$attributes = JRequest::getVar('attributes',array());

$db_table = $app->input->get('db_table', '', 'raw');
$m_type   = $session->get('m_type');
//$new_name         = JRequest::getVar('new_name',array());
//$custom_attribute = JRequest::getVar('custom_attribute',array());
$activeworkspace = $session->get('activeworkspace');

function getattributelist($db_table){
	$db = JFactory::getDBO();
	/*if($zoom == 5){
		$db_table = "State";
	}else if($zoom > 5 && $zoom <= 7){
		$db_table = "District";
	}else{
		$db_table = "Town";
	}*/

	$userAttr = userSelectedAttr($db_table);
	//print '<pre>'; print_r($userAttr);exit;
	if($userAttr == -2){
		return -2;
	}

	$str       = "";
	$todisplay = array();

	$query = "SELECT  * FROM ".$db->quoteName('#__mica_group_field')." AS allfields
		INNER JOIN ".$db->quoteName('#__mica_group')." AS grp ON ".$db->quoteName('grp.id')." = ".$db->quoteName('allfields.groupid')."
		WHERE ".$db->quoteName('allfields.table')." LIKE ".$db->quote($db_table)."
			AND ".$db->quoteName('grp.publish')." = ".$db->quote(1);
	$db->setQuery($query);
	$list = $db->loadAssocList();

	$change_group_name = 1;
	$icon              = array();
	$groupid           = array();
	foreach($list as $each){
		if($change_group_name == 1){
			$change_group_name = 0;
		}

		$each['field'] = str_replace("Rural_","",$each['field']);
		$each['field'] = str_replace("Urban_","",$each['field']);
		$each['field'] = str_replace("Total_","",$each['field']);

		$todisplay[$each['group']][] = $each['field'];
		$icon[$each['group']]        = $each['icon'];
		$todisplay[$each['group']][] = $each['field'];
		$groupid[]                   = $each['groupid'];
	}

	foreach($todisplay as $eachgrp => $vals){
		$todisplay[$eachgrp] = array_unique($todisplay[$eachgrp]);
	}
	return array($todisplay, $groupid, $icon);
}

function userSelectedAttr($db_table){
	$db   = JFactory::getDBO();
	$user = JFactory::getUser();

	$query = "SELECT plan_id FROM ".$db->quoteName('#__osmembership_subscribers')." WHERE ".$db->quoteName('user_id')." = ".$db->quote($user->id);
	$db->setQuery($query);
	$planid = $db->loadResult();

	$typeQry = "SELECT ".$db->quoteName('attribute')."
		FROM ".$db->quoteName('#__mica_user_attribute')."
		WHERE ".$db->quoteName('aec_plan_id')." = ".$db->quote($planid)."
			AND ".$db->quoteName('dbtable')." LIKE ".$db->quote($db_table);
	$db->setQuery($typeQry);
	$row  = $db->loadResult();
	$attr = explode(",", $row);

	return $attr;
}


function displaysttributes($grparray){
	$app     = JFactory::getApplication('site');
	$session = JFactory::getSession();

	$todisplay = $grparray[0];
	$grpid     = $grparray[1];
	$icon      = $grparray[2];

	$str      = "";
	$userAttr = $session->get('attributes');	//print '<pre>'; print_r($userAttr);
	$userAttr = explode(",",$userAttr);
	$popup    = 3;
	$i        = 0;

	foreach($todisplay as $key => $val){
		if(($i%2) == 0){
			$applyclass = "clear";
		}else{
			$applyclass = "";
		}

		$img = "";
		if($icon[$key]!=""){
			$img = '<img src="'.JUri::base().'components/com_mica/images/'.$icon[$key].'"/>';
		}

		$str .='<li class="'.$applyclass.'"><a href="javascript:void(0)" onclick="document.getElementById(\'light'.$popup.'\').style.display=\'block\';document.getElementById(\'fade\').style.display=\'block\'"><div class="leftgrpimg">'.$img.'</div><div class="rightgrptext">'.$key;//'.$grpid[count($val)+1].'"

		$popup++;
		$chk_count   = 0;
		$chk_count1  = 0;
		$unchk_count = 0;

			/* if($key=='Financial Services'){
			print '<pre>'; echo $key; echo '=====>'; print_r($val);
			echo '=========<br/>';
					print_r($userAttr);
			}
			 if($key=='Score'){echo '<br/>@@@@@@@@@@@@@@@@@@<br/>';
			print '<pre>'; echo $key; echo '=====>'; print_r($val);
			echo '=========<br/>';
					print_r($userAttr);
			}*/

		foreach($val as $eachval){
			if(strstr($app->input->get('sessionattributes', '', 'raw'),$eachval)){
				$chk_count1++;
			}
		}
		/*foreach($userAttr as $eachattr){
			if(in_array($eachattr,$val)){
				$chk_count++;
			}
		}
		$str.= " (<b>".$chk_count."</b> / ".count($val).")</div></a></li>;";*/
		$str.= " (<b>".$chk_count1."</b> / ".count($val).")</div></a></li>";
		$i++;
	}
	//$str .="</ul>";
	echo $str;
}

$listtoshow = getattributelist($db_table);//echo "in mod";exit;

?>
<!-- <div class="row summarydata">
	<div class="col-md-3">
		<div class="contentblock">
			<div class="contenthead">Variable Groups</div>
			<div class="blockcontent">
				<ul>
					<?php displaysttributes($listtoshow); ?>
				</ul>
			</div>
		</div>
	</div> -->
	<!-- <div class="col-md-3" id="customVariable">
		<?php
			$customarray    = explode(",",$this->customattribute);
			$custolibmarray = explode(",",$this->customattributelib);


			$finalarray     = array(array_filter($customarray), array_filter($custolibmarray));


			?>

			<div class="contentblock">
				<div class="contenthead">
					<div style="width:70%;float:left;">Custom Variable</div>
					<a class="addcustomvariable" href="javascript:void(0)" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'">
						<?php echo JText::_('ADD_ATTRIBUTE_LABEL');?>
					</a>
				</div>
				<div class="blockcontent">
					<ul class="custom_ver list">
						<li></li>
						<?php
						$i             = 0;
						$finalarray[1] = array_unique($finalarray[1]);
						foreach($finalarray[1] as $eacharray){
							if(in_array($eacharray, $finalarray[0])){
								$checked  = "checked";
								$selected = "Remove";
								$action   = "";
							}else{
								$checked  = "";
								$selected = "Select";
								$action   = "checked";
							}

							if(($i%2)==0){
								$clear="clear";
							}else{
								$clear="";
							}
							$namevalue=explode(":",$eacharray);
							echo '
							<li class="'.$clear.'">
								<div class="customvariable_name">
									<input style="display:none;" type="checkbox" name="variable1" '.$checked.' class="customvariablecheckbox" value="'.$namevalue[1].'" id="'.$namevalue[0].'">'.JTEXT::_($namevalue[0]).'
								</div>
								<div class="customvariable_select">
									<a href="#" onclick="toggleCustomAction_v2(this,\''.$action.'\')" class="addcustomvariable">'.$selected.'</a>
								</div>
							</li>';
							$i++;
						}

						if(!empty($finalarray[1])) {?>
							<li>
								<div class="frontbutton" style="margin-top:7px;">
									<a href="javascript:void(0)" onclick="document.getElementById('lightmanagevariable').style.display='block';document.getElementById('fade').style.display='block'">
										<input type="button" value ="<?php echo JTEXT::_('MANAGE_CUSTOM_BUTTON');?>" />
									</a>
								</div>
							</li>
						<?php } ?>
					</ul>

					<div id="lightmanagevariable" style="display:none;" class="white_content2">
						<div class="divclose">
							<a href="javascript:void(0);" onclick="document.getElementById('lightmanagevariable').style.display='none';document.getElementById('fade').style.display='none';">
								<img src="media/system/images/closebox.jpeg" alt="X">
							</a>
						</div>
						<div class="poptitle">
							<?php echo JTEXT::_('MANAGE_CUSTOM');?>
						</div>

						<table width="100%" cellpadding="0" cellspacing="1" class="cacss">
							<thead>
								<tr>
									<th align="center">Use</th>
									<th align="center">Variable</th>
									<th align="center">Action</th>
								</tr>
							</thead>
							<?php
							$i             =0;
							$finalarray[1] =array_unique($finalarray[1]);
							foreach($finalarray[1] as $eacharray){
								if(in_array($eacharray,$finalarray[0])){
									$checked  = "checked";
									$selected = "Remove";
									$action   = "";
								}else{
									$checked  = "";
									$selected = "Select";
									$action   = "checked";
								}

								if(($i%2)==0){
									$clear = "clear";
								}else{
									$clear ="";
								}
								$namevalue = explode(":",$eacharray);

								echo '
								<tr>
									<td align="center">
										<input type="checkbox" name="variable1"  class="dcv" value="'.$namevalue[1].'" id="'.$namevalue[0].'">
									</td>
									<td align="center" class="customdelete">'.$namevalue[0].'</td>
									<td align="center">
										<a href="javascript:void(0);" style="float:left;padding-left:5px;">
											<img src="'.JUri::base().'/components/com_mica/images/edit.png" alt="Edit" class="customedit" id="'.$namevalue[0].'" value="'.$namevalue[1].'">
										</a>
										<a href="javascript:void(0);"  style="float:left;padding-left:5px;" onclick="JQuery(this).parent().prev().prev().children().attr(\'checked\',\'checked\');JQuery(\'.deletecustomvariable\').click();">
											<img src="'.JUri::base().'/components/com_mica/images/delete.png" alt="Delete">
										</a>
									</td>
								</tr>';
								$i++;
							} ?>
						</table>

						<div class="readon frontbutton">
							<input type="button" name="Remove" value="Remove Selected" class="frontbutton deletecustomvariable" />
						</div>
					</div>
				</div>
			</div>
	</div> -->
	<!-- <div class="col-md-3">
		<div class="contentblock">
				<div class="contenthead">Workspace</div>
				<div class="blockcontent">
					<ul class="allworkspace">
					    <?php $id = $this->user->id;
						if($this->user->id == 0){
							return -1;
						}

						$query="SELECT name, id, is_default
							FROM ".$db->quoteName('#__mica_user_workspace')."
							WHERE ".$db->quoteName('userid')." = ".$db->quote($this->user->id)."
								AND ".$db->quoteName('is_default')." <> ".$db->quote(1);
						$db->setQuery($query);
						$result = $db->loadAssocList();

						$str               = "";
						$activeworkspace   = $app->input->get('activeworkspace', '', 'raw');
						$selected          = "";
						$activeprofilename = $eachresult['name'];
						$i                 = 1;
						foreach($result as $eachresult){
							if(($i%2)==0){
								$clear="clear";
							}else{
								$clear="";
							}

							if($activeworkspace == $eachresult['id']){
								$selected          = "selected";
								$edit              = "Edit/Delete";
								$activeprofilename = $eachresult['name'];

								echo "<script type='text/javascript'>JQuery('#activeworkspacename').html('Active Workspace :<font style=\'font-weight:normal;\'> ".$activeprofilename."</font>  ');</script>";
								$onclick = "document.getElementById('lightn').style.display='block';document.getElementById('fade').style.display='block'";
								echo "<input type='hidden' value='".$eachresult['id']."' id='profile' />";
								$active_text = "<span style='font-weight:normal;'>(Active)<span>";
								$active_text = "";

							}else{

								$selected    = "";
								$edit        = "Select";
								$onclick     = "changeWorkspace(".$eachresult['id'].")";
								$active_text = "";
							}

							$str .= '
								<li class="'.$clear." ".$selected.'">
									<div style="width:70%;float:left;">
										<label class="workspacelabel">'.$eachresult['name'].$active_text.'</label>
									</div>
									<div style="width:30%;float:right;">
										<a href="#" onclick="'.$onclick.'">'.$edit.'</a>
									</div>
								</li>';// '.$selected.'
						}
						echo $str; ?>
					</ul>
				</div>
			</div>
	</div> -->
	<!-- <div class="col-md-3">
		<div class="contentblock endcontent">
				<div class="contenthead">New Workspace</div>
				<div class="blockcontent newworkspace">
					<input type="text"  id="new_w_txt" class="inputbox" value="<?php echo $activeprofilename;?>"  class="newtextbox"/>
					<input type="button" name="Create" class="newbutton frontbutton" value="Create" id="createworkspace" />
				</div>
			</div>
	</div> -->
</div>
	<script>
		console.firebug = true;//fix the openlayer problem
		JQuery("#layers").click(function (){
			/*if($('#attr_image').hasClass('attr_open_image')){
				$('#attr_image').toggleClass('attr_close_image');
			}else if($('#attr_image').hasClass('attr_close_image')){
				$('#attr_image').toggleClass('attr_open_image');
			}*/
			JQuery(".layers_data").slideToggle("slow");
		});

		// function checkAll(){
		// 	JQuery('.statetotalselectall').click(function(){
		// 		JQuery('.statetotal_attributes').each(function(index){
		// 			JQuery(this).attr('checked','checked');
		// 		});
		// 	});
		// }

		// function uncheckall(){
		// 	JQuery('.statetotalunselectall').click(function(){
		// 		JQuery('.statetotal_attributes').each(function(index){
		// 			JQuery(this).attr('checked','');
		// 		});
		// 	});
		// }

		JQuery(function(){
			JQuery('.stateurbanselectall').click(function(){
				JQuery('.stateurban_attributes').each(function(index){
					JQuery(this).attr('checked','checked');
				});
			});

			JQuery('.stateurbanunselectall').click(function(){
				JQuery('.stateurban_attributes').each(function(index){
					JQuery(this).attr('checked','');
				});
			});

			JQuery('.stateruralselectall').click(function(){
				JQuery('.staterural_attributes').each(function(index){
					JQuery(this).attr('checked','checked');
				});
			});

			JQuery('.stateruralunselectall').click(function(){
				JQuery('.staterural_attributes').each(function(index){
					JQuery(this).attr('checked','');
				});
			});

			JQuery('.disttotalselectall').click(function(){
				JQuery('.disttotal_attributes').each(function(index){
					JQuery(this).attr('checked','checked');
				});
			});

			JQuery('.disttotalunselectall').click(function(){
				JQuery('.disttotal_attributes').each(function(index){
					JQuery(this).attr('checked','');
				});
			});

			JQuery('.disturbanselectall').click(function(){
				JQuery('.disturban_attributes').each(function(index){
					JQuery(this).attr('checked','checked');
				});
			});

			JQuery('.disturbanunselectall').click(function(){
				JQuery('.disturban_attributes').each(function(index){
					JQuery(this).attr('checked','');
				});
			});

			JQuery('.distruralselectall').click(function(){
				JQuery('.distrural_attributes').each(function(index){
					JQuery(this).attr('checked','checked');
				});
			});

			JQuery('.distruralunselectall').click(function(){
				JQuery('.distrural_attributes').each(function(index){
					JQuery(this).attr('checked','');
				});
			});

			JQuery('.towntotalselectall').click(function(){
				JQuery('.towntotal_attributes').each(function(index){
					JQuery(this).attr('checked','checked');
				});
			});

			JQuery('.towntotalunselectall').click(function(){
				JQuery('.towntotal_attributes').each(function(index){
					JQuery(this).attr('checked','');
				});
			});
		});

		// function getvalidation(){

		// 	var myurl = "";
		// 		myurl = "state=";
		// 		myurl += JQuery("#state").val();
		// 		myurl += "&district=";
		// 		myurl += JQuery("#district").val();
		// 		myurl += "&attributes=";

		// 	var attr = "";
		// 	JQuery('input.statetotal_attributes1[type=checkbox]').each(function () {
		// 		if(this.checked){
		// 			attr += JQuery(this).val() + ",";

		// 		}
		// 		alert(attr);
		// 		//$("#attributes1").val("");
		// 	});
		// 	//window.location="index.php?"+myurl+"&submit=Show+Data&option=com_mica&view=showresults";
		// 	return false;
		// }

		JQuery(".mica_popup").live("click",function(){
			var myid    = JQuery(this).attr("id");
			var display = JQuery("."+myid).css("display");
			var left    = JQuery(this).offset().left;
			var top     = JQuery(this).offset().top;

			if(display == "none"){
				JQuery("."+myid).css({"left":left});
				JQuery("."+myid).css({"top":top+24});
				JQuery("."+myid).css({"width":"20%"});
				JQuery("."+myid).css({"display":"block"});
			}else{
				JQuery("."+myid).css({"display":"none"});
			}
			//alert(myid);
		});

		JQuery(document).ready(function(){
			JQuery(".contenttoggle").css({"display":"none"});
			//var init=$(".active").attr("id");
			//$("."+init).css({"display":"block"});

			/*JQuery(".toggle").live("click",function(){
				alert("toggle");
				var myclass=JQuery(this).attr("class");
				JQuery(".toggle").css({"border-right":"1px solid #0154A1"});
				JQuery(this).prev("span").css({"border-right":"1px solid white"});
				//JQuery(this).next("span").css({"border-right":"1px solid white"});
				var myclasssplit=myclass.split(" ");
				if(typeof(myclasssplit[1]) != "undefined" && myclasssplit[1] == "active"){
					JQuery(".toggle").removeClass("active");
					JQuery(".contenttoggle").slideUp();
					return false;
				}
				//JQuery(".toggle").css({"border-right":"1px solid white"});
				var myid=JQuery(this).attr("id");
				JQuery(".toggle").removeClass("active");
				JQuery(this).addClass("active");
				JQuery(".contenttoggle").slideUp();
				JQuery("."+myid).slideDown();
			});

			JQuery(".toggle1").live("click",function(){
				JQuery(".toggle1").css({"border-right":"1px solid #0154A1"});
				JQuery(this).prev("span").css({"border-right":"none"});
				//JQuery(this).next("span").css({"border-right":"none"});
				JQuery(".toggle1").last("span").css({"border-right":"white"})
				var myclass      = JQuery(this).attr("class");
				var myclasssplit = myclass.split(" ");

				if(typeof(myclasssplit[1]) != "undefined" && myclasssplit[1] == "active"){
					JQuery(".toggle1").removeClass("active");
					JQuery(".contenttoggle1").slideUp();
					JQuery(".toggle1").css({"border-right":"1px solid #0154A1"});
					JQuery(".toggle1").last("span").css({"border-right":"white"})
					return false;
				}
				var myid=JQuery(this).attr("id");
				JQuery(".toggle1").removeClass("active");
				JQuery(this).addClass("active");
				JQuery(this).css({"border-right":"1px solid #0154A1"});
				JQuery(".contenttoggle1").slideUp();
				JQuery("."+myid).slideDown();
				JQuery.cookie("tab", myid);
				if(myid == "text"){
					JQuery('#jsscrollss').not(".firsttableheader,.secondtableheader,.thirdtableheader").jScrollPane({verticalArrowPositions: 'os',horizontalArrowPositions: 'os'});
				}
			});

			JQuery(".toggle2").live("click",function(){
				alert("toggle2");
				var myclass=JQuery(this).attr("class");
				var myclasssplit = myclass.split(" ");
				if(typeof(myclasssplit[1])!="undefined" && myclasssplit[1]=="active")
				{
					JQuery(".toggle2").removeClass("active");
					JQuery(".contenttoggle2").css({"display":"none"});
					return false;
				}
				var myid=JQuery(this).attr("id");

				JQuery(".toggle2").removeClass("active");
				JQuery(this).addClass("active");
				JQuery(".contenttoggle2").css({"display":"none"});
				JQuery("."+myid).css({"display":"block"});
			});*/
		});

		function scollPos() {
			var div1  = document.getElementById("tablescroll").scrollLeft;
			var width =JQuery("#tablescroll table").width();
			var div   =document.getElementById("tablescroll").scrollTop;
			//document.getElementById("matrixhead").scrollLeft=div1;
			if(JQuery("#matrixhead").html().length != 0){
				JQuery("#matrixhead1").html("<table cellpadding='0' cellspacing='1'><tr>"+JQuery("#matrixhead").html()+"</tr></table>");
				JQuery("#matrixhead").html("");
			}
			JQuery("#matrixhead1").scrollLeft(div1);
			//alert(document.getElementById("matrixhead").scrollLeft);
		}
	</script>

	<?php

	$popup = 3;


		echo "<form name='micaform5' method='post' action='index.php?option=com_mica&view=seishowresults&Itemid=".$this->Itemid."'>";
	$m_type_rating = $app->input->get('m_type_rating', '', 'raw');
	echo "<input type='hidden' name='m_type_rating[]' value='".$m_type_rating[0]."'>";
	echo "<input type='hidden' name='m_type_rating[]' value='".$m_type_rating[1]."'>";
	echo "<input type='hidden' name='m_type_rating[]' value='".$m_type_rating[2]."'>";

	foreach($listtoshow[0] as $key => $val){
		$i = 0;
		echo '
		<div id="light'.$popup.'" class="white_content2" style="display: none;">
			<div class="divclose">
				<a href="javascript:void(0);" onclick="document.getElementById(\'light'.$popup.'\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';">
					<img src="media/system/images/closebox.jpeg" alt="X" />
				</a>
			</div>
			<div class="poptitle">'.JTEXT::_($key).'</div>';

			//<form name='micaform".$popup."' method='post' action='index.php?option=com_mica&view=seishowresults&Itemid=".$this->Itemid."'>
			echo "	<table  width='100%' border='0' cellspacing='0' cellpadding='0'>
					<tr>";

						foreach($val as $key1 => $val1){
							if(($i%3) == 0){
								echo "</tr><tr>";
								$i = 0;
							}

							if(strstr( $app->input->get('sessionattributes', '', 'raw'),$val1)){
								$checked="checked";
							}else{
								$checked="";
							}

							$gname = ($key=="Score")?"composite[]":"popup_attributes[]";
							echo'
								<td align="left" valign="top" width="3%"><input type="checkbox" name="'.$gname.'" class="statetotal_attributes'.$popup.'" value="'.$val1.'" '.$checked.'></td>
								<td align="left" valign="top" width="30%"><span class="hovertext '.JTEXT::_($val1).'" style="padding-left:2px;">'.JTEXT::_($val1).'</span></td>';
							$i++;
						}

 		echo '		</tr>
 				</table>
 				<table  width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0px 0px 10px 0px;">
 					<tr>
 						<td >
 							<div class="maintableattr_new">
								<div>
									<a class="statetotalselectall" href="javascript:void(0);" onclick="checkAllgrp('.$popup.');">Select All</a> / <a class="statetotalunselectall" onclick="uncheckAllgrp('.$popup.');" href="javascript:void(0);">Unselect All</a>
								</div>
							</div>
						</td>';


		echo "		</tr>
				</table>
				<div class='frontbutton'>
					<input type='button' name='submit' class='frontbutton' value='Show Data' onclick='getvalidation(".$popup.")'>
				</div>";
		echo "<input type='hidden' id='state' name='state' value='".$app->input->get('state', '', 'raw')."' />";
		echo "<input type='hidden' id='zoom' name='zoom' value='".$app->input->get('zoom', '', 'raw')."' />";
		echo "<input type='hidden' id='district'name='district' value='".$app->input->get('district', '', 'raw')."' />";
		echo "<input type='hidden' name='option' value='".$app->input->get('option', '', 'raw')."' />";
		echo "<input type='hidden' name='view' value='".$app->input->get('view', '', 'raw')."' />";
		echo "<input type='hidden' name='Itemid' value='".$this->Itemid."' />";
		echo "<input type='hidden' name='m_type' value='".$m_type."' /><input type='hidden' id='minvalh' value=''/><input type='hidden' id='maxvalh' value=''/><input type='hidden' name='level' id='level'>";
		echo "</div>";
		$popup++;
	}
	echo "</form>";


	 ?>

	<?php
	// $popup = 3;

	// echo "<form name='micaform5' method='post' action='index.php?option=com_mica&view=showresults&Itemid=".$this->Itemid."'>";
	// $m_type_rating = $app->input->get('m_type_rating', '', 'raw');
	// echo "<input type='hidden' name='m_type_rating[]' value='".$m_type_rating[0]."'>";
	// echo "<input type='hidden' name='m_type_rating[]' value='".$m_type_rating[1]."'>";
	// echo "<input type='hidden' name='m_type_rating[]' value='".$m_type_rating[2]."'>";

	// foreach($listtoshow[0] as $key => $val){
	// 	$i = 0;
	// 	echo '
	// 	<div id="light'.$popup.'" class="white_content2" style="display: none;">
	// 		<div class="divclose">
	// 			<a href="javascript:void(0);" onclick="document.getElementById(\'light'.$popup.'\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';">
	// 				<img src="media/system/images/closebox.jpeg" alt="X" />
	// 			</a>
	// 		</div>
	// 		<div class="poptitle">'.JTEXT::_($key).'</div>';

	// 		//<form name='micaform".$popup."' method='post' action='index.php?option=com_mica&view=showresults&Itemid=".$this->Itemid."'>
	// 		echo "	<table  width='100%' border='0' cellspacing='0' cellpadding='0'>
	// 				<tr>";

	// 					foreach($val as $key1 => $val1){
	// 						if(($i%3) == 0){
	// 							echo "</tr><tr>";
	// 							$i = 0;
	// 						}

	// 						if(strstr( $app->input->get('sessionattributes', '', 'raw'),$val1)){
	// 							$checked="checked";
	// 						}else{
	// 							$checked="";
	// 						}

	// 						$gname = ($key=="Score")?"composite[]":"attributes[]";
	// 						echo'
	// 							<td align="left" valign="top" width="3%"><input type="checkbox" name="'.$gname.'" class="statetotal_attributes'.$popup.'" value="'.$val1.'" '.$checked.'></td>
	// 							<td align="left" valign="top" width="30%"><span class="hovertext '.JTEXT::_($val1).'" style="padding-left:2px;">'.JTEXT::_($val1).'</span></td>';
	// 						$i++;
	// 					}

 // 		echo '		</tr>
 // 				</table>
 // 				<table  width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0px 0px 10px 0px;">
 // 					<tr>
 // 						<td >
 // 							<div class="maintableattr_new">
	// 							<div>
	// 								<a class="statetotalselectall" href="javascript:void(0);" onclick="checkAllgrp('.$popup.');">Select All</a> / <a class="statetotalunselectall" onclick="uncheckAllgrp('.$popup.');" href="javascript:void(0);">Unselect All</a>
	// 							</div>
	// 						</div>
	// 					</td>';
	// 	$popup++;

	// 	echo "		</tr>
	// 			</table>
	// 			<div class='frontbutton'>
	// 				<input type='submit' name='submit' class='frontbutton' value='Show Data' onclick='getvalidation()'>
	// 			</div>";
	// 	echo "<input type='hidden' id='state' name='state' value='".$app->input->get('state', '', 'raw')."' />";
	// 	echo "<input type='hidden' id='zoom' name='zoom' value='".$app->input->get('zoom', '', 'raw')."' />";
	// 	echo "<input type='hidden' id='district'name='district' value='".$app->input->get('district', '', 'raw')."' />";
	// 	echo "<input type='hidden' name='option' value='".$app->input->get('option', '', 'raw')."' />";
	// 	echo "<input type='hidden' name='view' value='".$app->input->get('view', '', 'raw')."' />";
	// 	echo "<input type='hidden' name='Itemid' value='".$this->Itemid."' />";
	// 	echo "<input type='hidden' name='m_type' value='".$m_type."' /><input type='hidden' id='minvalh' value=''/><input type='hidden' id='maxvalh' value=''/><input type='hidden' name='level' id='level'>";
	// 	echo "</div>";
	// }
	// echo "</form>";


	// echo '
	// 	<div id="lightn" class="white_contentn" style="display: none;">
	// 		<div class="divclose">
	// 			<a href="javascript:void(0);" onclick="document.getElementById(\'lightn\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';">
	// 				<img src="media/system/images/closebox.jpeg" alt="X" />
	// 			</a>
	// 		</div>
	// 		<div align="left">
	// 			<input type="textbox" id="updateworkspacetext" value="'.$activeprofilename.'" class="inputbox"/>
	// 			<div class="frontbutton readon">
	// 				<input type="button" id="updateworkspace" onClick="JQuery(\'#new_w_txt\').val(JQuery(\'#updateworkspacetext\').val())" Value="Update" class="frontbutton readon"/>
	// 			</div>
	// 			<div class="frontbutton">
	// 				<input type="button" name="new"  id="deleteworkspace"  value="Delete" class=""/>
	// 			</div>
	// 		</div>';

	// $query  ="SELECT * FROM ".$db->quoteName('#__mica_sld_legend')."
	// 	WHERE ".$db->quoteName('workspace_id')." = ".$db->quote($activeworkspace)."
	// 	ORDER BY ".$db->quoteName('level').", ".$db->quoteName('range_from')." ASC";
	// $db->setQuery($query);
	// $result = $db->loadObjectList();
