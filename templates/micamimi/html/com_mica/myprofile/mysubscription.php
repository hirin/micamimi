<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$doc->addStyleSheet(JURI::base().'components/com_mica/css/dashboard.css', 'text/css');
?>

<div class="dashboard">
	<div class="header-rightpanel">
		<?php echo JTEXT::_("MY_SUBSCRIPTION"); ?>
	</div>
	<div class="data-rightpanel">
		<ul>
			<li>
				<span class="data-label"><?php echo JTEXT::_("PLAN");?></span>
				<?php if( $this->getParentinfo == null ){ ?>
					<span class="data-text"><?php echo $this->userinfo->plandetails->title; ?></span>
				<?php } else { ?>
					<span class="data-text"><?php print_r($this->getParentsubscriptioninfo->title); ?></span>
				<?php } ?>
			 </li>
			<li>
				<span class="data-label"><?php echo JTEXT::_("PURCHASE_DATE");?></span>
				<?php if( $this->getParentinfo == null ){ ?>
					<span class="data-text"><?php echo JHTML::_('date', $this->userinfo->plandetails->plan_subscription_from_date, 'd-M-y'); ?></span>
				<?php } else { ?>
					<span class="data-text"><?php echo JHTML::_('date', $this->getParentsubscriptioninfo->plan_subscription_from_date, 'd-M-y'); ?></span>
				<?php } ?>
				<span class="data-text"></span>
			</li>
			<li>
				<span class="data-label"><?php echo JTEXT::_("LAST_PAYMENT");?></span>
				<span class="data-text"><?php echo JHTML::_('date',$this->userinfo->date, 'd-M-y'); ?></span>
			</li>
			<li>
				<span class="data-label"><?php echo JTEXT::_("EXPIRY");?></span>
				<?php if( $this->getParentinfo == null ){ ?>
					<!-- <span class="data-text"><?php //echo JworkspacedateHTML::_('date',$this->userinfo->plandetails->plan_subscription_to_date, 'd-M-y'); ?></span> -->
					<span class="data-text"><?php echo JHTML::_('date',$this->userinfo->plandetails->plan_subscription_to_date, 'd-M-y'); ?></span>
				<?php } else { ?>
					<span class="data-text"><?php echo JHTML::_('date',$this->getParentsubscriptioninfo->plan_subscription_to_date, 'd-M-y'); ?></span>
				<?php } ?>
			</li>
			<?php //$params= unserialize(base64_decode($this->userinfo->plandetails->par));
			if( $this->getParentinfo == null ) {
				$totalAccount   = $this->getCreatedAccountFromAEC;
				$createdAccount = $this->getCreatedAccount;
				//echo $totalAccount."/".$createdAccount; ?>
				<li>
					<span class="data-label"><?php echo JTEXT::_("TOTAL_USERS_ADDED");?></span>
					<span class="data-text">
						<a href="index.php?option=com_adduserfrontend&view=childusers">
							<?php echo "(".$createdAccount."/".$totalAccount.")"; ?>
						</a>
						<?php //if($this->getCreatedAccount <= (($params['userlimit']-1))) {
						if($createdAccount < $totalAccount && empty($this->allowAdd)) {
							echo "&nbsp;<a class='createuserlink' href='index.php?option=com_adduserfrontend&view=adduserfrontend'>Create New User</a>";
						} ?>
					</span>
				</li>
			<?php } ?>
		</ul>
	</div>
</div>

