<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$doc->addStyleSheet(JURI::base().'components/com_mica/css/dashboard.css', 'text/css');
?>

<h1>
	<?php echo JTEXT::_("My Profile");?>
</h1>

<div class="dashboard box-block">	
	<div class="data-rightpanel">
		<ul>
			<li>
				<span class="data-label"><?php echo JTEXT::_("USERNAME");?></span>
				<span class="data-text">
					<?php echo $this->userinfo->username; ?>
				</span>
			</li>
			<li>
				<span class="data-label"><?php echo JTEXT::_("EMAIL");?></span>
				<span class="data-text">
					<?php echo $this->userinfo->email; ?>
				</span>
			</li>
			<!-- <li>
				<span class="data-label"><?php echo JTEXT::_("CHANGE_PASSWORD"); ?></span>
				<span class="data-text">
					<?php echo JTEXT::_("CHANGE_PASSWORD"); ?>
				</span>
			</li> -->
			<li>
				<span class="data-label"><?php echo JTEXT::_("DASHBOARD_COMPANY");?></span>
				<span class="data-text">
					<?php if( $this->getParentinfo == null ) { echo $this->userinfo->institute_name; } else { echo $this->getParentinfo->institute_name;} ?>
				</span>
			</li>
			<li>
				<span class="data-label"><?php echo JTEXT::_("ADDRESS");?></span>
				<span class="data-text">
					<?php if( $this->getParentinfo == null ) { echo $this->userinfo->address; } else { echo $this->getParentinfo->address;} ?>
				</span>
			</li>
			<li>
				<span class="data-label"><?php echo JTEXT::_("CONTEECT_NUMBER");?></span>
				<span class="data-text">
					<?php if( $this->getParentinfo == null ) { echo $this->userinfo->contactnumber; } else { echo $this->getParentinfo->contactnumber;} ?>
				</span>
			</li>
			<li>
				<span class="data-label"><?php echo JTEXT::_("MOBILE");?></span>
				<span class="data-text">
					<?php if( $this->getParentinfo == null ) { echo $this->userinfo->mobile; } else { echo $this->getParentinfo->mobile;} ?>
				</span>
			</li>
			<li>
				<span class="data-label"><?php echo JTEXT::_("PAYMENT_TYPE");?></span>
				<span class="data-text">
					<?php if( $this->getParentinfo == null ){
						if($this->userinfo->paymentoption==1) { $ptype="Cheque ".$this->userinfo->bankname; } else { $ptype="DD ".$this->userinfo->bankname; } echo $ptype;
					}else{
						if($this->getParentinfo->paymentoption==1) { $ptype="Cheque ".$this->getParentinfo->bankname; } else { $ptype="DD ".$this->getParentinfo->bankname; } echo $ptype;
					} ?>
				</span>
			</li>
		</ul>
	</div>
</div>
