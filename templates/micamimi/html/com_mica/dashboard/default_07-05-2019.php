<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

$doc = JFactory::getDocument();
$doc->addStyleSheet(JURI::base().'components/com_mica/css/dashboard.css', 'text/css');
?>

<div class="dashboard">
	<div class="leftpanel">
		<div class="header-leftpanel"><?php echo JTEXT::_("MY_WORKSPACE");?></div>
		<div class="data-leftpanel">
			<ul>
				<li>
					<span class='workspacelink' style='width:70%'>
						<b>Name</b>
					</span>
					<span class='workspacedate' style='width:30%'>
						<b>Last Modify</b>
					</span>
				</li>
				<?php
				foreach ($this->Workspace as $eachworkspace){

					$modify_date = " ---";
					if($eachworkspace['modify_date']!="0000-00-00 00:00:00"){
						$modify_date = JHTML::_('date', $eachworkspace['modify_date'], 'd-M-y');
					}?>
					<li >
						<span class='workspacelink'>
							<?php
								$type = unserialize($eachworkspace['data'])['dataof'] ? unserialize($eachworkspace['data'])['dataof'] : "District";
								$onclick = "changeWorkspace(".$eachworkspace['id'].",'".$type."')";
							?>
							<a href="#" onclick="<?php echo $onclick ?>"><?php echo $eachworkspace['name']; ?></a>
						</span>
						<span class='workspacedate' ><?php echo $modify_date; ?></span>
					</li>
				<?php } ?>
				<li class='exploredatalink' style="height:25px;">
					<a href="<?php echo JRoute::_( 'index.php?option=com_mica&view=micafront&Itemid=188' ); ?>">
						<?php echo JTEXT::_("Explore_Data"); ?>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="rightpanel">
		<div class="header-rightpanel">
			<?php echo JTEXT::_("Total Sub Users");?>
		</div>
		<div class="data-rightpanel">
			<ul>
				<li>
					<?php
						$totalAccount   = $this->getCreatedAccountFromAEC;
						$createdAccount = $this->getCreatedAccount;

					 	echo "<h4><b> Created sub user count :- </b>".$createdAccount."</h4>";
					?>
				</li>
				<li>
					<?php
						echo "<h4><b> Total sub user count :- </b>".$totalAccount."</h4>";
					?>
				</li>
			</ul>
		</div>
		<?php if($this->UserStatistics->allow_districtdata == 1){  ?>

		<div class="header-rightpanel">
			<?php echo JTEXT::_("District Download Data");?>
		</div>
		<div class="data-rightpanel">
			<ul>
				<canvas id="district-download-pie-chart" width="800" height="450"></canvas>
				<li>
					<?php
						//echo "<pre/>";print_r($this->UserStatistics);exit;
						$DownloadedCount             = $this->UserStatistics->mimi_download_data;
						$totalDownloadCount          = $this->UserStatistics->downloadpm_limit;
						$totalDownloadAvailableCount = $totalDownloadCount - $DownloadedCount;

					 	echo "<h4><b> Downloaded Count :- </b>".$this->UserStatistics->mimi_download_data."</h4>";
					?>
				</li>
				<li>
					<?php
						echo "<h4><b> Available Download Count :- </b>".$totalDownloadAvailableCount."</h4>";
					?>
				</li>
				<li>
					<?php
						echo "<h4><b> Total Download Count :- </b>".$totalDownloadCount."</h4>";
					?>
				</li>
			</ul>
		</div>

		<div class="header-rightpanel">
			<?php echo JTEXT::_("District Record View Data");?>
		</div>
		<div class="data-rightpanel">
			<ul>
				<canvas id="district-view-pie-chart" width="800" height="450"></canvas>
				<li>
					<?php
						$mimiRecordViews              = $this->UserStatistics->mimi_view_records;
						$totalMimiViewRecordLimit     = $this->UserStatistics->records_limit;
						$availableMimiViewRecordCount = $totalMimiViewRecordLimit - $mimiRecordViews;

					 	echo "<h4><b> Record Views Count:- </b>".$mimiRecordViews."</h4>";
					?>
				</li>
				<li>
					<?php
						echo "<h4><b> Available View Record Count :- </b>".$availableMimiViewRecordCount."</h4>";
					?>
				</li>
				<li>
					<?php
						echo "<h4><b> Total View Record Limit :- </b>".$totalMimiViewRecordLimit."</h4>";
					?>
				</li>
			</ul>
		</div>
		<?php } ?>

		<?php if($this->UserStatistics->allow_villagedata == 1){  ?>
		<div class="header-rightpanel">
			<?php echo JTEXT::_("Village Download Data");?>
		</div>
		<div class="data-rightpanel">
			<ul>
				<canvas id="village-download-pie-chart" width="800" height="450"></canvas>
				<li>
					<?php
						$mvmiDownloadedCount        = $this->UserStatistics->mvmi_download_data;
						$totalMvmiDownloadCount     = $this->UserStatistics->mvmi_downloadpm_limit;
						$availableMvmiDownloadCount = $totalMvmiDownloadCount - $mvmiDownloadedCount;

					 	echo "<h4><b> Downloaded Count :- </b>".$mvmiDownloadedCount."</h4>";
					?>
				</li>
				<li>
					<?php
						echo "<h4><b> Available Download Count :- </b>".$availableMvmiDownloadCount."</h4>";
					?>
				</li>
				<li>
					<?php
						echo "<h4><b> Total Download Count :- </b>".$totalMvmiDownloadCount."</h4>";
					?>
				</li>

			</ul>
		</div>

		<div class="header-rightpanel">
			<?php echo JTEXT::_("Village Record View Data");?>
		</div>
		<div class="data-rightpanel">
			<ul>
				<canvas id="village-view-pie-chart" width="800" height="450"></canvas>
				<li>
					<?php
						$mvmiRecordViewsCount          = $this->UserStatistics->mvmi_view_records;
						$totalMvmiViewRecordLimitCount = $this->UserStatistics->mvmi_records_limit;
						$availableMvmiViewRecordCount  = $totalMvmiViewRecordLimitCount - $mvmiRecordViewsCount;

					 	echo "<h4><b> Record Views :- </b>".$mvmiRecordViewsCount."</h4>";
					?>
				</li>
				<li>
					<?php
						echo "<h4><b> Total View Record Available Count :- </b>".$availableMvmiViewRecordCount."</h4>";
					?>
				</li>
				<li>
					<?php
						echo "<h4><b> Total View Record Limit Count :- </b>".$totalMvmiViewRecordLimitCount."</h4>";
					?>
				</li>
			</ul>
		</div>
		<?php } ?>

		<div class="header-rightpanel">
			<?php echo JTEXT::_("MY_ACCOUNT");?>
		</div>
		<div class="data-rightpanel">
			<ul>
				<li>
					<span class="data-label"><?php echo JTEXT::_("USERNAME");?></span>
					<span class="data-text">
						<?php echo $this->userinfo->username; ?>
					</span>
				</li>
				<li>
					<span class="data-label"><?php echo JTEXT::_("EMAIL");?></span>
					<span class="data-text">
						<?php echo $this->userinfo->email; ?>
					</span>
				</li>
				<?php /*<li>
					<span class="data-label">< ?php echo JTEXT::_("CHANGE_PASSWORD");? ></span>
					<span class="data-text">
						< ?php echo JTEXT::_("CHANGE_PASSWORD");? >
					</span>
				</li> */ ?>
				<li>
					<span class="data-label"><?php echo JTEXT::_("DASHBOARD_COMPANY");?></span>
					<span class="data-text">
						<?php if( $this->getParentinfo == null ) { echo $this->userinfo->institute_name; } else { echo $this->getParentinfo->institute_name;} ?>
					</span>
				</li>
				<li>
					<span class="data-label"><?php echo JTEXT::_("ADDRESS");?></span>
					<span class="data-text">
						<?php if( $this->getParentinfo == null ) { echo $this->userinfo->address; } else { echo $this->getParentinfo->address;} ?>
					</span>
				</li>
				<li>
					<span class="data-label"><?php echo JTEXT::_("CONTEECT_NUMBER");?></span>
					<span class="data-text">
						<?php if( $this->getParentinfo == null ) { echo $this->userinfo->contactnumber; } else { echo $this->getParentinfo->contactnumber;} ?>
					</span>
				</li>
				<li>
					<span class="data-label"><?php echo JTEXT::_("MOBILE");?></span>
					<span class="data-text">
						<?php if( $this->getParentinfo == null ) { echo $this->userinfo->mobile; } else { echo $this->getParentinfo->mobile;} ?>
					</span>
				</li>
				<li>
					<span class="data-label"><?php echo JTEXT::_("PAYMENT_TYPE");?></span>
					<span class="data-text">
						<?php if( $this->getParentinfo == null ){
							if($this->userinfo->paymentoption==1) { $ptype="Cheque ".$this->userinfo->bankname; } else { $ptype="DD ".$this->userinfo->bankname; } echo $ptype;
						}else{
							if($this->getParentinfo->paymentoption==1) { $ptype="Cheque ".$this->getParentinfo->bankname; } else { $ptype="DD ".$this->getParentinfo->bankname; } echo $ptype;
						} ?>
					</span>
				</li>
			</ul>
			<div class="header-rightpanel">
				<?php echo JTEXT::_("MY_SUBSCRIPTION"); ?>
			</div>
			<div class="data-rightpanel">
				<ul>
					<li>
						<span class="data-label"><?php echo JTEXT::_("PLAN");?></span>
						<?php if( $this->getParentinfo == null ){ ?>
							<span class="data-text"><?php echo $this->userinfo->plandetails->title; ?></span>
						<?php } else { ?>
							<span class="data-text"><?php print_r($this->getParentsubscriptioninfo->title); ?></span>
						<?php } ?>
					 </li>
					<li>
						<span class="data-label"><?php echo JTEXT::_("PURCHASE_DATE");?></span>
						<?php if( $this->getParentinfo == null ){ ?>
							<span class="data-text"><?php echo JHTML::_('date', $this->userinfo->plandetails->plan_subscription_from_date, 'd-M-y'); ?></span>
						<?php } else { ?>
							<span class="data-text"><?php echo JHTML::_('date', $this->getParentsubscriptioninfo->plan_subscription_from_date, 'd-M-y'); ?></span>
						<?php } ?>
						<span class="data-text"></span>
					</li>
					<li>
						<span class="data-label"><?php echo JTEXT::_("LAST_PAYMENT");?></span>
						<span class="data-text"><?php echo JHTML::_('date',$this->userinfo->date, 'd-M-y'); ?></span>
					</li>
					<li>
						<span class="data-label"><?php echo JTEXT::_("EXPIRY");?></span>
						<?php if( $this->getParentinfo == null ){ ?>
							<!-- <span class="data-text"><?php //echo JworkspacedateHTML::_('date',$this->userinfo->plandetails->plan_subscription_to_date, 'd-M-y'); ?></span> -->
							<span class="data-text"><?php echo JHTML::_('date',$this->userinfo->plandetails->plan_subscription_to_date, 'd-M-y'); ?></span>
						<?php } else { ?>
							<span class="data-text"><?php echo JHTML::_('date',$this->getParentsubscriptioninfo->plan_subscription_to_date, 'd-M-y'); ?></span>
						<?php } ?>
					</li>
					<?php //$params= unserialize(base64_decode($this->userinfo->plandetails->par));
					if( $this->getParentinfo == null ) {
						$totalAccount   = $this->getCreatedAccountFromAEC;
						$createdAccount = $this->getCreatedAccount;
						//echo $totalAccount."/".$createdAccount; ?>
						<li>
							<span class="data-label"><?php echo JTEXT::_("TOTAL_USERS_ADDED");?></span>
							<span class="data-text">
								<a href="index.php?option=com_adduserfrontend&view=childusers">
									<?php echo "(".$createdAccount."/".$totalAccount.")"; ?>
								</a>
								<?php //if($this->getCreatedAccount <= (($params['userlimit']-1))) {
								if($createdAccount < $totalAccount && empty($this->allowAdd)) {
									echo "&nbsp;<a class='createuserlink' href='index.php?option=com_adduserfrontend&view=adduserfrontend'>Create New User</a>";
								} ?>
							</span>
						</li>
					<?php } ?>
				</ul>
			</div>
		</div>
	</div>
</div>

<script src="<?php echo JURI::base()?>components/com_mica/js/Chart.bundle.js"></script>
<script type="text/javascript">

function changeWorkspace(value, type){
	jQuery("#main_loader").show();
	if(value == 0){
		jQuery('.createneworkspace').css({'display':'block'});
		jQuery('.createnewworkspace').css({'display':''});
		jQuery('.thematiceditoption').css('display','block');
	}else{
		if(type == "District"){
			jQuery.ajax({
	            url: "index.php?option=com_mica&task=showresults.loadWorkspace&workspaceid=" + value + "&tmpl=component",
	            method: 'GET',
	            success: function(data) {
	                result = jQuery.parseJSON(data);

	                jQuery("#workspacceedit").load(location.href + " #workspacceedit>*", "");

	                jQuery('#leftcontainer').load(location.href + " #leftcontainer>*", "", function() {
	                    getAttribute_v2(5, "district");

	                    if (typeof(preselected) != "undefined") {
	                        displaycombo_v2();
	                    }
	                });

	                jQuery("#activeworkspacename").html(result[0].name);
	                jQuery("#workspacceedit").hide();
	                jQuery("#fade").hide();
	                jQuery("#default").show();
	                window.location = "index.php?option=com_mica&view=micafront&Itemid=302";
	                //update form fields  as per selected workspace id
	                setTimeout(function() {
	                    jQuery('.scrollbar-inner').scrollbar();
	                    //getDataNew();
	                }, 1000);
	            }
	        });
		}

		if(type == "villages" || type == "Villages")
		{
			jQuery.ajax({
				url: "index.php?option=com_mica&task=villageshowresults.loadWorkspace&workspaceid="+value + "&view=villageshowresults&tmpl=component",
				method: 'GET',
				success: function(data)
				{
					result = jQuery.parseJSON(data);

					jQuery("#workspacceedit").load(location.href + " #workspacceedit>*", "");
					jQuery('#leftcontainer').load(location.href + " #leftcontainer>*", "", function() {
	                    getAttribute(9, "district");

	                    if (typeof(preselected) != "undefined") {
	                        displaycombo();
	                    }
	                });
	                jQuery("#activeworkspacename").html(result[0].name);
	                jQuery("#workspacceedit").hide();
	                jQuery("#fade").hide();
	                jQuery("#default").show();

	                window.location = "index.php?option=com_mica&view=villagefront&Itemid=388";

	                setTimeout(function() {
	                    jQuery('.scrollbar-inner').scrollbar();
	                    //getDataNew();
	                }, 1000);
	            }
	        });
		}

		if(type == "villageSummary"){
			jQuery.ajax({
				url: "index.php?option=com_mica&task=summeryresults.loadWorkspace&workspaceid="+value + "&view=summaryshowresults&tmpl=component",
				method: 'GET',
				success: function(data)
				{
					result = jQuery.parseJSON(data);

					jQuery("#workspacceedit").load(location.href + " #workspacceedit>*", "");
					jQuery('#leftcontainer').load(location.href + " #leftcontainer>*", "", function() {
	                        getAttribute(9, "district");

	                        if (typeof(preselected) != "undefined") {
	                            displaycombo();
	                        }
	                    });
	 				jQuery("#activeworkspacename").html(result[0].name);
	                jQuery("#workspacceedit").hide();
	                jQuery("#fade").hide();
	               	jQuery("#default").show();

	               	window.location = "index.php?option=com_mica&view=summeryfront&Itemid=398";

	               	setTimeout(function () {
	               		jQuery('.scrollbar-inner').scrollbar();
	               	}, 1000);
	            }
	        });
		}
	}
}

// START :: District Download Pie Chart
var canvas = document.getElementById("district-download-pie-chart");
var ctx = canvas.getContext('2d');

var data = {
    labels: ["Downloaded Count", "Available Download Count"],
     datasets: [{
        label: "District Download Data",
        backgroundColor: ["#B22222", "#006400","#1A5276"],
        data: [<?php echo $DownloadedCount.','. $totalDownloadAvailableCount;  ?>]
      }]
};

var options = {
    title: {
    display: true,
    text: 'District Download Data'
  }
};

// Chart declaration:
var myBarChart = new Chart(ctx, {
    type: 'doughnut',
    data: data,
    options: options
});
// END

// START :: District Record View Pie Chart
var districtViewPieChartCanvas = document.getElementById("district-view-pie-chart");
var districtViewPieChartCtx    = districtViewPieChartCanvas.getContext('2d');

var data = {
    labels: ["Record Views Count", "Available View Record Count"],
     datasets: [{
        label: "District Record View Data",
        backgroundColor: ["#B22222", "#006400","#1A5276"],
        data: [<?php echo $mimiRecordViews.','. $availableMimiViewRecordCount;  ?>]
      }]
};

var options = {
    title: {
    display: true,
    text: 'District Record View Data'
  }
};

// Chart declaration:
var districtViewPieChartCtxChart = new Chart(districtViewPieChartCtx, {
    type: 'doughnut',
    data: data,
    options: options
});
// END


// START :: Village Download Pie Chart
var canvas = document.getElementById("village-download-pie-chart");
var ctx = canvas.getContext('2d');

var data = {
    labels: ["Downloaded Count", "Available Download Count"],
     datasets: [{
        label: "Village Download Data",
        backgroundColor: ["#B22222", "#006400","#1A5276"],
        data: [<?php echo $mvmiDownloadedCount.','. $availableMvmiDownloadCount;  ?>]
      }]
};

var options = {
    title: {
    display: true,
    text: 'Village Download Data'
  }
};

// Chart declaration:
var myBarChart = new Chart(ctx, {
    type: 'doughnut',
    data: data,
    options: options
});
// END

// START :: Village Record View Pie Chart
var villageViewPieChartCanvas = document.getElementById("village-view-pie-chart");
var villageViewPieChartCtx    = villageViewPieChartCanvas.getContext('2d');

var data = {
    labels: ["Record Views Count", "Available View Record Count"],
     datasets: [{
        label: "village Record View Data",
        backgroundColor: ["#B22222", "#006400","#1A5276"],
        data: [<?php echo $mvmiRecordViewsCount.','. $availableMvmiViewRecordCount;  ?>]
      }]
};

var options = {
    title: {
    display: true,
    text: 'Village Record View Data'
  	}
};

// Chart declaration:
var villageViewPieChartCtxChart = new Chart(villageViewPieChartCtx, {
    type: 'doughnut',
    data: data,
    options: options
});
// END

</script>
