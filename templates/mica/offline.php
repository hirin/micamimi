<?php
/**
 * @package     Joomla.Site
 * @subpackage  Templates.mica
 *
 * @copyright   Copyright (C) 2005 - 2017 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/** @var JDocumentHtml $this */

$twofactormethods = JAuthenticationHelper::getTwoFactorMethods();
$app              = JFactory::getApplication();

// Output as HTML5
$this->setHtml5(true);

$fullWidth = 1;


JHtml::_('bootstrap.framework');// Add JavaScript Frameworks
JHtml::_('script', 'template.js', array('version' => 'auto', 'relative' => true));// Add template js
JHtml::_('script', 'jui/html5.js', array('version' => 'auto', 'relative' => true, 'conditional' => 'lt IE 9'));// Add html5 shiv
JHtml::_('stylesheet', 'template.css', array('version' => 'auto', 'relative' => true));// Add Stylesheets
JHtml::_('stylesheet', 'offline.css', array('version' => 'auto', 'relative' => true));
JHtml::_('stylesheet', 'user.css', array('version' => 'auto', 'relative' => true));// Check for a custom CSS file
JHtml::_('script', 'user.js', array('version' => 'auto', 'relative' => true));// Check for a custom js file
JHtml::_('bootstrap.loadCss', false, $this->direction);// Load optional RTL Bootstrap CSS

$sitename = $app->get('sitename');// Logo file or site title param

?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<jdoc:include type="head" />
</head>
<body class="site">
	<div class="outer">
		<div class="middle">
			<div class="inner well">

				<div class="header">
					<div id="logo">
						<h1 class="logo_img" title="mica">
							<a href="<?php echo $this->baseurl; ?>/" >
								<img src="<?php echo $this->baseurl; ?>/templates/<?php echo $this->template; ?>/images/logo_home.png" border="0" alt="mica" />
							</a>
						</h1>
					</div>

					<?php if ($app->get('offline_image') && file_exists($app->get('offline_image'))) : ?>
						<img src="<?php echo $app->get('offline_image'); ?>" alt="<?php echo htmlspecialchars($app->get('sitename')); ?>" />
					<?php endif; ?>

					<?php if ($app->get('display_offline_message', 1) == 1 && str_replace(' ', '', $app->get('offline_message')) != '') : ?>
						<p><?php echo $app->get('offline_message'); ?></p>
					<?php elseif ($app->get('display_offline_message', 1) == 2) : ?>
						<p><?php echo JText::_('JOFFLINE_MESSAGE'); ?></p>
					<?php endif; ?>

				</div>
				<jdoc:include type="message" />

				<form action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="form-login">
					<fieldset>
						<label for="username"><?php echo JText::_('JGLOBAL_USERNAME'); ?></label>
						<input name="username" id="username" type="text" title="<?php echo JText::_('JGLOBAL_USERNAME'); ?>" />

						<label for="password"><?php echo JText::_('JGLOBAL_PASSWORD'); ?></label>
						<input type="password" name="password" id="password" title="<?php echo JText::_('JGLOBAL_PASSWORD'); ?>" />

						<?php if (count($twofactormethods) > 1) : ?>
						<label for="secretkey"><?php echo JText::_('JGLOBAL_SECRETKEY'); ?></label>
						<input type="text" name="secretkey" id="secretkey" title="<?php echo JText::_('JGLOBAL_SECRETKEY'); ?>" />
						<?php endif; ?>

						<input type="submit" name="Submit" class="btn btn-primary" value="<?php echo JText::_('JLOGIN'); ?>" />

						<input type="hidden" name="option" value="com_users" />
						<input type="hidden" name="task" value="user.login" />
						<input type="hidden" name="return" value="<?php echo base64_encode(JUri::base()); ?>" />
						<?php echo JHtml::_('form.token'); ?>
					</fieldset>
				</form>

			</div>
		</div>
	</div>
</body>
</html>
