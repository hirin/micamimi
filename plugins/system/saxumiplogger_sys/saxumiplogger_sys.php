<?php
/**
 * $Id: saxumiplogger_sys.php 18 2013-12-19 12:26:36Z Szablac $
 * @Project		Saxum IPLogger Extension/Component
 * @author 		Laszlo Szabo
 * @package		Saxum IPLogger
 * @copyright	Copyright (C) 2010 Saxum 2003 Bt. All rights reserved.
 * @license 	http://www.gnu.org/licenses/old-licenses/gpl-3.0.html GNU/GPL version 3
*/

defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.plugin.plugin');

class plgSystemSaxumiplogger_sys extends JPlugin {

	function onAfterInitialise()
	{
		$mainframe = JFactory::getApplication();
		$session = JFactory::getSession();
		$db = JFactory::getDBO();
		
		$date = JFactory::getDate();

		jimport('joomla.application.component.helper');
		$params	= JComponentHelper::getParams('com_saxumiplogger');
		require_once JPATH_ADMINISTRATOR.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_saxumiplogger'.DIRECTORY_SEPARATOR.'controller.php';
		$ip=SaxumiploggerController::getip();		
		
		$db = JFactory::getDBO();
		$sql = $db->getQuery(true)
			->select('ip')
			->from('#__saxum_iplogger_excp');
		$db->setQuery($sql);
		$iptable = $db->loadColumn();
		$no=SaxumiploggerController::ipinlist($iptable,$ip);
		
		if($no!=1)
		{		
			$rowid = $session->get('saxumipid');
			if (!isset($rowid)) {
			    if ($params->get('client_to_log')!=2 and $mainframe->getClientId()==1){
		            $db->setQuery("insert into #__saxum_iplogger (user_id, ip, visitDate, client_id) values (0,'".$ip."',".$db->Quote($date->toSql()).",".$mainframe->getClientId().")");
		            $db->query();
		        }
		        if ($params->get('client_to_log')!=1 and $mainframe->getClientId()==0){
		            $db->setQuery("insert into #__saxum_iplogger (user_id, ip, visitDate, client_id) values (0,'".$ip."',".$db->Quote($date->toSql()).",".$mainframe->getClientId().")");
		            $db->query();
		        }
		        $rowid=$db->insertid();
		        $session->set('saxumipid', $rowid);
			}
		}
		
	    return true;
	}

}
