<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Content.Contact
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

use Joomla\Registry\Registry;

/**
 * Contact Plugin
 *
 * @since  3.2
 */
class PlgContentTableaucodes extends JPlugin
{
	/**
	 * Database object
	 *
	 * @var    JDatabaseDriver
	 * @since  3.3
	 */
	protected $db;

	/**
	 * Plugin that retrieves contact information for contact
	 *
	 * @param   string   $context  The context of the content being passed to the plugin.
	 * @param   mixed    &$row     An object with a "text" property
	 * @param   mixed    $params   Additional parameters. See {@see PlgContentContent()}.
	 * @param   integer  $page     Optional page number. Unused. Defaults to zero.
	 *
	 * @return  boolean	True on success.
	 */
	public function onContentPrepare($context, &$article, &$params, $page = 0)
	{
		$allowed_contexts = array('com_content.category', 'com_content.article', 'com_content.featured');

		if (!in_array($context, $allowed_contexts))
		{
			return true;
		}

		// Return if we don't have valid params or don't link the author
		if (!($params instanceof Registry) || !$params->get('link_author'))
		{
			return true;
		}

		// Return if we don't have a valid article id
		if (!isset($article->id) || !(int) $article->id)
		{
			return true;
		}

		// Expression to search for (tableaucodes)
		$regex = '/{tableaucode:(.*?)}/i';
		$style = $this->params->def('style', 'none');
	


		// Find all instances of plugin and put in $matches for loadposition
		// $matches[0] is full pattern match, $matches[1] is the position
		preg_match_all($regex, $article->text, $matches, PREG_SET_ORDER);

		// No matches, skip this
		if ($matches)
		{
			foreach ($matches as $match)
			{
				
				if($match[1]!=''){
					$query = $this->db->getQuery(true);
					$query->select('description');
					$query->from($this->db->quoteName('#__mica_tableau_pages', 'tableaucode'));
					$query->where('state = 1');
					$query->where('code = "'.$match[1].'"');
					$this->db->setQuery($query);
					$obj= $this->db->loadObject();
					$article->text = preg_replace("|$match[0]|", addcslashes($obj->description, '\\$'), $article->text, 1);
				}
			}
		}	
	}	
}
