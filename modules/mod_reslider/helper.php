<?php

#@license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL

defined('_JEXEC') or die;

class ModResliderHelper{

	public static function addInquiryAjax()
	{
		$jinput    = JFactory::getApplication()->input;
		$name      = $jinput->getString('name');
		$email     = $jinput->getString('email');
		$phone     = $jinput->getString('phone');
		$message   = $jinput->getString('message');
		$date      = JFactory::getDate();
		$create_at = $date->toSql(true);

		// Get a db connection.
		$db = JFactory::getDbo();

		// Create a new query object.
		$query = $db->getQuery(true);

		// Insert columns.
		$columns = array('name', 'email', 'phone', 'message', 'create_at');

		// Insert values.
		$values = array($db->quote($name), $db->quote($email), $db->quote($phone), $db->quote($message), $db->quote($create_at));

		// Prepare the insert query.
		$query
		    ->insert($db->quoteName('#__user_question_inquiry'))
		    ->columns($db->quoteName($columns))
		    ->values(implode(',', $values));

		// Set the query using our newly populated query object and execute it.
		$db->setQuery($query);
		$db->execute();

		$session = JFactory::getSession();
		$session->set("is_show_ans", "1");

		$response = array(
			'success' => '1',
			'msg'     => 'User Inquiry Submitted Succesfully.',
        );

        return json_encode($response);
	}

	public static function getImages(&$params){

		$imgsAndCaps = array();
		$i = 1;
		$database = JFactory::getDBO();

		while($i < 11):

			if($params->get('image'.$i)){
				$imageQues = JURI::root().$params->get('image'.$i);
				$imageAns  = JURI::root().$params->get('imageAns'.$i);
				$onclick   = "onclick=displayResult('".$imageQues."','".$imageAns."',".$i.")";
				$listitem  = "<li><img class='imageQues".$i."' id='myImage".$i."' src='".JURI::root().$params->get('image'.$i)."' alt='".$params->get('image'.$i.'alt')."'><p class='flex-caption'><button class='viewAnswer".$i."' id='viewAnswer".$i."' ".$onclick.">Answer</button></p></li>";
				array_push($imgsAndCaps, $listitem);
			}

			$i++;
		endwhile;
		return $imgsAndCaps;
	}

	public static function load_jquery(&$params){
		if($params->get('load_jquery')){
			JLoader::import( 'joomla.version' );
			$version = new JVersion();
			if (version_compare( $version->RELEASE, '2.5', '<=')) {
					$doc = &JFactory::getDocument();
					$app = &JFactory::getApplication();
					$file="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js";
					$file2="/modules/mod_reslider/assets/js/no-conflict.js";
					$doc->addScript($file);
					$doc->addScript($file2);
			} else {
				JHtml::_('jquery.framework');
			}
		}
	}
}
