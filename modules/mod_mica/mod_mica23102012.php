<?php
/**
* @version		$Id: mod_mica.php 14401 2010-01-26 14:10:00Z louis $
* @package		Joomla
* @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
$itemid = JRequest::getVar('Itemid',0);
//$state = JRequest::getVar('state','');
//$district = JRequest::getVar('district','');
//$town = JRequest::getVar('town','');
//$m_type = JRequest::getVar('m_type','');
//$attributes = JRequest::getVar('attributes',array());
 $db_table= JRequest::getVar('db_table');
 $session = JFactory::getSession();
 $m_type=$session->get('m_type');
//$new_name = JRequest::getVar('new_name',array());
//$custom_attribute = JRequest::getVar('custom_attribute',array());


function getattributelist($db_table)
{

		/*if($zoom == 5)
		{
			$db_table="State";
		}
		else if($zoom > 5 && $zoom <= 7)
		{
			$db_table="District";
		}
		else
		{
			$db_table="Town";
		}*/
		
		 $userAttr=userSelectedAttr($db_table);

		
		if($userAttr==-2)
		{
			return -2;
		}
	$str="";
	$todisplay=array();
		$db =& JFactory::getDBO();
		 $query="select  * from #__mica_group_field allfields JOIN #__mica_group grp ON grp.id=allfields.groupid where allfields.table like '".$db_table."' and grp.publish =1 ";
		$db->setQuery($query);
		$db->query();
		 $list=$db->loadAssocList();//echo "<pre>";print_r($list);exit;
		$change_group_name=1;
		$icon=array();
		foreach($list as $each)
		{
			if($change_group_name==1)
			{
				$change_group_name=0;
				
			}
				$each['field']=str_replace("Rural_","",$each['field']);
				$each['field']=str_replace("Urban_","",$each['field']);
				$each['field']=str_replace("Total_","",$each['field']);
				$todisplay[$each['group']][]=$each['field'];
				$icon[$each['group']]=$each['icon'];
			$todisplay[$each['group']][]=$each['field'];
			$groupid[]=$each['groupid'];
		}
		foreach($todisplay as $eachgrp=>$vals)
		{
			$todisplay[$eachgrp]=array_unique($todisplay[$eachgrp]);
		}
		//echo "<pre>";print_R($todisplay);exit;
	return array($todisplay,$groupid,$icon);
}
function userSelectedAttr($db_table)
	{
		$db =& JFactory::getDBO();
 		$user = &JFactory::getUser();
		$where		= "status =  'Active' and userid =".$user->id;
		

		$query = ' SELECT plan'
			. ' FROM #__acctexp_subscr AS a where '
			
			. $where

		;
		$db->setQuery($query);
		$planrow = $db->loadRow();

		 $query="select attribute from #__mica_user_attribute where aec_plan_id=".$planrow[0]." and dbtable like '".$db_table."'";
		$db->setQuery($query);
		$db->query();
		$row = $db->loadRow();
		$attr=explode(",",$row[0]);
		return $attr;
		
	}
	

	function displaysttributes($grparray)
	{	$todisplay=$grparray[0];
		$grpid=$grparray[1];
		$icon=$grparray[2];
		//echo "<pre>";print_r($todisplay);exit;
		$str= "";
		$session = JFactory::getSession();
		$userAttr=$session->get('attributes');
		$userAttr=explode(",",$userAttr);
		
		 
		$popup=3;
		$i=0;
		foreach($todisplay as $key=>$val)
		{	if(($i%2)==0){$applyclass="clear";}else{$applyclass="";}
			if($icon[$key]!=""){$img='<img src="'.JUri::base().'components/com_mica/images/'.$icon[$key].'"/>';}else{$img="";}
			$str .='<li class="'.$applyclass.'"><a href="javascript:void(0)" onclick="document.getElementById(\'light'.$popup.'\').style.display=\'block\';document.getElementById(\'fade\').style.display=\'block\'"><div class="leftgrpimg">'.$img.'</div><div class="rightgrptext">'.$key;//'.$grpid[count($val)+1].'"
		$popup++;

			$chk_count=0;
			$unchk_count=0;
			
		foreach($userAttr as $eachattr)
		{
			//$i++;
			if(in_array($eachattr,$val))
			{
				$chk_count++;
       		 		
			}
			//$unchk_count++;
		}
		$str.= " (<b>".$chk_count."</b> / ".count($val).")</div></a></li>";
		$i++;
		}

		//$str .="</ul>";
		echo $str;




	}
	$listtoshow=getattributelist($db_table);//echo "in mod";exit;
?>

<div class="mod_mica">
	<div class="collpsable">

		<span class="toggle" id="vw"> Variable &amp; Workspace</span>
		<span id="tq" class="toggle" >Thematic Query</span>
			<span class="toggle" id="matrix" > Matrix </span>
			<span class="toggle" id="speedometer" style="border-right:none;"> Speedometer </span>
		<span  class="modify toggle" ><?php echo $this->workspacelinkdiv; ?></span>
	</div>	
	<div class="activecontent">
		<div class="vw contenttoggle" style="display: none; ">
		<div class="vwsapretor clearfix">
			<div class="contentblock">
				<div class="contenthead">
				Variable Groups
				</div>
				<div class="blockcontent">
					<ul>
<?php
	displaysttributes($listtoshow);
	?>
	
			
					
				</ul>
				</div>
				
			</div>	
<?php 

$customarray=explode(",",$this->customattribute);
$custolibmarray=explode(",",$this->customattributelib);
$finalarray=array(array_filter($customarray),array_filter($custolibmarray));
?>
<div class="contentblock">
				<div class="contenthead">
				<div style="width:70%;float:left;">Custom Variable</div>
						<a class="addcustomvariable" href="javascript:void(0)" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'"><?php echo JText::_('ADD_ATTRIBUTE_LABEL');?></a>

				</div>
				<div class="blockcontent">
					<ul>
<li>
					</li>
						<?php 
						$i=0;$finalarray[1]=array_unique($finalarray[1]);
						foreach($finalarray[1] as $eacharray)
						{
							if(in_array($eacharray,$finalarray[0]))
							{
							$checked="checked";
							$selected="Remove";
							$action="";
							}
							else
							{
							$checked="";
							$selected="Select";
							$action="checked";
							}
							if(($i%2)==0){$clear="clear";}else{$clear="";}
							$namevalue=explode(":",$eacharray);
echo '<li class="'.$clear.'"><input style="display:none;" type="checkbox" name="variable1" '.$checked.' class="customvariablecheckbox" value="'.$namevalue[1].'" id="'.$namevalue[0].'">'.$namevalue[0].'
<a href="#" onclick="toggleCustomAction(this,\''.$action.'\')" class="addcustomvariable">'.$selected.'</a></li>';
							$i++;
						}

						?>
				<?php if(!empty($finalarray[1])) {?>		
					<li><div class="frontbutton" style="margin-top:7px;"><a href="javascript:void(0)" onclick="document.getElementById('lightmanagevariable').style.display='block';document.getElementById('fade').style.display='block'"><input type="button" value ="<?php echo JTEXT::_('MANAGE_CUSTOM_BUTTON');?>" /></a></div></li>	<?php }?>
					</ul>
<div id="lightmanagevariable" style="display:none;" class="white_content2">
<div class="divclose"><a href="javascript:void(0);" onclick="document.getElementById('lightmanagevariable').style.display='none';document.getElementById('fade').style.display='none';"><img src="/micamap/media/system/images/closebox.jpeg" alt="X"></a></div>
<div class="poptitle"><?php echo JTEXT::_('MANAGE_CUSTOM');?></div>

<table width="100%" cellpadding="0" cellspacing="1" class="cacss">
<thead>
<tr>

<th align="center">
Use
</th>

<th align="center">
Variable
</th>

<th align="center">
Action
</th>

</tr>
</thead>
<?php 
						$i=0;$finalarray[1]=array_unique($finalarray[1]);
						foreach($finalarray[1] as $eacharray)
						{
							if(in_array($eacharray,$finalarray[0]))
							{
							$checked="checked";
							$selected="Remove";
							$action="";
							}
							else
							{
							$checked="";
							$selected="Select";
							$action="checked";
							}
							if(($i%2)==0){$clear="clear";}else{$clear="";}
							$namevalue=explode(":",$eacharray);
echo '<tr><td align="center"><input type="checkbox" name="variable1"  class="dcv" value="'.$namevalue[1].'" id="'.$namevalue[0].'"></td><td align="center" class="customdelete">'.$namevalue[0].'</td><td align="center"><a href="javascript:void(0);" style="float:left;padding-left:5px;"> <img src="'.JUri::base().'/components/com_mica/images/edit.png" alt="Edit" class="customedit" id="'.$namevalue[0].'" value="'.$namevalue[1].'"></a> <a href="javascript:void(0);"  style="float:left;padding-left:5px;" onclick="JQuery(this).parent().prev().prev().children().attr(\'checked\',\'checked\');JQuery(\'.deletecustomvariable\').click();"> <img src="'.JUri::base().'/components/com_mica/images/delete.png" alt="Delete"></a></td></tr>';
							$i++;
						}

						?>
</table>

<div class="readon frontbutton">
<input type="button" name="Remove" value="Remove Selected" class="frontbutton deletecustomvariable" />
</div>
</div>
				</div>
				
			</div>
			<div class="contentblock">
				<div class="contenthead">
				Workspace
				</div>
				<div class="blockcontent">
					<ul class="allworkspace">
						
						
	


		
			   <?php  $db =& JFactory::getDBO();
		$user = &JFactory::getUser();
		$id=$user->id;
		if($user->id==0)
		{
			return -1;
		}
		$query="SELECT name,id,is_default FROM #__mica_user_workspace where userid = ".$id ." and is_default <> 1";
		$db->setQuery($query);
		$result=$db->loadAssocList();
		$str="";
		$activeworkspace=JRequest::getVar('activeworkspace');
		$selected="";
		$activeprofilename=$eachresult['name'];
		$i=1;
		foreach($result as $eachresult)
		{if(($i%2)==0){$clear="clear";}else{$clear="";}
		if($activeworkspace==$eachresult['id'])
		{
			$selected="selected";$edit="Edit/Delete";$activeprofilename=$eachresult['name'];
			echo "<script type='text/javascript'>JQuery('#activeworkspacename').html('Active Workspace :<font style=\'font-weight:normal;\'> ".$activeprofilename."</font>  ');</script>";
			$onclick="document.getElementById('lightn').style.display='block';document.getElementById('fade').style.display='block'";
			echo "<input type='hidden' value='".$eachresult['id']."' id='profile' />";
			$active_text="<span style='font-weight:normal;'>(Active)<span>";
			$active_text="";
		}
		else
		{
			$selected="";$edit="Select";
			$onclick="changeWorkspace(".$eachresult['id'].")";
			$active_text="";
		}
			$str .='<li class="'.$clear." ".$selected.'"><div style="width:70%;float:left;"><label class="workspacelabel">'.$eachresult['name'].$active_text.'</label></div><div style="width:30%;float:right;"><a href="#" onclick="'.$onclick.'">'.$edit.'</a></div></li>';// '.$selected.'
		}
		echo $str;
		?>
								</ul>
				</div>
				
			</div>
   
			   
	<div class="contentblock endcontent">
				<div class="contenthead">
				New Workspace
				</div>
				<div class="blockcontent newworkspace">
					<input type="text"  id="new_w_txt" class="inputbox" value="<?php echo $activeprofilename;?>"  class="newtextbox"/>
					<input type="button" name="Create" class="newbutton frontbutton" value="Create" id="createworkspace" />
				</div>
				
			</div>	
		<?php /* ?>
		<?php if(JRequest::getVar('activeworkspace')!="" && $session->get('is_default')=="0"){?>
		<input type="button" name="new" class="readon button" id="deleteworkspace"  value="Delete" />
		<?php }?>
		<input type="button" name="new" class="readon button" id="cancelupdate"  value="Cancel" onclick="JQuery('#thematiceditoption').css('display','block');JQuery('.thematiceditoption').css('display','none');" />
		</div>
	</div><?php */?>

	</div>

</div>


	


	
			
		

			
	

<script>
console.firebug=true;//fix the openlayer problem
JQuery("#layers").click(function (){
	/*if($('#attr_image').hasClass('attr_open_image')){
		$('#attr_image').toggleClass('attr_close_image');
	}else if($('#attr_image').hasClass('attr_close_image')){
		$('#attr_image').toggleClass('attr_open_image');
	}*/
	JQuery(".layers_data").slideToggle("slow");
});
function checkAll()
{
JQuery('.statetotalselectall').click(function(){
		JQuery('.statetotal_attributes').each(function(index){
			JQuery(this).attr('checked','checked');
		});
	});
}
function uncheckall()
{
	JQuery('.statetotalunselectall').click(function(){
		JQuery('.statetotal_attributes').each(function(index){
			JQuery(this).attr('checked','');
		});
	});
}
JQuery(function(){
	

	JQuery('.stateurbanselectall').click(function(){
		JQuery('.stateurban_attributes').each(function(index){
			JQuery(this).attr('checked','checked');
		});
	});

	JQuery('.stateurbanunselectall').click(function(){
		JQuery('.stateurban_attributes').each(function(index){
			JQuery(this).attr('checked','');
		});
	});

	JQuery('.stateruralselectall').click(function(){
		JQuery('.staterural_attributes').each(function(index){
			JQuery(this).attr('checked','checked');
		});
	});

	JQuery('.stateruralunselectall').click(function(){
		JQuery('.staterural_attributes').each(function(index){
			JQuery(this).attr('checked','');
		});
	});

	JQuery('.disttotalselectall').click(function(){
		JQuery('.disttotal_attributes').each(function(index){
			JQuery(this).attr('checked','checked');
		});
	});

	JQuery('.disttotalunselectall').click(function(){
		JQuery('.disttotal_attributes').each(function(index){
			JQuery(this).attr('checked','');
		});
	});

	JQuery('.disturbanselectall').click(function(){
		JQuery('.disturban_attributes').each(function(index){
			JQuery(this).attr('checked','checked');
		});
	});

	JQuery('.disturbanunselectall').click(function(){
		JQuery('.disturban_attributes').each(function(index){
			JQuery(this).attr('checked','');
		});
	});

	JQuery('.distruralselectall').click(function(){
		JQuery('.distrural_attributes').each(function(index){
			JQuery(this).attr('checked','checked');
		});
	});

	JQuery('.distruralunselectall').click(function(){
		JQuery('.distrural_attributes').each(function(index){
			JQuery(this).attr('checked','');
		});
	});

	JQuery('.towntotalselectall').click(function(){
		JQuery('.towntotal_attributes').each(function(index){
			JQuery(this).attr('checked','checked');
		});
	});

	JQuery('.towntotalunselectall').click(function(){
		JQuery('.towntotal_attributes').each(function(index){
			JQuery(this).attr('checked','');
		});
	});
});
function getvalidation()
{	var myurl="";
	myurl ="state=";
		
			   myurl +=JQuery("#state").val();
			  
		
		myurl +="&district=";
		
			   myurl +=JQuery("#district").val();
			  
		
		myurl +="&attributes=";
		var attr="";
		JQuery('input.statetotal_attributes1[type=checkbox]').each(function () {
		if(this.checked)
		{
			 attr +=JQuery(this).val()+",";

		}
		//alert(attr);
	//	$("#attributes1").val("");
		});
		//window.location="index.php?"+myurl+"&submit=Show+Data&option=com_mica&view=showresults&Itemid=100";
		return true;
	
}

JQuery(".mica_popup").live("click",function(){
var myid=JQuery(this).attr("id");
var display=JQuery("."+myid).css("display");
var left=JQuery(this).offset().left;
var top=JQuery(this).offset().top;
if(display=="none")
{
JQuery("."+myid).css({"left":left});
JQuery("."+myid).css({"top":top+24});
JQuery("."+myid).css({"width":"20%"});

JQuery("."+myid).css({"display":"block"});

}
else
{
JQuery("."+myid).css({"display":"none"});
}
//alert(myid);
});

JQuery(document).ready(function(){
	
JQuery(".contenttoggle").css({"display":"none"});

//var init=$(".active").attr("id");
//$("."+init).css({"display":"block"});
	JQuery(".toggle").live("click",function(){


	var myclass=JQuery(this).attr("class");
	JQuery(".toggle").css({"border-right":"1px solid #0154A1"});
	JQuery(this).prev("span").css({"border-right":"1px solid white"});
	//JQuery(this).next("span").css({"border-right":"1px solid white"});
	var myclasssplit=myclass.split(" ");
	if(typeof(myclasssplit[1])!="undefined" && myclasssplit[1]=="active")
	{
	JQuery(".toggle").removeClass("active");
	JQuery(".contenttoggle").slideUp();
	return false;
	}
	//JQuery(".toggle").css({"border-right":"1px solid white"});
	var myid=JQuery(this).attr("id");
	JQuery(".toggle").removeClass("active");
	JQuery(this).addClass("active");
	JQuery(".contenttoggle").slideUp();
	JQuery("."+myid).slideDown();

	if(myid!="speedometer")
	{
	JQuery("#speedometer").css({"border-right":"1px solid white"});
	}
	});
	JQuery(".toggle1").live("click",function(){
	

	JQuery(".toggle1").css({"border-right":"1px solid #0154A1"});
	JQuery(this).prev("span").css({"border-right":"none"});
	//JQuery(this).next("span").css({"border-right":"none"});
	JQuery(".toggle1").last("span").css({"border-right":"white"})
	var myclass=JQuery(this).attr("class");
	var myclasssplit=myclass.split(" ");
	if(typeof(myclasssplit[1])!="undefined" && myclasssplit[1]=="active")
	{
	JQuery(".toggle1").removeClass("active");
	JQuery(".contenttoggle1").slideUp();
	JQuery(".toggle1").css({"border-right":"1px solid #0154A1"});
	JQuery(".toggle1").last("span").css({"border-right":"white"})
	return false;
	}
	var myid=JQuery(this).attr("id");
	JQuery(".toggle1").removeClass("active");
	JQuery(this).addClass("active");
	JQuery(this).css({"border-right":"1px solid #0154A1"});
	JQuery(".contenttoggle1").slideUp();
	JQuery("."+myid).slideDown();
	JQuery.cookie("tab", myid);
	if(myid=="text")
	{
		JQuery('#jsscrollss').not(".firsttableheader,.secondtableheader,.thirdtableheader").jScrollPane({verticalArrowPositions: 'os',horizontalArrowPositions: 'os'});
		
		
	}

	});

	JQuery(".toggle2").live("click",function(){


	var myclass=JQuery(this).attr("class");
	var myclasssplit=myclass.split(" ");
	if(typeof(myclasssplit[1])!="undefined" && myclasssplit[1]=="active")
	{
	JQuery(".toggle2").removeClass("active");
	JQuery(".contenttoggle2").css({"display":"none"});
	return false;
	}
	var myid=JQuery(this).attr("id");
	
	JQuery(".toggle2").removeClass("active");
	JQuery(this).addClass("active");
	JQuery(".contenttoggle2").css({"display":"none"});
	JQuery("."+myid).css({"display":"block"});
	
	});

});
function scollPos() {
           
		var div1 = document.getElementById("tablescroll").scrollLeft;

		var width =JQuery("#tablescroll table").width();

	var div=document.getElementById("tablescroll").scrollTop;
//		document.getElementById("matrixhead").scrollLeft=div1;
           
if(JQuery("#matrixhead").html().length!=0)
{
JQuery("#matrixhead1").html("<table cellpadding='0' cellspacing='1'><tr>"+JQuery("#matrixhead").html()+"</tr></table>");
JQuery("#matrixhead").html("");
}

JQuery("#matrixhead1").scrollLeft(div1);
	//alert(document.getElementById("matrixhead").scrollLeft);
        }

</script>
<?php
$popup=3;

echo "<form name='micaform5' method='post' action='index.php?option=com_mica&view=showresults&Itemid=".JRequest::getVar('Itemid')."'>";

foreach($listtoshow[0] as $key=>$val)
{$i=0; 
	echo '<div id="light'.$popup.'" class="white_content2" style="display: none;"><div class="divclose"><a href="javascript:void(0);" onclick="document.getElementById(\'light'.$popup.'\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';"><img src="media/system/images/closebox.jpeg" alt="X" /></a></div><div class="poptitle">'.$key.'</div>';
echo "<form name='micaform".$popup."' method='post' action='index.php?option=com_mica&view=showresults&Itemid=".JRequest::getVar('Itemid')."'><table  width='100%' border='0' cellspacing='0' cellpadding='0'><tr>";
	foreach($val as $key1=>$val1)
	{

		if(($i%3)==0)
		{
		echo "</tr><tr>";
		$i=0;
		}
		if(strstr(JRequest::getVar('sessionattributes'),$val1)){$checked="checked";}else{$checked="";}
		echo'
			<td align="left" valign="top" width="3%"><input type="checkbox" name="attributes[]" class="statetotal_attributes'.$popup.'" value="'.$val1.'" '.$checked.'></td>
			<td align="left" valign="top" width="30%"><span class="hovertext '.JTEXT::_($val1).'" style="padding-left:2px;">'.$val1.'</span></td>
		';
		$i++;


 }
 echo '</tr></table><table  width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0px 0px 10px 0px;"><tr><td ><div class="maintableattr_new">
			<div>
			<a class="statetotalselectall" href="javascript:void(0);" onclick="checkAllgrp('.$popup.');">Select All</a> / <a class="statetotalunselectall" onclick="uncheckAllgrp('.$popup.');" href="javascript:void(0);">Unselect All</a>
			</div>
</div></td>';
$popup++;
echo "</tr></table><div class='frontbutton'><input type='submit' name='submit' class='frontbutton' value='Show Data' onclick='getvalidation()'></div>";
echo "<input type='hidden' id='state' name='state' value='".JRequest::getVar('state')."' />";
echo "<input type='hidden' id='zoom' name='zoom' value='".JRequest::getVar('zoom')."' />";
echo "<input type='hidden' id='district'name='district' value='".JRequest::getVar('district')."' />";
echo "<input type='hidden' name='option' value='".JRequest::getVar('option')."' />";
echo "<input type='hidden' name='view' value='".JRequest::getVar('view')."' />";
echo "<input type='hidden' name='Itemid' value='".JRequest::getVar('Itemid')."' />";
echo "<input type='hidden' name='m_type' value='".$m_type."' /><input type='hidden' id='minvalh' value=''/><input type='hidden' id='maxvalh' value=''/><input type='hidden' name='level' id='level'>";

echo "</div>";
}
echo "</form>";
?>
<div class="tq contenttoggle" style="display: none; ">
		<div class="" style="width:100%;float:left;">
        <div class="thematicarea clearfix">
<div class="blockcontent">
						
						<!--<ul class="no-bullet">
						<li>
						<b><a id="thematicquerypopup" href="javascript:void(0)"><?php //echo JText::_('ADD_THEMATIC_QUERY_LABEL');?></a></b>
						</li>
					</ul>-->
<?php
$activeworkspace=$session->get('activeworkspace');
  $query="select * from #__mica_sld_legend where workspace_id = ".$activeworkspace." order by level,range_from ASC";
	$db->setQuery($query);
	$db->query();
	$result=$db->loadObjectList();
	
	$id=array();
	$level=array();
	$i=1;
	$j=1;
	foreach($result as $range)
	{	
		if($range->level=="0")
		{
			$str =" <div class='col_".$i."' style='height:10px;width:10px;background-color:".$range->color.";float:right;'/></div></li>";
		}
		else
		{$pin=str_replace("#","",$range->color);
			$str =" <div class='col_".$i."' style='float:right;'/><img src='".JUri::base()."components/com_mica/maps/img/layer".$range->level."/pin".$pin.".png' /></div></li>";
		}
		$grouping[$range->custom_formula][]="<li class='range_".$range->level."' id='".$range->range_from." - ".$range->range_to."'><div style='float:left;width:80%;'>".$range->range_from." - ".$range->range_to."</div>".$str;
		//$id[]=$range->custom_formula;
		//$grouping[$range->custom_formula]=$range->level;
		$level[$range->custom_formula][]=$range->level;
		$i++;
		$j++;
	}
	$i=0;
	$range=array();
	$str=array();
	$totalcount=0;
	$l=0;
	$tojavascript=array();
	foreach($grouping as $key=>$val)
	{
			$grname=getCustomAttrName($key,$activeworkspace);
		$grpname[]=$grname;
		$str[]=implode(" ",$val);
		$ranges[]=count($val);
		$levelunique[]=$level[$key][0];
		$tojavascript[]=$key;
		$l++;
	}
	$tojavascript=implode(",",$tojavascript);
	//$str="";
	for($i=0;$i<count($grpname);$i++)
	{
		echo "<div class='contentblock1 '><div class='blockcontent'><ul class='maingrp bullet-add clear' ><li><div class='themehead'>".$grpname[$i][0]."</div><div class='themeedit'><a class='edittheme ".$ranges[$i]."' id='".$grpname[$i][1]."__".$levelunique[$i]."' ><img src='".JUri::base()."components/com_mica/images/edit.png' /></a><a class='deletegrp ".$levelunique[$i]."' id='del_".$grpname[$i][1]."' >&nbsp;&nbsp;&nbsp;<img src='".JUri::base()."components/com_mica/images/delete.png' /></a></div><ul class='range' >".$str[$i]."</ul></li></ul></div></div>";
	}
	echo "<div class='contentblock3'>";
	echo "<script type='text/javascript'>var totalthemecount=".count($grpname)."; var usedlevel='".implode(",",$levelunique)."'; var havingthematicquery='".$tojavascript."';</script>";
	function getCustomAttrName($name,$activeworkspace)
	{
		 $query="select name,attribute from #__mica_user_custom_attribute where profile_id = ".$activeworkspace." and attribute like '".$name."'";
	$db =& JFactory::getDBO(); 
	$db->setQuery($query);
	$db->query();
	$result=$db->loadRow();
	
		if(count($result)==0)
		{
			return array($name,$name);
		}
		else
		{	
			return array($result[0],$result[1]);
		}
	}
?>
<div id="themeconent">
<form name="micaform3" id="micaform3" action="index.php?option=com_mica&view=showresults&Itemid=<?php echo $itemid; ?>" method="get">
	<table cellspacing="4" cellpadding="0" border="0" width="90%" style="float: left;
width: 335px;">
		<tr>
			<td colspan="2" align="left"><h3><?php echo JText::_('SELECT_PARAMETER_LABEL');?></h3></td>
		</tr>
		<tr>
			<td><b><?php echo JText::_('ATTRIBUTE_LABEL');?></b></td>
			<td>
				<select name="thematic_attribute" id="thematic_attribute" class="inputbox" style="width: 200px;" onchange="getMinmaxVariable(this.value);">
					<option value=""><?php echo JText::_('PLEASE_SELECT');?></option>
					<optgroup label="Default Variable">
					<?php $attr= $this->themeticattribute;
							$eachattr=explode(",",$attr);
							foreach($eachattr as $eachattrs)
							{
								echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
							}
							?>
							</optgroup>
							<optgroup label="Custom Variable">
							<?php $attr= $this->customattribute;
							$eachattr=explode(",",$attr);
							$eachattr=array_filter($eachattr);
							foreach($eachattr as $eachattrs)
							{$eachattrs=explode(":",$eachattrs);
								echo '<option value="'.$eachattrs[1].'">'.JTEXT::_($eachattrs[0]).'</option>';
							}
							?>
							</optgroup>
				</select>
			</td>
		</tr>
		<tr id="minmaxdisplay">
			<td></td>
			<td class="minmaxdisplay">
				
			</td>
		</tr>
		<tr>
			<td><b><?php echo JText::_('NO_OF_INTERVAL_LABEL');?></b></td>
			<td><input type="text" name="no_of_interval" id="no_of_interval" class="inputbox" value="" style="width: 194px;"></td>
		</tr>
		<tr class="colorhide">
		<td><b><?php echo JText::_('SELECT_COLOR_LABEL');?></b></td>
		<td>
		<input class="simple_color" value="" style="width: 194px;"/>
		</td>
		</tr>
		<tr>
			<td colspan="2">
				<table cellspacing="0" cellpadding="0" border="1" width="100%" class="popupfromto" id="displayinterval">
					
					
					
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="2" align="center">
				<div class="readon frontbutton" style="float: left;">
					<input type="button" name="save" id="savesld" class="frontbutton" style="width: 100px;" value="<?php echo JText::_('OK_LABEL');?>" />
				</div>
				<div class="readon frontbutton" style="float: left;padding-left: 5px;">
					<input type="button" name="cancel" id="cancel" class="frontbutton" style="width: 100px;" value="<?php echo JText::_('CANCEL_LABEL');?>" onclick="closePopup();"/>
				<div>
			</td>
		</tr>
	</table>
</form>
</div>
</div>
</div></div>	</div>

</div><div class="matrix contenttoggle" >
<div id="fullscreentable">
<table   cellpadding="0" cellspacing="1" >
<?php
$keys=array_keys($this->MaxForMatrix[0]);
$vals=array_values($this->MaxForMatrix[0]);
$width=1024/count($keys)+1;
echo "<tr id='matrixhead'><td width='".$width."px'>Legends</td><td width='".$width."px'>Variable Name</td>";
foreach($keys as $eachkey)
{
	echo "<td width='".$width."px'><a href='javascript:void();' title='".$eachkey."' style='color:white !important;'>".substr($eachkey,0,10)."</td>";
}
 echo "</tr>";
 $i=0;
$classarray=array("#ECE0BA","#CC9966","#89570F","#5C3B0F","");
$classname=array("#ECE0BA"=>"Low","#CC9966"=>"Average","#89570F"=>"Medium","#5C3B0F"=>"High",""=>"",""=>"");
foreach($this->customdata as $eachdata)
{
echo "<tr>";
	if(isset($classarray[$i]) && $classarray[$i]!="")
	{
	echo "<td width='".$width."px' >".$classname[$classarray[$i]]."<div  style='float:right;width:10px;height:10px;margin-right: 2px;background-color:".$classarray[$i].";'></td>";
	}

else
{
	if($i==5)
	{
echo "<td width='".$width."px' class='frontbutton'></td>";
	}
	else if($i==4)
	{
		echo "<td width='".$width."px' class='fullscreenicon'><table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'><input style='margin-right: 3px;' type='button' class='frontbutton' name='fullscreen' value='Full Screen'/></td></tr></table></td>";
	}
	else
	{
		echo "<td width='".$width."px'></td>";
	}
	
}
$i++;
echo "<td width='".$width."px' >".JText::_($eachdata->name)."</td>";

	foreach($eachdata as $key=>$val)
	{
			 $q=$this->MaxForMatrix[0][$key]/4;
			 $q1=$q+$q;
			 $q2=$q+$q+$q;
			 $q4=$this->MaxForMatrix[0][$key];
			
		
		if(in_array($key,$keys) && $key!="name")
		{
			
			if($val<=$q)
			{
			$class="#ECE0BA";
			}
			else if($val>=$q && $val<$q1)
			{
			$class="#CC9966";
			}
			else if($val>=$q1 && $val<$q2)
			{
			$class="#89570F";
			}
			else if($val>=$q2 && $val<=$q4)
			{
				$class="#5C3B0F";
			}
			echo '<td  width="'.$width.'px" style="background-color:'.$class.';"></td>';//
		}
	
	//echo "<pre>"; print_r($this->customdata);print_r($keys);print_r($vals);echo "</pre>";
	} 
	echo "</tr>";
}
	if(count($eachdata)<=3 && $i<=3)
	{
		for($i=count($eachdata);$i<=count($classarray);$i++)
		{
				echo "<tr>";
		if($i==5)
			{
		echo "<td width='".$width."px' class='frontbutton'></td>";
			}		
		else if($i==4)
			{
				echo "<td width='".$width."px' class='fullscreenicon'><table cellpadding='1' cellspacing='1'border='0'><tr><td class='frontbutton' style='min-width: 75px !important;border-right: 0px !important;'><input style='margin-left: 3px;' type='button' class='frontbutton' name='fullscreen' value='Download' id='downmatrix'/></td><td class='frontbutton' style='min-width: 75px !important;'><input style='margin-right: 3px;' type='button' class='frontbutton' name='fullscreen' value='Full Screen'/></td></tr></table></td>";

			}
			else
			{
				echo "<td width='".$width."px' >".$classname[$classarray[$i]]."<div class=''style='float:right;width:10px;height:10px;margin-right: 2px;background-color:".$classarray[$i].";'></td>";
			}
		$td= "<td width='".$width."px'></td>";
		echo str_repeat($td,count($this->customdata[0]));
		echo "</tr>";
		}
	}
?>

</td></tr></table>
</div>	</div>
<div class="speedometer contenttoggle" >
<table cellspacing="4" style="float:left;">
<thead style="display:none;" id="speedfiltershow">
<th class="rightgrptext frontbutton" ><input style="margin-left:3px;"type="button" class="frontbutton" value="Edit Filter" name="editfilter"/></th>
</thead>
<tr class="sfilter">
<td colspan="2">
<table>
<tr>
<td><b><?php echo JText::_('FILTER_BY');?></b>&nbsp;&nbsp;&nbsp;&nbsp;</td>
<td><input type="radio" name="filter" value="0" class="filterspeed"/></td><td><?php echo JText::_('FILTER_BY_LAYER');?></td>
<td><input type="radio" name="filter" value="1" class="filterspeed"/></td><td> <?php echo JText::_('FILTER_BY_VARIABLE');?></td>
</tr>
</table>
</td>

</tr>
<tr class="sfilter">
<td colspan="2">
<div class="lft speed" style="width:210px;display:none;" >
<table cellspacing="4">
<tr>
<td>
<select name="speed_variable" id="speed_variable" class="inputbox" style="width: 200px;" multiple>
					<!--<option value="0"><?php echo JText::_('PLEASE_SELECT');?></option>-->
					
					<?php $attr= $this->themeticattribute;
							$eachattr=explode(",",$attr);
							foreach($eachattr as $eachattrs)
							{
								echo '<option value='.$eachattrs.'>'.JTEXT::_($eachattrs).'</option>';
							}
							?>
							
							
				</select>
</td>
</tr>
<tr>
<td>
<select name="speed_region" id="speed_region" class="inputbox" style="width: 200px;" multiple >
<!--<option value="0"><?php echo JText::_('PLEASE_SELECT');?></option>-->
<?php

foreach($this->customdata as $eachdata)
{
echo '<option value='.str_replace(" ","%20",$eachdata->name).'>'.$eachdata->name.'</option>';
}
?>
</select>
</td>
</tr>
<tr>
<td>
<div class="left frontbutton ">
				<input type="button" name="showspeed" id="showspeed" class="" value="Create">
			</div>
</td>
</tr>
</table>

</div>
</td>
</tr>
</table>
<div id="spedometer_region" style="float:left;width:560px;">

</div>
</div></div>
<?php 
echo '<div id="lightn" class="white_contentn" style="display: none;"><div class="divclose"><a href="javascript:void(0);" onclick="document.getElementById(\'lightn\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';"><img src="media/system/images/closebox.jpeg" alt="X" /></a></div><div align="left">
<input type="textbox" id="updateworkspacetext" value="'.$activeprofilename.'" class="inputbox"/>
<div class="frontbutton readon">
<input type="button" id="updateworkspace" onClick="JQuery(\'#new_w_txt\').val(JQuery(\'#updateworkspacetext\').val())" Value="Update" class="frontbutton readon"/></div><div class="frontbutton">
<input type="button" name="new"  id="deleteworkspace"  value="Delete" class=""/></div></div>';


?>

