<?php
/**
* @version		$Id: mod_mica.php 14401 2010-01-26 14:10:00Z louis $
* @package		Joomla
* @copyright	Copyright (C) 2005 - 2010 Open Source Matters. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* Joomla! is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
$itemid = JRequest::getVar('Itemid',0);
//$state = JRequest::getVar('state','');
//$district = JRequest::getVar('district','');
//$town = JRequest::getVar('town','');
//$m_type = JRequest::getVar('m_type','');
//$attributes = JRequest::getVar('attributes',array());
 $db_table= JRequest::getVar('db_table');
 $session = JFactory::getSession();
 $m_type=$session->get('m_type');
//$new_name = JRequest::getVar('new_name',array());
//$custom_attribute = JRequest::getVar('custom_attribute',array());


function getattributelist($db_table)
{

		/*if($zoom == 5)
		{
			$db_table="State";
		}
		else if($zoom > 5 && $zoom <= 7)
		{
			$db_table="District";
		}
		else
		{
			$db_table="Town";
		}*/
		
		 $userAttr=userSelectedAttr($db_table);

		
		if($userAttr==-2)
		{
			return -2;
		}
	$str="";
	$todisplay=array();
		$db =& JFactory::getDBO();
		 $query="select  * from #__mica_group_field allfields JOIN #__mica_group grp ON grp.id=allfields.groupid where allfields.table like '".$db_table."' and grp.publish =1 ";
		$db->setQuery($query);
		$db->query();
		 $list=$db->loadAssocList();//echo "<pre>";print_r($list);exit;
		$change_group_name=1;
		foreach($list as $each)
		{
			if($change_group_name==1)
			{
				$change_group_name=0;
				
			}
				$each['field']=str_replace("Rural_","",$each['field']);
				$each['field']=str_replace("Urban_","",$each['field']);
				$each['field']=str_replace("Total_","",$each['field']);
				$todisplay[$each['group']][]=$each['field'];
			
			$todisplay[$each['group']][]=$each['field'];
			$groupid[]=$each['groupid'];
		}
		foreach($todisplay as $eachgrp=>$vals)
		{
			$todisplay[$eachgrp]=array_unique($todisplay[$eachgrp]);
		}
		//echo "<pre>";print_R($todisplay);exit;
	return array($todisplay,$groupid);
}
function userSelectedAttr($db_table)
	{
		$db =& JFactory::getDBO();
 		$user = &JFactory::getUser();
		$where		= "status =  'Active' and userid =".$user->id;
		

		$query = ' SELECT plan'
			. ' FROM #__acctexp_subscr AS a where '
			
			. $where

		;
		$db->setQuery($query);
		$planrow = $db->loadRow();

		 $query="select attribute from #__mica_user_attribute where aec_plan_id=".$planrow[0]." and dbtable like '".$db_table."'";
		$db->setQuery($query);
		$db->query();
		$row = $db->loadRow();
		$attr=explode(",",$row[0]);
		return $attr;
		
	}
	

	function displaysttributes($grparray)
	{	$todisplay=$grparray[0];
		$grpid=$grparray[1];
		//echo "<pre>";print_r($todisplay);exit;
		$str= "<div class='slideingmodule'><a href='#' class='dropdown mica_popup' id='dd2'><span class='slideinghead'>Grouping</span> </a><div class='dropdown-menu top_mod dd2'><ul class='bullet-add'>";
		$session = JFactory::getSession();
		$userAttr=$session->get('attributes');
		$userAttr=explode(",",$userAttr);
		
		 
		$popup=3;
		foreach($todisplay as $key=>$val)
		{
			$str .='<li><b><a href="javascript:void(0)" onclick="document.getElementById(\'light'.$popup.'\').style.display=\'block\';document.getElementById(\'fade\').style.display=\'block\'">'.$key;//'.$grpid[count($val)+1].'"
		$popup++;

			$chk_count=0;
			$unchk_count=0;
			
		foreach($userAttr as $eachattr)
		{
			//$i++;
			if(in_array($eachattr,$val))
			{
				$chk_count++;
       		 		
			}
			//$unchk_count++;
		}
		$str.= "(".$chk_count."-".count($val).")</a></b></li>";
		
		}

		//$str .="</ul>";
		echo $str;




	}
	$listtoshow=getattributelist($db_table);
	displaysttributes($listtoshow);
	?>
	
			
					<li>
						<b><a href="javascript:void(0)" onclick="document.getElementById('light').style.display='block';document.getElementById('fade').style.display='block'"><?php echo JText::_('ADD_ATTRIBUTE_LABEL');?></a></b>
					</li>
				</ul></div></div>

	<div class='slideingmodule'>
<a href='#' class='dropdown mica_popup' id='dd3'><span class='slideinghead'>Workspace</span> </a>
		<div class='dropdown-menu dd3'>
		
			    <select id="profile" class="inputbox" onchange="changeWorkspace(this.value);">
			     <option value="">Select/Create Workspace</option>
			    <optgroup label="Load WorkSpace">
			   <?php  $db =& JFactory::getDBO();
		$user = &JFactory::getUser();
		$id=$user->id;
		if($user->id==0)
		{
			return -1;
		}
		$query="SELECT name,id,is_default FROM #__mica_user_workspace where userid = ".$id ." and is_default <> 1";
		$db->setQuery($query);
		$result=$db->loadAssocList();
		$str="";
		$activeworkspace=JRequest::getVar('activeworkspace');
		$selected="";
		$activeprofilename=$eachresult['name'];
		foreach($result as $eachresult)
		{	if($activeworkspace==$eachresult['id']){$selected="selected";$activeprofilename=$eachresult['name'];}else{$selected="";}
			$str .='<option value="'.$eachresult['id'].'" '.$selected.'>'.$eachresult['name'].'</option>';
		}
		echo $str;
		?>
			   
			    </optgroup>
			    <option value="0">Create New Workspace</option>
			    </select>
		<!--<div class="wrap" style="padding-top:5px;">
		<input type="button" name="new" class="readon button"  id="cancelchangecombo"  value="Cancel" onclick="JQuery('#combotheme').css('display','block');" />
		</div>-->
		
		<div class="thematiceditoption sapretorbottom" style="display: none;">
		<div class="<?php if(JRequest::getVar('activeworkspace')!="" && $session->get('is_default')=="0"){echo "updateworkspace";}else{echo "createneworkspace";}?>" style="padding: 5px;">
		
		<input type="text"  id="new_w_txt" class="inputbox" value="<?php echo $activeprofilename;?>" style="width: 161px;">
		
		</div>
		
		<div class="<?php if(JRequest::getVar('activeworkspace')!="" && $session->get('is_default')=="0"){echo "updateworkspace";}else{echo "createnewworkspace";}?>">
		
		<input type="button" name="new" id="<?php if(JRequest::getVar('activeworkspace')!="" && $session->get('is_default')=="0"){echo "updateworkspace";}else{echo "createworkspace";}?>" class="readon button"value="<?php if(JRequest::getVar('activeworkspace')!="" && $session->get('is_default')=="0"){echo "Update";}else{echo "Create";}?>" />
		<?php if(JRequest::getVar('activeworkspace')!="" && $session->get('is_default')=="0"){?>
		<input type="button" name="new" class="readon button" id="deleteworkspace"  value="Delete" />
		<?php }?>
		<input type="button" name="new" class="readon button" id="cancelupdate"  value="Cancel" onclick="JQuery('#thematiceditoption').css('display','block');JQuery('.thematiceditoption').css('display','none');" />
		</div>
	</div>
<div id="combotheme">
		
		<div class="right" style="text-align: right; margin:5px 0px 0px 0px;">
<!--<a href="javascript::void(0);" onclick="JQuery('#combotheme').css('display','none');JQuery('.combotheme').css('display','block');">Switch Workspace</a>-->
</div>
	
	<div id="thematiceditoption"  class="wrap ">
		<div class="left"><?php echo $activeprofilename;?></div>
	<?php if($activeprofilename!="") {?>	
		<div class="right" style="text-align: right;"><a href="javascript::void(0);" onclick="JQuery('#thematiceditoption').css('display','none');JQuery('.thematiceditoption').css('display','block');">Edit</a></div>
<?php }?>
	</div>
	</div>
	</div>

	</div>

<div class='slideingmodule'>
<a href='#' class='dropdown mica_popup' id='dd4'><span class='slideinghead'><?php echo JText::_('THEMATIC_QUERY_LABEL');?></span> </a>
<!-- id=""-->
	<div  class='dropdown-menu dd4'>
		
	<table style="float: left;clear: both;">

			
			<tr>
			<td>
						
						<ul class="no-bullet">
						<li>
						<b><a id="thematicquerypopup" href="javascript:void(0)" ><?php echo JText::_('ADD_THEMATIC_QUERY_LABEL');?></a></b>
						</li>
					</ul>
				
			</td>
		</tr>
	</table>
<script type="text/javascript">
</script>
	<form name="micaform2"  id="micaform2" action="index.php?option=com_mica&view=showresults&Itemid=<?php echo $itemid; ?>" method="get">

	<table cellspacing="0" cellpadding="0" border="0" width="100%" id="ajaxattribute">
		
	</table>

	</form>

<script>
JQuery("#layers").click(function (){
	/*if($('#attr_image').hasClass('attr_open_image')){
		$('#attr_image').toggleClass('attr_close_image');
	}else if($('#attr_image').hasClass('attr_close_image')){
		$('#attr_image').toggleClass('attr_open_image');
	}*/
	JQuery(".layers_data").slideToggle("slow");
});
function checkAll()
{
JQuery('.statetotalselectall').click(function(){
		JQuery('.statetotal_attributes').each(function(index){
			JQuery(this).attr('checked','checked');
		});
	});
}
function uncheckall()
{
	JQuery('.statetotalunselectall').click(function(){
		JQuery('.statetotal_attributes').each(function(index){
			JQuery(this).attr('checked','');
		});
	});
}
JQuery(function(){
	

	JQuery('.stateurbanselectall').click(function(){
		JQuery('.stateurban_attributes').each(function(index){
			JQuery(this).attr('checked','checked');
		});
	});

	JQuery('.stateurbanunselectall').click(function(){
		JQuery('.stateurban_attributes').each(function(index){
			JQuery(this).attr('checked','');
		});
	});

	JQuery('.stateruralselectall').click(function(){
		JQuery('.staterural_attributes').each(function(index){
			JQuery(this).attr('checked','checked');
		});
	});

	JQuery('.stateruralunselectall').click(function(){
		JQuery('.staterural_attributes').each(function(index){
			JQuery(this).attr('checked','');
		});
	});

	JQuery('.disttotalselectall').click(function(){
		JQuery('.disttotal_attributes').each(function(index){
			JQuery(this).attr('checked','checked');
		});
	});

	JQuery('.disttotalunselectall').click(function(){
		JQuery('.disttotal_attributes').each(function(index){
			JQuery(this).attr('checked','');
		});
	});

	JQuery('.disturbanselectall').click(function(){
		JQuery('.disturban_attributes').each(function(index){
			JQuery(this).attr('checked','checked');
		});
	});

	JQuery('.disturbanunselectall').click(function(){
		JQuery('.disturban_attributes').each(function(index){
			JQuery(this).attr('checked','');
		});
	});

	JQuery('.distruralselectall').click(function(){
		JQuery('.distrural_attributes').each(function(index){
			JQuery(this).attr('checked','checked');
		});
	});

	JQuery('.distruralunselectall').click(function(){
		JQuery('.distrural_attributes').each(function(index){
			JQuery(this).attr('checked','');
		});
	});

	JQuery('.towntotalselectall').click(function(){
		JQuery('.towntotal_attributes').each(function(index){
			JQuery(this).attr('checked','checked');
		});
	});

	JQuery('.towntotalunselectall').click(function(){
		JQuery('.towntotal_attributes').each(function(index){
			JQuery(this).attr('checked','');
		});
	});
});
function getvalidation()
{	var myurl="";
	myurl ="state=";
		
			   myurl +=$("#state").val();
			  
		
		myurl +="&district=";
		
			   myurl +=$("#district").val();
			  
		
		myurl +="&attributes=";
		var attr="";
		JQuery('input.statetotal_attributes1[type=checkbox]').each(function () {
		if(this.checked)
		{
			 attr +=JQuery(this).val()+",";

		}
		//alert(attr);
	//	$("#attributes1").val("");
		});
		//window.location="index.php?"+myurl+"&submit=Show+Data&option=com_mica&view=showresults&Itemid=100";
		return true;
	
}

JQuery(".mica_popup").live("click",function(){
var myid=JQuery(this).attr("id");
var display=JQuery("."+myid).css("display");
var left=JQuery(this).offset().left;
var top=JQuery(this).offset().top;
if(display=="none")
{
JQuery("."+myid).css({"left":left});
JQuery("."+myid).css({"top":top+24});
JQuery("."+myid).css({"width":"20%"});

JQuery("."+myid).css({"display":"block"});

}
else
{
JQuery("."+myid).css({"display":"none"});
}
//alert(myid);
});
</script>
<?php
$popup=3;
$i=0; 
echo "<form name='micaform5' method='post' action='index.php?option=com_mica&view=showresults&Itemid=".JRequest::getVar('Itemid')."'>";
foreach($listtoshow[0] as $key=>$val)
{
	echo '<div id="light'.$popup.'" class="white_content2" style="display: none;"><div align="right"><a href="javascript:void(0);" onclick="document.getElementById(\'light'.$popup.'\').style.display=\'none\';document.getElementById(\'fade\').style.display=\'none\';">Close (X)</a></div><div align="left"><input type="submit" name="submit" class="button" value="Show Data" onclick="getvalidation()"></div>';
echo "<form name='micaform".$popup."' method='post' action='index.php?option=com_mica&view=showresults&Itemid=".JRequest::getVar('Itemid')."'><table><tr>";
	foreach($val as $key1=>$val1)
	{

		if(($i%3)==0)
		{
		echo "</tr><tr>";
		}
		if(strstr(JRequest::getVar('sessionattributes'),$val1)){$checked="checked";}else{$checked="";}
		echo'
			<td align="left" valign="top"><input type="checkbox" name="attributes[]" class="statetotal_attributes'.$popup.'" value="'.$val1.'" '.$checked.'></td>
			<td align="left" valign="top"><span class="hovertext '.JTEXT::_($val1).'">'.$val1.'</span></td>
		';$i++;


 }
 echo '</tr></table><table><tr><td ><div class="maintableattr">
			<div>
			<a class="statetotalselectall" href="javascript:void(0);" onclick="checkAllgrp('.$popup.');">Select All</a> / <a class="statetotalunselectall" onclick="uncheckAllgrp('.$popup.');" href="javascript:void(0);">Unselect All</a>
			</div>
</div></td>';
$popup++;
echo "</tr></table><input type='submit' name='submit' class='button' value='Show Data' onclick='getvalidation()'>";
echo "<input type='hidden' id='state' name='state' value='".JRequest::getVar('state')."' />";
echo "<input type='hidden' id='zoom' name='zoom' value='".JRequest::getVar('zoom')."' />";
echo "<input type='hidden' id='district'name='district' value='".JRequest::getVar('district')."' />";
echo "<input type='hidden' name='option' value='".JRequest::getVar('option')."' />";
echo "<input type='hidden' name='view' value='".JRequest::getVar('view')."' />";
echo "<input type='hidden' name='Itemid' value='".JRequest::getVar('Itemid')."' />";
echo "<input type='hidden' name='m_type' value='".$m_type."' /><input type='hidden' id='minvalh' value=''/><input type='hidden' id='maxvalh' value=''/><input type='hidden' name='level' id='level'>";

echo "</div>";
}
echo "</form>";
$activeworkspace=$session->get('activeworkspace');
  $query="select * from #__mica_sld_legend where workspace_id = ".$activeworkspace." order by level,range_from ASC";
	$db->setQuery($query);
	$db->query();
	$result=$db->loadObjectList();
	
	$id=array();
	$level=array();
	$i=1;
	$j=1;
	foreach($result as $range)
	{	
		if($range->level=="0")
		{
			$str =" <div class='col_".$i."' style='height:10px;width:10px;background-color:".$range->color.";float:right;'/></div></li>";
		}
		else
		{$pin=str_replace("#","",$range->color);
			$str =" <div class='col_".$i."' style='float:right;'/><img src='".JUri::base()."components/com_mica/maps/img/layer".$range->level."/pin".$pin.".png' /></div></li>";
		}
		$grouping[$range->custom_formula][]="<li class='range_".$range->level."' id='".$range->range_from." - ".$range->range_to."'><div style='float:left;width:80%;'>".$range->range_from." - ".$range->range_to."</div>".$str;
		//$id[]=$range->custom_formula;
		//$grouping[$range->custom_formula]=$range->level;
		$level[$range->custom_formula][]=$range->level;
		$i++;
		$j++;
	}
	$i=0;
	$range=array();
	$str=array();
	$totalcount=0;
	$l=0;
	$tojavascript=array();
	foreach($grouping as $key=>$val)
	{
			$grname=getCustomAttrName($key,$activeworkspace);
		$grpname[]=$grname;
		$str[]=implode(" ",$val);
		$ranges[]=count($val);
		$levelunique[]=$level[$key][$l];
		$tojavascript[]=$key;
		$l++;
	}
	$tojavascript=implode(",",$tojavascript);
	//$str="";
	
	for($i=0;$i<count($grpname);$i++)
	{
		echo "<ul class='maingrp bullet-add' ><li style='background:none;'><div style='float:left;clear:both;font-weight:bold;'>".$grpname[$i][0]."</div><div style='float:right'><a class='edittheme ".$ranges[$i]."' id='".$grpname[$i][1]."__".$levelunique[$i]."' ><img src='".JUri::base()."components/com_mica/images/edit.png' /></a><a class='deletegrp ".$levelunique[$i]."' id='del_".$grpname[$i][1]."' >&nbsp;&nbsp;&nbsp;<img src='".JUri::base()."components/com_mica/images/delete.png' /></a></div><ul class='range' style='margin:0px 0px 0px 12px;'>".$str[$i]."</ul></li></ul>";
	}
	echo "<script type='text/javascript'>var totalthemecount=".count($grpname)."; var usedlevel='".implode(",",$levelunique)."'; var havingthematicquery='".$tojavascript."';</script>";
	function getCustomAttrName($name,$activeworkspace)
	{
		 $query="select name,attribute from #__mica_user_custom_attribute where profile_id = ".$activeworkspace." and attribute like '".$name."'";
	$db =& JFactory::getDBO(); 
	$db->setQuery($query);
	$db->query();
	$result=$db->loadRow();
	
		if(count($result)==0)
		{
			return array($name,$name);
		}
		else
		{	
			return array($result[0],$result[1]);
		}
	}
?>
</div>
