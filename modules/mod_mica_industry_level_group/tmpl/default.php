<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_users_latest
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<div class="row">
	<?php if (!empty($industryLevelGroups)) : ?>
		<?php foreach ($industryLevelGroups as $industryLevelGroup) : ?>
		<div class="col-xs-3">
			<div class="rounded-box">
				<img src="<?php echo JURI::base(). "/components/com_mica/images/" . $industryLevelGroup->icon; ?>" alt="">
				<div class="hover-content">
					<p><?php echo $industryLevelGroup->groupDescription; ?></p>
				</div>
			</div>
			<div class="industry-title">
				<h2><?php echo $industryLevelGroup->group; ?></h2>
			</div>
		</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>

