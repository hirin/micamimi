<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_users_latest
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Helper for mod_users_latest
 *
 * @since  1.6
 */
class ModMicaIndustryLevelGroupHelper
{
	/**
	 * Get users sorted by activation date
	 *
	 * @param   \Joomla\Registry\Registry  $params  module parameters
	 *
	 * @return  array  The array of users
	 *
	 * @since   1.6
	 */
	public static function getMicaIndustryLevelGroup()
	{
		// Create a new query object.
		$db    = JFactory::getDbo();
		$industryLevelGroups = array(
			'name' => '',
			'icon' => '',
			'type' => '',
		);
		$SQL = " SELECT g.*  FROM " . $db->quoteName('#__mica_industry_level_group') . " AS g where publish = 1";
		$db->setQuery($SQL);
		$industryLevelGroups = $db->loadObjectList();

		return $industryLevelGroups;
	}
}
